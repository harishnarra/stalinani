//
//  BlockWebView.swift
//
//
//  Created by Cem Olcay on 12/08/15.
//
//

#if os(iOS)

import UIKit

///Make sure you use  `[weak self] (NSURLRequest) in` if you are using the keyword `self` inside the closure or there might be a memory leak
open class BlockWebView {

}

#endif
