//
//  AppStrings.swift
//  IPAC
//
//  Created by Admin on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

enum AppStrings : String{
    
    //SignUpVc
    
    case name = "Name"
    case email = "Email Address"
    case password = "Password"
    case mobile = "Enter your Mobile Number"
    case SIGNUP = "SIGN UP"
    case SIGNIN = "SIGN IN"
    case alreadyHvAcnt = "Already have an account? "
    
    // validation controller
    
    case enterName = "Name can't be empty"
    case invalidName = "Invalid Name"
    case enterEmail = "Email Address can't be empty"
    case invalidEmail = "Invalid Email"
    case enterMobileNum = "Phone number can't be empty"
    case invalidMobile = "Invalid Mobile Number"
    case enterPassword = "Password can't be empty"
    case invalidPassword = "Password must be of minimum 6 characters"
    
    //ForgotResetPasswordVC
    
    case resetPasswordMessage = "Please enter your new password"
    case forgotPasswordMessage = "Please enter your registered Email address,\nwe will send you an OTP."
    case NewPassword = "New Password"
    case enterEmailAddress = "Enter your Email Address"
    case DONE = "DONE"
    case resetPass = "RESET PASSWORD"
    case SEND = "SEND"
    case Show = "Show"
    case Hide = "Hide"
    
    //OTPVC
    
    case verificationCode = "Verification Code"
    case OTPMessage = "Please enter the verification code sent \nto "
    case CONTINUE = "CONTINUE"
    case noOTP = "Didn’t receive the OTP?"
    case resendCode = "Resend Code"
    
    //NewsVC
    
    case News = "News"
    case readMore = "Read More"
    
    //Edit Profile
    
    case editProfile = "Edit Profile"
    case aboutMe = "About Me"
    case mobileNumber = "Mobile Number"
    case whatsappNumber = "WhatsApp Number"
    case paytmNumber = "Paytm Number"
    case UPINumber = "UPI Registered Number "
    case gender = "Gender"
    case dob = "Date of Birth"
    case city = "City"
    case district = "District"
    case state = "State"
    case collegeStudentStatus = "College Student Status "
    case collegeName = "College Name"
    case attachedFile = "Attached File"
    
    //HomeVC
    
    
}
