//
//  AppinventivImagePicker.swift
//  AppinventivCropper
//
//  Created by Appinventiv on 15/11/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary
import AVFoundation
import Photos


extension UIViewController {
    
    typealias ImagePickerDelegateController = (UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate)
    
    func captureImage(on controller: ImagePickerDelegateController,
                      photoGallery: Bool = true,
                      camera: Bool = true) {
        
        let chooseOptionText =  "Choose Option"
        let alertController = UIAlertController(title: chooseOptionText, message: nil, preferredStyle: .actionSheet)
        
        if photoGallery {
            
            let chooseFromGalleryText =  "Choose from gallery"
            let alertActionGallery = UIAlertAction(title: chooseFromGalleryText, style: .default) { _ in
                self.checkAndOpenLibrary(on: controller)
            }
            alertController.addAction(alertActionGallery)
        }
        
        if camera {
            
            let takePhotoText =  "Take Photo"
            let alertActionCamera = UIAlertAction(title: takePhotoText, style: .default) { action in
                self.checkAndOpenCamera(on: controller)
            }
            alertController.addAction(alertActionCamera)
        }
        
        let cancelText =  "Cancel"
        let alertActionCancel = UIAlertAction(title: cancelText, style: .cancel) { _ in
        }
        alertController.addAction(alertActionCancel)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func checkAndOpenCamera(on controller: ImagePickerDelegateController) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
            
        case .authorized:
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = controller
            imagePicker.allowsEditing = true
            
            let sourceType = UIImagePickerControllerSourceType.camera
            if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                
                imagePicker.sourceType = sourceType
                
                if imagePicker.sourceType == .camera {
                    imagePicker.showsCameraControls = true
                }
                controller.present(imagePicker, animated: true, completion: nil)
                
            } else {
                
                let cameraNotAvailableText = "Camera not available"
                self.showAlert(title: "Error", message: cameraNotAvailableText)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                
                if granted {
                    
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = controller
                         imagePicker.allowsEditing = true
                        let sourceType = UIImagePickerControllerSourceType.camera
                        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                            
                            imagePicker.sourceType = sourceType
                            if imagePicker.sourceType == .camera {
//                                imagePicker.allowsEditing = true
                                imagePicker.showsCameraControls = true
                            }
                            controller.present(imagePicker, animated: true, completion: nil)
                            
                        } else {
                            let cameraNotAvailableText = "Camera not available"
                            self.showAlert(title: "Error", message: cameraNotAvailableText)
                        }
                    }
                }
            })
            
        case .restricted:
            alertPromptToAllowCameraAccessViaSetting("You have been restricted from using the camera on this device without camera access this feature wont work")
            
        case .denied:
            alertPromptToAllowCameraAccessViaSetting("Please change your privacy setting from the Settings app and allow access to camera for your app")
        }
    }
    
    func checkAndOpenLibrary(on controller: ImagePickerDelegateController) {
        
        let authStatus = PHPhotoLibrary.authorizationStatus()
        switch authStatus {
            
        case .notDetermined:
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = controller
             imagePicker.allowsEditing = true
            let sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.sourceType = sourceType
//            imagePicker.allowsEditing = true
            controller.present(imagePicker, animated: true, completion: nil)
            
        case .restricted:
            alertPromptToAllowCameraAccessViaSetting("You have been restricted from using the camera on this device without camera access this feature wont work")
            
        case .denied:
            alertPromptToAllowCameraAccessViaSetting("Please change your privacy setting from the Settings app and allow access to camera for your app")
            
        case .authorized:
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = controller
             imagePicker.allowsEditing = true
            let sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.sourceType = sourceType
            controller.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func alertPromptToAllowCameraAccessViaSetting(_ message: String) {
        
        let alertText = "Alert"
        let cancelText = "Cancel"
        let settingsText = "Settings"
        
        let alert = UIAlertController(title: alertText, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: settingsText, style: .default, handler: { (action) in
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


