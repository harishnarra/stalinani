//
//  TwitterController.swift
//  IPAC
//
//  Created by Admin on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//


import Foundation
import TwitterKit

class TwitterController {
    
    static let shared = TwitterController()
    
    private init(){
        
    }
    
    fileprivate let K_SCREEN_NAME = "Stalinani"
    fileprivate let user = TwitterUserModel()
    fileprivate var completionHandle : ((Bool,TwitterUserModel?) -> Void)?
    
    func logout(){
        TWTRTwitter.sharedInstance().sessionStore.logOutUserID(self.user.userID)
    }
    
    func loginWithTwitter(completion : @escaping ((Bool,TwitterUserModel?) -> Void)){
        self.completionHandle = completion
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(/session?.userName)");
                self.getEmail()
            } else {
                completion(false,nil)
                print("error: \(/error?.localizedDescription)");
            }
        })
        
    }
    
    func getUserFollowings( task : TaskTabModel?, isSuccess : @escaping isSuccess){
        //https://api.twitter.com/1.1/favorites/list.json
        if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
            self.user.userID = userID
            let client = TWTRAPIClient.withCurrentUser()
            let url = "https://api.twitter.com/1.1/friends/ids.json"
            let params = ["user_id": userID , "count" : "200"]
            var clientError : NSError?
            let request = client.urlRequest(withMethod: "GET", urlString: url, parameters: [:], error: &clientError)
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if let someData = data {
                    do {
                        if let jsonDataDict  = try JSONSerialization.jsonObject(with: someData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] {
                            print(jsonDataDict)
                            DispatchQueue.main.async(execute: { () -> Void in
                                print_debug(/task?.twitter_follow_id)
                                isSuccess(jsonDataDict.description.contains(/task?.twitter_follow_id))
                            })
                        }else{
                            isSuccess(false)
                        }
                    } catch {
                        self.completionHandle?(false, nil)
                        isSuccess(false)

                    }
                }else{
                    isSuccess(false)

                }
            }
        }
    }
    
    func getUserLikes( task : TaskTabModel?, isSuccess : @escaping isSuccess){
        //https://api.twitter.com/1.1/favorites/list.json
        if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
            self.user.userID = userID
            let client = TWTRAPIClient.withCurrentUser()
            let url = "https://api.twitter.com/1.1/favorites/list.json"
            let params = ["user_id": userID , "count" : "200"]
            var clientError : NSError?
            let request = client.urlRequest(withMethod: "GET", urlString: url, parameters: params, error: &clientError)
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if let someData = data {
                    do {
                        if let jsonDataDict  = try JSONSerialization.jsonObject(with: someData, options: [JSONSerialization.ReadingOptions.allowFragments]) as? [Any]{
                            print(jsonDataDict)
                            DispatchQueue.main.async(execute: { () -> Void in
                                let splitArr = task?.post_url.split("tweet_id=")
                                let some = splitArr?[0]
                                      print("String: ",some!)
                                      let fileArray = some?.components(separatedBy: "/")
                                      let finalFileName = fileArray?.last
                                      print("\"id\":"  + (finalFileName ?? ""))
                                isSuccess(jsonDataDict.description.contains("id = \(finalFileName ?? "")"))
                            })
                        }else{
                            isSuccess(false)
                        }
                    } catch {
                        self.completionHandle?(false, nil)
                        isSuccess(false)
                    }
                }else{
                    isSuccess(false)
                }
            }
        }
        
        
    }
    
    fileprivate func getUserInfo(){
        if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
            self.user.userID = userID
            let client = TWTRAPIClient(userID: userID)
            let url = "https://api.twitter.com/1.1/users/show.json"
            let params = ["user_id": userID]
            var clientError : NSError?
            let request = client.urlRequest(withMethod: "GET", urlString: url, parameters: params, error: &clientError)
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if let someData = data {
                    do {
                        if let jsonDataDict  = try JSONSerialization.jsonObject(with: someData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] {
                            print(jsonDataDict)
                            DispatchQueue.main.async(execute: { () -> Void in
                                if let name = jsonDataDict["name"], let profile_image_url = jsonDataDict["profile_image_url"], let screen_name = jsonDataDict["screen_name"]{
                                    let url = URL(string: "\(profile_image_url)")
                                    self.user.imageUrl = url
                                    self.user.name = "\(name)"
                                    self.user.screen_name = "\(screen_name)"
                                    self.completionHandle?(true,self.user)
                                }
                            })
                        }
                    } catch {
                        self.completionHandle?(false, nil)
                    }
                }
            }
        }
    }
    
    func getTweets(params: [String: Any] , success : @escaping TweetsResponse, failure : @escaping FailureResponse){
        if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
            self.user.userID = userID
            print_debug(userID)
            let client = TWTRAPIClient(userID: userID)
            let url = "https://api.twitter.com/1.1/statuses/user_timeline.json"
            let params = params
            var clientError : NSError?
            let request = client.urlRequest(withMethod: "GET", urlString: url, parameters: params, error: &clientError)
            
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if let someData = data {
                    let d1 = String(decoding: someData, as: UTF8.self)
                    print_debug(d1)
                    success(d1)
                } else {
                    success("")
                    print_debug("Could not json Serailized")
                }
                
            }
            
        }
        
    }
    
    fileprivate func getEmail(){
        let client = TWTRAPIClient.withCurrentUser()
        client.requestEmail { email, error in
            self.user.email = email ?? ""
            self.getUserInfo()
        }
    }
    
    func shareOnTwitter(vc : UIViewController,
                        image : UIImage? = nil,
                        text : String,
                        success: @escaping (() -> Void),
                        failure: @escaping ((Error?) -> Void)) {
        
        let composer = TWTRComposer()
        
        composer.setText(text)
        if let image = image {
            composer.setImage(image)
        }
        
        guard let nvc = vc.navigationController else {
            failure(NSError(domain: "Navigation controller not found", code: 0, userInfo: [NSLocalizedDescriptionKey : "Unable to present twitter share controller"]))
            return
        }
        
        composer.show(from: nvc){ result in
            if (result == .done) {
                success()
            } else {
                failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Unable to post on twitter"]))
            }
        }
        
    }
    
    func retweet(tweetId: String, completion: @escaping (Error?)->()) {
        
        let client = TWTRAPIClient.withCurrentUser()
        
        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/retweet/" + "\(tweetId)" + ".json"
        let params = ["id": "\(tweetId)"]
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "POST", urlString: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if let connectionError = connectionError {
                print("Error: \(connectionError)")
                completion(connectionError)
            } else {
                completion(nil)
            }
        }
    }
}

class TwitterUserModel {
    
    var name: String = ""
    var email: String = ""
    var imageUrl: URL?
    var userID: String = ""
    var screen_name : String = ""
    
    init(){}
}

