//
//  QRCodeScannerController.swift
//  QRCodeScannerDemo
//
//  Created by Appinventiv on 23/10/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

protocol QRCodeScannerControllerDelegate {
    
    func finishScanning(withResult result : String)
    
}

//Replace with your torch/flash on/off images
let TORCH_OFF_IMAGE = #imageLiteral(resourceName: "ic_flash_off")
let TORCH_ON_IMAGE = #imageLiteral(resourceName: "ic_flash")

class QRCodeScannerController: UIViewController {
    
    fileprivate var captureSession:AVCaptureSession?
    fileprivate var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    fileprivate var qrCodeFrameView:UIView?
    fileprivate var scanLine : CALayer!
    fileprivate var torchBtn:UIButton!
    fileprivate var backBtn:UIButton!
    
    var delegate : QRCodeScannerControllerDelegate?
    var taskId : String?
    var taskCode : String?
    
    let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                              AVMetadataObject.ObjectType.code39,
                              AVMetadataObject.ObjectType.code39Mod43,
                              AVMetadataObject.ObjectType.code93,
                              AVMetadataObject.ObjectType.code128,
                              AVMetadataObject.ObjectType.ean8,
                              AVMetadataObject.ObjectType.ean13,
                              AVMetadataObject.ObjectType.aztec,
                              AVMetadataObject.ObjectType.pdf417,
                              AVMetadataObject.ObjectType.qr]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.qrCodeScannerSetup()
    }
    
    private func qrCodeScannerSetup() {

        self.title = "Task"
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        torchBtn = UIButton(type: UIButtonType.custom)
        torchBtn.addTarget(self, action: #selector(onClickTorchBtn(button:)), for: UIControlEvents.touchUpInside)
        torchBtn.setImage(TORCH_OFF_IMAGE, for: UIControlState.normal)
        torchBtn.setImage(TORCH_ON_IMAGE, for: UIControlState.selected)
        backBtn = UIButton(type: UIButtonType.custom)
        backBtn.frame = CGRect(x: 15, y: 15, width: 44, height: 44)
        torchBtn.frame = CGRect(x: (self.view.frame.width / 2) - 22, y: self.view.frame.height - 64, width: 44, height: 44)
        backBtn.addTarget(self, action: #selector(onBackBtn(button:)), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "icSignupBack"), for: UIControlState.normal)
        backBtn.addShadow(offset: CGSize.init(width: 0, height: 0), radius: 4, color: UIColor.black, opacity: 0.5)
        self.view.addSubview(torchBtn)
        self.view.addSubview(backBtn)
        
        self.checkCameraAccess { (permission) in
            
            if !permission{
                if let nvc = self.navigationController {
                    nvc.popViewController(animated: true)
                }else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        guard let captureDevice = AVCaptureDevice.default(for: .video)else{
      
            return
        }
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            self.captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            self.captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            self.captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = self.supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            self.videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.videoPreviewLayer?.frame = view.layer.bounds
            
            self.scanLine = CALayer()
            
            self.scanLine.backgroundColor = UIColor.red.cgColor
            
            let drop = CABasicAnimation(keyPath: "position")
            
            drop.fromValue = self.view.frame.origin
            
            drop.toValue = CGPoint(x: self.view.frame.origin.x, y: self.view.layer.bounds.height)
            
            drop.duration = 2.5
            drop.autoreverses = true
            drop.repeatCount = 10000
            
            self.scanLine.add(drop, forKey: "drop")
            
            if let videoPreviewLayer = self.videoPreviewLayer {
                
                view.layer.addSublayer(videoPreviewLayer)
                videoPreviewLayer.addSublayer(self.scanLine)
                self.scanLine.frame.origin = videoPreviewLayer.bounds.origin
                self.scanLine.frame.size = CGSize(width: UIScreen.main.bounds.width * 2, height: 3)
                
            }
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            view.bringSubview(toFront: backBtn)
            view.bringSubview(toFront: torchBtn)


            
       
            
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    fileprivate func checkIfFlashAvailable(completion:((Bool, AVCaptureDevice)->Void)){
        
        var device : AVCaptureDevice!
        
        if #available(iOS 10.0, *) {
            let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDuoCamera], mediaType: .video, position: .unspecified)
            let devices = videoDeviceDiscoverySession.devices
            device = devices.first!
            
        } else {
            // Fallback on earlier versions
            device = AVCaptureDevice.default(for: .video)
        }
        
        if ((device as AnyObject).hasMediaType(AVMediaType.video))
        {
            if (device.hasTorch)
            {
                completion(true, device)
                return
            }
        }
        completion(false, device)
    }
    
    private func flashOn()
    {
        checkIfFlashAvailable { (success, device) in
            
            guard success else {return}
            
            let capturSession = AVCaptureSession()
            capturSession.beginConfiguration()
            
            do{
                
                try device.lockForConfiguration()
                device.torchMode = .on
                device.flashMode = .on
                device.unlockForConfiguration()
                
                capturSession.commitConfiguration()
                
            }
            catch{
                //DISABEL FLASH BUTTON HERE IF ERROR
                print("Device tourch Flash Error ");
            }
        }
    }
    private func flashOff()
    {
        checkIfFlashAvailable { (success, device) in
            
            guard success else {return}
            
            let capturSession = AVCaptureSession()
            capturSession.beginConfiguration()
            
            do{
                
                try device.lockForConfiguration()
                device.torchMode = .off
                device.flashMode = .off
                device.unlockForConfiguration()
                
                capturSession.commitConfiguration()
                
            }
            catch{
                //DISABEL FLASH BUTTON HERE IF ERROR
                print("Device tourch Flash Error ");
            }
        }
    }
    
    @objc   func onClickTorchBtn(button:UIButton){
        
        torchBtn.isSelected ? flashOff():flashOn()
        torchBtn.isSelected = !torchBtn.isSelected
    }
    
    @objc   func onBackBtn(button:UIButton){
        self.navigationController?.popViewController(animated: false)
        CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
    }
    
    
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
//=======================================================
extension QRCodeScannerController : AVCaptureMetadataOutputObjectsDelegate {
    
    
    func checkString(str : String){
        if self.taskCode?.MD5(/self.taskCode) == str{
            updateTask(taskId: /self.taskId , str : str)
        }else{
            CommonFunction.showToast(msg: AlertMessage.wrongQR.localized)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    private func updateTask(taskId: String , str : String){
        let params = [ApiKeys.taskId: taskId]
        WebServices.updateTask(params: params, success: { [weak self](result) in
            print_debug(result)
            if let nvc = self?.navigationController {
                nvc.popViewController(animated: true)
            }
            self?.delegate?.finishScanning(withResult: str)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if  metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            print("No QR/barcode is detected")
            return
        }
        
        // Get the metadata object.
        guard let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject else {
            return
        }
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            guard let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj) else {
                return
            }
            qrCodeFrameView?.frame = barCodeObject.bounds
            
            if metadataObj.stringValue != nil {
                
                self.scanLine.removeFromSuperlayer()
                self.captureSession?.stopRunning()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    
                    self.checkString(str :  metadataObj.stringValue!)
                    
                    
                })
                
            }
        }
    }
    
    
    
}

// MARK: - Helpers
extension UIViewController{
    
    func checkCameraAccess(completion:((_ response:Bool)->Void)?) {
        
        var messageStr:String?
        
        func showAlert(){
            
            let alertController = UIAlertController(title: "Alert", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
            
            let alertActionSettings = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
                UIApplication.openSettingsApp
            }
            let alertActionCancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
                
                completion?(false)
            }
            alertController.addAction(alertActionSettings)
            alertController.addAction(alertActionCancel)
            self.present(alertController, animated: true, completion: nil)
        }
        
        let status : PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch status {
            
        case .restricted:
            
            messageStr = "You have been restricted from using the camera on this device Without camera access this feature wont work"
            showAlert()
            
        case .authorized: completion?(true)
            
        case .notDetermined:
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {(granted: Bool) in
                
                if granted {
                    completion?(true)
                }else{
                    
                    messageStr = "You have been restricted from using the camera on this device Without camera access this feature wont work"
                    showAlert()
                }
                
            })
        case .denied:
            
            messageStr = "Please change your privacy setting from the Settings app and allow access to camera for your app"
            showAlert()
        }
    }
}
extension UIApplication{
    
    //MARK: Open Settings
    @nonobjc class var openSettingsApp:Void{
        if self.shared.canOpenURL(URL(string : UIApplicationOpenSettingsURLString)!){
            self.shared.open(URL(string : UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)}
    }
}
