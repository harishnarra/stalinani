//
//  UploadDocuments.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

//enum ProofType:Int {
//
//    case voterCard      = 1
//    case aadharCard     = 2
//    case panCard        = 3
//    case drivingLience  = 4
//    case none           = 5
//
//    init(type: Int){
//
//        switch type {
//        case 1:
//            self = .voterCard
//        case 2:
//            self = .aadharCard
//        case 3:
//            self = .panCard
//        case 4:
//            self = .drivingLience
//        default:
//            self = .none
//        }
//    }
//}
enum ProofType:Int {
    
    case voterCard      = 1
    case aadharCard     = 6
    case panCard        = 2
    case drivingLience  = 3
    case none           = 5
    
    init(type: Int){
        
        switch type {
        case 1:
            self = .voterCard
        case 2:
            self = .panCard
        case 3:
            self = .drivingLience
        case 4:
            self = .none
        default:
            self = .none
        }
    }
}


struct UploadDocuments {
    
    var proofType: ProofType = .voterCard
    var proofName : String = StringConstant.Voter_Card.localized
    var proofImage: [String] = []
    var idNumber: String = ""
    var proofRawImage  = [UIImage]()
    var isProofUploaded = 0
    
    mutating func getDictionary() -> JSONDictionary {
        
        proofImage.count >= 0 ? (isProofUploaded = 1) : (isProofUploaded = 0)
        let dictionary = [ApiKeys.proofType: self.proofType.rawValue,
                          ApiKeys.proofImage: proofImage.joined(separator: ","),
                          ApiKeys.idNumber: idNumber,
                          ApiKeys.isProofUploaded : String(describing: isProofUploaded) ] as JSONDictionary
        
        return dictionary
    }
    
}
