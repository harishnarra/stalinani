//
//  CompletedHomeModel.swift
//  Stalinani
//
//  Created by Haritej on 18/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

class CompletedHomeModel : NSObject{
    var message : String?
    var count : Int?
    var getcompletedHomeResult : [COmpletedHomeTaskResultData]?
    var next : String?
    var total : String?
    
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.message = json["MESSAGE"].stringValue
        self.count = json["COUNT"].intValue
        if let results = json["RESULT"].array { self.getcompletedHomeResult = results.map { COmpletedHomeTaskResultData(json: $0) } }
        self.next = json["NEXT"].stringValue
        self.total = json["TOTAL"].stringValue
    }
}
class COmpletedHomeTaskResultData : NSObject{
    
    var fullName : String?
    var registerationNo : String?
    var userId : String?
    var userImage : String?
    var like_type : String?
    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        self.fullName = json["full_name"].stringValue
        self.registerationNo = json["registeration_no"].stringValue
        self.userId = json["user_id"].stringValue
        self.userImage = json["user_image"].stringValue
        self.like_type = json["like_type"].stringValue

    }
}




