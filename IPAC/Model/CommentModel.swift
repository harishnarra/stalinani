//
//  CommentModel.swift
//  Stalinani
//
//  Created by HariTeju on 11/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

class CommentModel : NSObject{
    var message : String?
    var count : Int?
    var addmodelResult : AddModelResultData?

    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.message = json["MESSAGE"].stringValue
        self.count = json["COUNT"].intValue
        let results = json["RESULT"]
        if !results.isEmpty{
            addmodelResult = AddModelResultData(json: results)
        }
        
    }
}

class AddModelResultData : NSObject{
    
    var commentId : Int?
    var fullName : String?
    var registerationNo : String?
    var submittedTimestamp : String?
    var userComment : String?
    var userId : String?
    var userImage : String?
    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        self.commentId = json["comment_id"].int
        self.fullName = json["full_name"].stringValue
        self.registerationNo = json["registeration_no"].stringValue
        self.submittedTimestamp = json["submitted_timestamp"].stringValue
        self.userComment = json["user_comment"].stringValue
        self.userId = json["user_id"].stringValue
        self.userImage = json["user_image"].stringValue
        

    }
}



