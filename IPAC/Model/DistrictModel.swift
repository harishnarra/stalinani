//
//  DistrictModel.swift
//  IPAC
//
//  Created by macOS on 23/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DistrictModel {
    
    let districtId: String
    let stateId   : String
    let districtName: String
    let districtCode: String
    
    init(dict: JSON) {
        districtId = dict["district_id"].stringValue
        stateId = dict["state_id"].stringValue
        districtCode = dict["district_code"].stringValue
        districtName = dict["district_name"].stringValue
    }
}




struct Expense {
    var  particular: String
    var amount   : String
    var arrImages: [Image]
    
    init(particular : String , amount : String , arrImages : [Image]) {
        self.particular = particular
        self.amount = amount
        self.arrImages = arrImages
    }
}

struct Image{
    var url : String
    var size : String
    
    init( url : String , size : String){
        self.url = url
        self.size = size
    }
}
