//
//  RegistrationModels.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

class RegistrationResponse {
    var registrationNo : String = ""
    var otp : String = ""
    var userID : String = ""
    
    init () {}
    
    init(json : JSON) {
        let dictionary = json.dictionaryObject ?? [String:AnyObject]()
        self.populate(dictionary)
    }
    
    func populate(_ dictionary : JSONDictionary) {
        let json = JSON(dictionary)
        self.registrationNo = json["registeration_no"].stringValue
        self.otp = json["otp"].stringValue
        self.userID = json["user_id"].stringValue
    }
    
}

