//
//  CollegeList.swift
//  IPAC
//
//  Created by macOS on 23/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CollegeList {
   
    let collegeId   : String
    let districtId  : String
    let collegeName : String
    let collegeCode : String
    let stateId     : String
    
    init(dict: JSON) {
        collegeId       = dict["college_id"].stringValue
        districtId      = dict["district_id"].stringValue
        collegeName     = dict["college_name"].stringValue
        collegeCode     = dict["college_code"].stringValue
        stateId         = dict["state_id"].stringValue
    }
}
