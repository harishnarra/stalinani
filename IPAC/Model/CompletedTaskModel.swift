//
//  CompletedTaskModel.swift
//  Stalinani
//
//  Created by Haritej on 17/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

class CompletedTaskModel : NSObject{
    var message : String?
    var count : Int?
    var getcompletedTaskResult : [COmpletedTaskResultData]?
    var next : String?
    var total : String?
    
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.message = json["MESSAGE"].stringValue
        self.count = json["COUNT"].intValue
        if let results = json["RESULT"].array { self.getcompletedTaskResult = results.map { COmpletedTaskResultData(json: $0) } }
        self.next = json["NEXT"].stringValue
        self.total = json["TOTAL"].stringValue
    }
}
class COmpletedTaskResultData : NSObject{
    
    var fullName : String?
    var registerationNo : String?
    var userId : String?
    var userImage : String?
    var phone_number : String?
    var added_on : String?

    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        self.fullName = json["full_name"].stringValue
        self.registerationNo = json["registeration_no"].stringValue
        self.userId = json["user_id"].stringValue
        self.userImage = json["user_image"].stringValue
        self.phone_number = json["phone_number"].stringValue
        self.added_on = json[""].stringValue

    }
}




