//
//  DailyRewardModel.swift
//  Stalinani
//
//  Created by HariTeju on 29/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

class RewardData : NSObject{
    
    var CODE : Int?
    var MESSAGE : String?
    var new_unlock_message : String?
    var RESULT : [rewardResult]?
    var new_unlock : Bool?
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.MESSAGE = json["MESSAGE"].stringValue
        self.CODE = json["CODE"].intValue
        self.new_unlock_message = json["new_unlock_message"].stringValue
        self.new_unlock = json["new_unlock"].bool
        if let results = json["RESULT"].array { self.RESULT = results.map { rewardResult(json: $0) } }
    }
}

class rewardResult : NSObject{
    var rewardDay : Int?
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.rewardDay = json["rewardDay"].intValue
        
    }
}
