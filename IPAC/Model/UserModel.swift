//
//  UserModel.swift
//
//  Created by Appinventiv on 03/08/18
//  Copyright (c) . All rights reserved.
//


import Foundation
import SwiftyJSON

 class UserModel: NSObject, NSCoding {
    
    // MARK: Properties
    var is_number_verified : String?
    var state: String?
    var district: String?
    var fullName: String?
    var isActive: String?
    var registerationNo: String?
    var paytmNumber: String?
    var phoneNumber: String?
    var otp: String?
    var emailId: String?
    var otpSentTime: String?
    var whatsupNumber: String?
    var facebookId: String?
    var userId: String?
    var userImage: String?
    var twitterId: String?
    var accessToken: String?
    var emailApproved: String?
    var isProofUploaded : Int? = 0
    var idProofs: [IdProof]?
    var temp: IdProof?
    var is_bonus_received : String?
    
    
    var gender     : String?
    var requestedForEditProfile     : String?
    var college                : String?
    var registeredOn    : String?
    var imageThumb : String?
    var image : String?
    var referalCode : String?
    var isUserCollegeStudent : String?
    var dob: String?
    var isProfileEdited : String?
    
    var districtName: String?
    var language     : Int?
    var campaignParticipated : Int?
    var fbUsername: String?
    var twitterUsername: String?
    var stateName: String?
    
    override init() {
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(is_number_verified , forKey : "is_number_verified")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(district, forKey: "district")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(isActive, forKey: "isActive")
        aCoder.encode(registerationNo, forKey: "registerationNo")
        aCoder.encode(paytmNumber, forKey: "paytmNumber")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(otp, forKey: "otp")
        aCoder.encode(emailId, forKey: "emailId")
        aCoder.encode(otpSentTime, forKey: "otpSentTime")
        aCoder.encode(whatsupNumber, forKey: "whatsup_number")
        aCoder.encode(facebookId, forKey: "facebookId")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(userImage, forKey: "user_image")
        aCoder.encode(twitterId, forKey: "twitterId")
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(idProofs, forKey: "idProofs")
        aCoder.encode(isProofUploaded, forKey: "isProofUploaded")
        aCoder.encode(emailApproved, forKey: "emailApproved")
        
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(requestedForEditProfile, forKey: "requestedForEditProfile")
        aCoder.encode(college, forKey: "college")
        aCoder.encode(registeredOn, forKey: "registeredOn")
        aCoder.encode(imageThumb, forKey: "imageThumb")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(referalCode, forKey: "referalCode")
        aCoder.encode(isUserCollegeStudent, forKey: "isUserCollegeStudent")
        aCoder.encode(dob, forKey: "dob")
        aCoder.encode(isProfileEdited, forKey: "isProfileEdited")
        
        aCoder.encode(districtName, forKey: "districtName")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(campaignParticipated, forKey: "campaignParticipated")
        aCoder.encode(fbUsername, forKey: "fbUsername")
        aCoder.encode(twitterUsername, forKey: "twitterUsername")
        aCoder.encode(stateName, forKey: "stateName")
        aCoder.encode(is_bonus_received , forKey : "is_bonus_received")
        
    }
    
     required  init(coder aDecoder: NSCoder) {
        is_number_verified = aDecoder.decodeObject(forKey: "is_number_verified") as? String

         state = aDecoder.decodeObject(forKey: "state") as? String
         district = aDecoder.decodeObject(forKey: "district") as? String
         fullName = aDecoder.decodeObject(forKey: "fullName") as? String
         isActive = aDecoder.decodeObject(forKey: "isActive") as? String
         registerationNo = aDecoder.decodeObject(forKey: "registerationNo") as? String
         paytmNumber = aDecoder.decodeObject(forKey: "paytmNumber") as? String
         phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
         otp = aDecoder.decodeObject(forKey: "otp") as? String
         emailId = aDecoder.decodeObject(forKey: "emailId") as? String
         otpSentTime = aDecoder.decodeObject(forKey: "otpSentTime") as? String
         whatsupNumber = aDecoder.decodeObject(forKey: "whatsup_number") as? String
         facebookId = aDecoder.decodeObject(forKey: "facebookId") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? String
         userImage = aDecoder.decodeObject(forKey: "user_image") as? String
         twitterId = aDecoder.decodeObject(forKey: "twitterId") as? String
         accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String
         emailApproved = aDecoder.decodeObject(forKey: "emailApproved") as? String
         isProofUploaded = aDecoder.decodeObject(forKey: "isProofUploaded") as? Int
         idProofs = aDecoder.decodeObject(forKey: "idProofs") as? [IdProof]
        
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         requestedForEditProfile = aDecoder.decodeObject(forKey: "requestedForEditProfile") as? String
         college = aDecoder.decodeObject(forKey: "college") as? String
         registeredOn = aDecoder.decodeObject(forKey: "registeredOn") as? String
         imageThumb = aDecoder.decodeObject(forKey: "imageThumb") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         referalCode = aDecoder.decodeObject(forKey: "referalCode") as? String
         isUserCollegeStudent = aDecoder.decodeObject(forKey: "isUserCollegeStudent") as? String
         dob = aDecoder.decodeObject(forKey: "dob") as? String
         isProfileEdited = aDecoder.decodeObject(forKey: "isProfileEdited") as? String
        
         language = aDecoder.decodeObject(forKey: "language") as? Int
         districtName = aDecoder.decodeObject(forKey: "districtName") as? String
         campaignParticipated = aDecoder.decodeObject(forKey: "campaignParticipated") as? Int
         fbUsername = aDecoder.decodeObject(forKey: "fbUsername") as? String
         twitterUsername = aDecoder.decodeObject(forKey: "twitterUsername") as? String
         stateName = aDecoder.decodeObject(forKey: "stateName") as? String
        is_bonus_received =  aDecoder.decodeObject(forKey : "is_bonus_received") as? String

    }
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let state = "state"
        
        static let district = "district"
        static let fullName = "full_name"
        static let isActive = "is_active"
        static let registerationNo = "registeration_no"
        static let paytmNumber = "paytm_number"
        static let phoneNumber = "phone_number"
        static let otp = "otp"
        static let emailId = "email_id"
        static let otpSentTime = "otp_sent_time"
        static let whatsupNumber = "whatsup_number"
        static let facebookId = "facebook_id"
        static let userId = "user_id"
        static let userImage = "user_image"
        static let twitterId = "twitter_id"
        static let accessToken = "accesstoken"
        static let idProofs = "id_proof"
        static let emailApproved = "is_email_approved"
        static let isProofUploaded = "isProofUploaded"
        
        static let gender = "gender"
        static let requestedForEditProfile = "requested_for_edit_profile"
        static let college          = "college"
        static let registeredOn    = "registered_on"
        static let imageThumb     = "image_thumb"
        static let image        = "image"
        static let   referalCode           = "referal_code"
        static let isUserCollegeStudent = "isUserCollegeStudent"
        static let  dob   = "dob"
        static let isProfileEdited = "is_profile_edited"
        
        static let districtName                 = "district_name"
        static let language                     = "language"
        static let campaignParticipated         = "campaign_participated"
        static let   fbUsername                 = "fb_username"
        static let twitterUsername              = "twitter_username"
        static let  stateName                    = "state_name"
        
    }
    
    
    
    
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
    
    
    required  init(json: JSON) {
        self.is_number_verified = json["is_number_verified"].stringValue
        self.state = json[SerializationKeys.state].stringValue
        self.district = json[SerializationKeys.district].string
        self.fullName = json[SerializationKeys.fullName].string
        self.isActive = json[SerializationKeys.isActive].string
        self.registerationNo = json[SerializationKeys.registerationNo].string
        self.paytmNumber = json[SerializationKeys.paytmNumber].string
        self.phoneNumber = json[SerializationKeys.phoneNumber].string
        self.otp = json[SerializationKeys.otp].string
       self.emailId = json[SerializationKeys.emailId].string
        self.otpSentTime = json[SerializationKeys.otpSentTime].string
        self.whatsupNumber = json[SerializationKeys.whatsupNumber].string
        self.facebookId = json[SerializationKeys.facebookId].string
        self.userId = json[SerializationKeys.userId].string
        self.userImage = json[SerializationKeys.userImage].string
        self.twitterId = json[SerializationKeys.twitterId].string
        self.accessToken = json[SerializationKeys.accessToken].string
        self.emailApproved = json[SerializationKeys.emailApproved].string
        if let items = json[SerializationKeys.idProofs].array { idProofs = items.map { IdProof(json: $0) } }
        self.isProofUploaded = json[SerializationKeys.isProofUploaded].int ?? 0
        if let temps = json[""].dictionary { temp = IdProof.init(json: JSON(temps))}
        
        
        self.gender = json[SerializationKeys.gender].string
        self.requestedForEditProfile = json[SerializationKeys.requestedForEditProfile].string
        self.college = json[SerializationKeys.college].string
        self.registeredOn = json[SerializationKeys.registeredOn].string
        self.imageThumb = json[SerializationKeys.imageThumb].string
        self.image = json[SerializationKeys.image].string
        self.referalCode = json[SerializationKeys.referalCode].string
        self.isUserCollegeStudent = json[SerializationKeys.isUserCollegeStudent].string
        self.dob = json[SerializationKeys.dob].string
        self.isProfileEdited = json[SerializationKeys.isProfileEdited].string
        
        self.language = json[SerializationKeys.isProfileEdited].int
        self.districtName = json[SerializationKeys.districtName].string
        self.stateName = json[SerializationKeys.stateName].string
        self.campaignParticipated = json[SerializationKeys.campaignParticipated].int
        self.fbUsername = json[SerializationKeys.fbUsername].string
        self.twitterUsername = json[SerializationKeys.twitterUsername].string
        self.is_bonus_received = json["is_bonus_received"].string
    }
    
    static func getUserArr(jsonArr: JSON)->[UserModel]{
        var tempArr : [UserModel] = []
        for item in jsonArr.arrayValue {
            let ob = UserModel.init(json: item)
            tempArr.append(ob)
        }
        return (tempArr)
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
//     func dictionaryRepresentation() -> [String: Any] {
//        var dictionary: [String: Any] = [:]
//        if let value = state { dictionary[SerializationKeys.state] = value }
//        if let value = district { dictionary[SerializationKeys.district] = value }
//        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
//        if let value = isActive { dictionary[SerializationKeys.isActive] = value }
//        if let value = registerationNo { dictionary[SerializationKeys.registerationNo] = value }
//        if let value = paytmNumber { dictionary[SerializationKeys.paytmNumber] = value }
//        if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
//        if let value = otp { dictionary[SerializationKeys.otp] = value }
//        if let value = emailId { dictionary[SerializationKeys.emailId] = value }
//        if let value = otpSentTime { dictionary[SerializationKeys.otpSentTime] = value }
//        if let value = whatsupNumber { dictionary[SerializationKeys.whatsupNumber] = value }
//        if let value = facebookId { dictionary[SerializationKeys.facebookId] = value }
//        if let value = userId { dictionary[SerializationKeys.userId] = value }
//        if let value = userImage { dictionary[SerializationKeys.userImage] = value }
//        if let value = twitterId { dictionary[SerializationKeys.twitterId] = value }
//        if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
//        if let value = emailApproved { dictionary[SerializationKeys.emailApproved] = value }
//        //        if let value = idProofs { dictionary[SerializationKeys.idProofs] = value.map { $0.dictionaryRepresentation() } }
//        if let value = idProofs { dictionary[SerializationKeys.idProofs] = value }
//        dictionary[SerializationKeys.isProofUploaded] = isProofUploaded
//
//
//        if let value = gender { dictionary[SerializationKeys.gender] = value }
//        if let value = requestedForEditProfile { dictionary[SerializationKeys.requestedForEditProfile] = value }
//        if let value = college { dictionary[SerializationKeys.college] = value }
//        if let value = registeredOn { dictionary[SerializationKeys.registeredOn] = value }
//        if let value = imageThumb { dictionary[SerializationKeys.imageThumb] = value }
//        if let value = image { dictionary[SerializationKeys.image] = value }
//        if let value = isUserCollegeStudent { dictionary[SerializationKeys.isUserCollegeStudent] = value }
//        if let value = dob { dictionary[SerializationKeys.dob] = value }
//        if let value = isProfileEdited { dictionary[SerializationKeys.isProfileEdited] = value }
//        if let value = referalCode { dictionary[SerializationKeys.referalCode] = value }
//
//
//
//
//        if let value = language { dictionary[SerializationKeys.language] = value }
//        if let value = districtName { dictionary[SerializationKeys.districtName] = value }
//        if let value = stateName { dictionary[SerializationKeys.stateName] = value }
//        if let value = campaignParticipated { dictionary[SerializationKeys.campaignParticipated] = value }
//        if let value = fbUsername { dictionary[SerializationKeys.fbUsername] = value }
//        if let value = twitterUsername { dictionary[SerializationKeys.twitterUsername] = value }
//        return dictionary
//    }
    
}





public class IdProof: NSObject, NSCoding {
    // MARK: Properties
    public var proofType: String?
    public var image: String?
    public var idNumber: String?
    //public var isProofUploaded: Int = 0
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let proofType = "proof_type"
        static let image = "image"
        static let idNumber = "id_number"
        //static let isProofUploaded = "isProofUploaded"
    }
    
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(proofType, forKey: "proofType")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(idNumber, forKey: "idNumber")
        //aCoder.encode(isProofUploaded, forKey: "isProofUploaded")
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let proofType = aDecoder.decodeObject(forKey: "proofType") as? String
        let image = aDecoder.decodeObject(forKey: "image") as? String
        let idNumber = aDecoder.decodeObject(forKey: "idNumber") as? String
        //let isProofUploaded = aDecoder.decodeObject(forKey: "isProofUploaded") as? Int
        self.init(proofType: proofType, image: image, idNumber: idNumber)
    }
    
    init(proofType: String?, image: String?, idNumber: String?) {
        self.proofType = proofType
        self.image = image
        self.idNumber = idNumber
        //self.isProofUploaded = proofUploaded ?? 0
    }
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    public required init(json: JSON) {
        proofType = json[SerializationKeys.proofType].string
        image = json[SerializationKeys.image].string
        idNumber = json[SerializationKeys.idNumber].string
        //isProofUploaded = json[SerializationKeys.isProofUploaded].int ?? 0
    }
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = proofType { dictionary[SerializationKeys.proofType] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = idNumber { dictionary[SerializationKeys.idNumber] = value }
        //dictionary[SerializationKeys.isProofUploaded] = isProofUploaded
        return dictionary
    }
    
}
