//
//  GetCommentModel.swift
//  Stalinani
//
//  Created by HariTeju on 12/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire


class GetCommentModel : NSObject{
    var message : String?
    var count : Int?
    var getCommentResult : [GetCommentResultData]?
    var next : String?
    var total : String?
    
    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.message = json["MESSAGE"].stringValue
        self.count = json["COUNT"].intValue
        if let results = json["RESULT"].array { self.getCommentResult = results.map { GetCommentResultData(json: $0) } }
        self.next = json["NEXT"].stringValue
        self.total = json["TOTAL"].stringValue
    }
}
class GetCommentResultData : NSObject{
    
    var commentId : Int?
    var fullName : String?
    var registerationNo : String?
    var submittedTimestamp : String?
    var userComment : String?
    var userId : String?
    var userImage : String?
    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        self.commentId = json["comment_id"].intValue
        self.fullName = json["full_name"].stringValue
        self.registerationNo = json["registeration_no"].stringValue
        self.submittedTimestamp = json["submitted_timestamp"].stringValue
        self.userComment = json["user_comment"].stringValue
        self.userId = json["user_id"].stringValue
        self.userImage = json["user_image"].stringValue
        

    }
}




