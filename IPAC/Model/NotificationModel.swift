//
//  Notification.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import  SwiftyJSON
/*
 
 "notification_id": "1",
 "user_id": "0",
 "task_id": "0",
 "news_id": "67",
 "notification_type": "add_news",
 "notification_text": "one more news is added",
 "inserted_on": "2018-08-13 06:12:00"

 */


struct PushNotification{
    let data : PushData
    let alert : AlertData
    
    init(dict : JSON){
        data = PushData.init(dict: JSON(dict["data"].dictionaryValue))
        alert = AlertData.init(dict: JSON(dict["alert"].dictionaryValue))
    }
}

struct PushData{
    var type : String
    init(dict : JSON){
        type = dict["type"].stringValue
    }
}
struct AlertData {
    var body : String?
    var title : String?
    
    init(dict : JSON){
        body = dict["body"].stringValue
        title = dict["title"].stringValue
    }
}


struct NotificationModel {
    let notificationId : String
    let userId : String
    let taskId: String
    let newsId : String
    let notificationType: String
    let notificationText: String
    let notificationDate : String
    
 
    init(dict: JSON) {
        notificationId = dict["notification_id"].stringValue
        userId = dict["user_id"].stringValue
        taskId = dict["task_id"].stringValue
        newsId = dict["news_id"].stringValue
        notificationType = dict["notification_type"].stringValue
        notificationText = dict["notification_text"].stringValue
        notificationDate = dict["inserted_on"].stringValue
        
    }
}

struct TermsCondition {
    let version : String
    let campaign_status : String
    let term_accept_status : String
    let id_proff_added : String
    let completed_task : String
    
    init(dict : JSON){
        version = dict["version"].stringValue
        campaign_status = dict["campaign_status"].stringValue
        term_accept_status = dict["term_accept_status"].stringValue
        id_proff_added = dict["id_proff_added"].stringValue
        completed_task = dict["completed_task"].stringValue
    }
}

