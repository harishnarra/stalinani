//
//  HomeKnowledgeModel.swift
//  Stalinani
//
//  Created by HariTeju on 30/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON



class HomeKnowledgeModel : NSObject{
    var message : String?
    var code : Int?
    var resultt : [HomeKnowledgeResultModel]?

    override init() {
        super.init()
    }
    required init(json : JSON) {
        self.message = json["MESSAGE"].stringValue
        self.code = json["CODE"].intValue
        if let results = json["RESULT"].array { self.resultt = results.map { HomeKnowledgeResultModel(json: $0) } }
    }
}

class HomeKnowledgeResultModel : NSObject{
    
    var faqdescription : String?
    var faqtitle : String?
    
    override init() {
        super.init()
    }
    
    required init(json : JSON) {
        self.faqdescription = json["faq_description"].stringValue
        self.faqtitle = json["faq_title"].stringValue

    }
}
