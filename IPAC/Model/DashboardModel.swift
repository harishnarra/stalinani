//
//  DashboardModel.swift
//  IPAC
//
//  Created by macOS on 17/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DashboardModel {
    let userInfo            : UserInfo
    let topLeaderboards     : [TopLeaderboard]
    var tasks               : [Task]
    var districttotal : String?
    var stateTotal: String?
    var stateRank: String?
    var districtRank: String?
    
    init(dict: JSON) {
        self.districttotal = dict["district_total"].stringValue
        self.stateTotal = dict["state_total"].stringValue
        self.stateRank = dict["state_rank"].stringValue
        self.districtRank = dict["district_rank"].stringValue

        userInfo        = UserInfo(dict: dict["userInfo"])
        topLeaderboards = dict["topleaders"].arrayValue.map({ (topLeaderboard) -> TopLeaderboard  in
            return TopLeaderboard(dict: topLeaderboard)
        })
        tasks           = dict["task"].arrayValue.map({ (task) -> Task in
            return Task(dict: task)

        })
    }
}

struct UserInfo {
    let rewards : Int
    let fullName : String
    let isAactive: String
    let userImage : String
    let rating : Int
    let rank: Int
    let userId: String
    let referal_code : String
    let referal_content : String
    //wallet
    let total_point : String
    let updated_date : String
    let updated_expense_date : String
    
    
    
    init(dict: JSON) {
        rewards     = dict["rewards"].intValue
        fullName    = dict["full_name"].stringValue
        isAactive   = dict["is_active"].stringValue
        userImage   = dict["user_image"].stringValue
        rating      = dict["rating"].intValue
        rank        = dict["rank"].intValue
        userId      = dict["user_id"].stringValue
        referal_code = dict["referal_code"].stringValue
        referal_content = dict["referal_content"].stringValue
        
        total_point = dict["total_point"].stringValue
        updated_date = dict["updated_date"].stringValue
        updated_expense_date = dict["updated_expense_date"].stringValue


    }
}

struct WalletDetail {
    let reward : String
    let expense : String
    let bonus: String
    
    init(dict: JSON) {
        reward     = dict["reward"].stringValue
        expense    = dict["expense"].stringValue
        bonus   = dict["bonus"].stringValue
        
        
    }
}

struct TopLeaderboard {
    let rewards     : Int
    let fullName    : String
    let userImage   : String
    let rating      : String
    let ranking     : Int
    
    init(dict: JSON) {
        rewards     = dict["rewards"].intValue
        fullName    = dict["full_name"].stringValue
        userImage   = dict["user_image"].stringValue
        rating   = dict["rating"].stringValue
        ranking     = dict["ranking"].intValue
    }
}

struct Task {
    let totalFbTask     : Int
    let totalTask       : Int
    let rewards         : Int
    let completedFbTask : Int
    let totalTwitterTask: Int
    let userId          : String
    let completedTask   : Int
    let addedOn         : String
    let completedTwitterTask :Int
    
    let totalYoutubeTask : Int
    let completed_youtube_task : Int
    let total_form_task : Int
    let completed_form_task : Int
    let total_offline_task : Int
    let completed_offline_task : Int
    let total_whatsapp_task : Int
    let completed_whatsapp_task : Int

    
    
    init(dict : JSON) {
        totalFbTask             = dict["total_fb_task"].intValue
        totalTask               = dict["total_task"].intValue
        rewards                 = dict["rewards"].intValue
        completedFbTask         = dict["completed_fb_task"].intValue
        totalTwitterTask        = dict["total_twitter_task"].intValue
        userId                  = dict["user_id"].stringValue
        completedTask           = dict["completed_task"].intValue
        addedOn                 = dict["added_on"].stringValue
        completedTwitterTask    = dict["completed_twitter_task"].intValue
        
        totalYoutubeTask    = dict["total_youtube_task"].intValue
        completed_youtube_task   = dict["completed_youtube_task"].intValue
        total_form_task    = dict["total_form_task"].intValue
        completed_form_task    = dict["completed_form_task"].intValue
        total_offline_task    = dict["total_offline_task"].intValue
        completed_offline_task    = dict["completed_offline_task"].intValue
        total_whatsapp_task    = dict["total_whatsapp_task"].intValue
        completed_whatsapp_task    = dict["completed_whatsapp_task"].intValue


        
    }
}
