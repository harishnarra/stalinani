//
//  ContactUsModel.swift
//  IPAC
//
//  Created by macOS on 17/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ContactUsModel {
    let categoryId: Int
    let categoryName: String
    let categoryName_Tn: String
    
    init(json: JSON) {
        self.categoryId = json["category_id"].intValue
        self.categoryName = json["category_name"].stringValue
        self.categoryName_Tn = json["category_name_tn"].stringValue

    }
}
