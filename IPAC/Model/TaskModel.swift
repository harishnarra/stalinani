//
//  TaskModel.swift
//  IPAC
//
//  Created by macOS on 18/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

enum TaskType: String{
    
    case install
    case twitter
    case facebook
    case online
    case offline
    case youtube
    case whatsapp
    
    init(rawValue: String){
        
        switch rawValue {
        case "twitter":
            self = .twitter
        case "facebook":
            self = .facebook
        case "online":
            self = .online
        case "offline":
            self = .offline
        case "youtube":
            self = .youtube
        case "whatsapp":
            self = .whatsapp
        default:
            self = .install
        }
    }
}

enum TaskAction: String{
    
    case install = "install"
    case like
    case share
    case tweet
    case retweet
    case event
    case qrcode = "qr code"
    case aboutUs = "about us"
    case createGroup = "create group"
    case follow
    case post
    case setdp = "set dp"
    case link
    case image
    case video
    case subscribe
    case comment
    case form
    case downLoadMedia = "download media"
    
    init(rawValue: String){
        
        switch rawValue {
        case "like":
            self = .like
        case "share":
            self = .share
        case "post":
            self = .post
        case "tweet":
            self = .tweet
        case "retweet":
            self = .retweet
        case "set dp":
            self = .setdp
        case "link":
            self = .link
        case "image":
            self = .image
        case "video":
            self = .video
        case "event":
            self = .event
        case "qrcode" :
            self = .qrcode
        case "about us":
            self = .aboutUs
        case "create group":
            self = .createGroup
        case "follow":
            self = .follow
        case "subscribe":
            self = .subscribe
        case "comment":
            self = .comment
        case "form":
            self = .form
        case "download media":
            self = .downLoadMedia
   
        default:
            self = .install
        }
    }
}

struct TaskTabModel {
    
    let text:String
    let district: String
    let action: TaskAction
    let state: String
    let start_date: String
    let registeration_no: String
    let pc: String
    let created_date: String
    let gender: String
    let task_title: String
    let task_description: String
    let college: String
    let task_type: TaskType
    let ac: String
    let task_status: String
    let post_id: String
    let task_id: String
    let task_code :  String
    let channel_id : String
    let video_id : String
    let end_date: String
    let post_url: String
    let points: String
    var taskComplete: String
    let task_media_set: [MediaSet]
    let instruction_description : String
    let instruction_video_url : String
    var total_survay_form : String
    var total_survay_form_complete : String
    let latitude  : String
    let longitude : String
    let radius  : String
    let twitter_follow_id : String
    let twitter_follow_name : String
    let facebook_follow_name : String
    let task_completed_user_list : [TaskCompletedUsers]?
    let task_completed_user_count : String?
    
    
    init(from json: JSON) {
        self.text = json["text"].stringValue
        self.district = json["district"].stringValue
        let action = json["action"].stringValue
        self.action = TaskAction(rawValue: action)
        self.state = json["state"].stringValue
        self.start_date = json["start_date"].stringValue
        self.registeration_no = json["registeration_no"].stringValue
        self.pc = json["pc"].stringValue
        self.created_date = json["created_date"].stringValue
        self.task_title = json["task_title"].stringValue
        self.task_description = json["task_description"].stringValue
        self.college = json["college"].stringValue
        let task_type = json["task_type"].stringValue
        self.task_type = TaskType(rawValue: task_type)
        self.ac = json["ac"].stringValue
        self.task_status = json["task_status"].stringValue
        self.post_id = json["post_id"].stringValue
        self.task_id = json["task_id"].stringValue
        self.end_date = json["end_date"].stringValue
        self.post_url = json["post_url"].stringValue
        self.points = json["points"].stringValue
        self.taskComplete = json["task_completed"].stringValue
        self.task_media_set = json["task_media_set"].arrayValue.map({ (media) -> MediaSet in
            return MediaSet(json: media)
        })
        self.gender = json["gender"].stringValue
        self.instruction_description = json["instruction_description"].stringValue
        self.instruction_video_url = json["instruction_video_url"].stringValue
        total_survay_form_complete = json["total_survay_form_complete"].stringValue
        total_survay_form = json["total_survay_form"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        radius = json["radius"].stringValue
        self.channel_id = json["channel_id"].stringValue
        self.video_id = json["video_id"].stringValue
        self.twitter_follow_id = json["twitter_follow_id"].stringValue
        self.twitter_follow_name = json["twitter_follow_name"].stringValue
        self.task_code = json["task_code"].stringValue
        self.facebook_follow_name = json["facebook_follow_name"].stringValue
        self.task_completed_user_count = json["task_completed_user_count"].stringValue
        self.task_completed_user_list = json["task_completed_user_list"].arrayValue.map({ (media) -> TaskCompletedUsers in
            return TaskCompletedUsers(json: media)
        })



    }
    
    static func getTabTaskModel(from json: JSON)->[TaskTabModel]{
        
        return json.arrayValue.map({TaskTabModel(from: $0)})
    }
}
public class TaskCompletedUsers {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let user_image = "user_image"
        static let full_name = "full_name"
        static let registeration_no = "registeration_no"
        static let user_id = "user_id"
        static let phone_number = "phone_number"

    }
    // MARK: Properties
    public var user_image: String?
    public var full_name: String?
    public var registeration_no: String?
    public var user_id: String?
    public var phone_number: String?

    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    public required init(json: JSON) {
        user_image = json[SerializationKeys.user_image].string
        full_name = json[SerializationKeys.full_name].string
        registeration_no = json[SerializationKeys.registeration_no].string
        user_id = json[SerializationKeys.user_id].string
        phone_number = json[SerializationKeys.phone_number].string

    }
}


