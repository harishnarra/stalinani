//
//  AppDelegateExtension.swift
//  IPAC
//
//  Created by Appinventiv on 24/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import EZSwiftExtensions
import SwiftyJSON

//MARK::- VERSIONING API
extension AppDelegate{
    func sendDateToAPI(){
        guard let date = AppUserDefaults.value(forKey: .date).string else {return}
        WebServices.logs(device_id: DeviceDetail.device_id, deactive_date: date, success: { (_) in
        }) {(_) -> (Void) in
        }
    }
    
   
}


