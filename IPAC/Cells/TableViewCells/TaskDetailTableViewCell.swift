//
//  TaskDetailTableViewCell.swift
//  IPAC
//
//  Created by Appinventiv on 01/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class TaskDetailTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK::- FUNCTION
    func configure(data : Image , index : Int){
        img.kf.setImage(with: URL(string : /data.url))
        lblName.text = "File \((index + 1).description)"
        lblSize.text = data.size + "KB"
    }
}
