//
//  WalletTableViewCell.swift
//  IPAC
//
//  Created by Appinventiv on 23/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WalletTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var imgCoin: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    
    //MARK::- PROPERTIES
    var item : ReferralResult?
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK::- CONFIGURE
    func configure(item : ReferralResult?){
        self.item  = item
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = formatter.date(from: /item?.created_date) else { return  }
        formatter.dateFormat = "h:mm a"
        let stringTime = formatter.string(from: date)
        self.lblTime.text = stringTime
        if date .isToday{
            formatter.dateFormat = "d MMM yyyy"
            let stringDate = formatter.string(from: date)
            self.lblDate.text = "Today, " + stringDate
        }else{
            formatter.dateFormat = "EEEE, d MMM yyyy"
            let stringDate = formatter.string(from: date)
            self.lblDate.text = stringDate
        }
        
        lblTitle.text = /item?.title
        lblDescription.text = /item?.descriptions
        imgIcon.image = getIcon(type :/item?.type?.lowercased() , status : /item?.status)
        if /item?.type?.lowercased() == "expense"{
            lblStatus.isHidden = false
            lblStatus.text = /item?.status == "1" ? TitleType.approved.localized : TitleType.pending.localized
            lblStatus.textColor = /item?.status == "1" ? UIColor.green : UIColor.red
            imgCoin.image = #imageLiteral(resourceName: "icCoin")
        }else if /item?.type?.lowercased() == "redeem"{
            lblStatus.isHidden = true
            imgCoin.image = #imageLiteral(resourceName: "icCoin")
        }else{
            lblStatus.isHidden = true
            imgCoin.image = #imageLiteral(resourceName: "icTaskrewards")
        }
        
    }
    
    func getIcon(type : String , status : String) -> UIImage{
        self.lblPoint.text = "+" + ((self.item?.point?.toInt()?.description) ?? "0")
        self.lblPoint.textColor = AppColors.label.dark
        switch type{
        case "facebook":

            return #imageLiteral(resourceName: "icFb")
            
        case "twitter":

            return #imageLiteral(resourceName: "icProfileTwitter")
        
        case "offline":
            return #imageLiteral(resourceName: "icMedia")

        
        case "whatsapp":
            return #imageLiteral(resourceName: "icWhatsapp")

        
        case "youtube":
            return #imageLiteral(resourceName: "icYoutube")

        
        case "expense":
            if status != "1"{
                return #imageLiteral(resourceName: "icPendingExpences")
            }else{
                return #imageLiteral(resourceName: "icApprovedExpences")
            }

        case "bonus":
            return #imageLiteral(resourceName: "icCoin")
        
        case "online":
            return #imageLiteral(resourceName: "icInfo")
            
        case "redeem":
            self.lblPoint.text = "-" + ((self.item?.point?.toInt()?.description) ?? "0")
            self.lblPoint.textColor = AppColors.Green.whatsupGreen
            return #imageLiteral(resourceName: "icRewardreddem")
        
        default :
            return #imageLiteral(resourceName: "icInfo")

        }
    }
}
