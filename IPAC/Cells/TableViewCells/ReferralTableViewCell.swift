//
//  ReferralTableViewCell.swift
//  IPAC
//
//  Created by Appinventiv on 17/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class ReferralTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var imgProfile: AnimatableImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK::- CONFIGURE CELL
    func configure(item : ReferralResult?){
        imgProfile.kf.setImage(with: URL(string : /item?.user_image), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
        lblName.text = /item?.full_name
        lblMobile.text = /item?.phone_number
        lblDate.text = calculateFormat(date : /item?.created_date , formatterReq : "d MMM, yyyy")
    }
}

//MARK::- FUNCTION
extension ReferralTableViewCell{
    func calculateFormat(date : String? , formatterReq : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.date(from: /date) ?? Date()
        formatter.dateFormat = formatterReq
        return /formatter.string(from: newDate)
    }
}
