//
//  FormTableViewCell.swift
//  IPAC
//
//  Created by Appinventiv on 26/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class FormTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnRegister: AnimatableButton!
    
    
    //MARK::- LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        btnRegister.addLeftToRightGradient()
    }
    
    
    //MARK::- FUNCTIONS
    func populateCell(with data: News) {
        lblTitle.text = data.newsTitle
        btnRegister.isHidden = data.is_completed != "0"
        let data = (data.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        lblDesc.attributedText = attrStr
        
    }
    
}
