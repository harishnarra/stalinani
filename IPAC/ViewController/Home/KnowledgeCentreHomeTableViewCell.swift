//
//  KnowledgeCentreHomeTableViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 29/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class KnowledgeCentreHomeTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {


    var sendStoryData: String?
    var sendStoryDesc: String?

       @IBOutlet weak var pagesss: UIPageControl!
    
      var knowledgeData : HomeKnowledgeModel?
      var arrKnowledgeData : [HomeKnowledgeResultModel]?  = []
       
       //var newsarray = [News]
    var news: [News] = []
    var newspost: [News] = []
    
       @IBOutlet weak var collectionView: UICollectionView!
     override func awakeFromNib() {
            super.awakeFromNib()
            
           // self.getHomeNewss(count: 0, loader: false)
      //  self.getHomeknowledgeDatas(loader: true)

        self.arrKnowledgeData = self.knowledgeData?.resultt
       // self.pagesss.numberOfPages = self.knowledgeData?.resultt?.count ?? 0
        self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.showsHorizontalScrollIndicator = false
            self.collectionView.showsVerticalScrollIndicator = false

            self.collectionView.layoutIfNeeded()
            
          
            
    //
    //        for (index, name) in self.news.enumerated() {
    //
    //        }

            
    //        for i in 0..< self.news.count {
    //            if self.news[i].newsCategory == "" {
    //
    //            }
    //        }
            
           
    //        let cellWidth : CGFloat = collectionView.frame.size.width / 4.0
    //        let cellheight : CGFloat = collectionView.frame.size.height - 2.0
    //        let cellSize = CGSize(width: cellWidth , height:cellheight)
    //
    //        let layout = UICollectionViewFlowLayout()
    //        layout.scrollDirection = .vertical //.horizontal
    //        layout.itemSize = cellSize
    //        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    //        layout.minimumLineSpacing = 1.0
    //        layout.minimumInteritemSpacing = 1.0
    //        collectionView.setCollectionViewLayout(layout, animated: true)
            
            collectionView.reloadData()
            
    //        if self.newspost.count == 0 {
    //            HomenewsTableviewCell.tableFooterView = UIView()
    //
    //        }
            // Initialization code
        }

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
//            print("self.arrKnowledgeData.count", self.arrKnowledgeData?.count ?? 0)
            return self.arrKnowledgeData?.count ?? 0
           // return 0

        }
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout
            collectionViewLayout: UICollectionViewLayout,
                            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
        func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            self.pagesss.currentPage = indexPath.row
        }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KnowledgeHomeCollectionViewCell", for: indexPath) as? KnowledgeHomeCollectionViewCell else{
            fatalError() }
        
        let data = (self.arrKnowledgeData?[indexPath.row].faqdescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        cell.headLabel.text = self.arrKnowledgeData?[indexPath.row].faqtitle ?? ""
        sendStoryDesc = attrStr?.string
        cell.bottomLabel.text = sendStoryDesc
        sendStoryData = self.arrKnowledgeData?[indexPath.row].faqtitle ?? ""
        cell.seemoreBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "seemore", comment: ""), for: .normal)
        cell.seemoreBtn.addTarget(self, action: #selector(seemoreClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func seemoreClicked(_ sender: UIButton) {
        let bannerSceen = KnowledgeStory.instantiate(fromAppStoryboard: .Home)
        bannerSceen.recieveStoryName = sendStoryData
        bannerSceen.recieveStoryDesc = sendStoryDesc
        self.parentViewController?.pushVC(bannerSceen)
    }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            let bannerSceen = KnowledgeStory.instantiate(fromAppStoryboard: .Home)
            bannerSceen.recieveStoryName = sendStoryData
            bannerSceen.recieveStoryDesc = sendStoryDesc
            self.parentViewController?.pushVC(bannerSceen)

        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
    
    func getHomeknowledgeDatas(loader : Bool){
        WebServices.HomeKnowledgeList(loader
            , success: { [weak self] (json) in
                print("JSON", json)
                self?.knowledgeData = HomeKnowledgeModel.init(json: json)
                self?.arrKnowledgeData = self?.knowledgeData?.resultt
                self?.pagesss.numberOfPages = self?.knowledgeData?.resultt?.count ?? 0
                self?.collectionView.reloadData()
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
        
//
//        func getHomeNewss(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
//            print_debug("check api")
//            WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
//                refreshControl?.endRefreshing()
//                //self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
//               // self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
//                var news = [News]()
//                for obj in json[ApiKeys.result].arrayValue {
//                    let newsObj = News(json: obj)
//                    news.append(newsObj)
//                }
//                var priorityNews = [News]()
//                for obj in json["PRIORITY_NEWS"].arrayValue {
//                    let newsObj = News(json: obj)
//                    priorityNews.append(newsObj)
//                }
//                if count == 0{
//                    self?.news = priorityNews
//                    self?.news.append(contentsOf: news)
//                }else{
//                    self?.news.append(contentsOf: news)
//                }
//
//                print("self.news.count", self?.news.count ?? 0)
//                // print(self.news.count)
//    //            for i in self!.news   {
//    //
//    //                print("iiiiii:" , i)
//    //
//    //            }
//
//                if self?.news.count == 0 {
//
//                           } else {
//                for (index, element) in self!.news.enumerated() {
//                    print("Item \(index): \(element)")
//                    print(self?.news[index].newsCategory ?? "")
//
//                    if (self?.news[index].newsCategory)!.rawValue == "post" || (self?.news[index].newsCategory)!.rawValue == "article"{
//
//                        print("self.minnews", self?.news[index] ?? "")
//
//                        //self?.newspost =
//                        self?.newspost.append((self?.news[index])!)
//
//                       // newspost = newspost.
//
//    //                    newsarray = newsarray.append((self?.news[index])!)
//
//                    }
//                    }
//
//                }
//                self?.pagesss.numberOfPages = self?.newspost.count ?? 0
//
//                self?.collectionView.reloadData()
//
//    //            self?.lblNoData.isHidden = !(self?.news.isEmpty ?? true)
//    //            self?.reloadWithoutAnimation()
//    //            self?.count = json[ApiKeys.next].intValue
//    //            self?.checkBonus(json : json)
//
//            }) { (error) -> (Void) in
//                CommonFunction.showToast(msg: error.localizedDescription)
//                refreshControl?.endRefreshing()
//    //            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
//    //            self.lblNoData.isHidden = !(self.news.isEmpty )
//                return
//            }
//        }

    }
    //extension HomenewsTableviewCell {
    //
    //    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
    //
    //        collectionView.delegate = dataSourceDelegate
    //        collectionView.dataSource = dataSourceDelegate
    //        collectionView.tag = row
    //        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
    //        collectionView.reloadData()
    //    }
    //
    //    var collectionViewOffset: CGFloat {
    //        set { collectionView.contentOffset.x = newValue }
    //        get { return collectionView.contentOffset.x }
    //    }
    //}




