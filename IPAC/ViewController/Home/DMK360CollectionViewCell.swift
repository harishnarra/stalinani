//
//  DMK360CollectionViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 24/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class DMK360CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
      @IBOutlet weak var backView: UIView!
      @IBOutlet weak var timeLabel: UILabel!
      @IBOutlet weak var descriptionLabel: UILabel!
      @IBOutlet weak var playButton: UIButton!
     // @IBOutlet weak var viewForCarousel: iCarousel!
      @IBOutlet weak var imageOrVideo: UIImageView!
     // @IBOutlet weak var carouselViewHeight: NSLayoutConstraint!
      @IBOutlet weak var seeMoreBtn: UIButton!
    @IBOutlet weak var extraView: UIView!
    
    @IBOutlet weak var commentsBtn: UIButton!
     
     @IBOutlet weak var likeOthersLabel: UILabel!
       @IBOutlet weak var likeImageTwo: UIImageView!
       @IBOutlet weak var likeImageOne: UIImageView!
       @IBOutlet weak var likeView: UIView!
    
    @IBOutlet weak var frontBtnTap: AnimatableButton!
    @IBOutlet weak var backBtnTapp: AnimatableButton!
    
    @IBOutlet weak var reactionBtn: UIButton!
    override func draw(_ rect: CGRect) {
              super.draw(rect)
             
                     seeMoreBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "seemore", comment: ""), for: .normal)
          }
    
}
