//
//  WebPageVC.swift
//  IPAC
//
//  Created by Appinventiv on 14/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

class WebPageVC: BaseViewController, WKNavigationDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var url = ""
    var titleHead = "News"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonFunction.showLoader()

        lblTitle.text = titleHead
        self.webView.navigationDelegate = self
        if let url =  URL(string: url){
            let request = URLRequest(url:url)
            self.webView.load(request)
        }

    }

    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        popVC()
    }
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
           CommonFunction.showLoader()
              let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
            
            webView.evaluateJavaScript(jscript)
           
       }
       func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
           CommonFunction.hideLoader()
           print("two")
         
       }
       
       func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("three")
           
       }
       func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("four")
           
       }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
         //  let urlStr = navigationAction.request.url?.absoluteString
           
           
   

           decisionHandler(.allow)
       }
    

}
