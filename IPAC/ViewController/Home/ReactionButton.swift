//
//  ReactionButton.swift
//  tableViewDemo
//
//  Created by naresh banavath on 11/08/20.
//  Copyright © 2020 naresh banavath. All rights reserved.
//
import UIKit
class ReactionButton : UIButton
{
    lazy var btnsMainView : UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        view.backgroundColor = .gray
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.masksToBounds = true
        
        view.isHidden = true
        
        return view
    }()
    var btn1 : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "FilledHeart"), for: .normal)
        btn.addTarget(self, action: #selector(btnTapped1), for: .touchUpInside)
        return btn }()
    var btn2 : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "Clap"), for: .normal)
        btn.addTarget(self, action: #selector(btnTapped2), for: .touchUpInside)
        return btn }()
    
    var btn3 : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "Fist"), for: .normal)
        btn.addTarget(self, action: #selector(btnTapped3), for: .touchUpInside)
        return btn }()
    
    var leftMainViewAnchor : NSLayoutConstraint?
    var bottomMainViewAnchor : NSLayoutConstraint?
    var widthMainViewAnchor : NSLayoutConstraint?
    var heightMainViewAnchor : NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
         addStackView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addStackView()
        btn2.isUserInteractionEnabled = true
        //btn2.addTarget(self, action: #selector(btnTapped1), for: .touchUpInside)
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(btnTapped1(_:)))
        tapgesture.numberOfTouchesRequired = 1
        btnsMainView.addGestureRecognizer(tapgesture)
        self.addTarget(self, action: #selector(btnTapped(_:)), for: .touchUpInside)
    }
    
    override func sendAction(_ action: Selector, to target: Any?, for event: UIEvent?) {
        btnsMainView.isHidden = false
        bottomMainViewAnchor?.constant = -30
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.layoutIfNeeded()
        }, completion: nil)
        super.sendAction(action, to: target, for: event)
    }
    
    func addStackView()
    {
        
        self.addSubview(btnsMainView)
        btnsMainView.translatesAutoresizingMaskIntoConstraints = false
        leftMainViewAnchor = btnsMainView.leftAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
        leftMainViewAnchor?.isActive = true
        widthMainViewAnchor = btnsMainView.widthAnchor.constraint(equalToConstant: 100)
        widthMainViewAnchor?.isActive = true
        heightMainViewAnchor = btnsMainView.heightAnchor.constraint(equalToConstant: 30)
        heightMainViewAnchor?.isActive = true
        bottomMainViewAnchor = btnsMainView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        bottomMainViewAnchor?.isActive = true
        
        let stackView = UIStackView(arrangedSubviews: [btn1 , btn2,btn3])
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.spacing = 2.0
        //stackView.isUserInteractionEnabled = false
        btnsMainView.isUserInteractionEnabled = true
        btnsMainView.addSubview(stackView)
      
        //stackView Constraints
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leftAnchor.constraint(equalTo: btnsMainView.leftAnchor, constant: 0).isActive = true
        stackView.rightAnchor.constraint(equalTo: btnsMainView.rightAnchor, constant: 0).isActive = true
        stackView.topAnchor.constraint(equalTo: btnsMainView.topAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: btnsMainView.bottomAnchor, constant: 0).isActive = true
    }
    
    @objc func btnTapped(_ sender : UIButton)
    {

    }
    @objc func btnTapped1(_ sender : UIButton)
    {
        print("gjhhghgh")
        self.setImage(sender.image(for: .normal), for: .normal)
        bottomMainViewAnchor?.constant = 0
        UIView.animate(withDuration: 0.30, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.layoutIfNeeded()
        }) { (flag) in
            self.btnsMainView.isHidden = true
        }

    }
    @objc func btnTapped2(_ sender : UIButton)
      {
          print("gjhhghg2222h")
          self.setImage(sender.image(for: .normal), for: .normal)
          bottomMainViewAnchor?.constant = 0
          UIView.animate(withDuration: 0.30, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
              self.layoutIfNeeded()
          }) { (flag) in
              self.btnsMainView.isHidden = true
          }

      }
    @objc func btnTapped3(_ sender : UIButton)
      {
          print("gjhhghg3333h")
          self.setImage(sender.image(for: .normal), for: .normal)
          bottomMainViewAnchor?.constant = 0
          UIView.animate(withDuration: 0.30, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
              self.layoutIfNeeded()
          }) { (flag) in
              self.btnsMainView.isHidden = true
          }

      }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // if btnsMainView is hidden, we've tapped on self
        if btnsMainView.isHidden {
            return self
        }
        // loop through subviews, checking hitTest until we find one
        for v in subviews.reversed() {
            let p = v.convert(point, from: self)
            let r = v.hitTest(p, with: event)
            if r != nil {
                return r
            }
        }
        // didn't tap on a subview, so return nil
        return nil
    }
    
}
