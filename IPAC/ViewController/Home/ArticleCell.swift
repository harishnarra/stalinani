//
//  ArticleCell.swift
//  IPAC
//
//  Created by macOS on 10/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import AVFoundation
import AVKit

class ArticleCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var seeMoreBtn: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    
    //MARK::- PROPERTIES
    var news : News?
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        seeMoreBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "seemore", comment: ""), for: .normal)

    }
    
    //MARK::- FUNCTIONS
    func populateCell(with data : News) {
        titleLabel.text = data.newsTitle
        self.news = data
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date =  formatter.date(from: data.createdDate ?? "") else { return }
        let displayDate = date.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
        self.dateLabel.text = displayDate
        
        let rawText = (data.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        do {
            let attrStr = try NSAttributedString(
                data: rawText,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            descLabel.attributedText = attrStr
            
        } catch {
            print_debug(error.localizedDescription)
        }
        guard let mediaSet = data.mediaSet else { return }
        if /mediaSet.first?.mediaType == "2" {
            btnPlay.setImage(#imageLiteral(resourceName: "playIcon"), for: .normal)
            self.articleImage.kf.setImage(with: URL(string: mediaSet.first?.mediaThumb ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
        }else{
            self.articleImage.kf.setImage(with: URL(string: mediaSet.first?.mediaThumb ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
            btnPlay.setImage(nil, for: .normal)
        }
        
    }
    
    @IBAction func btnActionPlay(_ sender: UIButton) {
        if /self.news?.mediaSet?.first?.mediaType == "2" {
            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            vc.url = /self.news?.mediaSet?.first?.mediaUrl
            vc.desc = self.news?.newsTitle
            ez.topMostVC?.presentVC(vc)

        } else {
            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
            previewVC.previewImage = /self.news?.mediaSet?.first?.mediaUrl
            previewVC.tittle = news?.newsTitle ?? ""
            self.parentViewController?.pushVC(previewVC)
        }
    }
}
