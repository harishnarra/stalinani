//
//  RootViewController.swift
//  IPAC
//
//  Created by Appinventiv on 01/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RootViewController: SSASideMenu,SSASideMenuDelegate {
    
    override func awakeFromNib() {
        self.contentViewController = self.storyboard!.instantiateViewController(withIdentifier: "TaskVC")
        self.leftMenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuVC")
        

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuViewController?.delegate = self
        
        self.sideMenuViewController?.presentLeftMenuViewController()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
