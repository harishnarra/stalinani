//
//  SideMenuVC.swift
//  IPAC
//
//  Created by Appinventiv on 01/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

class SideMenuVC: UIViewController {
    
    // MARK:- VARIABLES
    //====================
    var user : UserModel?

  var iconsArray  = [UIImage(),#imageLiteral(resourceName: "icSidemenuHome"),#imageLiteral(resourceName: "icSidemenuProfile"),#imageLiteral(resourceName: "icMywallet") ,#imageLiteral(resourceName: "icInfo"),#imageLiteral(resourceName: "icSidemenuContact"),#imageLiteral(resourceName: "icSidemenuFaq"),#imageLiteral(resourceName: "icSidemenuPrivacy"),#imageLiteral(resourceName: "newRefer"),UIImage(),#imageLiteral(resourceName: "icSidemenuLogout")]

    var selecteMenu = 1
 
    var sideMenuLabelsArray = ["",LocalizationSystem.sharedInstance.localizedStringForKey(key: "Home", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Profile", comment: ""), LocalizationSystem.sharedInstance.localizedStringForKey(key: "My_Rewards", comment: "") ,LocalizationSystem.sharedInstance.localizedStringForKey(key: "About_DMK", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Contact Us", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "FAQs", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Privacy_Policy", comment: ""), LocalizationSystem.sharedInstance.localizedStringForKey(key: "Referral", comment: ""), "" , LocalizationSystem.sharedInstance.localizedStringForKey(key: "LogOut", comment: "")]
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = CommonFunction.getUser()
        self.initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}


// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension SideMenuVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return sideMenuLabelsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuProfileCellid", for: indexPath) as? SideMenuProfileCell else { fatalError() }
            cell.backgroundColor = .clear
            cell.profileImageView.image = #imageLiteral(resourceName: "icPlaceholder")
            if let urlStr = self.user?.userImage, let url = URL(string: urlStr) {
                cell.profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "icPlaceholder"))
            }
            cell.nameLabel.text = self.user?.fullName ?? ""
            if LocalizationSystem.sharedInstance.getLanguage() == "en" {
                  cell.languageLabel.text = "English"

               // LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            } else {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "ta")
                cell.languageLabel.text = "தமிழ்"

            }
          //  cell.languageLabel.text = "English"
           // cell.languageChangeBtn.addTarget(self, action: #selector(didTaponLangChanged), for: .touchUpInside)

            return cell
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCellid", for: indexPath) as? SideMenuCell
                else { fatalError() }
            cell.sideMenuIcon.image = iconsArray[indexPath.row]
            cell.backgroundColor = .clear
            cell.sideMenuLabel.text = sideMenuLabelsArray[indexPath.row]
            if self.selecteMenu == indexPath.row {
                print_debug(indexPath.row)
                 cell.sideMenuLabel.font  = AppFonts.ProximaNova_Extrabold.withSize(20)
                cell.sideMenuLabel.textColor = UIColor.black
            } else {
                print_debug(indexPath.row)
                cell.sideMenuLabel.font  = AppFonts.Proxima_Nova_Rg_Regular.withSize(16)
            }
            return cell
        }
    }
//    @objc func didTaponLangChanged(sender: UIButton){
//        let selectedCell = tableView.cellForRow(at: indexPath) as! SideMenuProfileCell
//
//        print("")
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: //profile
            let profileSceen = ChangeProfilePicVC.instantiate(fromAppStoryboard: .Home)
            profileSceen.delegate = self
            profileSceen.modalTransitionStyle = .coverVertical
            profileSceen.modalPresentationStyle = .overCurrentContext
            self.present(profileSceen, animated: true, completion: nil)
        case 1://tasks
            if selecteMenu == 1 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 1
            let homeVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabBarVC") as! HomeTabBarVC
            sideMenuViewController?.contentViewController = UINavigationController.init(rootViewController: homeVC)
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 2://profile
            if selecteMenu == 2 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 2
            let nav = UINavigationController(rootViewController:UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC)
            nav.navigationBar.isHidden = true
            sideMenuViewController?.contentViewController = nav
            sideMenuViewController?.hideMenuViewController()
            break

        case 3: //wallet
            if selecteMenu == 3 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 3
            let nav = UINavigationController(rootViewController:UIStoryboard(name: AppStoryboard.ContactUs.rawValue, bundle: nil).instantiateViewController(withIdentifier: "MyWalletNewVC") as! MyWalletNewVC)
            nav.navigationBar.isHidden = true
            sideMenuViewController?.contentViewController = nav
            sideMenuViewController?.hideMenuViewController()
            break
            
            
        case 4: //about us

            if selecteMenu == 4 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 4
            
            let nav = UINavigationController(rootViewController:UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC)
            nav.setNavigationBarHidden(true, animated: false)
            sideMenuViewController?.contentViewController = nav
            webViewType = .aboutus
            sideMenuViewController?.hideMenuViewController()
        case 5: // contact us

            if selecteMenu == 5 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 5
            
            let nav = UINavigationController(rootViewController:UIStoryboard(name: AppStoryboard.ContactUs.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC)
            nav.setNavigationBarHidden(true, animated: false)
            sideMenuViewController?.contentViewController = nav
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 6://FAQ

            if selecteMenu == 6 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 6
            let nav = UINavigationController(rootViewController:UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC)
            nav.setNavigationBarHidden(true, animated: false)
            sideMenuViewController?.contentViewController = nav
            webViewType = .faq
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 7: //privacy policy

            if selecteMenu == 7 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 7
            let nav = UINavigationController(rootViewController:UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "StaticPageVC") as! StaticPageVC)
            nav.setNavigationBarHidden(true, animated: false)
            sideMenuViewController?.contentViewController = nav
            webViewType = .privacy
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 8: //referral

            if selecteMenu == 8 {
                sideMenuViewController?.hideMenuViewController()
                return
            }
            selecteMenu = 8
            let nav = UINavigationController(rootViewController:UIStoryboard(name: AppStoryboard.ContactUs.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ReferralVC") as! ReferralVC)
            nav.navigationBar.isHidden = true
            sideMenuViewController?.contentViewController = nav
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 9: //empty
            break
        case 10: //logout
            sideMenuViewController?.hideMenuViewController()
            self.logOutTapped()
        default:
            break
        }
        tableView.reloadData()
    }
    
}

// MARK:- FUNCTIONS
//====================
extension SideMenuVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        super.addImageInBackground()
        sideMenuTableView.delegate = self
        sideMenuTableView.dataSource = self
        sideMenuTableView.backgroundColor = .clear
        sideMenuTableView.register(UINib.init(nibName: "SideMenuProfileCell", bundle: nil), forCellReuseIdentifier: "SideMenuProfileCellid")
         sideMenuTableView.register(UINib.init(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCellid")
        
    }
    
    private func logOutTapped(){
        
       
        let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "LogOut", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "LogOutMSG", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "YES", comment: ""), style: .default, handler: { action in
           self.logOutAPI()
        }))
        alert.addAction(UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "NO", comment: ""), style: .cancel, handler: { action in
           self.dismiss(animated: true, completion: nil)
        }))
       
        sideMenuViewController?.present(alert, animated: true)
    }
    
    func updateView() {
        guard let user = CommonFunction.getUser() else { return }
        self.user = user
        sideMenuTableView.reloadData()
    }
    

}

// MARK:- Protocol Methods
//========================
extension SideMenuVC: UpdateView {
    func reloadViewsData() {
        self.updateView()
    }    
}

//MARK::- API
extension SideMenuVC{
    func logOutAPI(){
        WebServices.logout(success: { (_) in
            CommonFunction.logout()
        }) { (e) -> (Void) in
            CommonFunction.showToast(msg: e.localizedDescription)
        }
    }
}
