//
//  CommentVC.swift
//  Stalinani
//
//  Created by HariTeju on 11/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import PullToRefreshKit

class CommentVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tableViewDataSource : TableViewCustomDatasource?{
          didSet{
              tableview.delegate = tableViewDataSource
              tableview.dataSource = tableViewDataSource
              tableview.reloadData()
          }
      }
    var count = 0
    var userId = ""
    var user : UserModel?
    var commentIDs: Int?
    var allComments : GetCommentModel?
    var commentsData : [GetCommentResultData]?  = []
    var addcomments : CommentModel?
    @IBOutlet weak var commentText: UITextField!
    @IBOutlet weak var tableview: UITableView!
    var recieveQuestionStr: String?
    var recievenewsIDStr: String?
    
    @IBOutlet weak var commentQuestion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.isHidden = true
        commentQuestion.text = recieveQuestionStr ?? ""
        self.handlePaging()
        getCommentsFromAPI(loader: true)

      //  self.tableview.reloadData()
    }
//    func handlePaging(){
//        let footer = DefaultRefreshFooter.footer()
//        footer.refreshMode = .scroll
//        footer.tintColor = UIColor.gray
//        footer.setText("", mode: .pullToRefresh)
//        footer.setText("", mode: .noMoreData)
//        footer.setText("loading", mode: .refreshing)
//        footer.setText("", mode: .scrollAndTapToRefresh)
//        footer.setText("", mode: .tapToRefresh)
//        tableview.configRefreshFooter(with: footer, container: self) {
//            self.count == -1 ? self.tableview.switchRefreshFooter(to: .normal) :  self.getCommentsFromAPI(loader : false)
//        }
//    }
    
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableview.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableview.switchRefreshFooter(to: .normal) :
                self.getCommentsFromAPI(loader : false)
        }
    }
    
    
    //Tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentCell else{
            fatalError() }
       
        let userIDToken = CommonFunction.getUser()?.userId
        print("ApiKeys.user_id : ", userIDToken ?? "")
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editCommentSelected), for: .touchUpInside)
        if userIDToken == /commentsData?[indexPath.row].userId {
            cell.editBtn.isHidden = false

        } else {
            cell.editBtn.isHidden = true
        }
        cell.selectionStyle = .none
        tableView.separatorStyle = .none
        cell.profileIcon.roundCorners(corners: [.bottomRight, .bottomLeft, .topLeft, .topRight], radius: 20)
        cell.profileIcon.kf.setImage(with: URL(string : /commentsData?[indexPath.row].userImage), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.nameLabel.text = /commentsData?[indexPath.row].fullName
        cell.commentLabel.text = /commentsData?[indexPath.row].userComment
        cell.dateLabel.text = calculateFormatsComment(date: /commentsData?[indexPath.row].submittedTimestamp, formatterReq: "d MMM, yyyy")
        return cell
    }
    @objc func editCommentSelected(sender: UIButton){
        print(sender.tag)
        self.commentText.becomeFirstResponder()
        commentText.text = /commentsData?[sender.tag].userComment
        self.commentIDs = Int(commentsData?[sender.tag].commentId?.description ?? "")
        print("(self.commentIDs)", self.commentIDs ?? "")
        print("emmmm :", /commentsData?[sender.tag].fullName?.description)
        print("emmmm :", /commentsData?[sender.tag].userId?.description)
        print("emmmm :", /commentsData?[sender.tag].commentId?.description)
       // print(String(commentsData?[sender.tag].commentId))
        print(Int(commentsData?[sender.tag].commentId?.description ?? "") ?? "")
        //print("emmmm :", /commentsData?[sender.tag].commentId)

    }
    func calculateFormatsComment(date : String? , formatterReq : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.date(from: /date) ?? Date()
        formatter.dateFormat = formatterReq
        return /formatter.string(from: newDate)
    }
    func setupUI(){
        self.tableview.switchRefreshFooter(to: .normal)
        //self.commentsData?.append(contentsOf : self.allComments?.getCommentResult ?? [])
        for item in self.allComments?.getCommentResult ?? []{
                   self.commentsData?.append(item)
               }
               if count == 0{
                   self.commentsData = self.allComments?.getCommentResult ?? []
               }
        if commentsData?.count == 0 { self.tableview.isHidden = true } else { self.tableview.isHidden = false }
        self.count = self.allComments?.count ?? -1
        self.tableViewDataSource?.items = self.commentsData
        self.tableview.reloadData()
        self.tableview.isHidden = false
       // self.tableview.reloadData()
    }
//    func getCommentsFromAPI(loader : Bool){
//        WebServices.getallComments(count: self.count.description, newsID: recievenewsIDStr ?? ""
//            , success: { [weak self] (json) in
//              //  print("JSOOOON:, ", json)
//                self?.commentIDs = nil
//                self?.allComments = GetCommentModel.init(json: json)
//                //self?.commentsData?.append(contentsOf : self?.allComments?.getCommentResult ?? [])
//                //self?.tableview.reloadData()
//                self?.setupUI()
//        }) {(error) -> (Void) in
//            CommonFunction.showToast(msg: error.localizedDescription)
//        }
//    }
    
    
    func getCommentsFromAPI(loader : Bool){
     WebServices.getallComments(count: self.count.description, newsID: recievenewsIDStr ?? ""
        , success: { [weak self] (json) in
                self?.commentIDs = nil
                self?.allComments = GetCommentModel.init(json: json)
                self?.tableview.isHidden = false
                self?.tableview.switchRefreshFooter(to: .normal)
            for item in self?.allComments?.getCommentResult ?? []{
                    self?.commentsData?.append(item)
                }
                if self?.count == 0{
                    self?.commentsData = self?.allComments?.getCommentResult ?? []
                }
                self?.count = self?.allComments?.next?.toInt() ?? -1
                self?.tableview.reloadData()
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    
    
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendCommentClicked(_ sender: Any) {
        print(commentText.text ?? "")
        //commentText.text = "..."
        if commentIDs == nil {
            if commentText.text == "" || commentText.text == nil {
                CommonFunction.showToast(msg: AlertMessage.commentCheck.localized)
            } else {
                addCommentsToAPI(loader: true)
                commentText.text = ""
            }

        } else {
            if commentText.text == "" || commentText.text == nil {
                CommonFunction.showToast(msg: AlertMessage.commentCheck.localized)
            } else {
                editCommentsToAPI(loader: true, commentID: commentIDs ?? 0)
                commentText.text = ""
            }
        }
        
    }
    func addCommentsToAPI(loader : Bool){
        
        WebServices.addComments(usercomment: commentText.text ?? "", newsID: recievenewsIDStr ?? "", success: { [weak self] (json) in
            self?.commentsData = []
            self?.commentIDs = nil
            self?.addcomments = CommentModel.init(json: json)
           // print(self?.addcomments?.message ?? "")
            self?.count = 0
            self?.getCommentsFromAPI(loader: true)
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    func editCommentsToAPI(loader : Bool, commentID: Int){
        WebServices.editComments(commentid: commentID.description, usercomment: commentText.text ?? "", newsID: recievenewsIDStr ?? "", success: { [weak self] (json) in
            self?.commentsData = []
            self?.addcomments = CommentModel.init(json: json)
            self?.count = 0

          //  print(self?.addcomments?.message ?? "")
            self?.getCommentsFromAPI(loader: true)
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}
