//
//  NotifyCell.swift
//  IPAC
//
//  Created by macOS on 10/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class NotifyCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnSeeMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        bgView.rounded(cornerRadius: 5.0, clip: true)
    }
    
        func populateCell(with data : News) {
            titleLabel.text = data.newsTitle
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            guard let date =  formatter.date(from: data.createdDate ?? "") else { return }
            let displayDate = date.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
            self.dateLabel.text = displayDate
            let dataa = (data.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
            do {
                let attrStr = try NSAttributedString(
                    data: dataa,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                descLabel.attributedText = attrStr
        
               // descLabel.text = descLabel.attributedText?.pref
                
            } catch {
                print_debug(error.localizedDescription)
            }
            
    }
}
