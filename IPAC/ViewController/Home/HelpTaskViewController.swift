//
//  HelpTaskViewController.swift
//  Stalinani
//
//  Created by HariTeju on 28/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable
import EZSwiftExtensions


class HelpTaskViewController: UIViewController {

    @IBOutlet weak var hideView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        hideView.addGestureRecognizer(tap)


        // Do any additional setup after loading the view.
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismissVC(completion: nil)
    }
    @IBAction func fbClicked(_ sender: Any) {
        self.dismissVC(completion: nil)

        let vc =  PlayerViewController2.instantiate(fromAppStoryboard: .Form)
           vc.url = facebookUrl
           vc.desc = ""
           ez.topMostVC?.presentVC(vc)
    }
    
    @IBAction func twitterCliked(_ sender: Any) {
        self.dismissVC(completion: nil)

        let vc =  PlayerViewController2.instantiate(fromAppStoryboard: .Form)
                      vc.url = twitterUrl
                      vc.desc = ""
                      ez.topMostVC?.presentVC(vc)
    }
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
