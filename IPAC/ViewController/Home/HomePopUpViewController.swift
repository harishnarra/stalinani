//
//  HomePopUpViewController.swift
//  Stalinani
//
//  Created by HariTeju on 20/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class HomePopUpViewController: UIViewController {

    @IBOutlet weak var hiddenShowView: AnimatableView!
    @IBOutlet weak var daysImage: UIImageView!
    var recieveDay: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if recieveDay == 1 {
            daysImage.image = UIImage.init(named: "day1")
        } else if recieveDay == 2 {
            daysImage.image = UIImage.init(named: "day2")
        } else {
            daysImage.image = UIImage.init(named: "day3")
        }
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        //self.nav
        self.dismiss(animated: true, completion: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
