//
//  HomenewsTableviewCell.swift
//  IPAC
//
//  Created by HariTeju on 05/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import IBAnimatable

class HomenewsTableviewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
  //  var news = [News]()
   // var news2 = [News]()
    
    var checkArray : NSMutableArray = []

    

    var news: [News] = []
    var newspost: [News] = []
    var senderQuestion : String?
    var senderID : String?

    var sendTnewaskID: String?
    
    let newcell = HomeNewsCollectionViewCell()
    var statePreservationArray : [Bool] = []
    var checkLike : Bool?



    @IBOutlet weak var pagesss: UIPageControl!
    
    //var newsarray = [News]
 
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.getHomeNewss(count: 0, loader: false)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        self.pagesss.isHidden = true

        collectionView.layoutIfNeeded()
        

        
        collectionView.reloadData()
        
//        if self.newspost.count == 0 {
//            HomenewsTableviewCell.tableFooterView = UIView()
//
//        }
        // Initialization code
    }
    
    @objc func likeViewTapTapped(_ sender: UIGestureRecognizer) {
          print("Right button is tapped")
          let tag = sender.view!.tag
          print(tag)
          
        self.sendTnewaskID = self.newspost[tag].newsId
          
//
          let vcs = CompletedHomeVC.instantiate(fromAppStoryboard: .Home)
          vcs.recieveTaskID = self.sendTnewaskID

        self.parentViewController?.presentVC(vcs)

//
//          self.presentViewController(vcs, animated: true, completion: nil)

        
          //vc.recieveTaskID = self.sendTaskID
           // self.navigationController?.present(vc, animated: true, completion: nil)
      }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("self.newspost.count", self.newspost.count)
                return self.newspost.count

    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pagesss.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeNewsCollectionViewCell", for: indexPath) as? HomeNewsCollectionViewCell else{
            fatalError() }
        
        cell.reactionBtn1.tag = indexPath.row
        cell.reactionBtn2.tag = indexPath.row
        cell.reactionBtn3.tag = indexPath.row
        
        
        let theIndexpath = indexPath.row.description
        
        if (checkArray.contains(theIndexpath))
        {
            UIView.transition(with: cell.reactionView, duration: 0.4,
                options: .transitionCrossDissolve,
                animations: {
               cell.reactionView.isHidden = false
            })
          
        }
        else
        {
            UIView.transition(with: cell.reactionView, duration: 0.4,
                options: .transitionCrossDissolve,
                animations: {
               cell.reactionView.isHidden = true
            })
        }
        
       // NSString *theIndexpath = [NSString stringWithFormat:@"%ld", (long)indexPath.row];

        //  cell.reactionView.isHidden = self.statePreservationArray[indexPath.row]
        
        cell.reactionBtn1.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        cell.reactionBtn2.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        cell.reactionBtn3.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        
        
        
        if indexPath.row == 0 {
            cell.backArrow.isHidden = true
        } else {
            cell.backArrow.isHidden = false
        }
        
        if indexPath.row == self.newspost.count {
            cell.frontArrow.isHidden = true
        } else {
            cell.frontArrow.isHidden = false
        }
        cell.likeView.tag = indexPath.row
        let likeViewTap = UITapGestureRecognizer(target: self, action: #selector(likeViewTapTapped(_:)))
        cell.likeView.addGestureRecognizer(likeViewTap)
        
        cell.commentBtn.tag = indexPath.row
        cell.commentBtn.addTarget(self, action: #selector(commentSelected), for: .touchUpInside)
        
        cell.seeMoreBtn.tag = indexPath.row
        cell.seeMoreBtn.isUserInteractionEnabled = true
        cell.seeMoreBtn.addTarget(self, action: #selector(seemoreClicked), for: .touchUpInside)
        
        cell.reactionButton.tag = indexPath.row
        cell.reactionButton.addTarget(self, action: #selector(reactionButtonSelected), for: .touchUpInside)
        
        cell.backArrow.tag = indexPath.row
        cell.backArrow.addTarget(self, action: #selector(backArrowSelected), for: .touchUpInside)
        
        cell.frontArrow.tag = indexPath.row
        cell.frontArrow.addTarget(self, action: #selector(frontArrowSelected), for: .touchUpInside)
        
        cell.titleLabel.text = self.newspost[indexPath.row].newsTitle ?? ""
        senderQuestion = self.newspost[indexPath.row].newsTitle ?? ""
        senderID = self.newspost[indexPath.row].newsId ?? ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date =  formatter.date(from: self.newspost[indexPath.row].createdDate ?? "") 
        let displayDate = date?.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
        cell.timeLabel.text = displayDate
        let data = (self.newspost[indexPath.row].newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        cell.descriptionLabel.text = attrStr?.string
        cell.likeView.isHidden = true
        cell.likeImageOne.isHidden = true
        cell.likeImageTwo.isHidden = true
        cell.likeOthersLabel.isHidden = true
        if self.newspost[indexPath.row].is_like == "dislike" {
            if self.newspost[indexPath.row].clicks_user_list?.count == 0 {
            } else if self.newspost[indexPath.row].clicks_user_list?.count == 2 &&  self.newspost[indexPath.row].clicks_user_count?.count ?? 0 >= 2 {
                print("and ", self.newspost[indexPath.row].clicks_user_count ?? "", " others")
                cell.likeView.isHidden = false
                cell.likeImageOne.isHidden = false
                cell.likeImageTwo.isHidden = false
                cell.likeOthersLabel.isHidden = false
                let labelCount:Int? = self.newspost[indexPath.row].clicks_user_count?.toInt()
                let count2 = (labelCount ?? 2) - 2
                cell.likeOthersLabel.text = "and \(count2.description) others"
                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
            }
            
            else  if self.newspost[indexPath.row].clicks_user_list?.count == 1 {
                cell.likeView.isHidden = false
                cell.likeImageOne.isHidden = false
                cell.likeImageTwo.isHidden = true
                cell.likeOthersLabel.isHidden = true
                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
            } else  if self.newspost[indexPath.row].clicks_user_list?.count == 2 {
                cell.likeView.isHidden = false
                cell.likeImageOne.isHidden = false
                cell.likeImageTwo.isHidden = false
                cell.likeOthersLabel.isHidden = true
                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
            }
            
        } else {
            let liketype = self.newspost[indexPath.row].like_type
            print(liketype ?? "")
            if liketype == "clap" {
                cell.reactionButton.setImage(UIImage(named: "Clap"), for: .normal)
            } else if liketype == "heart" {
                cell.reactionButton.setImage(UIImage(named: "FilledHeart"), for: .normal)
            } else if liketype == "fist" {
                cell.reactionButton.setImage(UIImage(named: "Fist"), for: .normal)
            } else {
                cell.reactionButton.setImage(UIImage(named: "EmptyHeart"), for: .normal)
            }
            if self.newspost[indexPath.row].clicks_user_list?.count == 0 {
            } else if self.newspost[indexPath.row].clicks_user_list?.count == 2 &&  self.newspost[indexPath.row].clicks_user_count?.count ?? 0 >= 2 {
                print("and ", self.newspost[indexPath.row].clicks_user_count ?? "", " others")
                cell.likeView.isHidden = false
                cell.likeImageOne.isHidden = false
                cell.likeImageTwo.isHidden = false
                cell.likeOthersLabel.isHidden = false
                let labelCount:Int? = self.newspost[indexPath.row].clicks_user_count?.toInt()
                let count2 = (labelCount ?? 2) - 2
                cell.likeOthersLabel.text = "and \(count2.description) others"
                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
            } else  if self.newspost[indexPath.row].clicks_user_list?.count == 1 {
                cell.likeView.isHidden = false
                cell.likeImageOne.isHidden = false
                cell.likeImageTwo.isHidden = true
                cell.likeOthersLabel.isHidden = true
                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
            } else  if self.newspost[indexPath.row].clicks_user_list?.count == 2 {
                cell.likeView.isHidden = false
                cell.likeImageOne.isHidden = false
                cell.likeImageTwo.isHidden = false
                cell.likeOthersLabel.isHidden = true
                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
            }
        }
         if newspost[indexPath.row].mediaSet?.count == 0 {
            cell.imageOrVideo.image = UIImage.init(named: "placeHolder")
            cell.playButton.setImage(nil, for: .normal)
               } else {
                 //  cell.imageOrVideo.image = UIImage.init(named: #imageLiteral(resourceName: "placeHolder"))
                  // cell.imageOrVideo.kf.setImage(with: URL(string: mediaSet?.mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
                   let mediaSet = newspost[indexPath.row].mediaSet?[0]
                   if mediaSet?.mediaType == "2" {
                       cell.playButton.setImage(#imageLiteral(resourceName: "playIcon"), for: .normal)
                       cell.imageOrVideo.kf.setImage(with: URL(string: mediaSet?.mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
                   }else{
                       cell.imageOrVideo.kf.setImage(with: URL(string: mediaSet?.mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
                       cell.playButton.setImage(nil, for: .normal)
                      // btnPlay.setImage(nil, for: .normal)
                   }
               }
        return cell
    }
    
    @objc func btnClick(_ sender : UIButton)
        
    {
        
//        let myob = sender.tag.description
//        if (checkArray.contains(myob)) {
//            checkArray.remove(myob)
//           }
//           else
//           {
//            checkArray.add(myob)
//           }
//
//
//
//        collectionView.reloadData()
//
        
        //Next update
        
        
        
//
//        let selectedRow = sender.tag
//       // statePreservationArray[selectedRow] = !statePreservationArray[selectedRow]
//        let cell = collectionView.cellForItem(at: IndexPath(row: selectedRow, section: 0)) as! HomeNewsCollectionViewCell
//        cell.reactionView.isHidden = false
        
        
        
//        UIView.animate(withDuration: 0.4, animations: {
//             cell.reactionView.transform = CGAffineTransform(translationX: -50, y: 0)
//        }) { (flag) in
//            cell.reactionView.transform = CGAffineTransform.identity
//        }
       
        //login to make all view to hidden except selected view
//        for i in 0..<statePreservationArray.count
//        {
//            if i == selectedRow
//            {
//                continue
//            }
//            else{
//                statePreservationArray[i] = true//making all view hidden to false except selected row
//            }
//        }
        //collectionView.reloadData()

    }
    
    
    @objc func commentSelected(sender: UIButton){
        print(sender.tag)
        let bannerSceen = CommentVC.instantiate(fromAppStoryboard: .Home)
        bannerSceen.recievenewsIDStr = senderID ?? ""
        bannerSceen.recieveQuestionStr = senderQuestion ?? ""; self.parentViewController?.pushVC(bannerSceen)
    }
    
    @objc func seemoreClicked(sender: UIButton){
           print(sender.tag)
         if self.newspost[sender.tag].mediaSet?.count == 0 {
                    
                    let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
                    bannerSceen.news = self.newspost[sender.tag]
                    self.parentViewController?.pushVC(bannerSceen)
                } else {
                    
                
                
            //    print(/self.newspost[indexPath.row].mediaSet?[0].mediaType)
                
                         //   self.navigationController?.pushViewController(bannerSceen, animated: true)
                if /self.newspost[sender.tag].mediaSet?[0].mediaType == "2" {
        //            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
        //            vc.url = /self.newspost[indexPath.row].mediaSet?[0].mediaUrl
        //            vc.desc = /self.newspost[indexPath.row].newsTitle
        //            ez.topMostVC?.presentVC(vc)
                    let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
                    bannerSceen.news = self.newspost[sender.tag]
                    self.parentViewController?.pushVC(bannerSceen)
                } else {
        //            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
        //            previewVC.previewImage = /self.newspost[indexPath.row].mediaSet?[0].mediaUrl
        //            previewVC.tittle = self.newspost[indexPath.row].newsTitle ?? ""
        //            self.parentViewController?.pushVC(previewVC)
                    let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
                    bannerSceen.news = self.newspost[sender.tag]
                    self.parentViewController?.pushVC(bannerSceen)
                }
                }
          
       }
    
    @objc func reactionButtonSelected(sender: UIButton){
//        print(sender.tag)
//        if self.newspost[sender.tag].like_type == nil {
//            checkLike = false
//        } else {
//            checkLike = true
//        }
//        if checkLike == true {
//            print("checkLike True")
//            dislikenewsFromAPI(newsID: senderID?.description ?? "")
//        } else {
//            print("checkLike false")
//            let myob = sender.tag.description
//            if (checkArray.contains(myob)) {
//                checkArray.remove(myob)
//            }
//            else
//            {
//                checkArray.add(myob)
//            }
//            collectionView.reloadData()
//        }
    }
    
    
     @objc func backArrowSelected(sender: UIButton){
        
        print("work")

        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
               if nextItem.row < self.newspost.count {
            self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)

        }
        
        
     }
    
    @objc func frontArrowSelected(sender: UIButton){
        
           print("workf")
        
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        if nextItem.row < self.newspost.count && nextItem.row >= 0{
            self.collectionView.scrollToItem(at: nextItem, at: .right, animated: true)

        }

        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(self.newspost[indexPath.row])
        
       
    }
//    func collectionView(collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAtIndexPath indexPath:  NSIndexPath) -> CGSize {
//
//        return CGSize(width: 351, height:341)
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func getHomeNewss(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            }else{
                self?.news.append(contentsOf: news)
            }
            if self?.news.count == 0 {
                
            } else {
                for (index, element) in self!.news.enumerated() {
                    print("Item \(index): \(element)")
                    print(self?.news[index].newsCategory ?? "")
                    if (self?.news[index].newsCategory)!.rawValue == "post" || (self?.news[index].newsCategory)!.rawValue == "article"{
                        print("self.minnews", self?.news[index] ?? "")
                        self?.newspost.append((self?.news[index])!)
                    }
                }
            }
            self?.pagesss.numberOfPages = self?.newspost.count ?? 0
            self?.collectionView.reloadData()
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
            return
        }
    }
    
    func likenewsFromAPI(likeType: String, newsID: String) {
        WebServices.likenews(liketype: likeType, newsID: newsID, success: { [weak self] (json) in
            print(json)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            return
        }
    }
    func dislikenewsFromAPI(newsID: String) {
           WebServices.dislikenews(newsID: newsID, success: { [weak self] (json) in
               print(json)
           }) { (error) -> (Void) in
               CommonFunction.showToast(msg: error.localizedDescription)
               return
           }
       }

}
