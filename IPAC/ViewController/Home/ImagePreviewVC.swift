//
//  ImagePreviewVC.swift
//  IPAC
//
//  Created by macOS on 10/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ImagePreviewVC: UIViewController {
    
    // MARK:- Properties
    //====================
    var previewImage = ""
    var tittle = ""
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var bottomTitleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
       self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- IBACTIONS
    //====================
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
//    tempView.addSubview(view)
//    if let url = URL(string: mediaURls[index]), mediaTypes[index] == "1" {
//    print(url)
//    ImageDownloader.default.authenticationChallengeResponder = self
//    carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
//    }
}

// MARK:- FUNCTIONS
//====================
extension ImagePreviewVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        if let url = URL(string: previewImage) {
            previewImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
        }
        scrollView.delegate = self
        self.bottomTitleLabel.text = tittle
        previewImageView.contentMode = .scaleAspectFit
        scrollView.maximumZoomScale = 2
       // scrollView.isScrollEnabled = false
        //previewImageView.imageFromURl(previewImage, placeHolderImage: nil, loader: false, completion: nil)
    }
}


extension ImagePreviewVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return previewImageView
    }
}
