//
//  Empty3Cell.swift
//  IPAC
//
//  Created by HariTeju on 26/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class Empty3Cell: UITableViewCell {

    @IBOutlet weak var namesLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        namesLab.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Gallery", comment: "")

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
