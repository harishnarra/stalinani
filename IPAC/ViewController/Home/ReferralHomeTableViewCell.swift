//
//  ReferralHomeTableViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 29/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ReferralHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var referralMain: UILabel!
    @IBOutlet weak var referallCodeLabel: UILabel!
    @IBOutlet weak var youReferalHead: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var yourreferalBottom: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
