//
//  PollTableViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 29/06/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionPoll: UILabel!
    @IBOutlet weak var option1: UILabel!
    @IBOutlet weak var optionButton1: UIButton!
    @IBOutlet weak var option2: UILabel!
    @IBOutlet weak var optionButton2: UIButton!
    @IBOutlet weak var optionButton3: UIButton!
    @IBOutlet weak var option3: UILabel!
    @IBOutlet weak var optionButton4: UIButton!
    @IBOutlet weak var option4: UILabel!
    @IBOutlet weak var submitBtnPoll: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
