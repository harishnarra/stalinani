//
//  BannerTableViewCell.swift
//  IPAC
//
//  Created by HariTeju on 05/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class BannerTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    //  var news = [News]()
    // var news2 = [News]()
    
    var news: [News] = []
    var newspost: [News] = []
    
    
    
    @IBOutlet weak var pages: UIPageControl!
    //var newsarray = [News]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      //  self.getHomeNewss(count: 0, loader: false)
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.layoutIfNeeded()
        
        
        
        //
        //        for (index, name) in self.news.enumerated() {
        //
        //        }
        
        
        //        for i in 0..< self.news.count {
        //            if self.news[i].newsCategory == "" {
        //
        //            }
        //        }
        
        
        //        let cellWidth : CGFloat = collectionView.frame.size.width / 4.0
        //        let cellheight : CGFloat = collectionView.frame.size.height - 2.0
        //        let cellSize = CGSize(width: cellWidth , height:cellheight)
        //
        //        let layout = UICollectionViewFlowLayout()
        //        layout.scrollDirection = .vertical //.horizontal
        //        layout.itemSize = cellSize
        //        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        //        layout.minimumLineSpacing = 1.0
        //        layout.minimumInteritemSpacing = 1.0
        //        collectionView.setCollectionViewLayout(layout, animated: true)
        
        collectionView.reloadData()
        
        //        if self.newspost.count == 0 {
        //            HomenewsTableviewCell.tableFooterView = UIView()
        //
        //        }
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("self.newspost.countsss", self.newspost.count)
        //return 2
        return self.newspost.count
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pages.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as? BannerCollectionViewCell else{
            fatalError() }
        
        cell.displayImage.kf.setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))

        
       // cell.displayImage.sd_setImage(with: URL(string: mediaArr[indexPath.row].postMedia!), placeholderImage: UIImage(named: "dummyImage"))
      // cell.displayImage.sd_setImage(with: URL(string: self.newspost[indexPath.row].mediaSet?[0].mediaUrl ?? ""), placeholderImage: UIImage(named: ""))


//        cell.titleLabel.text = self.newspost[indexPath.row].newsTitle ?? ""
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date =  formatter.date(from: self.newspost[indexPath.row].createdDate ?? "")
//        let displayDate = date?.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
//        cell.timeLabel.text = displayDate
//
//        let data = (self.newspost[indexPath.row].newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
//        let attrStr = try? NSAttributedString( // do catch
//            data: data,
//            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
//            documentAttributes: nil)
//        cell.descriptionLabel.attributedText = attrStr
//
        
        
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //   self.navigationController?.pushViewController(bannerSceen, animated: true)
        if /self.newspost[indexPath.row].mediaSet?[0].mediaType == "2" {
            //            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            //            vc.url = /self.newspost[indexPath.row].mediaSet?[0].mediaUrl
            //            vc.desc = /self.newspost[indexPath.row].newsTitle
            //            ez.topMostVC?.presentVC(vc)
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.newspost[indexPath.row]
            self.parentViewController?.pushVC(bannerSceen)
        } else {
            //            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
            //            previewVC.previewImage = /self.newspost[indexPath.row].mediaSet?[0].mediaUrl
            //            previewVC.tittle = self.newspost[indexPath.row].newsTitle ?? ""
            //            self.parentViewController?.pushVC(previewVC)
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.newspost[indexPath.row]
            self.parentViewController?.pushVC(bannerSceen)
        }
    }
    //    func collectionView(collectionView: UICollectionView,
    //                        layout collectionViewLayout: UICollectionViewLayout,
    //                        sizeForItemAtIndexPath indexPath:  NSIndexPath) -> CGSize {
    //
    //        return CGSize(width: 351, height:341)
    //    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func getHomeNewss(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            } else {
                self?.news.append(contentsOf: news)
            }
            print("self.news.count", self?.news.count ?? 0)
            if self?.news.count == 0 {
            } else {
                for (index, element) in self!.news.enumerated() {
                    print("Item \(index): \(element)")
                    print(self?.news[index].newsCategory ?? "")
                    if (self?.news[index].newsCategory)!.rawValue == "banner"{
                        print("self.minnews", self?.news[index] ?? "")
                        self?.newspost.append((self?.news[index])!)
                    }
                }
            }
            self?.pages.numberOfPages = self?.newspost.count ?? 0
            self?.collectionView.reloadData()
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
            //            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            //            self.lblNoData.isHidden = !(self.news.isEmpty )
            return
        }
    }
    
}
