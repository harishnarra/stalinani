//
//  HomeNewsCollectionViewCell.swift
//  IPAC
//
//  Created by HariTeju on 05/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import iCarousel
import IBAnimatable

class HomeNewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
   // @IBOutlet weak var viewForCarousel: iCarousel!
    @IBOutlet weak var likeOthersLabel: UILabel!
    @IBOutlet weak var likeImageTwo: UIImageView!
    @IBOutlet weak var likeImageOne: UIImageView!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var imageOrVideo: UIImageView!
   // @IBOutlet weak var carouselViewHeight: NSLayoutConstraint!
    @IBOutlet weak var reactionBtn3: UIButton!
    @IBOutlet weak var reactionBtn2: UIButton!
    @IBOutlet weak var reactionBtn1: UIButton!
    @IBOutlet weak var reactionView: UIView!
    @IBOutlet weak var reactionButton: UIButton!
    @IBOutlet weak var frontArrow: AnimatableButton!
    @IBOutlet weak var backArrow: AnimatableButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var seeMoreBtn: UIButton!
    
    
    
    override func draw(_ rect: CGRect) {
           super.draw(rect)
          
         seeMoreBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "seemore", comment: ""), for: .normal)
       }
}
