//
//  HomeTabBarVC.swift
//  IPAC
//
//  Created by macOS on 20/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TwitterCore
import TwitterKit

class HomeTabBarVC: UITabBarController {

    
    //MARK:- IBOutlets
    @IBOutlet weak var customTabBar: UITabBar!
    
    //MARK:- Properties
    var selectIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       // onViewWillAppear()
    }
}

//MARK:-
//MARK:- Private Functions
extension HomeTabBarVC {
   
    
    func initialSetup() {
        
        self.navigationController?.viewControllers = [self]
        let homeVC             = HomeController.instantiate(fromAppStoryboard: .Home)
        let taskVC              = TaskVC.instantiate(fromAppStoryboard: .Home)
        let dashboardVC            = DashboardVC.instantiate(fromAppStoryboard: .Dashboard)
        self.viewControllers = [homeVC, taskVC, dashboardVC]
        for item in self.customTabBar.items! {
            switch item {
            case (self.customTabBar.items?[0])!:
                item.title          = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Home", comment: "")
                //unselectedItemTintColor = AppColors.appThemeColor

                item.image          = #imageLiteral(resourceName: "icTasksNews")
                item.selectedImage  = #imageLiteral(resourceName: "icTasksSelectnews")
                
                
            case (self.customTabBar.items?[1])!:
                item.title          = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Tasks", comment: "")
                item.image          = #imageLiteral(resourceName: "icTasksTasks")
                item.selectedImage  = #imageLiteral(resourceName: "icTasksSelecttasks")
            case (self.customTabBar.items?[2])!:
                item.title          = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Dashboard", comment: "")
                item.image          = #imageLiteral(resourceName: "icTasksDashboard")
                item.selectedImage  = #imageLiteral(resourceName: "icTasksSelectdashboard")
                
            default:
                break
            }
            
           //
//            if #available(iOS 13.0, *) {
//                item.selectedImage = item.selectedImage?.withTintColor(.red, renderingMode: .alwaysTemplate)
//            } else {
//                // Fallback on earlier versions
                item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            //}
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        
        self.customTabBar.unselectedItemTintColor = UIColor.lightGray
      //  self.customTabBar.selectedItemTintColor = AppColors.appThemeColor

        self.selectIndex = DeviceDetail.pushType == 2 ? 1 : 0
        DeviceDetail.pushType = 0
        self.selectedIndex = self.selectIndex
        self.customTabBar.backgroundColor = AppColors.whiteColor
        
        //getTwitterKey()
    }
    func getTwitterKey(){
        WebServices.twitter(success: { [weak self] (json) in
            let twitterObj = json[ApiKeys.result].dictionaryValue
            let twitter_consumer_key = twitterObj["twitter_consumer_key"]?.stringValue
            let twitter_secret_key = twitterObj["twitter_secret_key"]?.stringValue
            print_debug(twitter_consumer_key)
            print_debug(twitter_secret_key)

//            TWTRTwitter.sharedInstance().start(withConsumerKey: SocialKeys.Twitter.K_CONSUMER_KEY, consumerSecret: SocialKeys.Twitter.K_CONSUMER_SECRET)

        }) { (_) -> (Void) in
        }
    }
    
}
