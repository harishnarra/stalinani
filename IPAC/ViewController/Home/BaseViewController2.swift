//
//  BaseViewController2.swift
//  IPAC
//
//  Created by HariTeju on 11/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import MapKit


class BaseViewController2: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var tableViewDataSource : TableViewCustomDatasource?{
        didSet{
            tableView.delegate = tableViewDataSource
            tableView.dataSource = tableViewDataSource
            tableView.reloadData()
        }
    }
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
    }
    
    func onDidLayoutSubviews(){
        guard let headerView = tableView?.tableHeaderView else {return}
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView?.tableHeaderView = headerView
            tableView?.layoutIfNeeded()
        }
        guard let footerView = tableView?.tableFooterView else {return}
        let sizeF = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        if footerView.frame.size.height != sizeF.height {
            footerView.frame.size.height = sizeF.height
            tableView?.tableFooterView = footerView
            tableView?.layoutIfNeeded()
        }
    }
    
    //MARK::- FUNCTIONS
    func callToANumber(_ number : String = "9876543210") {
        if let url = URL(string: "tel://\(number)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func openMap(lat: String, lng: String, name: String?, openWith: String?) {
        let destLat: Float = Float(lat) ?? 0.0
        let destLong: Float = Float(lng) ?? 0.0
        let destLocation = CLLocationCoordinate2DMake(CLLocationDegrees(destLat), CLLocationDegrees(destLong))
        let placeMark = MKPlacemark(coordinate: destLocation, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placeMark)
        mapItem.name = /name
        mapItem.openInMaps(launchOptions: nil)
    }
    func getDistanceBetweenLatLongs(lat : Double , long : Double , latitude: Double , longitude: Double ) -> String {
        let userCoordinates = CLLocation(latitude: CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude)) //current lat long
        return String(format: "%.02f" ,userCoordinates.distance(from: CLLocation(latitude: Double(lat) , longitude: Double(long))))
    }
    
}
