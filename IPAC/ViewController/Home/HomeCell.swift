//
//  HomeCell.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVKit
import iCarousel
import Kingfisher
import AVFoundation
import EZSwiftExtensions

class HomeCell: UITableViewCell {
    
    //MARK :- Variables
    var news : News?
//

    //MARK :- ibOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var viewForCarousel: iCarousel!
    @IBOutlet weak var carouselViewHeight: NSLayoutConstraint!
    @IBOutlet weak var seeMoreBtn: UIButton!
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        seeMoreBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "seemore", comment: ""), for: .normal)

        viewForCarousel.dataSource = self
        viewForCarousel.delegate = self
        viewForCarousel.reloadData()
        viewForCarousel.type = .invertedTimeMachine
        viewForCarousel.setCornerRadius(radius: 5.0)
        self.backView.rounded(cornerRadius: 5.0, clip: true)
    }
    
    //MARK::- FUNCTIONS
    func configureCellWith(_ news: News, parent: UIViewController? = nil) {
        self.news = news
        self.titleLabel.text = news.newsTitle
        self.descriptionLabel.attributedText = NSMutableAttributedString(string: news.newsDescription ?? "")
        let data = (news.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        descriptionLabel.attributedText = attrStr
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date =  formatter.date(from: news.createdDate ?? "") else { return }
        let displayDate = date.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
        self.timeLabel.text = displayDate
        self.carouselViewHeight.constant = 200
        guard let arr = self.news?.mediaSet else {
            self.carouselViewHeight.constant = 0
            return
        }
        if arr.count == 0{
            self.carouselViewHeight.constant = 0
        }
        
        viewForCarousel.reloadData()
    }
    
}


//MARK::- ICAROUSEL DELEGATE
extension HomeCell : iCarouselDelegate,iCarouselDataSource{
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.news?.mediaSet?.count ?? 0
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var tempView: UIView
        tempView = UIView.init(frame: CGRect(x:0 , y:0, width:self.frame.width - 10,height:210))
        let carouselImageview = UIImageView()
        carouselImageview.frame = CGRect(x: 25, y: 0, width: tempView.bounds.width - 60, height: tempView.bounds.height - 20)
        carouselImageview.contentMode = .scaleAspectFill
        carouselImageview.clipsToBounds = true
        carouselImageview.setCornerRadius(radius: 5.0)
        tempView.addSubview(carouselImageview)
        if let url = URL(string: /self.news?.mediaSet?[index].mediaUrl), /self.news?.mediaSet?[index].mediaType == "1" {
            ImageDownloader.default.authenticationChallengeResponder = self
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
        } else if let url = URL(string: /self.news?.mediaSet?[index].mediaThumb), /self.news?.mediaSet?[index].mediaType == "2" {
            let image = UIImageView()
            image.frame = CGRect(origin: carouselImageview.center, size: CGSize(width: 35, height: 35))
            image.center = carouselImageview.center
            image.image = #imageLiteral(resourceName: "playIcon")
            image.contentMode = .center
            image.setCornerRadius(radius: 5.0)
            tempView.addSubview(image)
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
        }
        else {
            carouselImageview.image = UIImage(named: "videoPlayer")
        }
        // layoutIfNeeded()
        return tempView
        
    }
    
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .fadeMin)
        {
            return 0.1
        } else if (option == .fadeMinAlpha)
        {
            return 0.0
        } else if (option == .fadeMax)
        {
            return 0.3;
        }
        else if option == .showBackfaces {
            return 2
        }
        else if option == .wrap {
            return 1
        }
        return value;
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if /self.news?.mediaSet?[index].mediaType == "2" {
            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            vc.url = /self.news?.mediaSet?[index].mediaUrl
            vc.desc = /self.news?.newsTitle
            ez.topMostVC?.presentVC(vc)
        } else {
            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
            previewVC.previewImage = /self.news?.mediaSet?[index].mediaUrl
            previewVC.tittle = news?.newsTitle ?? ""
            self.parentViewController?.pushVC(previewVC)
        }
    }
}

