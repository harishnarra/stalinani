//
//  BannerCollectionViewCell.swift
//  IPAC
//
//  Created by HariTeju on 05/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var displayImage: UIImageView!
}
