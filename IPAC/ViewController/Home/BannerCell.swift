//
//  BannerCell.swift
//  IPAC
//
//  Created by macOS on 10/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Kingfisher
import EZSwiftExtensions

class BannerCell: UITableViewCell {

    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var conatinerView: UIView!
    @IBOutlet weak var heightContainer: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func populateCell(with data: News) {
        guard let mediaSet = data.mediaSet else { return }
        bannerImage.kf.setImage(with: URL(string: mediaSet[0].mediaUrl ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
        //heightContainer.constant = (((Int(/data.banner_height) ?? 0)  ).toCGFloat * ez.screenHeight ) / 100.0
        
    }
    
}
