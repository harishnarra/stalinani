//
//  EmptyCell1.swift
//  IPAC
//
//  Created by HariTeju on 26/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class EmptyCell1: UITableViewCell {

    @IBOutlet weak var nameLabell: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabell.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Form", comment: "")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
