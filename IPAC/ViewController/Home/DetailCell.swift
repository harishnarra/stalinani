//
//  DetailCell.swift
//  IPAC
//
//  Created by Appinventiv on 06/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import iCarousel
import Kingfisher
import AVFoundation
import AVKit
import EZSwiftExtensions


class DetailCell: UITableViewCell {    
    
    var news : News?
    var parentVC: UIViewController?
    var mediaURLs : [String] = []
    var mediaTypes: [String] = []
    var mediaThumbnail = [String]()
    var timer : Timer?  = Timer()
    
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var carouselViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var newsBodyLabel: UILabel!
    
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var btnReadMore: GradientButton!
    @IBOutlet weak var heightReadMore: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        timer?.invalidate()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setParent(parent : UIViewController?,news : News){
        self.parentVC = parent
        self.news = news
        btnReadMore.isHidden = /news.url == ""
        heightReadMore.constant = /news.url == "" ? 0 : 32

        carouselView.dataSource = self
        carouselView.delegate = self
        carouselView.isPagingEnabled = true
        //carouselView.autoscroll = -0.3
        self.populateData()
        carouselView.clipsToBounds = true
        carouselView.type = .invertedTimeMachine        
        guard let mediaSet = self.news?.mediaSet else {
            carouselViewHeight.constant = 0
            return
        }
        if mediaSet.count == 0{
            carouselViewHeight.constant = 0
            return
        }
        self.mediaURLs = []
        self.mediaTypes = []
        self.mediaThumbnail = []
        _ = mediaSet.map({ (media) in
            self.mediaTypes.append(media.mediaType ?? "")
            self.mediaURLs.append(media.mediaUrl ?? "")
            self.mediaThumbnail.append(media.mediaThumb ?? "")
        })
        
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerOn), userInfo: nil, repeats: true)
 
        carouselView.reloadData()
    }
    
    @objc func timerOn(){
        
        self.carouselView.scrollToItem(at: (self.carouselView.currentItemIndex + 1) % self.mediaThumbnail.count , animated: true)

    }
    
    func populateData()
    {
        self.newsTitleLabel.text = self.news?.newsTitle
        self.timeLabel.text = self.news?.createdDate
        let data = (self.news?.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        newsBodyLabel.attributedText = attrStr
    }
    
}

extension DetailCell:iCarouselDelegate,iCarouselDataSource{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return mediaTypes.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        // var tempView = UIView.init(frame: carouselView.frame)
        let tempView = UIView(frame: CGRect(origin: carouselView.frame.origin, size: CGSize(width: carouselView.frame.width - 0, height: carouselView.frame.height)))
        
        let imageFrame = CGRect(x: 25, y: 0, width: tempView.bounds.width - 60, height: tempView.bounds.height - 20)
        let carouselImageview = UIImageView()
        carouselImageview.frame = imageFrame
        carouselImageview.contentMode = .scaleAspectFill
        carouselImageview.clipsToBounds = true
        carouselImageview.rounded(cornerRadius: 2.0, clip: true)
        tempView.addSubview(carouselImageview)
        if let url = URL(string: mediaURLs[index]), mediaTypes[index] == "1" {
            ImageDownloader.default.authenticationChallengeResponder = self
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
        } else if let url = URL(string: mediaThumbnail[index]), mediaTypes[index] == "2" {
            let image = UIImageView()
            image.frame = CGRect(origin: carouselImageview.center, size: CGSize(width: 35, height: 35))
            image.center = carouselImageview.center
            image.image = #imageLiteral(resourceName: "playIcon")
            image.contentMode = .center
            tempView.addSubview(image)
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
        }
        else {
            carouselImageview.image = UIImage(named: "videoPlayer")
        }
        return tempView
        
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .fadeMin)
        {
            return 0.1
        } else if (option == .fadeMinAlpha)
        {
            return 0.0
        } else if (option == .fadeMax)
        {
            return 0.3;
        }
        else if option == .showBackfaces {
            return 2
        }
        else if option == .wrap {
            return 1
        }
        return value;
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if mediaTypes[index] == "2" {
            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            vc.url = /mediaURLs[index]
            vc.desc = /self.news?.newsTitle
            ez.topMostVC?.presentVC(vc)
            //            let videoURL = URL(string: mediaURLs[index])
            //            let player = AVPlayer(url: videoURL!)
            //            let playerViewController = AVPlayerViewController()
            //            playerViewController.player = player
            //            parentVC?.present(playerViewController, animated: true) {
            //                playerViewController.player!.play()
            //            }
        } else {
            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
            previewVC.previewImage = mediaURLs[index]
            previewVC.tittle = news?.newsTitle ?? ""
            parentVC?.navigationController?.pushViewController(previewVC, animated: true)
        }
    }
}
