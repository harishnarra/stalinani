//
//  NewsDetailVC.swift
//  IPAC
//
//  Created by Appinventiv on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class NewsDetailVC: BaseViewController {
    
    // MARK:- VARIABLES
    //====================
    var news : News?
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var detailTableView: UITableView!
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK:- IBACTIONS
    //====================
    @IBAction func backButtonTapped(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
    
}


// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension NewsDetailVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCellid", for: indexPath) as? DetailCell else {fatalError()}
        cell.setParent(parent : self,news : self.news!)
        cell.btnReadMore.addTarget(self, action: #selector(self.readMore(_:)), for: .touchUpInside)
        cell.btnReadMore.isUserInteractionEnabled = true

        return cell
    }
    @objc func readMore(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.detailTableView) else { return }
        
        let vc = WebPageVC.instantiate(fromAppStoryboard: .Form)
        vc.url = /self.news?.url
        //vc.titleHead = ""
        self.navigationController?.pushViewController(vc, animated: false)
        //gotoweb
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

// MARK:- FUNCTIONS
//====================
extension NewsDetailVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        
        super.addImageInBackground()
        detailTableView.delegate = self
        detailTableView.dataSource = self
        
    }
}
