//
//  HomeController
//  IPAC
//
//  Created by macOS on 29/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import SwiftyJSON
import PullToRefreshKit

class HomeController: BaseViewController {
    
    // MARK:- Properties
    //===================
    var news: [News] = []
    var newspostmain: [News] = []
    var newspostmaingallery: [News] = []
    var dmk360News: [News] = []
    var murasoliNews: [News] = []

    var newsBanner: [News] = []
    var newsData: [News] = []
    //var pollResultsArray : [PollResultStruct]?

    private var count = 0
    var formUrl = ""
    
    var referData : ReferralModel?
    var referralArrData : [ReferralResult]?  = []
    
    var pollMainData : MainRootClass?
    var pollResultData : [MainRESULT]? = []
    var pollResultsArray : [MainRESULT]? = []
    var polloptionData : [PollOptionResult]? = []
    
    var rewardMainData : RewardData?
    
    
    
    var pollResultDataText : [MainRESULT]? = []
    var polloptionDataText : [PollOptionResult]? = []


    var pollcheck : [String]? = []
    
    var knowledgeData : HomeKnowledgeModel?
    var arrKnowledgeData : [HomeKnowledgeResultModel]?  = []
    //var count = 0
    
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    
    
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Home", comment: "")
        getDataFromdailyRewardAPI(loader: true)
        getDataFromreferralAPI(loader: true)
        getPollData(loader: true)
        getHomeknowledgeData(loader: true)
        self.initialSetup()
        registerCells()
    }
    
    func registerCells() {
        tableView.register(UINib(nibName: "InnerCell1", bundle: nil), forCellReuseIdentifier: "InnerCell1")
        tableView.register(UINib(nibName: "InnerCell2", bundle: nil), forCellReuseIdentifier: "InnerCell2")
        tableView.register(UINib(nibName: "InnerCell3", bundle: nil), forCellReuseIdentifier: "InnerCell3")
        tableView.register(UINib(nibName: "InnerCell4", bundle: nil), forCellReuseIdentifier: "InnerCell4")
    }
    
    func getDataFromreferralAPI(loader : Bool){
        WebServices.referralList(count: self.count.description,loader
            , success: { [weak self] (json) in
                self?.referData = ReferralModel.init(json: json)
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    func getDataFromdailyRewardAPI(loader : Bool){
        WebServices.getDailyReward(loader
            , success: { [weak self] (json) in
                self?.rewardMainData = RewardData.init(json: json)
                print(self?.rewardMainData?.CODE ?? "")
                print(self?.rewardMainData?.MESSAGE ?? "")
//                print("self?.rewardMainData?.RESULT?[0].rewardDay", self?.rewardMainData?.RESULT?[0].rewardDay ?? "")
//                if self?.rewardMainData?.MESSAGE == "Error!" {
//                } else {
//                let vc = HomePopUpViewController.instantiate(fromAppStoryboard: .Home)
//                vc.recieveDay = self?.rewardMainData?.RESULT?[0].rewardDay ?? 1
//                self?.navigationController?.present(vc, animated: false, completion: nil)
//                }
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    func getPollData(loader : Bool){
        WebServices.HomePollData(loader
            , success: { [weak self] (json) in
                self?.pollMainData = MainRootClass.init(json: json)
                if self?.pollMainData?.MESSAGE == "No record found." {
                } else {
                    for (index, element) in (self!.pollMainData?.RESULT?.enumerated())! {
                        if self?.pollMainData?.RESULT?[index].isvoted == "yes"{
                            self?.pollcheck = []
                        } else {
                            self?.pollcheck?.append("test")
                            self?.pollResultsArray?.append((self?.pollMainData?.RESULT?[index])!)
                            self?.pollResultData?.append((self?.pollMainData?.RESULT?[index])!)
                            self?.polloptionData = self?.pollMainData?.RESULT?[0].pollOption
                        }
                    }
                }
                self?.tableView.reloadData()
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
   
    func getHomeknowledgeData(loader : Bool){
        
        WebServices.HomeKnowledgeList(loader
            , success: { [weak self] (json) in
                self?.knowledgeData = HomeKnowledgeModel.init(json: json)
                if self?.knowledgeData?.message == "success" {
                    self?.arrKnowledgeData = self?.knowledgeData?.resultt
                } else {
                    self?.arrKnowledgeData = []
                }
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    // MARK:- IBACTIONS
    //====================
    @IBAction func notificationBtnTap(_ sender: UIButton) {
        let notificationSceen = NotificationVC.instantiate(fromAppStoryboard: .Dashboard)
        self.navigationController?.pushViewController(notificationSceen, animated: true)
    }
    
    // MARK:- Targets
    //================
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        self.count = 0
        self.getHomeNews(count: self.count, loader: false ,  sender)
        getDataFromreferralAPI(loader: true)
        getPollData(loader: true)
        getHomeknowledgeData(loader: true)
    }
}

// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension HomeController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 16 ///10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 11
        {
            return pollResultsArray?.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.newsBanner.count == 0 {
                return 0
            } else {
                return 205 }
        }
        else if indexPath.section == 1 {
            
            return 190 //referral
        }
        else if indexPath.section == 2 {
            
            if self.newspostmain.count == 0 {
                return 0
            } else {
                return 40
                
            }
        }  else if indexPath.section == 3 {
            
            if self.newspostmain.count == 0 {
                return 0
            } else {
                
                return 133
            }
        }  else if indexPath.section == 4 {
            
            return 40
        }  else if indexPath.section == 5 {
            
            return 400
        }  else if indexPath.section == 6 { //new
            if self.dmk360News.count == 0 {
                return 0
            } else {
                return 40
            }
        }  else if indexPath.section == 7 { //new
            if self.dmk360News.count == 0 {
                return 0
            } else {
                return 400
            }
        } else if indexPath.section == 8 { //new2
            if self.murasoliNews.count == 0 {
                return 0
            } else {
                return 40
            }
        }  else if indexPath.section == 9 { //new2
            
            if self.murasoliNews.count == 0 {
                return 0
            } else {
                return 400
            }
        } else if indexPath.section == 10 {
            if self.pollMainData?.MESSAGE == "No record found." || self.pollcheck?.count == 0 {
                return 0
            } else {
                return 40
            }
            //emptycell poll //40
        } else if indexPath.section == 11 {
            return UITableViewAutomaticDimension // poll //234
            
        }
        else if indexPath.section == 12 {
            if self.arrKnowledgeData?.count == 0 {
                return 0
            } else {
                return 40
            }
            
        } else if indexPath.section == 13 {
            if self.arrKnowledgeData?.count == 0 {
                return 0
            } else {
                return 145
            }
            //Knowledge
            
        } else if indexPath.section == 14 {
            if self.newspostmaingallery.count == 0 {
                return 0
            } else {
                return 40
            }
        } else  {
            if self.newspostmaingallery.count == 0 {
                return 0
            } else {
                return 450
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }
    @objc func referalShareAction(_ sender: UIButton) {
                let someText:String =  /self.referData?.userInfo?.referal_content.htmlToString + Constants.referralMsg.rawValue.localized +  /self.referData?.userInfo?.referal_code
                let activityViewController = UIActivityViewController(activityItems : [someText], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
                self.present(activityViewController, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTableViewCell", for: indexPath) as? BannerTableViewCell else{
                fatalError() }
            cell.news = news
            cell.newspost = newsBanner
            cell.pages.numberOfPages = self.newsBanner.count
            cell.collectionView.reloadData()
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralHomeTableViewCell", for: indexPath) as? ReferralHomeTableViewCell else{
                fatalError() }
            cell.selectionStyle = .none
            cell.referralMain.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Referral", comment: "")
            cell.youReferalHead.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Your referral", comment: "")
            cell.yourreferalBottom.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Share your referral code", comment: "")
            cell.shareBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Share", comment: ""), for: .normal)
            cell.shareBtn.addTarget(self, action: #selector(referalShareAction(_:)), for: .touchUpInside)
            cell.shareBtn.setTitleColor(.red, for: .normal)
            cell.referallCodeLabel.text =  /self.referData?.userInfo?.referal_code
            cell.referallCodeLabel.setBorder(color: .clear, width: 1.0, cornerRadius: 5.0)
            return cell
        } else if indexPath.section == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell1", for: indexPath) as? EmptyCell1 else{
                fatalError() }
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 3  {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FormNewTableViewCell", for: indexPath) as? FormNewTableViewCell else{
                fatalError() }
                   cell.news = news
                   cell.newspost = newspostmain
                   cell.pagess.numberOfPages = self.newspostmain.count
                   cell.collectionView.reloadData()
                   cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell2", for: indexPath) as? EmptyCell2 else{
                fatalError() }
            cell.selectionStyle = .none
            return cell
        }  else if indexPath.section == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomenewsTableviewCell", for: indexPath) as? HomenewsTableviewCell else{
                fatalError() }
            cell.news = news
            cell.newspost = newsData
            cell.pagesss.numberOfPages = self.newsData.count
            cell.collectionView.reloadData()
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 8 { //new
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MusuroliEmptyCell", for: indexPath) as? MusuroliEmptyCell else{
                fatalError() }
            cell.selectionStyle = .none

            return cell
        }  else if indexPath.section == 9 { //new
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MusuroliHomeTableViewCell", for: indexPath) as? MusuroliHomeTableViewCell else{
                fatalError() }
            cell.news = news
            cell.newspost = murasoliNews
            cell.pagesss.numberOfPages = self.murasoliNews.count
            cell.collectionView.reloadData()
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 6 { //new2
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DMK360EmptyCell", for: indexPath) as? DMK360EmptyCell else{
                fatalError() }
            cell.selectionStyle = .none
            return cell
        }  else if indexPath.section == 7 { //new2
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DMK360HomeTableViewCell", for: indexPath) as? DMK360HomeTableViewCell else{
                fatalError() }
            cell.news = news
            cell.newspost = dmk360News
            cell.pagesss.numberOfPages = self.dmk360News.count
            cell.collectionView.reloadData()
            cell.selectionStyle = .none
            return cell
        } else  if indexPath.section == 10 {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell5", for: indexPath) as? EmptyCell5 else{
            fatalError() }
        cell.selectionStyle = .none
        cell.pollHead.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Poll", comment: "")
        return cell
        } else if indexPath.section == 11 {
            let type = pollResultsArray?[indexPath.row].pollType
            switch type {
            case "linear":
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "InnerCell4") as! InnerCell4
                cell1.titleLabel.text = pollResultsArray?[indexPath.row].pollHeading
                cell1.subTitleLabel.text = pollResultsArray?[indexPath.row].pollQuestion
                cell1.isSubmitClicked = {
                    if cell1.selectedOption == ""{
                        CommonFunction.showToast(msg: AlertMessage.dataChecking.localized)
                    }else{
                        let pollid = self.pollResultsArray?[indexPath.row].pollId
                        let pollOptionID = self.pollResultsArray?[indexPath.row].pollOption?[cell1.selectedIndex].option_id
                        print("Success Msg: ", pollid!, pollOptionID!, cell1.selectedOption, cell1.selectedIndex)
                        WebServices.requestToSubmitPoll(pollId: pollid ?? "", optionsId: [(self.pollResultsArray?[indexPath.row].pollOption?[cell1.selectedIndex].option_id ?? "")], optionsName: [cell1.selectedOption], success: { (json) in
                            
                            self.getPollData(loader: true)
                            tableView.reloadData()
                            
                            print("Response main", json)
                            
                        }) {(error) -> (Void) in
                            CommonFunction.showToast(msg: error.localizedDescription)
                        }
                        
                        //                        WebServices.requestToSubmitPoll(pollId: pollid ?? "", optionsId: pollOptionID!, optionsName: cell1.selectedOption, success: { (json) in
                        //                            print("Response main", json)
                        //                            self.getPollData(loader: true)
                        //                            tableView.reloadData()
                        //
                        //                        }) {(error) -> (Void) in
                        //                            CommonFunction.showToast(msg: error.localizedDescription)
                        //                        }
                        
                        //                        self.requestToSubmitPoll(pollId: self.feedsData[indexPath.row].pollId ?? "", optionsId: [self.feedsData[indexPath.row].pollOption![0].optionId ?? ""] , optionsName: [cell.selectedOption])
                        //                        cell.submitBtn.setTitle("Submitted", for: .normal)
                        //                        cell.submitBtn.backgroundColor = UIColor.gray
                    }
                }
                
                
                
                
                return cell1
            case "selectlist":
                let cell2 = tableView.dequeueReusableCell(withIdentifier: "InnerCell2") as! InnerCell2
                cell2.titleLabel.text = pollResultsArray?[indexPath.row].pollHeading
                cell2.subTitleLabel.text = pollResultsArray?[indexPath.row].pollQuestion
                cell2.pollsInitialization(polls : self.pollResultsArray?[indexPath.row].pollOption! ?? [])
                
                cell2.isSubmitClicked = {
                    if cell2.selectedOption.count == 0{
                        CommonFunction.showToast(msg: AlertMessage.dataChecking.localized)
                    }else{
                        
                        let pollid = self.pollResultsArray?[indexPath.row].pollId
                        
                        // let ss = cell2.selectedIndex
                        //  let pollOptionIDs = self.pollResultsArray?[indexPath.row].pollOption?[ss].option_id
                        
                        print("Success Msg: ", pollid!, cell2.selectedOption, cell2.selectedIndex)
                        
                        WebServices.requestToSubmitPoll(pollId: pollid ?? "", optionsId: cell2.selectedIndex, optionsName: cell2.selectedOption, success: { (json) in
                            self.pollResultsArray?.remove(at:indexPath.row)
                            self.pollResultData?.remove(at:indexPath.row)
                            tableView.reloadData()
                            print("Response main", json)
                            
                        }) {(error) -> (Void) in
                            CommonFunction.showToast(msg: error.localizedDescription)
                        }
                        
                        
                    }
                }
                
                
                
                return cell2
            case "textarea":
                let cell3 = tableView.dequeueReusableCell(withIdentifier: "InnerCell3") as! InnerCell3
                cell3.titleLabel.text = pollResultsArray?[indexPath.row].pollHeading
                cell3.subTitleLabel.text = pollResultsArray?[indexPath.row].pollQuestion
                
                cell3.isSubmitClicked = {
                    if cell3.userText.text == "" || cell3.userText.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
                        
                        CommonFunction.showToast(msg: AlertMessage.dataChecking2.localized)
                        
                        //TUGHelpers().showAlertWithTitle(alertTitle: APP_NAME, messageBody: "Enter something to submit", controller: self)
                    }else{
                        
                        let pollid = self.pollResultsArray?[indexPath.row].pollId
                        
                        WebServices.requestToSubmitPoll(pollId: pollid ?? "", optionsId: [(self.pollResultsArray?[indexPath.row].pollOption?[0].option_id ?? "")], optionsName: [cell3.userText.text], success: { (json) in
                            self.pollResultsArray?.remove(at:indexPath.row)
                            self.pollResultData?.remove(at:indexPath.row)
                            tableView.reloadData()
                            print("Response main", json)
                            
                        }) {(error) -> (Void) in
                            CommonFunction.showToast(msg: error.localizedDescription)
                        }
                        
                        
                        //  self.requestToSubmitPoll(pollId: self.feedsData[indexPath.row].pollId ?? "", optionsId: [self.feedsData[indexPath.row].pollOption![0].optionId ?? ""] , optionsName: [cell.pollAnswer.text!] )
                        
                    }
                }
                
                
                return cell3
            case "radio":
                let cell4 = tableView.dequeueReusableCell(withIdentifier: "InnerCell1") as! InnerCell1
                cell4.titleLabel.text = pollResultsArray?[indexPath.row].pollHeading
                cell4.subTitleLabel.text = pollResultsArray?[indexPath.row].pollQuestion
                cell4.polloptionData2 = polloptionData
                cell4.pollsInitialization(polls : self.pollResultsArray?[indexPath.row].pollOption! ?? [])
                
                cell4.isSubmitClicked = {
                    if cell4.selectedOption == ""{
                        CommonFunction.showToast(msg: AlertMessage.dataChecking.localized)
                    } else {
                        let pollid = self.pollResultsArray?[indexPath.row].pollId
                        let pollOptionID = self.pollResultsArray?[indexPath.row].pollOption?[cell4.selectedIndex].option_id
                        print("Success Msg: ", pollid!, pollOptionID!, cell4.selectedOption, cell4.selectedIndex)
                        WebServices.requestToSubmitPoll(pollId: pollid ?? "", optionsId: [(self.pollResultsArray?[indexPath.row].pollOption?[cell4.selectedIndex].option_id ?? "")], optionsName: [cell4.selectedOption], success: { (json) in                        self.pollResultsArray?.remove(at:indexPath.row)
                            self.pollResultData?.remove(at:indexPath.row)
                            tableView.reloadData()
                        }) {(error) -> (Void) in
                            CommonFunction.showToast(msg: error.localizedDescription)
                        }
                    }
                }
                return cell4
            default:
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "InnerCell1") as! InnerCell1
                return cell1
            }
        } else  if indexPath.section == 12 {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell4", for: indexPath) as? EmptyCell4 else{
            fatalError() }
        cell.selectionStyle = .none
        cell.heading.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Knowledge_Centre", comment: "")
        return cell
        } else  if indexPath.section == 13 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "KnowledgeCentreHomeTableViewCell", for: indexPath) as? KnowledgeCentreHomeTableViewCell else{
                fatalError() }
            cell.knowledgeData = knowledgeData
            cell.arrKnowledgeData = arrKnowledgeData
            cell.pagesss.numberOfPages = self.knowledgeData?.resultt?.count ?? 0
            cell.collectionView.reloadData()
            cell.selectionStyle = .none
            return cell
        }
        else  if indexPath.section == 14 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell3", for: indexPath) as? Empty3Cell else{
                fatalError() }
            cell.selectionStyle = .none
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as? GalleryTableViewCell else{
                fatalError() }
            cell.selectionStyle = .none
            cell.news = news
            cell.newspost = newspostmaingallery
            cell.pagessss.numberOfPages = self.newspostmaingallery.count
            cell.collectionView.reloadData()
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
// MARK:- FUNCTIONS
//====================
extension HomeController {
    private func initialSetup() {
        super.addImageInBackground()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        navigationController?.navigationBar.isHidden = true
        self.registerXib()
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
       // self.handlePaging()
        self.count = 0
        self.getHomeNews(count: self.count, loader: true ,   nil) //true-> isFirst
    }
    
    func registerXib() {
        tableView.register(UINib.init(nibName: "GalleryCellTableViewCell", bundle: nil), forCellReuseIdentifier: "GalleryCellTableViewCell")
        tableView.register(UINib(nibName: "BannerCell", bundle: nil), forCellReuseIdentifier: "BannerCell")
        tableView.register(UINib(nibName: "NotifyCell", bundle: nil), forCellReuseIdentifier: "NotifyCell")
        tableView.register(UINib.init(nibName: "ArticleCell", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        tableView.register(UINib.init(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: "FormTableViewCell")
        tableView.register(UINib.init(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCellid")
        
    }
    
    //MARK::- API
    func getHomeNews(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            }else{
                self?.news.append(contentsOf: news)
            }
            
            if self?.news.count == 0 {
            } else {
                for (index, element) in self!.news.enumerated() {
                 //   print(self?.news[index].newsCategory ?? "")
                    if (self?.news[index].newsCategory)!.rawValue == "form"{
                        if  self?.news[index].is_completed == "1"{
                        } else {
                            self?.newspostmain.append((self?.news[index])!)
                        }
                    }
                    if (self?.news[index].newsCategory)!.rawValue == "gallery"{
                        self?.newspostmaingallery.append((self?.news[index])!)
                    }
                    if (self?.news[index].newsCategory)!.rawValue == "dmk360"{
                        self?.dmk360News.append((self?.news[index])!)
                    }
                    if (self?.news[index].newsCategory)!.rawValue == "murasoli"{
                        self?.murasoliNews.append((self?.news[index])!)
                    }
                    if (self?.news[index].newsCategory)!.rawValue == "banner"{
                        self?.newsBanner.append((self?.news[index])!)
                    }
                    
                    if (self?.news[index].newsCategory)!.rawValue == "post" || (self?.news[index].newsCategory)!.rawValue == "article"{
                        self?.newsData.append((self?.news[index])!)
                    }
                }
            }
            self?.lblNoData.isHidden = !(self?.news.isEmpty ?? true)
            self?.reloadWithoutAnimation()
            self?.count = json[ApiKeys.next].intValue
            self?.checkBonus(json : json)
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            self.lblNoData.isHidden = !(self.news.isEmpty )
            return
        }
    }
}


//MARK::- PAGINATION
extension HomeController{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            print_debug("paging")
            self.count == -1 ? self.tableView.switchRefreshFooter(to: FooterRefresherState.noMoreData) :  self.getMore()}
    }
    func getMore(){
        self.getHomeNews(count: self.count, loader: false)
    }
    
}

//MARK::- TERMS CONDITION DELEGATE
extension HomeController : TermsAcceptDelegate , BonusPopupDelegate , FormCompletedDelegate{
    func accepted(version: String) {
        let termsSceen = TemsAndCondition.instantiate(fromAppStoryboard: .Main)
        termsSceen.isNewTerms = true
        termsSceen.term_condition_version = version
        self.presentVC(termsSceen)
        
    }
    func completed(){
        self.count = 0
        self.getHomeNews(count: self.count, loader: true)
    }
    
    func done(status : Int){
        switch status{
        case 0:
            let vc = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
            vc.isSidePanel = false
            self.navigationController?.pushViewController(vc, animated: false)
            break
        case 1:
            break
        default:
            let vc = WebViewVC.instantiate(fromAppStoryboard: .Form)
            vc.url = self.formUrl
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        //0 profile , 1 task , 2 form
    }
    
    func checkBonus(json : JSON){
        let data = json["TERM_CONDITION"].dictionaryValue
        let termsCondition =  TermsCondition(dict :  JSON(data))
        if termsCondition.term_accept_status == "1"{
            let vc = AcceptTermsVC.instantiate(fromAppStoryboard: .ContactUs)
            vc.version = termsCondition.version
            vc.delegate = self
            self.navigationController?.present(vc, animated: false, completion: nil)
        }else{
            let user = CommonFunction.getUser()
            if user?.is_bonus_received == "1"{return}
            //                if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || json["TOTAL_COMPLETE_TASK"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""{
            
            
            
//            if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || SingletonTask.shared.completedTask == 0 || user?.facebookId == "" || user?.twitterId == ""{
//                let vc = BonusPopupVC.instantiate(fromAppStoryboard: .Form)
//                self.formUrl = json["CAMPAIGN_FORM_URL"].stringValue
//                vc.isFormFill = json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1"
//                vc.isFirstTask = SingletonTask.shared.completedTask == 0
//                vc.isProof = json["IS_ID_PROOF_ADDED"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""
//                vc.isPaytmDetail = json["PAYMENT_DETAIL_ADDED"].stringValue == "0"
//                vc.isDismiss = !(SingletonTask.shared.completedTask >= 3)
//                vc.delegate = self
//                self.navigationController?.present(vc, animated: false, completion: nil)
//            }
        }
    }
}

//MARK::- RELOAD TABLE

extension HomeController {
    func reloadWithoutAnimation() {
        let lastScrollOffset = self.tableView.contentOffset
        tableView.reloadData()
        if self.count == 0{
            tableView.setContentOffset(CGPoint.zero, animated: false)
        }else{
            tableView.setContentOffset(lastScrollOffset, animated: false)
            
        }
    }
}
struct PollResult : Decodable
{
    var RESULT : [PollResultStruct]
}

struct PollResultStruct: Decodable {
    let type, pollID, pollHeading, pollQuestion: String
    let pollType, createdOn, isvoted: String
    let pollOption : [PollOptionsStruct]?

    enum CodingKeys: String, CodingKey {
        case type
        case pollID = "poll_id"
        case pollHeading = "poll_heading"
        case pollQuestion = "poll_question"
        case pollType = "poll_type"
        case createdOn = "created_on"
        case pollOption = "poll_option"
        case isvoted
    }
}

struct PollOptionsStruct : Decodable
{
    let option_id , option_name , vote_count  : String
   
}
