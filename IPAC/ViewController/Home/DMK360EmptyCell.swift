//
//  DMK360EmptyCell.swift
//  Stalinani
//
//  Created by HariTeju on 24/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class DMK360EmptyCell: UITableViewCell {
    
    @IBOutlet weak var nameLabe: UILabel!
          
          override func awakeFromNib() {
              super.awakeFromNib()
           self.nameLabe.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Party_Roundup", comment: "")

              // Initialization code
          }

          override func setSelected(_ selected: Bool, animated: Bool) {
              super.setSelected(selected, animated: animated)

              // Configure the view for the selected state
          }

}
