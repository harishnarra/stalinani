//
//  KnowledgeStory.swift
//  Stalinani
//
//  Created by HariTeju on 08/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class KnowledgeStory: UIViewController {
    

    var recieveStoryName: String?
    var recieveStoryDesc: String?

    @IBOutlet weak var naviTitle: UILabel!
    @IBOutlet weak var storyName: UILabel!
    @IBOutlet weak var storyDesc: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let data = (self.arrKnowledgeData?[indexPath.row].faqdescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
//                               let attrStr = try? NSAttributedString( // do catch
//                                   data: recieveStoryDesc,
//                                   options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
//                                   documentAttributes: nil)
//                   //            cell.descriptionLabel.attributedText = attrStr
        naviTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Knowledge_Centre", comment: "")
                   storyName.text = recieveStoryName
                   storyDesc.text = recieveStoryDesc
                   

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
