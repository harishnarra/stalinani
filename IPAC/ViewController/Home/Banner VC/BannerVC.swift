//
//  BannerVC.swift
//  IPAC
//
//  Created by macOS on 10/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import EZSwiftExtensions

class BannerVC: UIViewController {

    @IBOutlet weak var bannerTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    //MARK:-
    //MARK:- Properties
    var news: News?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK:- Targets
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension BannerVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.news?.newsCategory == .article {
            return 1
        }
        guard let mediaSet = news?.mediaSet else { return 0 }
        return mediaSet.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "StoryHeaderCellTableViewCell") as! StoryHeaderCellTableViewCell
        guard let news = news else { return UIView() }
        header.populateHeaderData(data: news)
        header.btnReadMore.isHidden = true
        header.heightReadMore.constant = 0
        header.descLabel.text = ""
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BannerImageCell") as? BannerImageCell else { fatalError("invalid cell \(self)")
        }
        guard let news = news else { fatalError("empty news model") }
        cell.populateCell(with: news, indexPath: indexPath)
        cell.btnReadMore.isUserInteractionEnabled = true
        cell.btnReadMore.addTarget(self, action: #selector(self.readMore(_:)), for: .touchUpInside)
        return cell
    }
    @objc func readMore(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.bannerTableView) else { return }
        let vc = WebPageVC.instantiate(fromAppStoryboard: .Form)
        vc.url = /self.news?.url
        //vc.titleHead = ""
        self.navigationController?.pushViewController(vc, animated: false)
        //gotoweb
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 80

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let mediaSet = news?.mediaSet else { return }
        if mediaSet[indexPath.row].mediaType == "2"{return}
        let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
        previewVC.previewImage = mediaSet[indexPath.row].mediaUrl ?? ""
        previewVC.tittle = news?.newsTitle ?? ""
        self.navigationController?.pushViewController(previewVC, animated: true)
    }
}

//MARK:-
//MARK:- Private Methods
private extension BannerVC {
    
    func initialSetup() {
       bannerTableView.delegate = self
        bannerTableView.dataSource = self
        
        setupText()
        registerXib()
    }
    
    func setupText() {
        self.titleLabel.text = StringConstant.Story.localized
    }
    
    func registerXib() {
        let headerNib = UINib.init(nibName: "StoryHeaderCellTableViewCell", bundle: Bundle.main)
        bannerTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "StoryHeaderCellTableViewCell")
    }
}

//MARK:- Prototype Cell
//=========================
class BannerImageCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    var news : News?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var btnReadMore: GradientButton!
    @IBOutlet weak var heightReadMore: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
       // bgView.rounded(cornerRadius: 5.0, clip: true)
    }

    @IBAction func btnActionPlay(_ sender: UIButton) {
        
        let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
        vc.url = /self.news?.mediaSet?.first?.mediaUrl
        vc.desc = /self.news?.newsTitle
        ez.topMostVC?.presentVC(vc)
//        guard let videoURL = URL(string: /self.news?.mediaSet?.first?.mediaUrl) else {return}
//        let player = AVPlayer(url: videoURL)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        ez.topMostVC?.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//        }
    }
    
    func populateCell(with data: News, indexPath: IndexPath) {
        guard let mediaSet = data.mediaSet else { return }
        
        self.news = data
        btnReadMore.isHidden = /data.url == ""
        heightReadMore.constant = /data.url == "" ? 0 : 32

        if /mediaSet[indexPath.row].mediaType == "2"{
            self.bannerImage.kf.setImage(with: URL(string: mediaSet[indexPath.row].mediaThumb ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))
            self.btnPlay.isHidden = false

        }else{
            self.bannerImage.kf.setImage(with: URL(string: mediaSet[indexPath.row].mediaUrl ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))
            self.btnPlay.isHidden = true
        }
        
        let data = (data.newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        do {
            let attrStr = try NSAttributedString(
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            descriptionLabel.attributedText = attrStr
            
        } catch {
            print_debug(error.localizedDescription)
        }
        
        
    }
}
