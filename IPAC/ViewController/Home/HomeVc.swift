//
//  HomeVc.swift
//  IPAC
//
//  Created by macOS on 29/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import SwiftyJSON
import PullToRefreshKit

class HomeVc: BaseViewController {
    
    // MARK:- Properties
    //===================
    var news: [News] = []
    private var count = 0
    var formUrl = ""
    
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var lblNoData: UILabel!

    
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    // MARK:- IBACTIONS
    //====================
    @IBAction func notificationBtnTap(_ sender: UIButton) {
        let notificationSceen = NotificationVC.instantiate(fromAppStoryboard: .Dashboard)
        self.navigationController?.pushViewController(notificationSceen, animated: true)
    }
    
    // MARK:- Targets
    //================
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        self.count = 0
        self.getHomeNews(count: self.count, loader: false ,  sender)
    }    
}

// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension HomeVc : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.news.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch self.news[indexPath.row].newsCategory {
        case .gallery:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryCellTableViewCell", for: indexPath) as? GalleryCellTableViewCell else{
                fatalError() }
            cell.populate(with: self.news[indexPath.row])
            cell.selectionStyle = .none
            return cell
            
        case .banner:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as? BannerCell else{
                fatalError() }
        cell.populateCell(with: self.news[indexPath.row])
            cell.selectionStyle = .none
            return cell
            
        case .notification:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotifyCell", for: indexPath) as? NotifyCell else{
                fatalError() }
            cell.populateCell(with: self.news[indexPath.row])
            cell.descLabel.numberOfLines = 0
            cell.selectionStyle = .none
            if (cell.descLabel.text?.length ?? 0) > 200 {
                if self.news[indexPath.row].isSelected == false{
                    cell.descLabel.numberOfLines = 2
                }
            }else{
                self.news[indexPath.row].isSelected = true
            }
            cell.btnSeeMore.isHidden = self.news[indexPath.row].isSelected
            
            return cell
        case .article:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as? ArticleCell else{
                fatalError() }
            cell.populateCell(with: self.news[indexPath.row])
            cell.selectionStyle = .none
            
            return cell
        case .form:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FormTableViewCell", for: indexPath) as? FormTableViewCell else{
                fatalError() }
            cell.populateCell(with: self.news[indexPath.row])
            cell.selectionStyle = .none
            
            return cell
        case .post: //with caroesel
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCellid", for: indexPath) as? HomeCell else{
                fatalError() }
            cell.configureCellWith(self.news[indexPath.row])
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
                self.tableView.reloadData()
                return
        switch self.news[indexPath.row].newsCategory  {
        case .gallery :
            let storySceen = StoryVC.instantiate(fromAppStoryboard: .Home)
            storySceen.news = self.news[indexPath.row]
            self.navigationController?.pushViewController(storySceen, animated: true)
            
        case .banner:
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.news[indexPath.row]
            self.navigationController?.pushViewController(bannerSceen, animated: true)
        case .article:
            let bannerSceen = BannerVC.instantiate(fromAppStoryboard: .Home)
            bannerSceen.news = self.news[indexPath.row]
            self.navigationController?.pushViewController(bannerSceen, animated: true)
            
        case .form :
            if  self.news[indexPath.row].is_completed == "1"{
                return
            }
            let webViewVC = WebViewVC.instantiate(fromAppStoryboard: .Form)
            webViewVC.url = /self.news[indexPath.row].url
            webViewVC.news = self.news[indexPath.row]
            webViewVC.flag = 1
            self.navigationController?.pushViewController(webViewVC, animated: true)
            
        case .post:
            let vc = NewsDetailVC.instantiate(fromAppStoryboard: .Home)
            vc.news = self.news[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        case .notification:
            if self.news[indexPath.row].isSelected  == false{
                self.news[indexPath.row].isSelected  = true
                self.tableView.reloadRows(at: [IndexPath.init(row: indexPath.row, section: 0)], with: .none)
            }
        default:
            return
        }
    }
}


// MARK:- FUNCTIONS
//====================
extension HomeVc {
    private func initialSetup() {
        super.addImageInBackground()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        navigationController?.navigationBar.isHidden = true
        self.registerXib()
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.handlePaging()
        self.count = 0
        self.getHomeNews(count: self.count, loader: true) //true-> isFirst
    }
    
    func registerXib() {
        tableView.register(UINib.init(nibName: "GalleryCellTableViewCell", bundle: nil), forCellReuseIdentifier: "GalleryCellTableViewCell")
        tableView.register(UINib(nibName: "BannerCell", bundle: nil), forCellReuseIdentifier: "BannerCell")
        tableView.register(UINib(nibName: "NotifyCell", bundle: nil), forCellReuseIdentifier: "NotifyCell")
        tableView.register(UINib.init(nibName: "ArticleCell", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        tableView.register(UINib.init(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: "FormTableViewCell")
        tableView.register(UINib.init(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCellid")
        
    }
    
    //MARK::- API
    func getHomeNews(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: true, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            }else{
                self?.news.append(contentsOf: news)
            }
            self?.lblNoData.isHidden = !(self?.news.isEmpty ?? true)
            self?.tableView.reloadData()
            self?.count = json[ApiKeys.next].intValue
            self?.checkBonus(json : json)
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            self.lblNoData.isHidden = !(self.news.isEmpty )
            return
        }
    }
}


//MARK::- PAGINATION
extension HomeVc{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        
        tableView.configRefreshFooter(with: footer, container: self) {
            
            print_debug("paging")
            self.count == -1 ? self.tableView.switchRefreshFooter(to: FooterRefresherState.noMoreData) :  self.getMore()}
        
    }
    func getMore(){
        self.getHomeNews(count: self.count, loader: false)
    }
    
    func checkBonus(json : JSON){
        let data = json["TERM_CONDITION"].dictionaryValue
        let termsCondition =  TermsCondition(dict :  JSON(data))
        if termsCondition.term_accept_status == "1"{
            let vc = AcceptTermsVC.instantiate(fromAppStoryboard: .ContactUs)
            vc.version = termsCondition.version
            vc.delegate = self
            self.navigationController?.present(vc, animated: false, completion: nil)
        }else{
            let user = CommonFunction.getUser()
            if user?.is_bonus_received == "1"{return}
            //                if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || json["TOTAL_COMPLETE_TASK"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""{
            if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || termsCondition.completed_task == "0" || user?.facebookId == "" || user?.twitterId == ""{
                
                let vc = BonusPopupVC.instantiate(fromAppStoryboard: .Form)
                self.formUrl = json["CAMPAIGN_FORM_URL"].stringValue
                vc.isFormFill = json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1"
                vc.isFirstTask = false
                vc.isProof = json["IS_ID_PROOF_ADDED"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""
                vc.isPaytmDetail = json["PAYMENT_DETAIL_ADDED"].stringValue == "0"
                vc.isDismiss = !((termsCondition.completed_task.toInt() ?? 0) >= 3)
                vc.delegate = self
                self.navigationController?.present(vc, animated: false, completion: nil)
            }
        }
    }
}

//MARK::- TERMS CONDITION DELEGATE
extension HomeVc : TermsAcceptDelegate , BonusPopupDelegate{
    func accepted(version: String) {
        let termsSceen = TemsAndCondition.instantiate(fromAppStoryboard: .Main)
        termsSceen.isNewTerms = true
        termsSceen.term_condition_version = version
        self.presentVC(termsSceen)
        
    }
    func done(status : Int){
        switch status{
        case 0:
            let vc = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
            vc.isSidePanel = false
            self.navigationController?.pushViewController(vc, animated: false)
            break
        case 1:
            break
        default:
            let vc = WebViewVC.instantiate(fromAppStoryboard: .Form)
            vc.url = self.formUrl
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        //0 profile , 1 task , 2 form
    }
}
