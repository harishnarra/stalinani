//
//  PlayerViewController2.swift
//  Stalinani
//
//  Created by Haritej on 31/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import AVKit


class PlayerViewController2: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var viewPlayer: UIView!
    
    //MARK::- PROPERTIES
    var url : String?
    var desc : String?
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }

    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK::- FUNCTION
    func onViewDidLoad(){
//        lblDescription.text = /desc//hidden
//        txtViewDesc.text = /desc
        let player = AVPlayer(url: URL(string : /url)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        playerController.view.frame = viewPlayer.bounds
        self.addChildViewController(playerController)
        viewPlayer.addSubview(playerController.view)
        playerController.showsPlaybackControls = true
        player.play()
    }
    
}

