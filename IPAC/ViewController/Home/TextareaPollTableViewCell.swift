//
//  TextareaPollTableViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 09/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class TextareaPollTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var tableView: UITableView!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 2
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextareaPollListTableViewCell", for: indexPath) as? TextareaPollListTableViewCell else{
               fatalError() }
               cell.selectionStyle = .none
        return cell
       }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return tableView.contentSize.height
//    }
}
