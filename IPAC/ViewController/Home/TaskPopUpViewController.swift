//
//  TaskPopUpViewController.swift
//  Stalinani
//
//  Created by HariTeju on 20/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class TaskPopUpViewController: UIViewController {

    @IBOutlet weak var trendImage: UIImageView!
    @IBOutlet weak var showHiddenView: AnimatableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
