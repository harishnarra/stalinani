//
//  FormsCollectionViewCell.swift
//  IPAC
//
//  Created by HariTeju on 06/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class FormsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnRegister: AnimatableButton!
    
    @IBOutlet weak var registeredLabel: UILabel!
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        registeredLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Registered", comment: "")
                      btnRegister.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Register", comment: ""), for: .normal)
               btnRegister.addLeftToRightGradient()
        btnRegister.addLeftToRightGradient()
    }
}
