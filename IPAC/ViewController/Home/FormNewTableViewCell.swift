//
//  FormNewTableViewCell.swift
//  IPAC
//
//  Created by HariTeju on 06/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class FormNewTableViewCell: UITableViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagess: UIPageControl!
    
    var news: [News] = []
    var newspost: [News] = []
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
       // self.getHomeNewss(count: 0, loader: false)

        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.layoutIfNeeded()
        collectionView.reloadData()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        print("self.newspost.countsss", self.newspost.count)
//        //return 2
        return self.newspost.count
      //  return 3
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pagess.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FormsCollectionViewCell", for: indexPath) as? FormsCollectionViewCell else{
            fatalError() }
        
        cell.lblTitle.text = self.newspost[indexPath.row].newsTitle
        cell.btnRegister.isHidden = self.newspost[indexPath.row].is_completed != "0"
        let data = (self.newspost[indexPath.row].newsDescription ?? "").data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        cell.lblDesc.attributedText = attrStr
        
      return cell
        
}
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if  self.newspost[indexPath.row].is_completed == "1"{
                            return
                        }
                        let webViewVC = WebViewVC.instantiate(fromAppStoryboard: .Form)
                        webViewVC.url = /self.newspost[indexPath.row].url
                        webViewVC.news = self.newspost[indexPath.row]
                       // webViewVC.delegateForm = self
                        webViewVC.flag = 1
                       self.parentViewController?.pushVC(webViewVC)

                      //  self.navigationController?.pushViewController(webViewVC, animated: true)
        
    }
    
    
    func getHomeNewss(count: Int, loader: Bool , _ refreshControl : UIRefreshControl? = nil){
        print_debug("check api")
        WebServices.getHomeData(params: [ApiKeys.count: count], loader: loader, success: { [weak self] (json) in
            refreshControl?.endRefreshing()
            //self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            // self?.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            var news = [News]()
            for obj in json[ApiKeys.result].arrayValue {
                let newsObj = News(json: obj)
                news.append(newsObj)
            }
            var priorityNews = [News]()
            for obj in json["PRIORITY_NEWS"].arrayValue {
                let newsObj = News(json: obj)
                priorityNews.append(newsObj)
            }
            if count == 0{
                self?.news = priorityNews
                self?.news.append(contentsOf: news)
            }else{
                self?.news.append(contentsOf: news)
            }
            
            print("self.news.count", self?.news.count ?? 0)
            // print(self.news.count)
//            for i in self!.news   {
//                
//                print("iiiiii:" , i)
//                
//            }
            
            if self?.news.count == 0 {
                
            } else {
                for (index, element) in self!.news.enumerated() {
                    print("Item \(index): \(element)")
                    print(self?.news[index].newsCategory ?? "")
                    
                    if (self?.news[index].newsCategory)!.rawValue == "form"{
                        
                        print("self.minnews", self?.news[index] ?? "")

                        self?.newspost.append((self?.news[index])!)

                    }
                }
            }
            self?.pagess.numberOfPages = self?.newspost.count ?? 0

            self?.collectionView.reloadData()
            
            //            self?.lblNoData.isHidden = !(self?.news.isEmpty ?? true)
            //            self?.reloadWithoutAnimation()
            //            self?.count = json[ApiKeys.next].intValue
            //            self?.checkBonus(json : json)
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            refreshControl?.endRefreshing()
            //            self.tableView.switchRefreshFooter(to: FooterRefresherState.normal)
            //            self.lblNoData.isHidden = !(self.news.isEmpty )
            return
        }
    }
}
