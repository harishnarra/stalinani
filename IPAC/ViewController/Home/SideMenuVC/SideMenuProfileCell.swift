//
//  SideMenuProfileCell.swift
//  IPAC
//
//  Created by Appinventiv on 01/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SideMenuProfileCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var englishBtn: UIButton!
    @IBOutlet weak var tamilBtn: UIButton!
    @IBOutlet weak var languageChangeBtn: UIButton!
    
    var checkLang: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        checkLang = true
        
//        englishBtn.contentHorizontalAlignment = .left
//        tamilBtn.contentHorizontalAlignment = .left

        languageView.isHidden = true

    //    languageBtn.contentHorizontalAlignment = .left
//        engBtn.contentHorizontalAlignment = .left
//        tamilBtn.contentHorizontalAlignment = .left
//
//        langStack.addBackground(color: .white)
//        langStack.setCornerRadius(radius: 5.0)
    }
    @IBAction func didTapOnChangeLanguage(_ sender: Any) {
        
        if checkLang == true {
            languageView.isHidden = false
            checkLang = false

        } else {
            
            languageView.isHidden = true
            checkLang = true


        }
    }
    
    @IBAction func englishClicked(_ sender: Any) {
        languageView.isHidden = true
        languageLabel.text = "English"
      //  if LocalizationSystem.sharedInstance.getLanguage() == "ta" {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
        
        print(LocalizationSystem.sharedInstance.getLanguage())
       // UserDefaults.standard.set("en", forKey: "selectedLanguage")
        AppUserDefaults.save(value: LocalizationSystem.sharedInstance.getLanguage() , forKey: .languagess)

        

        
            print("Remember_me:", StringConstant.Remember_me.localized)
        self.awakeFromNib()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
       // let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let appDlg = UIApplication.shared.delegate as? AppDelegate
        appDlg?.window?.rootViewController = vc

        

      //      UIView.appearance().semanticContentAttribute = .forceLeftToRight
      //  } else {
       //     UIView.appearance().semanticContentAttribute = .forceRightToLeft
       // }


    }
    @IBAction func tamilClicked(_ sender: Any) {
        
//        sideMenuViewController?.delegate = self
//        self.sideMenuViewController?.presentLeftMenuViewController()
//        languageView.isHidden = true
        languageLabel.text = "தமிழ்"
        LocalizationSystem.sharedInstance.setLanguage(languageCode: "ta")
        print(LocalizationSystem.sharedInstance.getLanguage())
      //  UserDefaults.standard.set("ta", forKey: "selectedLanguage")
        AppUserDefaults.save(value: LocalizationSystem.sharedInstance.getLanguage() , forKey: .languagess)


        print("Remember_me:", StringConstant.Remember_me.localized)
       // setHome()
        self.awakeFromNib()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
        // let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let appDlg = UIApplication.shared.delegate as? AppDelegate
        appDlg?.window?.rootViewController = vc
        //self.viewdid
        
//        let homeVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabBarVC") as! HomeTabBarVC
//         UINavigationController.init(rootViewController: homeVC)
//
//        sideMenuViewController?.contentViewController = UINavigationController.init(rootViewController: homeVC)
//        sideMenuViewController?.hideMenuViewController()
//        
//        if LocalizationSystem.sharedInstance.getLanguage() == "ta" {
//            print("tamil")
//        } else {
//            print("english")
//        }




    }
//    func setHomeP()
//    {
//        let storyboard : UIStoryboard   =   UIStoryboard.init(name: "Home", bundle: nil)
//        //let storyboard = UIStoryboard(name: "Home", bundle: nil)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let homeVC = storyboard.instantiateViewController(withIdentifier: "TabBar")
//        appDelegate.navController = UINavigationController.init(rootViewController:homeVC)
//        appDelegate.navController.navigationBar.isHidden = true
//        appDelegate.navController.interactivePopGestureRecognizer?.isEnabled = false
//        appDelegate.window?.rootViewController = appDelegate.navController
//        appDelegate.window?.makeKeyAndVisible()
//        appDelegate.setIntialVCWithDrawer(vc: storyboard.instantiateViewController(withIdentifier: "TabBar"))
//    }
    func setHome() {
        
        let storyboard : UIStoryboard   =   UIStoryboard.init(name: "Home", bundle: nil)

        let vc = storyboard.instantiateViewController(withIdentifier: "HomeController") as! HomeController
        let appDlg = UIApplication.shared.delegate as? AppDelegate
        appDlg?.window?.rootViewController = vc
        
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        
        profileImageView.setBorder(color: AppColors.whiteColor, width: 2, cornerRadius: 5.0)
    }
    
   
}
extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
   
}
