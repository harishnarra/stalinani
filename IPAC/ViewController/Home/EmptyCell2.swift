//
//  EmptyCell2.swift
//  IPAC
//
//  Created by HariTeju on 26/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class EmptyCell2: UITableViewCell {
    @IBOutlet weak var nameLabe: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabe.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "MKS_Roundup", comment: "")

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
