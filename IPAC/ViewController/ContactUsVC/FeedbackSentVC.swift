//
//  FeedbackSentVC.swift
//  IPAC
//
//  Created by Appinventiv on 17/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class FeedbackSentVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblIssueHeading: UILabel!
    @IBOutlet weak var lblIssueDescription: UILabel!
    @IBOutlet weak var lblReplyMsg: UILabel!
    
    //MARK::- PROPERTIES
    var contactNo : String?
    var issueHeading : String?
    var issueDesc : String?
    var replyMsg : String?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        for item in self.navigationController?.viewControllers ?? []{
            if item is ContactUsVC{
                self.navigationController?.popToViewController(item, animated: false)
            }
        }
    }
}

//MARK::- FUNCTIONS
extension FeedbackSentVC{
    func onViewDidLoad(){
        lblIssueHeading.text = issueHeading
        lblIssueDescription.text = issueDesc
        let text: NSMutableAttributedString = NSMutableAttributedString(string: /replyMsg + " " + /contactNo)
        text.setColorForText(textForAttribute: /contactNo, withColor: AppColors.appThemeColor, font: AppFonts.ProximaNova_Bold.withSize(16))
        self.lblReplyMsg.attributedText = text
        lblReplyMsg.addTapGesture {  [weak self] (_) in
            self?.callToANumber(/self?.contactNo)
        }
    }
}
