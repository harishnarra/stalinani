//
//  TaskFeedbackVC.swift
//  IPAC
//
//  Created by macOS on 17/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class TaskFeedbackVC: BaseVC {

    //MARK:- Properties
    //==================
   // var contactList = [ContactUsModel]()
    var headingText = ""
    var categoryId = 0
    let lengthLimit = 180
    var contactNo : String?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var textViewContainerView: UIView!
   // @IBOutlet weak var contactUsTableView: UITableView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }

    override func loadView() {
        super.loadView()
        sendBtn.addRightToLefttGradient()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- IBAction
    //================
    @IBAction func sendBtnTap(_ sender: UIButton) {
        self.view.endEditing(true)
        if textView.text.trimmed() == "" ||  textView.text.trimmed() == LocalizationSystem.sharedInstance.localizedStringForKey(key: "Write_description", comment: ""){
            CommonFunction.showToast(msg: AlertMessage.writeDescription.localized)
            return
        }
        sendFeedback(feedback: textView.text)
    }
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension TaskFeedbackVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == AppColors.Gray.gray166 {
            textView.text = nil
            textView.textColor = AppColors.BlackColor.black12
            self.sendBtn.isEnabled = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textViewText = textView.text , lengthLimit != 0 else { return true }
        
        let newLength = textViewText.utf16.count + text.utf16.count - range.length
        
        if newLength <= lengthLimit {
            countLabel.text = "\(newLength)/\(lengthLimit)"
            // drhDelegate?.didReachCharacterLimit(false)
        } else {
            //  drhDelegate?.didReachCharacterLimit(true)
            UIView.animate(withDuration: 0.1, animations: {
                self.countLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                
            }, completion: { (finish) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.countLabel.transform = CGAffineTransform.identity
                })
            })
        }
        
        return newLength <= lengthLimit
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = StringConstant.Write_Something.localized
            textView.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Write_description", comment: "")
            textView.textColor = AppColors.Gray.gray166
            self.sendBtn.isEnabled = false
        }
    }
    
}

//MARK:- Function
//==================
extension TaskFeedbackVC {
    
    func initialSetup() {
        
        containerView.rounded(cornerRadius: 5.0, clip: true)
        textView.rounded(cornerRadius: 5.0, clip: true)
        textView.textColor = AppColors.Gray.gray234
        sendBtn.rounded(cornerRadius: 5.0, clip: true)
        sendBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Send", comment: ""), for: .normal)
        self.textView.delegate = self
        setText()
    }
    
    func setText() {
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Contact Us", comment: "")
        headingLabel.text = headingText
        setupTextAndColor()
    }
    
    
    func setupTextAndColor() {
       // self.countLabel.font = AppFonts.Poppins_Regular.withSize(12)
        self.countLabel.textColor = AppColors.BlackColor.black12
        self.textView.text = StringConstant.Write_Something.localized
        self.textView.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Write_description", comment: "")

        self.textView.textColor = AppColors.Gray.gray166
        self.textView.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(12)
        self.countLabel.text = "0/\(lengthLimit)"
    }
    func sendFeedback(feedback: String) {
        let params  = [ApiKeys.categoryId: String(describing: categoryId), ApiKeys.contactContent: feedback]
        WebServices.saveContact(params: params, success: { (json) in
            print_debug(json)
            let vc  = FeedbackSentVC.instantiate(fromAppStoryboard: .ContactUs)
            vc.contactNo = self.contactNo
            vc.issueDesc = feedback
            vc.issueHeading = self.headingText
            vc.replyMsg = json[ApiKeys.message].stringValue
            self.navigationController?.pushViewController(vc, animated: false)
        }) { (error) -> (Void) in
            print_debug(error.localizedDescription)
            CommonFunction.showToast(msg: error.localizedDescription)

        }
    }
}
