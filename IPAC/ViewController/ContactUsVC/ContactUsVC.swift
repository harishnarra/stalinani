//
//  ContactUsVC.swift
//  IPAC
//
//  Created by macOS on 17/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ContactUsVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var contactList = [ContactUsModel]()
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var sideMenuBtn: MenuButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contactUsTableView: UITableView!
    @IBOutlet weak var supportLabel: UILabel!
    @IBOutlet weak var phoneNoBtn: UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        getContactDetail(loader : true)
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
    //================
    @IBAction func phoneNoBtnTap(_ sender: UIButton) {
        callToANumber(/sender.title(for: .normal))
    }
    
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension ContactUsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsCell") as? ContactUsCell else { fatalError("invalid cell \(self)")
        }
        cell.populate(with: self.contactList[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //guard let cell = tableView.cellForRow(at: indexPath) as? ContactUsCell else { return }
        let taskSceen = TaskFeedbackVC.instantiate(fromAppStoryboard: .ContactUs)
        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            taskSceen.headingText = self.contactList[indexPath.row].categoryName
        } else {
            taskSceen.headingText = self.contactList[indexPath.row].categoryName_Tn
        }
    
       // taskSceen.headingText = self.contactList[indexPath.row].categoryName
        taskSceen.categoryId = self.contactList[indexPath.row].categoryId
        taskSceen.contactNo = self.phoneNoBtn.title(for: .normal)
        self.navigationController?.pushViewController(taskSceen, animated: true)
    }
}

//MARK:- Function
//==================
extension ContactUsVC {
    
    func initialSetup() {
        contactUsTableView.delegate = self
        contactUsTableView.dataSource = self
        bgView.rounded(cornerRadius: 5.0, clip: true)
        self.contactUsTableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
        setText()
    }
    
    func setText() {
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Contact Us", comment: "")
        supportLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "SupportHelp", comment: "")
    }
    func getContactDetail(loader : Bool) {
        WebServices.getContactusDetail(params: [:], loader : loader , success: { (results,contactNo)   in
            print_debug(results)
            self.contactList = []
            let _ = results.arrayValue.map({ (result)  in
                self.contactList.append(ContactUsModel(json: result))
            })
            self.phoneNoBtn.setTitle(contactNo, for: .normal)
            self.contactUsTableView.reloadData()
        }) { (error) -> (Void) in
            print_debug(error.localizedDescription)
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.getContactDetail(loader : false)
    }
}


//MARK:- Prototype Cell
//=========================
class ContactUsCell: UITableViewCell {
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        headingLabel.textColor = AppColors.BlackColor.black12
        seperatorView.backgroundColor = AppColors.Gray.gray208
    }
    
    func populate(with values: ContactUsModel, indexPath: IndexPath) {
        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
        headingLabel.text = values.categoryName
        } else {
            headingLabel.text = values.categoryName_Tn
        }
    }
}

