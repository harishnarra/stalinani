//
//  WelcomeVC.swift
//  IPAC
//
//  Created by Admin on 10/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import FBSDKLoginKit


class WelcomeVC: UIViewController {
    
    //MARK: - Outlets.
    //MARK: -
    
    @IBOutlet weak var signinButton: UIButton!
    
    @IBOutlet weak var signupButton: UIButton!
    
    @IBOutlet weak var gmailButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    
    
    
    //MARK: - Properties.
    //MARK: -
    
    //MARK: - View life cycle.
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Actions.
    //MARK: -
    
    @IBAction func signinButtonTapp(_ sender: UIButton) {
  
        let editProfileVC = EditProfileVC.instantiate(fromAppStoryboard: .Profile)
        self.navigationController?.pushViewController(editProfileVC, animated: true)
 
    }
    
    @IBAction func signupButtonTapp(_ sender: UIButton) {
        
        let signUpVC = SignUpVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(signUpVC, animated: true)
        
    }
    
    @IBAction func facebookButtonTapp(_ sender: UIButton) {

        
        FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result, string) in
            
            print_debug("\(result) \(string)")
            print("Your profile Image Url is... \(String(describing: result.picture!))")
            
            
        }) { (error) in
            print(error?.localizedDescription ?? "")
        }

        
    }
    
    @IBAction func gmailButtonTapp(_ sender: UIButton) {
        GoogleLoginController.shared.login(success: { (model :  GoogleUser) in
            
            print_debug(model)
        })
        { (err : Error) in
            print(err.localizedDescription)
        }

    }
    
}

//MARK: - Private methods.
//MARK: -
private extension WelcomeVC{
    
    func initialSetup(){
        self.view.layoutIfNeeded()
        self.facebookButton.rounded(cornerRadius: 3.0, clip: true)
        self.gmailButton.rounded(cornerRadius: 3.0, clip: true)
        self.signinButton.rounded(cornerRadius: 3.0, clip: true)
        self.signupButton.rounded(cornerRadius: 3.0, clip: true)
        self.facebookButton.dropShadow(color: #colorLiteral(red: 0.2470588235, green: 0.3647058824, blue: 0.5607843137, alpha: 1), opacity: 0.6, offSet: CGSize.zero, radius: 5.0, scale: true)
        self.gmailButton.dropShadow(color: #colorLiteral(red: 0.2470588235, green: 0.3647058824, blue: 0.5607843137, alpha: 1), opacity: 0.6, offSet: CGSize.zero, radius: 5.0, scale: true)
        self.signinButton.dropShadow(color: #colorLiteral(red: 0.2470588235, green: 0.3647058824, blue: 0.5607843137, alpha: 1), opacity: 0.6, offSet: CGSize.zero, radius: 5.0, scale: true)
        self.signupButton.dropShadow(color: #colorLiteral(red: 0.2470588235, green: 0.3647058824, blue: 0.5607843137, alpha: 1), opacity: 0.6, offSet: CGSize.zero, radius: 5.0, scale: true)
        self.view.layoutIfNeeded()
    }
    
}
