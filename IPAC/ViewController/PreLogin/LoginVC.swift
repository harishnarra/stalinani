//
//  LoginVC.swift
//  IPAC
//
//  Created by Admin on 10/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON

enum RemeberBtn {
    case on
    case off
}
var rememberBtn: RemeberBtn = .on
class LoginVC: BaseViewController {

    //MARK: - Outlets.
    //MARK: -
    @IBOutlet weak var mobileTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var passwordButton: GradientButton!
    
    
    @IBOutlet weak var welcomeMsg: UILabel!
    @IBOutlet weak var otpButton: GradientButton!
    
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var newUser: UILabel!
    @IBOutlet weak var rememberMeBtn: UIButton!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var signinButton: GradientButton!
    //@IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var signUpStackView: UIStackView!
    @IBOutlet weak var showButton: UIButton!
    
    //MARK: - Properties.
    //MARK: -
    
    //MARK: - View life cycle.
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextField.text = ""
       // print("Teja", LocalizationSystem.sharedInstance.getLanguage())
   //     print("Tejas", AppUserDefaults.value(forKey: .languagess))
        
        welcomeMsg.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Welcome to stalin ani", comment: "")
        showButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Show", comment: ""), for: .normal)

        signupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Sign_Up", comment: ""), for: .normal)

        newUser.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New_User", comment: "")
        
       // welcomeMsg.text = "Welcome to Stalin Ani"

     //   self.titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Home", comment: "")

//        mobileTextField.text = "9032735525"
//        passwordTextField.text = "12345678"
//    

        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.resignFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // self.view.addBackgroundImage()
       // self.frontView.rounded(5.0)
        
        //==========
//       self.whiteView.rounded(cornerRadius: 5.0, clip: true)
//        self.signinButton.rounded(cornerRadius: 3.0, clip: true)
//        self.signinButton.dropShadow(color: UIColor.init(red: 79/255, green: 114/255, blue: 161/255, alpha:0.3), opacity: 0.25, offSet: CGSize.zero, radius: 10, scale: true)
    }
    
    //MARK: - Actions.
    //MARK: -
    @IBAction func passwordButtonTapped(_ sender: UIButton) {
        frontView.isHidden = true
    }
    
    
    @IBAction func otpButtonTapped(_ sender: UIButton) {
        let enteredNum = mobileTextField.text!
        let result = ValidationController.validatePhone(phone: enteredNum)
        if  result.0{
                let otpScene = OTPVC.instantiate(fromAppStoryboard: .Main)
                otpScene.isRemember  = !self.rememberMeBtn.isSelected
                self.navigationController?.pushViewController(otpScene, animated: true)
        }
        else{
            CommonFunction.showToast(msg: result.1)
        }
       
    }
    
    
    @IBAction func rememberBtnTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isSelected ? (rememberBtn = .off) : (rememberBtn = .on)
        
    }
    
    @IBAction func signinButtonTapp(_ sender: UIButton) {
        let enteredMobile = mobileTextField.text!
        let enteredPassword = passwordTextField.text!
        let result = ValidationController.validateLogin(mobile: enteredMobile , password: enteredPassword )
        if result.0{
            WebServices.loginAPI(enteredMobile, password: enteredPassword, loginType: ApiKeys.password, success: { (result) in
                print(result)
                let user = UserModel(json: result)
                CommonFunction.saveUser(user)

                if user.is_number_verified == "0"{
//                    let otpScene = OTPVC.instantiate(fromAppStoryboard: .Main)
//                    otpScene.isSignup  = true
//                    otpScene.userId = /user.userId
//                    otpScene.isRemember = !self.rememberMeBtn.isSelected
//                    self.navigationController?.pushViewController(otpScene, animated: true)
//                    return
                    let signUpScene = ForgotResetPasswordVC.instantiate(fromAppStoryboard: .Main)
                    signUpScene.screenNum = 3
                    signUpScene.registrationNo = /user.registerationNo
                    signUpScene.userId = /user.userId
                    signUpScene.isRemember = !self.rememberMeBtn.isSelected
                    signUpScene.phoneNum = /user.phoneNumber
                    self.navigationController?.pushViewController(signUpScene, animated: true)
                    return
                }
                if !self.rememberMeBtn.isSelected {
                    CommonFunction.rememberLoginCred()
                }
                
                CommonFunction.gotoHome()
                
            }, failure: { (error) -> (Void) in
                CommonFunction.showToast(msg: error.localizedDescription)
            })

        }
        else{
            CommonFunction.showToast(msg: result.1)
        }
    }

    @IBAction func signupButtonTapp(_ sender: UIButton) {
//        let signUpScene = ForgotResetPasswordVC.instantiate(fromAppStoryboard: .Main)
//        signUpScene.screenNum = 3
//        self.navigationController?.pushViewController(signUpScene, animated: true)
        let registerSceen = RegisterVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(registerSceen, animated: true)

        
        
    }
    
    @IBAction func forgotButtonTapp(_ sender: UIButton) {
        
        let forgotResetPasswordVC = ForgotResetPasswordVC.instantiate(fromAppStoryboard: .Main)
       forgotResetPasswordVC.screenNum = 2
        self.navigationController?.pushViewController(forgotResetPasswordVC, animated: true)
    }
    
    @IBAction func showButtonTap(_ sender: UIButton) {
        if passwordTextField.isSecureTextEntry{
            passwordTextField.isSecureTextEntry = false
            self.showButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Hide", comment: ""), for: .normal)
        }
        else{
            passwordTextField.isSecureTextEntry = true
            self.showButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Show", comment: ""), for: .normal)
        }
    }
    
}

//MARK: - Private methods.
//MARK: -
private extension LoginVC{
    
    func initialSetup(){
        
        super.addImageInBackground()
        
        
        CommonFunction.setAttributesto(currentTextField: passwordTextField)
        CommonFunction.setAttributesto(currentTextField: mobileTextField)
        mobileTextField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mobile_Number", comment: "")
        passwordTextField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password", comment: "")
        signinButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "LOGIN", comment: ""), for: .normal)
       //
        forgotButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Forgot_Password", comment: ""), for: .normal)
        

        self.signUpStackView.isHidden = false
        passwordTextField.delegate = self
        
        mobileTextField.delegate = self
        
        self.whiteView.rounded(cornerRadius: 5.0, clip: true)
        
        self.signinButton.dropShadow(color: UIColor.init(red: 79/255, green: 114/255, blue: 161/255, alpha:0.3), opacity: 1, offSet: CGSize.zero, radius: 10, scale: true)
        self.signinButton.rounded(cornerRadius: 5.0, clip: true)
        self.rememberMeBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Remember_me", comment: ""), for: .normal)
        mobileTextField.keyboardType = .numberPad

    }
    

    
    
//    var checkValidity: Bool {
//        let mobile = self.mobileTextField.text
//        let password = self.passwordTextField.text
//
//        switch (mobile, password) {
//        case let (.some(mobile), .some(password)):
//            if mobile.checkIfInvalid(.mobileNumber) {
//
//                CommonFunction.showToast(msg: "Invalid Mobile number")
//                return false
//
//            } else if password.checkIfInvalid(ValidityExression.password) {
//
//                CommonFunction.showToast(msg: "Password must be 8 characters long")
//                return false
//            }
//            return true
//
//        case (.some(_), .none):
//            CommonFunction.showToast(msg: "Please enter password.")
//            return false
//        default:
//            CommonFunction.showToast(msg: "Please enter mobile number")
//            return false
//
//        }
//
//
//    }
    
    
}


//tex field delegte

extension LoginVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField === passwordTextField{
            self.showButton.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength : Int
        if textField === mobileTextField{
         maxLength = 10
        }
        else{
            maxLength = 30
        }
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
         return newString.length <= maxLength
    }
    
    
  
}
