//
//  UploadIDVC.swift
//  IPAC
//
//  Created by Admin on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class UploadIDVC: UIViewController {
    
    var user : UserModel?
    var selectedIDType = 0
    var idNumber = ""
    var proofType = ""
    let iDDataSource = ["Voter Card", "Aadhar Card", "PAN Card", "Driving Licence"]
    
    @IBOutlet weak var whiteView: UIView!

    
    @IBOutlet weak var selectIDTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var UINumberTextField: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var uploadFileView: UIView!
    @IBOutlet weak var uploadFileLabel: UILabel!
    @IBOutlet weak var continueButton: GradientButton!
    @IBOutlet weak var uploadedIDView: UIView!
    @IBOutlet weak var cancelFileButton: UIButton!
    @IBOutlet weak var fileSizeLabel: UILabel!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var idImageView: UIImageView!

    @IBOutlet weak var cancelUploadedViewButton: UIButton!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var termsConditionsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var whiteViewConstraint: NSLayoutConstraint!
    // MARK: - Properties.
    // MARK: -
    
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        populateUserData()
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.whiteView.rounded(cornerRadius: 5.0, clip: true)
        self.continueButton.rounded(cornerRadius: 5.0, clip: true)
        self.continueButton.dropShadow(color: .shadowColor, opacity: 0.6, offSet: CGSize.zero, radius: 5.0, scale: true)
        
        self.tickButton.setBorder(color: UIColor.init(red: 16.0/255.0, green: 62.0/255.0, blue: 157/255.0, alpha: 1.0), width: 1.5, cornerRadius: 0)
    }
    
    func populateUserData() {
        guard let user = self.user else { return }
        
    }
    
    
    
    
    // MARK: - Actions.
    // MARK: -
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelIDTapped(_ sender: UIButton) {
        
        print("Cancel uploaded ID proof")
        
    }
    
    @IBAction func boxTapped(_ sender: UIButton) {
        if sender.isSelected{
            self.tickButton.setBorder(color: UIColor.init(red: 16.0/255.0, green: 62.0/255.0, blue: 157/255.0, alpha: 1.0), width: 1.5, cornerRadius: 0)
            self.tickButton.setImage(nil, for: .normal)
            sender.isSelected = false
        }
        else{
            self.tickButton.setImage(#imageLiteral(resourceName: "icSignupCheckbox"), for: .normal)
            sender.isSelected = true
        }
    }
    
    
    
    @IBAction func cancelButtonTapp(_ sender: UIButton) {
        //delete proof APi
        print("Stop uploading ID proof")
    }
    
    @IBAction func continueButtonTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        let userSelectedID = selectIDTextField.text!
        let UINumber = UINumberTextField.text!
        if ValidationController.validateUpload(selectedID: userSelectedID, IdNumber: UINumber ).0{
            if tickButton.isSelected{
                 guard let user = user?.userId else { return }
                WebServices.signUpAPI(user, proofType: "\(self.selectedIDType)", proofImage: "1233", idNumber: UINumber, success: { (json) in
                    let user = UserModel(json: json)
                    CommonFunction.saveUser(user)
                    let profileVC = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
                    let sideMenu = SSASideMenu(contentViewController: UINavigationController(rootViewController:profileVC), leftMenuViewController: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC)
                    sideMenu.backgroundImage = #imageLiteral(resourceName: "icSidemenuBg")
                    sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
                    sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
                    sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
                    sideMenu.delegate = UIApplication.shared.delegate as? SSASideMenuDelegate
                    //self.navigationController?.pushViewController(sideMenu, animated: true)
                    let navVc = UINavigationController()
                    navVc.isNavigationBarHidden = true
                    navVc.viewControllers = [sideMenu]
                    UIApplication.shared.keyWindow?.rootViewController = navVc
                    //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    //self.navigationController?.pushViewController(profileVC, animated: true)
                }) { (error) -> (Void) in
                     CommonFunction.showToast(msg: error.localizedDescription)
                }
            }
            else{
                CommonFunction.showToast(msg: "Please accept the Terms & Conditions")
            }
        }
        
        
        
    }
    
    @IBAction func uploadFileButtonTappp(_ sender: UIButton) {
        
        self.captureImage(on: self)
        
    }
    
}
// MARK: - Private methods.
// MARK: -
private extension UploadIDVC{
    
    func initialSetup(){
        
        super.addImageInBackground()
       
        CommonFunction.setAttributesto(currentTextField: selectIDTextField)
        CommonFunction.setAttributesto(currentTextField: UINumberTextField)
        
        self.idImageView.setBorder(color: #colorLiteral(red: 0.3098039216, green: 0.4470588235, blue: 0.631372549, alpha: 0.3), width: 1.0, cornerRadius: 8.0)
        self.uploadedIDView.setBorder(color: #colorLiteral(red: 0.7294117647, green: 0.7725490196, blue: 0.8392156863, alpha: 0.45), width: 1.0, cornerRadius: 8.0)
        self.continueButton.rounded(cornerRadius: 5.0, clip: true)
        
        let attributedString = NSMutableAttributedString(string: "I accept TERMS & CONDITIONS", attributes: [
            .font: UIFont(name: "ProximaNova-Semibold", size: 11.0)!,
            .foregroundColor: UIColor(red: 16.0 / 255.0, green: 62.0 / 255.0, blue: 157.0 / 255.0, alpha: 1.0)
            ])
        attributedString.addAttributes([
            .font: UIFont(name: "ProximaNova-Regular", size: 11.0)!,
            .foregroundColor: UIColor(red: 18.0 / 255.0, green: 39.0 / 255.0, blue: 82.0 / 255.0, alpha: 1.0)
            ], range: NSRange(location: 0, length: 8))
        
        self.termsConditionsLabel.attributedText = attributedString
        
        self.view.layoutIfNeeded()
        self.uploadedIDView.isHidden = true
        setPickerInputForTextField(selectIDTextField)
    }
    
    func setPickerInputForTextField(_ field: SkyFloatingLabelTextField) {
        let picker = UIPickerView()
        field.inputView = picker
        picker.delegate = self
        picker.dataSource = self
    }
}

//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension UploadIDVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            self.idImageView.contentMode = .scaleAspectFill
            self.idImageView.clipsToBounds = true
            self.idImageView.image = imagePicked
            self.uploadFileView.isHidden = true
            self.whiteViewConstraint.constant = 150
            self.descriptionLabel.isHidden = true
            
            self.tickButton.isHidden = true
            
            self.termsConditionsLabel.isHidden = true
            self.uploadedIDView.isHidden = false
            picker.dismiss(animated: true, completion: nil)
            //            self.uploadImage(image: imagePicked)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension UploadIDVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return iDDataSource.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return iDDataSource[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedIDType = row+1
        self.idNumber = iDDataSource[row]
        self.proofType = iDDataSource[row]
        
        selectIDTextField.text = iDDataSource[row]
    }
    
}
