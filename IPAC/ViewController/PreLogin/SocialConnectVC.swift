//
//  SocialConnectVC.swift
//  IPAC
//
//  Created by Admin on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SocialConnectVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var skifAfterSelectButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var googleLabel: UILabel!
    @IBOutlet weak var fbLabel: UILabel!
    @IBOutlet weak var twitterLabel: UILabel!
    @IBOutlet weak var skipInitialButton: UIButton!
    @IBOutlet weak var googleImage: UIImageView!
    @IBOutlet weak var fbImage: UIImageView!
    @IBOutlet weak var twitterImage: UIImageView!
    @IBOutlet weak var selecteSocialBgView: UIView!
    @IBOutlet weak var googleSelectedImage: UIImageView!
    @IBOutlet weak var fbSelectedImage: UIImageView!
    @IBOutlet weak var twitterSelectedImage: UIImageView!
    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet weak var buttonStackViewWidth: NSLayoutConstraint!
    
    
    // MARK: - Properties.
    // MARK: -
    
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions.
    // MARK: -
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skipButtonTapp(_ sender: UIButton) {
        
    }
    
    @IBAction func googleButtonTapp(_ sender: UIButton) {
        self.selectedSocialMedia(1)
    }
    
    @IBAction func fbButtonTapp(_ sender: UIButton) {
        
        self.selectedSocialMedia(2)
    }
    
    @IBAction func twitterButtonTapp(_ sender: UIButton) {
        
        self.selectedSocialMedia(3)
    }
    
}

// MARK: - Private metods.
// MARK: -

private extension SocialConnectVC{
    
    func initialSetup(){
        super.addImageInBackground()
        self.googleButton.setBorder(color: AppColors.borderColor, width: 1.0, cornerRadius: self.googleButton.frame.size.height/2)
        self.fbButton.setBorder(color: AppColors.borderColor, width: 1.0, cornerRadius: self.fbButton.frame.size.height/2)

        self.twitterButton.setBorder(color: AppColors.borderColor, width: 1.0, cornerRadius: self.twitterButton.frame.size.height/2)

        self.selecteSocialBgView.isHidden = true
    }
    
    func selectedSocialMedia(_ totalSelected: Int){
        self.selecteSocialBgView.isHidden = false
        switch totalSelected {
        case 1:
            self.googleButton.isHidden = true
            self.buttonStackViewWidth.constant = self.buttonStackView.frame.width -  (self.buttonStackView.frame.width/3)
            self.fbLabel.isHidden = true
            self.twitterLabel.isHidden = true
            self.fbSelectedImage.isHidden = true
            self.twitterSelectedImage.isHidden = true
            self.selectedButton.setTitle("1 MORE TO GO!", for: .normal)
        case 2:
            self.googleButton.isHidden = true
            self.fbButton.isHidden = true
            self.buttonStackViewWidth.constant = self.buttonStackView.frame.width - 180
            self.fbLabel.isHidden = false
            self.fbSelectedImage.isHidden = false
            self.twitterLabel.isHidden = true
            self.twitterSelectedImage.isHidden = true
             self.selectedButton.setTitle("2 MORE TO GO!", for: .normal)
        default:
            self.buttonStackView.isHidden = true
            self.fbLabel.isHidden = false
            self.twitterLabel.isHidden = false
            self.fbSelectedImage.isHidden = false
            self.twitterSelectedImage.isHidden = false
            self.selectedButton.setTitle("3 MORE TO GO!", for: .normal)
        }
    }
}


