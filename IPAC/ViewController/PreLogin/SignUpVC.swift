//
//  SignUpVC.swift
//  IPAC
//
//  Created by Appinventiv on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    
    // MARK:- VARIABLES
    //====================
    
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var alreadyHvAcntLabel: UILabel!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    
    @IBOutlet weak var showButton: UIButton!
    
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.signUpButton.dropShadow(color: .shadowColor, opacity: 0.6, offSet: CGSize.zero, radius: 7.0, scale: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func signUpBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let name = nameTextField.text
        let email = emailTextField.text
        let passwrd = passwordTextField.text
        let mobileNum = mobileTextField.text
        let result = ValidationController.validateSignUpFields(name: name!, email: email!,password: passwrd!, mobileNum: mobileNum!)
        if result.0 {
            print("Sign up complete")
        }
        else{
            print(result.1)
        }
        
    }
    
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func profileButtonTapp(_ sender: UIButton) {
        
        self.captureImage(on: self)
    }
    
    @IBAction func countryCodeButtonTapped(_ sender: UIButton){
        print("Selecting country code")
    }
    
    
    @IBAction func signInBtnTapped(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func showButtonTapped(_ sender: UIButton) {
        if passwordTextField.isSecureTextEntry{
            showButton.setTitle(AppStrings.Hide.rawValue.localized, for: .normal)
            passwordTextField.isSecureTextEntry = false
        }
        else{
            showButton.setTitle(AppStrings.Show.rawValue.localized, for: .normal)
            passwordTextField.isSecureTextEntry = true
            
        }
    }
}


// MARK := Text field delegate
extension SignUpVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField{
        case nameTextField:
            emailTextField.becomeFirstResponder()
            nameTextField.resignFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
            emailTextField.resignFirstResponder()
        case passwordTextField:
            mobileTextField.becomeFirstResponder()
            passwordTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}

// MARK:- FUNCTIONS
//====================
extension SignUpVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {

        super.addImageInBackground()
        self.signUpButton.rounded(cornerRadius: 3.0, clip: true)
        self.nameLabel.text = AppStrings.name.rawValue.localized
        self.emailLabel.text = AppStrings.email.rawValue.localized
        self.passwordLabel.text = AppStrings.password.rawValue.localized
        self.alreadyHvAcntLabel.text = AppStrings.alreadyHvAcnt.rawValue.localized
        self.mobileLabel.text = AppStrings.mobile.rawValue.localized
        self.signInButton.setTitle(AppStrings.SIGNIN.rawValue.localized, for: .normal)
        self.signUpButton.setTitle(AppStrings.SIGNUP.rawValue.localized, for: .normal)
        
        self.nameTextField.delegate = self
        self.emailTextField.delegate = self
        self.mobileTextField.delegate = self
        self.passwordTextField.delegate = self
        self.showButton.setTitle(AppStrings.Show.rawValue.localized, for: .normal)
        
        nameTextField.returnKeyType = .next
        emailTextField.returnKeyType = .next
        passwordTextField.returnKeyType = .next
        mobileTextField.returnKeyType = .done
        passwordTextField.isSecureTextEntry = true
        mobileTextField.keyboardType = .numberPad
        emailTextField.keyboardType = .emailAddress
        
        
        self.profileImageView.rounded(cornerRadius: self.profileImageView.frame.height/2, clip: true)
    }
}

//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension SignUpVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            self.profileImageView.contentMode = .scaleAspectFill
            self.profileImageView.clipsToBounds = true
            self.profileImageView.image = imagePicked
            picker.dismiss(animated: true, completion: nil)
//            self.uploadImage(image: imagePicked)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
