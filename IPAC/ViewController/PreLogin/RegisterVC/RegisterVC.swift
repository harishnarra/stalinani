//
//  RegisterVC.swift
//  IPAC
//
//  Created by macOS on 21/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

enum Gender: Int {
    case male   = 1
    case female = 2
    case other  = 3
}



class RegisterVC: BaseVC {
    
    //MARK:- Properties
    //==================
    private var gender: Gender = .male
    var rowsInWhatsAppSection = 1
    private var rowsInAreYouCollecgeSection = 0
    private var stateList = [StateModel]()
    private var collegeStudent = [StringConstant.YES.localized, StringConstant.NO.localized]
    private var signupDict = [String: String]()
    private var districtList = [DistrictModel]()
    private var registrationNo = ""
    private var userId  = ""
    private var phoneNum = ""
    
    //  private var districtId = ""
    // private var stateName = [String]()
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var registerTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    //MARK:- IBActions
    //================
    @IBAction func backBtnTapp(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Targets
    //================
    
    @objc func addProfileImageTap(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let cell = sender.tableViewCell as? ProfilePictureCell else { return }
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected  {
            // self.view.isUserInteractionEnabled = false
            //cell.activityIndicator.startAnimating()
            self.captureImage(on: self)
        } else {
            cell.activityIndicator.stopAnimating()
            
        }
    }
    
    @objc func whatsAppBtnTap(_ sender: UIButton) {
        rowsInWhatsAppSection == 1 ? (rowsInWhatsAppSection = 0) : (rowsInWhatsAppSection = 1)
        //        registerTableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        self.registerTableView.reloadData()
        
        
    }
    @objc func showPasswordBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let cell = sender.tableViewCell as? EmailTextFieldCell else { return }
        sender.isSelected = !sender.isSelected
        cell.textField.isSecureTextEntry = !cell.textField.isSecureTextEntry
    }
    
    @objc func maleBtnTap(_ sender: UIButton) {
        if sender.isSelected {
            return
        }
        self.gender = .male
        registerTableView.reloadData()
    }
    
    @objc func femaleBtnTap(_ sender: UIButton) {
        if sender.isSelected {
            return
        }
        self.gender = .female
        registerTableView.reloadData()
        
    }
    
    @objc func otherBtnTap(_ sender: UIButton) {
        if sender.isSelected {
            return
        }
        
        self.gender = .other
        registerTableView.reloadData()
    }
    
    @objc func collegeStudentTxtFldDidBegin(_ textField: UITextField) {
        print_debug("Tap TextField")
        
        MultiPicker.noOfComponent = 1
        MultiPicker.openMultiPickerIn(textField, firstComponentArray: self.collegeStudent, secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { (data, response, index) in
            
            if data == StringConstant.YES {
                self.signupDict[ApiKeys.areYouClgStudent] = "1"
            } else {
                self.signupDict[ApiKeys.areYouClgStudent] = "2"
            }
            self.rowsInAreYouCollecgeSection = 1
            self.signupDict.removeValue(forKey: ApiKeys.state)
            self.signupDict.removeValue(forKey: ApiKeys.stateName)
            
            //            self.registerTableView.reloadSections(IndexSet(integer: 2), with: .automatic)
            self.registerTableView.reloadData()
            
        }
    }
    
    @objc func stateTextFldEditingBegin(_ textField: UITextField) {
        // self.view.endEditing(true)
        //        textField.resignFirstResponder()
        
    }
    
    @objc func registerBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.signupDict[ApiKeys.gender] = String(self.gender.rawValue)
        if validation {
            registerUser()
        }
    }
    
    @objc func termsCheckBocBtnTapped(_ sender: UIButton) {
        // self.view.endEditing(true)
        sender.isSelected = !sender.isSelected
        sender.isSelected ? (signupDict[ApiKeys.acceptTermNdCondition] = "1") : (signupDict[ApiKeys.acceptTermNdCondition] = "0")
    }
    
    @objc func openTermsConditions(_ sender: UITapGestureRecognizer) {
        let termsSceen = TemsAndCondition.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.presentVC(termsSceen)
    }
    
    // Validate Data
    var validation: Bool {  //= { (dictionary, rowsInWhatsAppSection) in
        
        //
        //                let name = ValidationController.validateName(name: signupDict[ApiKeys.fullName] ?? "")
        //                if name.0 == false {
        //                    CommonFunction.showToast(msg: name.1)
        //                    return false
        //                }
        
        let fullName = signupDict[ApiKeys.fullName] ?? ""
        if fullName.count < 2 {
            CommonFunction.showToast(msg: StringConstant.Enter_Your_Correct_Full_Name.localized)
            return false
        }
        
        if signupDict[ApiKeys.dob] == nil {
            CommonFunction.showToast(msg: StringConstant.Please_Enter_Dob.localized)
            return false
        }
        
        let email = ValidationController.validateEmail(email: signupDict[ApiKeys.emailId] ?? "")
        if !email.0 {
            CommonFunction.showToast(msg: email.1)
            return false
        }
        let password = ValidationController.validatePassword(password: signupDict[ApiKeys.password] ?? "")
        if !password.0{
            CommonFunction.showToast(msg: password.1)
            return false
        }
        
        let phoneNo = ValidationController.validatePhone(phone: signupDict[ApiKeys.phoneNumber] ?? "")
        if !phoneNo.0 {
            CommonFunction.showToast(msg: phoneNo.1)
            return false
        }
        // check whats app same as mobile no un checked
        if rowsInWhatsAppSection == 1 {
            let whatsAppNo = ValidationController.validatePhone(phone: signupDict[ApiKeys.whatsappNo] ?? "")
            if !whatsAppNo.0 {
                CommonFunction.showToast(msg: whatsAppNo.1)
                return false
            }
        } else {
            //signupDict.removeValue(forKey: ApiKeys.whatsappNo)
        }
        if signupDict[ApiKeys.areYouClgStudent] == nil {
            CommonFunction.showToast(msg: StringConstant.Plese_Select_are_you_college_student.localized)
            return false
        }
        
        if signupDict[ApiKeys.state] == nil {
            CommonFunction.showToast(msg: StringConstant.Please_select_state.localized)
            return false
        }
        
        if signupDict[ApiKeys.district] == nil {
            CommonFunction.showToast(msg: StringConstant.Please_select_district.localized)
            return false
        }
        
        //        if signupDict[ApiKeys.college] == nil {
        //            CommonFunction.showToast(msg: StringConstant.Please_enter_your_college_name.localized)
        //            return false
        //        }
        
        if signupDict[ApiKeys.acceptTermNdCondition] == nil || signupDict[ApiKeys.acceptTermNdCondition] == "0" {
            CommonFunction.showToast(msg: StringConstant.Please_accept_Terms.localized)
            return false
        }
        
        return true
    }
}
//MARK:-
//MARK:- Table view Delegate and DataSource
extension RegisterVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 6
        } else if section == 1 {
            return rowsInWhatsAppSection
            
        } else if section == 2  {
            return rowsInAreYouCollecgeSection
        } else if section == 3 || section == 4 || section == 5 {
            return 1
            
        }   else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "RoundHeaderViewTableViewCell") as! RoundHeaderViewTableViewCell
            return footer
        } else if section == 1{
            let whatsNoView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "WhatsNoHeaderView") as! WhatsNoHeaderView
            rowsInWhatsAppSection == 1 ? ( whatsNoView.whatsNoCheckBox.isSelected = false) : ( whatsNoView.whatsNoCheckBox.isSelected = true)
            whatsNoView.whatsNoCheckBox.addTarget(self, action: #selector(whatsAppBtnTap(_:)), for: .touchUpInside)
            return whatsNoView
        } else if section == 2{
            
            let collegeStudentView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CollegeStudentHeaderView") as! CollegeStudentHeaderView
            CommonFunction.setAttributesto(currentTextField: collegeStudentView.collegeStudentTextFld)
            collegeStudentView.collegeStudentTextFld.placeholder = StringConstant.Are_you_a_College.localized
            collegeStudentView.collegeStudentTextFld.addTarget(self, action: #selector(collegeStudentTxtFldDidBegin(_:)), for: .editingDidBegin)
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: CGFloat(collegeStudentView.collegeStudentTextFld.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
            button.titleLabel?.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(11.0)
            button.setTitleColor(AppColors.Gray.gray166, for: .normal)
            button.setImage(#imageLiteral(resourceName: "icHomeDropdown"), for: .normal)
            button.backgroundColor = UIColor.black
            collegeStudentView.collegeStudentTextFld.rightOffset(25)
            
            collegeStudentView.collegeStudentTextFld.rightView = button
            collegeStudentView.collegeStudentTextFld.rightViewMode = .always
            if signupDict[ApiKeys.areYouClgStudent] == "1" {
                collegeStudentView.collegeStudentTextFld.text = StringConstant.YES.localized
            } else if signupDict[ApiKeys.areYouClgStudent] == "2" {
                collegeStudentView.collegeStudentTextFld.text = StringConstant.NO.localized
            }
            
            //  collegeStudentView.collegeStudentTextFld.delegate = self
            return collegeStudentView
        } else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 5 {
            let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SocialFoterView") as! SocialFoterView
            footer.leadingConstraint.constant = 0
            footer.trailingConstraint.constant = 0
            return footer
        } else {
            return UIView()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePictureCell") as? ProfilePictureCell else { fatalError("invalid cell \(self)")
                }
                CommonFunction.setAttributesto(currentTextField: cell.fullNameTextField)
                cell.fullNameTextField.placeholder = StringConstant.Full_Name.localized
                cell.fullNameTextField.text = signupDict[ApiKeys.fullName]
                cell.profileImage.kf.setImage(with: URL(string : /signupDict["user_image"]), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
                cell.addProfileImageBtn.addTarget(self, action: #selector(addProfileImageTap(_:)), for: .touchUpInside)
                cell.fullNameTextField.delegate = self
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell") as? GenderCell else { fatalError("invalid cell \(self)")
                }
                
                cell.maleButton.isSelected = false
                cell.femaleBtn.isSelected = false
                cell.otherbtn.isSelected = false
                if self.gender == .male {
                    cell.maleButton.isSelected = true
                    
                } else if self.gender == .female {
                    cell.femaleBtn.isSelected = true
                    
                } else {
                    cell.otherbtn.isSelected = true
                }
                cell.maleButton.addTarget(self, action: #selector(maleBtnTap(_:)), for: .touchUpInside)
                cell.femaleBtn.addTarget(self, action: #selector(femaleBtnTap(_:)), for: .touchUpInside)
                cell.otherbtn.addTarget(self, action: #selector(otherBtnTap(_:)), for: .touchUpInside)
                
                return cell
            case 2...5:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else { fatalError("invalid cell \(self)")
                }
                switch indexPath.row {
                case 2:
                    cell.textField.placeholder = StringConstant.Date_Of_Birth.localized + " (dd/mm/yyyy) "
                    let button = UIButton(type: .custom)
                    button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                    button.titleLabel?.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(11.0)
                    button.setTitleColor(AppColors.Gray.gray166, for: .normal)
                    button.setImage(#imageLiteral(resourceName: "icHomeCalender"), for: .normal)
                    cell.textField.rightView = button
                    cell.textField.rightViewMode = .always
                    //                    button.addTarget(self, action: #selector(self.showPasswordBtnTapped(_:)), for: .touchUpInside)
                    CommonFunction.setAttributesto(currentTextField: cell.textField)
                    cell.textField.text = signupDict[ApiKeys.displayDate]
                    cell.textField.isSecureTextEntry = false
                    
                case 3:
                    cell.textField.placeholder = StringConstant.Email_Id.localized.uppercased()
                    CommonFunction.setAttributesto(currentTextField: cell.textField)
                    cell.textField.text = signupDict[ApiKeys.emailId]
                    cell.textField.rightViewMode = .never
                    cell.textField.keyboardType = UIKeyboardType.emailAddress
                    cell.textField.isSecureTextEntry = false
                case 4:
                    cell.textField.placeholder = StringConstant.Password.localized
                    cell.textField.isSecureTextEntry = true
                    cell.textField.text = self.signupDict[ApiKeys.password]
                    cell.textField.keyboardType = UIKeyboardType.default
                    
                    CommonFunction.setAttributesto(currentTextField: cell.textField)
                    let button = UIButton(type: .custom)
                    button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 30), y: CGFloat(5), width: CGFloat(30), height: CGFloat(25))
                    button.titleLabel?.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(11.0)
                    button.setTitleColor(AppColors.BlackColor.black12, for: .normal)
                    button.setTitle(StringConstant.Show.localized, for: .normal)
                    button.setTitle(StringConstant.Hide.localized, for: .selected)
                    cell.textField.rightView = button
                    cell.textField.rightViewMode = .always
                    button.addTarget(self, action: #selector(self.showPasswordBtnTapped(_:)), for: .touchUpInside)
                    
                case 5:
                    cell.textField.placeholder = StringConstant.Mobile_Number.localized.uppercased()
                    cell.textField.keyboardType = .numberPad
                    CommonFunction.setAttributesto(currentTextField: cell.textField)
                    cell.textField.rightViewMode = .never
                    
                    cell.textField.isSecureTextEntry = false
                    cell.textField.text = signupDict[ApiKeys.phoneNumber]
                default:
                    fatalError("invalid email cell EmailTextFieldCell")
                }
                cell.textField.delegate = self
                
                return cell
            default:
                fatalError("invalid cell")
            }
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonFunction.setAttributesto(currentTextField: cell.textField)
            cell.textField.placeholder = StringConstant.WhatsApp.localized.uppercased()
            cell.textField.keyboardType = .numberPad
            cell.textField.isSecureTextEntry = false
            cell.textField.rightViewMode = .never
            cell.textField.delegate = self
            cell.textField.text = signupDict[ApiKeys.whatsappNo]
            return cell
            
        case 2:
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else { fatalError("invalid cell \(self)")
                }
                CommonFunction.setAttributesto(currentTextField: cell.textField)
                if signupDict[ApiKeys.areYouClgStudent] == "1" {
                    cell.textField.placeholder = StringConstant.CollegeState.localized
                } else {
                    cell.textField.placeholder = StringConstant.HomeState.localized
                }
                
                cell.textField.text = signupDict[ApiKeys.stateName]
                
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                button.titleLabel?.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(11.0)
                button.setTitleColor(AppColors.Gray.gray166, for: .normal)
                button.setImage(#imageLiteral(resourceName: "icHomeDropdown"), for: .normal)
                button.backgroundColor = UIColor.black
                cell.textField.rightView = button
                cell.textField.rightOffset(25)
                cell.textField.rightViewMode = .always
                cell.textField.delegate = self
                cell.textField.isSecureTextEntry = false
                return cell
            } else if indexPath.row == 1{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else { fatalError("invalid cell \(self)")
                }
                CommonFunction.setAttributesto(currentTextField: cell.textField)
                if self.signupDict[ApiKeys.areYouClgStudent] == "1"{
                    cell.textField.placeholder = StringConstant.College_District.localized
                    
                }else{
                    cell.textField.placeholder = StringConstant.Current_District.localized
                    
                }
                
                cell.textField.text = signupDict[ApiKeys.districtName]
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                button.titleLabel?.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(11.0)
                button.setTitleColor(AppColors.Gray.gray166, for: .normal)
                button.setImage(#imageLiteral(resourceName: "icHomeDropdown"), for: .normal)
                button.backgroundColor = UIColor.black
                cell.textField.rightOffset(25)
                
                cell.textField.isSecureTextEntry = false
                cell.textField.rightView = button
                cell.textField.rightViewMode = .always
                cell.textField.delegate = self
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else { fatalError("invalid cell \(self)")
                }
                CommonFunction.setAttributesto(currentTextField: cell.textField)
                cell.textField.placeholder = StringConstant.College_Name.localized
                cell.textField.text = signupDict[ApiKeys.collegeName]
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: CGFloat(cell.textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                button.titleLabel?.font = AppFonts.Proxima_Nova_Rg_Regular.withSize(11.0)
                button.setTitleColor(AppColors.Gray.gray166, for: .normal)
                button.setImage(#imageLiteral(resourceName: "icHomeDropdown"), for: .normal)
                button.backgroundColor = UIColor.black
                cell.textField.rightOffset(25)
                cell.textField.isSecureTextEntry = false
                cell.textField.rightView = button
                cell.textField.rightViewMode = .always
                cell.textField.delegate = self
                return cell
            }
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonFunction.setAttributesto(currentTextField: cell.textField)
            cell.textField.placeholder = StringConstant.Referal_Code_Optional.localized
            cell.textField.text = signupDict[ApiKeys.referalCode]
            cell.textField.isSecureTextEntry = false
            cell.textField.keyboardType = UIKeyboardType.default
            
            cell.textField.delegate = self
            cell.textField.rightViewMode = .never
            return cell
            
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TermsAndConditionCell") as? TermsAndConditionCell else { fatalError("invalid cell \(self)")
            }
            cell.checkBoxbtn.addTarget(self, action: #selector(termsCheckBocBtnTapped(_:)), for: .touchUpInside)
            let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.openTermsConditions(_:)))
            cell.acceptTermLabel.isUserInteractionEnabled = true
            cell.acceptTermLabel.addGestureRecognizer(tapAction)
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as? ButtonCell else { fatalError("invalid cell \(self)")
            }
            cell.submitBtn.addTarget(self, action: #selector(registerBtnTapped(_:)), for: .touchUpInside)
            cell.submitBtn.setTitle(StringConstant.Register.localized.uppercased(), for: .normal)
            
            return cell
        default:
            fatalError("invalid section in RegisterVC")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 12
        case 1:
            return 35
        case 2:
            return self.signupDict[ApiKeys.areYouClgStudent] == "2" ? 80 : 47
        default:
            return 0.00001
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 5 {
            return 24
        } else {
            return 0.00001
        }
        
    }
}

//MARK:- Private Methods
//=====================
extension RegisterVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.registerTableView ) else { return false }
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0 : // first name text field
                if (textField.text ?? "").count == 0, string == " " {
                    return false
                } else if (textField.text ?? "").count - range.length < 30 {
                    
                    let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                    let filtered = string.components(separatedBy: cs).joined(separator: "")
                    
                    return (string == filtered)
                    
                } else {
                    return false
                }
            case 3:
                if string == " " {
                    return false
                } else  {
                    
                    return (textField.text ?? "").count - range.length < 50
                }
            case 4: // password text field
                if  string == " " {
                    return false
                } else  {
                    
                    return (textField.text ?? "").count - range.length < 15
                }
            case 5:  //phone number text field
                if  string == " " {
                    return false
                } else  {
                    
                    return (textField.text ?? "").count - range.length < 10
                }
            default:
                return true
            }
            
        case 1:  //whats text field
            if string == " " {
                return false
            } else  {
                
                return (textField.text ?? "").count - range.length < 10
            }
            
        default:
            return true
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.registerTableView) else {
            return false
            
        }
        textField.inputView = nil
        textField.inputAccessoryView = nil
        textField.reloadInputViews()
        if indexPath.row == 4 && indexPath.section == 0{
            return true
        }
        
        if indexPath.row == 2 && indexPath.section == 0{
            self.setStartDatePicker(textField: textField)
            return true
        }
            
        else if (indexPath.section == 2  && indexPath.row == 0) { // Are you college student section
            let stateName = self.stateList.map({$0.stateName})
            //            let stateName = self.stateList.map({ (lists) -> String in
            //                return lists.stateName
            //            })
            print_debug(stateName.count)
            MultiPicker.noOfComponent = 1
            MultiPicker.openMultiPickerIn(textField, firstComponentArray: stateName, secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { (data, response, index) in
                
                self.signupDict.removeValue(forKey: ApiKeys.district)
                self.signupDict.removeValue(forKey: ApiKeys.districtName)
                self.signupDict[ApiKeys.stateName] = data
                self.signupDict[ApiKeys.state]     = self.stateList[index].stateId
                // self.registerTableView.reloadRows(at: [indexPath], with: .automatic)
                self.rowsInAreYouCollecgeSection = 2
                //self.registerTableView.reloadSections(IndexSet(integer: 2), with: .bottom)
                self.registerTableView.reloadData()
                self.getDistrictList(stateCode: self.stateList[index].stateId)
            }
            
            return true
        }
            
        else if (indexPath.section == 2  && indexPath.row == 1){
            let districtName = self.districtList.map({ $0.districtName})
            //            let districtName = self.districtList.map({ (lists) -> String in
            //                return lists.districtName
            //            })
            print_debug(districtName.count)
            MultiPicker.noOfComponent = 1
            MultiPicker.openMultiPickerIn(textField, firstComponentArray: districtName, secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { (data, response, index) in
                
                self.signupDict.removeValue(forKey: ApiKeys.college)    // district cell
                self.signupDict.removeValue(forKey: ApiKeys.collegeName)
                self.signupDict[ApiKeys.districtName] = data
                self.signupDict[ApiKeys.district] = self.districtList[index].districtId
                if self.signupDict[ApiKeys.areYouClgStudent] == "1" {
                    self.rowsInAreYouCollecgeSection = 3
                } else {
                    self.rowsInAreYouCollecgeSection = 2
                }
                
                //self.registerTableView.reloadSections(IndexSet(integer: 2), with: .bottom)
                self.registerTableView.reloadData()
                
            }
            return true
        }  else if (indexPath.section == 2  && indexPath.row == 2) {
            let collegeSceen = ChooseCollegeVC.instantiate(fromAppStoryboard: .Main)
            collegeSceen.delegate = self
            collegeSceen.destrictId = signupDict[ApiKeys.district] ?? ""
            collegeSceen.modalTransitionStyle = .crossDissolve
            collegeSceen.modalPresentationStyle = .overCurrentContext
            self.present(collegeSceen, animated: true, completion: nil)
            return false
        }
        else {
            return true
        }
    }
    
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //        guard let indexPath = textField.tableViewIndexPath(self.registerTableView) else { return }
    //        if indexPath == [2, 0] {
    //            getStateList(textField: textField)
    //        }
    //    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let indexPath = textField.tableViewIndexPath(self.registerTableView) else { return }
        
        if indexPath.row == 0 && indexPath.section == 0{
            signupDict[ApiKeys.fullName] = textField.text
        }else if indexPath.row == 3 && indexPath.section == 0{
            signupDict[ApiKeys.emailId] = textField.text
        }else if indexPath.row == 4 && indexPath.section == 0{
            signupDict[ApiKeys.password] = textField.text
        }else if indexPath.row == 5 && indexPath.section == 0{
            signupDict[ApiKeys.phoneNumber] = textField.text
        }else if indexPath.row == 0 && indexPath.section == 1{
            signupDict[ApiKeys.whatsappNo] = textField.text
        }else if indexPath.row == 2 && indexPath.section == 2{
            signupDict[ApiKeys.collegeName] = textField.text
        }else if indexPath.row == 0 && indexPath.section == 3{
            signupDict[ApiKeys.referalCode] = textField.text
        }
    }
    
}
//MARK:- Private Methods
//=====================
private extension RegisterVC {
    
    func getStateList()  {
        
        WebServices.getStateList(params: [:], success: { (results) in
            
            self.stateList.removeAll()
            let _ =   results.arrayValue.map({ (result)  in
                self.stateList.append(StateModel(dict: result))
            })
            print_debug(self.stateList.count)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    func registerUser()  {
        print_debug(self.signupDict)
        var tempDict = signupDict
        tempDict.removeValue(forKey: ApiKeys.collegeName)
        tempDict.removeValue(forKey: ApiKeys.stateName)
        tempDict.removeValue(forKey: ApiKeys.districtName)
        tempDict.removeValue(forKey: ApiKeys.displayDate)
        tempDict.removeValue(forKey: ApiKeys.acceptTermNdCondition)
        if /tempDict[ApiKeys.isNewCollege] != "0"{
            tempDict[ApiKeys.isNewCollege] = "1"
        }
        if /tempDict[ApiKeys.whatsappNo] == ""{
            tempDict[ApiKeys.whatsappNo] = tempDict[ApiKeys.phoneNumber]
        }
        
        AlertsClass.shared.showAlertController(withTitle: "", message: "We will be verifying the phone number:\n +91 \(/tempDict[ApiKeys.phoneNumber]?.description)\nIs this OK, or would you like to edit the number?", buttonTitles: ["EDIT" , "OK"]) { (tag) in
            switch tag as AlertTag{
            case .yes:
                return
            case .no:
                WebServices.registerUser(params: tempDict, success: { (results) in
                    self.userId = results["user_id"].stringValue
                    self.phoneNum = results[ApiKeys.phone_Number].stringValue
                    print_debug(self.userId)
                    let registrationSceen = RegistrationNoPopupVC.instantiate(fromAppStoryboard: .Main)
                    registrationSceen.modalTransitionStyle = .crossDissolve
                    registrationSceen.modalPresentationStyle = .overCurrentContext
                    registrationSceen.delegate = self
                    registrationSceen.registrationNo = results[ApiKeys.registeration_no].stringValue
                    self.registrationNo = results[ApiKeys.registeration_no].stringValue
                    self.present(registrationSceen, animated: true, completion: nil)
                    
                }) { (error) -> (Void) in
                    CommonFunction.showToast(msg: error.localizedDescription)
                }
                break
            case .done:
                break
            }
        }
        
        
    }
    
    func getDistrictList(stateCode: String){
        
        WebServices.getDistrictList(params: [ApiKeys.stateCode: stateCode], success: { (results) in
            print_debug(results)
            //
            self.districtList.removeAll()
            let _ =   results.arrayValue.map({ (result)  in
                self.districtList.append(DistrictModel(dict: result))
            })
            print_debug(self.districtList.count)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    func initialSetup() {
        
        titleLabel.text = StringConstant.Sign_Up.localized
        titleLabel.font = AppFonts.ProximaNova_Bold.withSize(18)
        registerTableView.delegate      = self
        registerTableView.dataSource    = self
        registerXib()
        self.getStateList()
    }
    
    func registerXib() {
        
        registerTableView.register(UINib(nibName: "SocialFoterView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "SocialFoterView")
        registerTableView.register(UINib(nibName: "WhatsNoHeaderView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "WhatsNoHeaderView")
        registerTableView.register(UINib(nibName: "CollegeStudentHeaderView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "CollegeStudentHeaderView")
        registerTableView.register(UINib(nibName: "RoundHeaderViewTableViewCell", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "RoundHeaderViewTableViewCell")
        registerTableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
    }
    
    
    func setStartDatePicker(textField: UITextField) {
        
        _ = PKDatePicker.openDateMaxPickerIn(textField,
                                             outPutFormate: EventDateFormat,
                                             mode: .date, minimumDate: dateManager.getMinimumStartDateAndTime(), maximumDate:
            dateManager.getMaximumStartDateAndTime(), minuteInterval: 1, selectedDate: Date(), doneBlock: { (selectStr) in
                print_debug(selectStr)
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                guard let date = formatter.date(from: selectStr) else { return }
                let dateInString = date.toString(dateFormat: "MM/dd/yyyy")
                let dateInStringToShow = date.toString(dateFormat: "dd/MM/yyyy")
                print_debug(dateInString)
                textField.text = dateInStringToShow
                self.signupDict[ApiKeys.displayDate] = dateInStringToShow
                self.signupDict[ApiKeys.dob] = dateInString
                self.registerTableView.reloadData()
        })
    }
    
    
}

//MARK:-
//MARK:- UpdateCollege And OpenForgotPassVC  Protocol
extension RegisterVC: UpdateCollege, OpenForgotPassVC{
    func openVC() {
        let signUpScene = ForgotResetPasswordVC.instantiate(fromAppStoryboard: .Main)
        signUpScene.screenNum = 3
        signUpScene.registrationNo = self.registrationNo
        signUpScene.userId = self.userId
        signUpScene.phoneNum = self.phoneNum
        signUpScene.isFromSignup = true
        self.navigationController?.pushViewController(signUpScene, animated: true)
    }
    
    
    func getNewCollegeDetail(collegeName: String) {
        signupDict[ApiKeys.isNewCollege] = "1"
        signupDict[ApiKeys.college] = collegeName
        signupDict[ApiKeys.collegeName] = collegeName
        self.registerTableView.reloadData()
        //        registerTableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
    
    func getCollegeDetail(detail: CollegeList) {
        signupDict[ApiKeys.isNewCollege] = "0"
        signupDict[ApiKeys.college] = detail.collegeId
        signupDict[ApiKeys.collegeName] = detail.collegeName
        self.registerTableView.reloadData()
        //        registerTableView.reloadSections(IndexSet(integer: 2), with: .none)
    }
}

//MARK:-
//MARK:- UIImagePicker Delegate
extension RegisterVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            //  if  imagePickerOpenFor == ImagePickerOpenFor.UserPhoto{
            
            guard let cell = self.registerTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfilePictureCell else { return }
            cell.profileImage.image = imagePicked
            cell.profileImage.contentMode = .scaleAspectFill
            cell.profileImage.clipsToBounds = true
            self.uploadImageToS3(image: imagePicked)
            //  self.pickedImage = imagePicked
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func uploadImageToS3(image: UIImage){
        
        guard let cell = self.registerTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfilePictureCell else { return }
        
        cell.activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        CommonFunction.showLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            CommonFunction.hideLoader()
            if uploaded {
                self.signupDict["user_image"] = imageUrl
                cell.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, size: {
            (_) in
        }, failure: { (err : Error) in
            CommonFunction.hideLoader()
            if (err.localizedDescription  == "The Internet connection appears to be offline.") {
                CommonFunction.showToast(msg: "Please check your internet connection.")
            }
            cell.activityIndicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
        })
    }
}
