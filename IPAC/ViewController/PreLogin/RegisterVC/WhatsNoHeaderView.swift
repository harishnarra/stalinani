//
//  WhatsNoHeaderView.swift
//  IPAC
//
//  Created by macOS on 22/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WhatsNoHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var whatsNoCheckBox: UIButton!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.whatsNoCheckBox.setTitle(StringConstant.WhatsApp_same_as_Mobile_Number.localized, for: .normal)
    }

    
    
}
