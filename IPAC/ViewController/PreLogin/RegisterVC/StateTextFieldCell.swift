//
//  StateTextFieldCell.swift
//  IPAC
//
//  Created by macOS on 23/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class StateTextFieldCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var stateTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var stateButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
