//
//  RoundHeaderViewTableViewCell.swift
//  IPAC
//
//  Created by macOS on 22/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RoundHeaderViewTableViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var whiteView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        whiteView.roundCorners([.topLeft, .topRight], radius: 5.0)
    }
}
