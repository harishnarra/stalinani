//
//  TutorialVC.swift
//  IPAC
//
//  Created by Appinventiv on 16/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVFoundation

class TutorialVC: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var btnGetStarted: GradientButton!
    @IBOutlet weak var videoView: VideoView!
    
    //MARK::- PROPERTIES
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }

    //MARK::- BUTTON ACTION
    @IBAction func btnActionGetStarted(_ sender: GradientButton) {
        CommonFunction.gotoHome()
    }
    
    //MARK::- FUNCTIONS
    func onViewDidLoad(){
        let size = (resolutionForLocalVideo(url : URL(string : tutorialUrl)!))
        print(size ?? CGSize.init())
        videoView.configure(url: tutorialUrl)
        videoView.isLoop = true
        videoView.play()
        btnGetStarted.rounded(cornerRadius: 4.0, clip: true)
    }
    
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: fabs(size.width), height: fabs(size.height))
    }
}
