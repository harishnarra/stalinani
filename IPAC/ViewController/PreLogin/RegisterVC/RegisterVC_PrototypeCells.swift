//
//  RegisterVC_PrototypeCells.swift
//  IPAC
//
//  Created by macOS on 22/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ProfilePictureCell: UITableViewCell {
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    
    @IBOutlet weak var addProfileImageBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fullNameTextField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         setupActivityIndicator()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
       
    }
    
    func setupActivityIndicator() {
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = AppColors.appThemeColor
        activityIndicator.center = profileImage.center
        profileImage.addSubview(activityIndicator)
    }
}

class GenderCell: UITableViewCell {
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var otherbtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //maleButton.isSelected = true
        setTitle()
    }
    
    func setTitle() {
        genderLabel.text = StringConstant.Gender.localized
        maleButton.setTitle(StringConstant.Male.localized, for: .normal)
        femaleBtn.setTitle(StringConstant.Female.localized, for: .normal)
        otherbtn.setTitle(StringConstant.Other.localized, for: .normal)
        genderLabel.textColor = UIColor.textFieldTextColor
    }
    
}

class EmailTextFieldCell: UITableViewCell {
    
    @IBOutlet weak var textField: SkyFloatingLabelTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class TermsAndConditionCell: UITableViewCell {
    
    @IBOutlet weak var checkBoxbtn: UIButton!
    @IBOutlet weak var acceptTermLabel: UILabel!
    //
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let termsNdCondition: NSMutableAttributedString = NSMutableAttributedString(string: StringConstant.I_Accept_Terms_Condition.localized)
        termsNdCondition.setColorForText(textForAttribute: StringConstant.TREMS_CONDITIONS.localized, withColor: AppColors.appThemeColor, font: AppFonts.ProximaNova_Semibold.withSize(14))
        let range: NSRange = termsNdCondition.mutableString.range(of:  StringConstant.TREMS_CONDITIONS.localized, options: .caseInsensitive)
        termsNdCondition.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        self.acceptTermLabel.attributedText = termsNdCondition
    }
}
