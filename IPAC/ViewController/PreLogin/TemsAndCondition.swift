//
//  TemsAndCondition.swift
//  IPAC
//
//  Created by macOS on 28/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable
import WebKit

class TemsAndCondition: UIViewController, WKNavigationDelegate {

    //MARK:- Properties
    //==================
    
    var isNewTerms = false
    var term_condition_version : String?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var btnAccept: AnimatableButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        //CommonFunction.showLoader()

        self.initialSetup()
    }

    @IBAction func backBtnTapped(_ sender: UIButton) {
        if isNewTerms{
            CommonFunction.showToast(msg: AlertMessage.acceptTerms.localized)
            return
        }
        self.dismissVC(completion: nil)
    }
    
    @IBAction func btnActionAccept(_ sender: AnimatableButton) {
        acceptTerms()
    }
    func initialSetup() {
            btnAccept.isHidden = !isNewTerms
            btnAccept.addLeftToRightGradient()
            titleLabel.text = StringConstant.TREMS_CONDITIONS.localized
            titleLabel.font = AppFonts.ProximaNova_Bold.withSize(18)
            self.webView.navigationDelegate = self
            if let url =  URL(string: "\(STATIC_PAGE_BASE_URL)term"){
    //            let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
    //           // loadHTMLString(headerString + content, baseURL: url)
    //            webView.loadHTMLString(headerString + url , baseURL: url)


                let request = URLRequest(url:url)
                self.webView.load(request)
            }
        }
    
}

//MARK:- Private Methods
//=====================
//extension TemsAndCondition {
    
    
//}

// MARK: -
//extension TemsAndCondition : WKNavigationDelegate {
    

     func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!){
        
       // CommonFunction.showLoader()


        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        
        webView.evaluateJavaScript(jscript)
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString{
          //urlStr is what you want, I guess.
            print(urlStr)
       }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonFunction.hideLoader()
        print("two")

        CommonFunction.hideLoader()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
        CommonFunction.showToast(msg: error.localizedDescription)
        print("three")

    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
            CommonFunction.showToast(msg: error.localizedDescription)
        print("four")

        }
//}

//MARK::- API
extension TemsAndCondition{
    func acceptTerms(){
        WebServices.aceeptTerms(version: /self.term_condition_version
            , success: { [weak self] (json) in
                self?.dismissVC(completion: nil)
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.dismissVC(completion: nil)

        }
    }
}

