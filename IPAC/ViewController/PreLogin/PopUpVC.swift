//
//  PopUpVC.swift
//  IPAC
//
//  Created by Appinventiv on 26/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

struct PopupMessage {
    var message : String
    var buttonTitle: String
    var buttonHandler: (() -> ())?
}

class PopUpVC: UIViewController {

    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var popUpMsgLabel: UILabel!
    @IBOutlet weak var popUpButton: GradientButton!
    @IBOutlet weak var PopUpView: UIView!
    
    var navController: UINavigationController?
    var message: PopupMessage?
    var otpResponse: RegistrationResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        if message == nil {
            self.popUpMsgLabel.text = "An OTP has been sent to your registered  Mobile Number"
            self.popUpButton.setTitle("CONTINUE", for: .normal)
        }
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.PopUpView.rounded(cornerRadius: 7.0, clip: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func popUpButtonTapped(_ sender: UIButton) {
        if message?.buttonHandler == nil {
            dismiss(animated: true) {
                let otpScene = OTPVC.instantiate(fromAppStoryboard: .Main)
                otpScene.otpResponse = self.otpResponse
                self.navController?.pushViewController(otpScene, animated: true)
            }
        }
        else {
            message?.buttonHandler?()
        }
        //dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func crossButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}
