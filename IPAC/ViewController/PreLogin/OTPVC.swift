//
//  OTPVC.swift
//  IPAC
//
//  Created by Appinventiv on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class OTPVC: BaseVC {
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var continueButton: GradientButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var resendCodeBtn: UIButton!
    @IBOutlet weak var notReceivedOtpLabel: UILabel!
    
    //    var timer: Timer?
    //    var value = 120
    var countdown =  Timer()
    var isRemember = false
    var totalTimeInSec = 30
    var otpResponse : RegistrationResponse?
    var userMobile : String = ""
    var userId = ""
    var isSignup = false
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.initialSetup()
        //        textField1.becomeFirstResponder()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        countdown.invalidate()
    }
    
    // MARK:- IBACTIONS
    //====================
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        self.hitVerifyOTPAPI()
    }
    
    func continueToSignUp(_ user : UserModel) {
        //UploadIDVC
        let uploadIDVC = UploadIDVC.instantiate(fromAppStoryboard: .Main)
        uploadIDVC.user = user
        self.navigationController?.pushViewController(uploadIDVC, animated: true)
    }
    
    
    
    @IBAction func resendCodeBtnTapped(_ sender: UIButton) {
        self.view.resignFirstResponder()
        clearText(in: self.textField1)
        clearText(in: self.textField2)
        clearText(in: self.textField3)
        clearText(in: self.textField4)
        self.hitResendOTP()
    }
    
    func clearText(in textField: UITextField) {
        textField.text = ""
    }
}


// MARK:- FUNCTIONS
//====================
extension OTPVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        
        super.addImageInBackground()
        
        
        self.startTimer()
        seupTextField()
        
        
    }
    
    func seupTextField() {
        self.continueButton.dropShadow(color: #colorLiteral(red: 0.2470588235, green: 0.3647058824, blue: 0.5607843137, alpha: 1), opacity: 0.6, offSet: CGSize.zero, radius: 5.0, scale: true)
        self.continueButton.rounded(cornerRadius: 3.0, clip: true)
        whiteView.rounded(cornerRadius: 5.0, clip: true)
        continueButton.setTitle("VERIFY", for: .normal)
        notReceivedOtpLabel.text = AppStrings.noOTP.rawValue.localized
        resendCodeBtn.setTitle(AppStrings.resendCode.rawValue.localized, for: .normal)
        titleLabel.text = AppStrings.verificationCode.rawValue.localized
        
        resendCodeBtn.isEnabled = false
        
        
        resendCodeBtn.setTitleColor(AppColors.Gray.gray166, for: .normal)
        
        
        setTextFieldBorder(currentTextField: textField1)
        setTextFieldBorder(currentTextField: textField2)
        setTextFieldBorder(currentTextField: textField3)
        setTextFieldBorder(currentTextField: textField4)
        
        configureTextField(textField1)
        configureTextField(textField2)
        configureTextField(textField3)
        configureTextField(textField4)
        
        self.whiteView.rounded(cornerRadius: 5.0, clip: true)
        
    }
    
    func configureTextField(_ textField: UITextField){
        
        textField.keyboardType = .numberPad
        textField.delegate     = self
    }
    
    //    func setTimer() {
    //       // value = 120
    //        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.otpTimer), userInfo: nil, repeats: true)
    //    }
    //
    //    @objc func otpTimer() {
    //        if value > 0 {
    //            value -= 1
    //            resendCodeBtn.isEnabled = false
    //            resendCodeBtn.titleLabel?.textColor = AppColors.Gray.gray166
    //            setSecondsToTimer()
    //
    //        }  else {
    //            timerLabel.text = "00:00"
    //            timer?.invalidate()
    ////            resendCodeBtn.titleLabel?.textColor = UIColor(red: 18/255, green: 38/255, blue: 82/255, alpha: 1)
    //    resendCodeBtn.isEnabled = false
    //    resendCodeBtn.titleLabel?.textColor = AppColors.Gray.gray166
    //   }
    //    }
    //
    //    func setSecondsToTimer() {
    //        value.getHMS { (h, m, s) in
    //            self.timerLabel.text = "\(m.getTimeString()):\(s.getTimeString())"
    //        }
    //    }
    
    //    @objc func textFieldDidChange(textField: UITextField){
    //        let text = textField.text
    //        if  text?.count == 1 {
    //            switch textField{
    //            case textField1:
    //                self.textField2.becomeFirstResponder()
    //            case textField2:
    //                self.textField3.becomeFirstResponder()
    //            case self.textField3:
    //                self.textField4.becomeFirstResponder()
    //            case self.textField4:
    //                 hitVerifyOTPAPI()
    //                //self.textField4.resignFirstResponder()
    //                self.textField4.endEditing(true)
    //
    //            default:
    //                break
    //            }
    //        }
    //        if  text?.count == 0 {
    //            switch textField{
    //            case textField1:
    //                textField1.becomeFirstResponder()
    //            case textField2:
    //                textField1.becomeFirstResponder()
    //            case self.textField3:
    //                textField2.becomeFirstResponder()
    //            case self.textField4:
    //                self.textField3.becomeFirstResponder()
    //            default:
    //                break
    //            }
    //        }
    //        else{
    //
    //        }
    //    }
    //
    
    private func setTextFieldBorder(currentTextField : UITextField)
    {
        currentTextField.setBorder(color: #colorLiteral(red: 0.07058823529, green: 0.1529411765, blue: 0.3215686275, alpha: 0.2), width: 1, cornerRadius: 3.0)
    }
    
    
    
    
    
    private func hitVerifyOTPAPI()
    {
        if textField1.text!.isEmpty || textField2.text!.isEmpty || textField3.text!.isEmpty || textField4.text!.isEmpty{
        }
        else{
            //continueToSignUp()
            let otp = textField1.text! + textField2.text! + textField3.text! + textField4.text!
            
            WebServices.verifyOTP(self.userId, otp, success: { (result) in
                let userData = UserModel(json: result)
                CommonFunction.saveUser(userData)
                if self.isSignup{
                    CommonFunction.rememberLoginCred()
                    let tutorialVC = TutorialVC.instantiate(fromAppStoryboard: .Main)
                    self.navigationController?.pushViewController(tutorialVC, animated: true)
                }else{
                    if self.isRemember{
                        CommonFunction.rememberLoginCred()
                    }
                    CommonFunction.gotoHome()
                }
                
            }, failure: { (err) -> (Void) in
                CommonFunction.showToast(msg: err.localizedDescription)
            })
            
        }
    }
    
    
    private func hitResendOTP(){
        
        WebServices.resendOTPAPI(userId: userId, success: { (json) in
            
            self.otpResponse?.otp = json["otp"].stringValue
            self.totalTimeInSec = 30
            self.resendCodeBtn.isEnabled = false
            self.resendCodeBtn.setTitleColor(AppColors.Gray.gray166, for: .normal)
            self.startTimer()
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
}


//MARK: - Text Field Delegate
//===========================
extension OTPVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        
        if (textField == self.textField1)
        {
            if (range.length == 0)
            {
                textField.text = string
                hitVerifyOTPAPI()
                self.textField2.becomeFirstResponder()
            }
            else
            {
                textField.text = ""
            }
        }
        else if (textField == self.textField2)
        {
            if (range.length == 0)
            {
                textField.text = string
                hitVerifyOTPAPI()
                self.textField3.becomeFirstResponder()
            }
            else
            {
                textField.text = ""
                self.textField1.becomeFirstResponder()
            }
        }
            
        else if (textField == self.textField3)
        {
            if (range.length == 0)
            {
                textField.text = string
                hitVerifyOTPAPI()
                self.textField4.becomeFirstResponder()
            }
            else
            {
                textField.text = ""
                self.textField2.becomeFirstResponder()
            }
        }
        else if (textField == self.textField4)
        {
            if (range.length == 0)
            {
                textField.text = string
                hitVerifyOTPAPI()
                textField.resignFirstResponder()
            }
            else
            {
                textField.text = ""
                self.textField3.becomeFirstResponder()
            }
        }
        //   self.textFieldShouldHighLight()
        return false
    }
    
}


// SET UP Timer
extension OTPVC {
    
    func startTimer() {
        // if !countdown.isValid{
        countdown = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        // }
    }
    
    @objc func updateTime() {
        
        
        self.timerLabel.text = "\(timeFormatted(totalTimeInSec))"
        if totalTimeInSec != 0 {
            totalTimeInSec -= 1
            
        } else {
            stopTimer()
            
        }
    }
    
    func stopTimer() {
        countdown.invalidate()
        //  self.timerLabel.text = ""
        resendCodeBtn.isEnabled = true
        
        resendCodeBtn.setTitleColor(AppColors.BlackColor.black, for: .normal)
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let sec     = (totalSeconds % 60)
        let min     = (totalSeconds / 60) % 60
        //  let hours   = (totalSeconds / 3600)
        print_debug(sec)
        return String(format: "%02d:%02d", min, sec)
    }
    
}



