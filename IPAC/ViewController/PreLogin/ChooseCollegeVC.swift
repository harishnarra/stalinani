//
//  ChooseCollegeVC.swift
//  IPAC
//
//  Created by macOS on 23/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

protocol UpdateCollege: class {
    func getCollegeDetail(detail: CollegeList)
    
    func getNewCollegeDetail(collegeName: String)
}

class ChooseCollegeVC: UIViewController {
 
    //MARK:- Properties
    //==================
    var destrictId      = ""
    private var collegeLists    = [CollegeList]()
    private var filteredList    = [CollegeList]()
    weak var delegate :UpdateCollege?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        getCollegeList(districtId: destrictId)
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.containerView.rounded(cornerRadius: 5.0, clip: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  //  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //MARK:- remove popup when touch otuside of popup
        //================================================
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            
            self.view.endEditing(true)
            let touch = touches.first!
            let point = touch.location(in: self.view) //touch.location(in: self.view)
            
            if !self.containerView.frame.contains(point) {
//                dismiss(animated: true, completion: {
//                    <#code#>
//                })
                dismiss(animated: true, completion: nil)
            }
        }
    //}
    
    //MARK:- Targets
    //=================
    @objc func addCollegeBtnTap(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.searchBar.text?.trimmed() == ""{
            CommonFunction.showToast(msg: AlertMessage.enterColleeName.localized)
            return
        }
        self.delegate?.getNewCollegeDetail(collegeName: self.searchBar.text ?? "")
        dismiss(animated: true, completion: nil)
    }
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension ChooseCollegeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredList.count == 0 {
            return 1
        }
            return filteredList.count
  
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  filteredList.count == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddCollegeCell") as? AddCollegeCell else { fatalError("invalid cell \(self)") }
            
            cell.addCollegeButton.addTarget(self, action: #selector(self.addCollegeBtnTap(_:)), for: .touchUpInside)
         return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CollegeNameCell") as? CollegeNameCell else { fatalError("invalid cell \(self)")
        }
         (cell.clgNameLabel.text = filteredList[indexPath.row].collegeName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  filteredList.count == 0 {
            return (tableView.frame.height - self.searchContainerView.frame.height)
        } else {
           return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  filteredList.count == 0 { return }
        self.delegate?.getCollegeDetail(detail: self.filteredList[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}

//MARK:-
//MARK:- Private Methods
private extension ChooseCollegeVC {
    
    func initialSetup() {
        tableView.delegate = self
        tableView.dataSource = self
        setupView()
        setupSearchBar()
    }
    
    func setupView() {
        searchContainerView.backgroundColor = AppColors.appThemeColor
    }
    
    func setupSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = StringConstant.Search_College.localized
       
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = AppColors.whiteColor
        textFieldInsideSearchBar?.backgroundColor = AppColors.whiteColor
    }
    
    func getCollegeList(districtId: String){
        
        WebServices.getDistrictList(params: [ApiKeys.districtCode: districtId], success: { (results) in
            print_debug(results)
            self.collegeLists.removeAll()
            let _ =   results.arrayValue.map({ (lists)  in
                self.collegeLists.append(CollegeList(dict: lists))
            })
            self.filteredList = self.collegeLists
            print_debug(self.collegeLists.count)
            self.tableView.reloadData()
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

//MARK:- Search Delegate
//========================
extension ChooseCollegeVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        guard let text = searchBar.text else { return }
                
      
//
//        if text.isEmpty {
//            searchBar.textField?.resignFirstResponder()
//        } else {
//            self.getSavedJob(loader: true, pageNo: 0, searchText: text)
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText, searchText.count)
        
        let filtereddList = self.collegeLists.filter { (collegeList) -> Bool in
            return collegeList.collegeName.lowercased().contains(searchText.lowercased())
        }
        if searchText.count == 0{
            filteredList = self.collegeLists
        }else{
            self.filteredList = filtereddList
        }
         tableView.reloadData()
    }
}

//MARK:- Prototype Cell
//=========================
class CollegeNameCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var clgNameLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK:- Prototype Cell
//=========================
class AddCollegeCell: UITableViewCell {

    //MARK:- Properties
    //==================
    @IBOutlet weak var addCollegeButton: GradientButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addCollegeButton.rounded(cornerRadius: 5, clip: true)
        addCollegeButton.setTitle(StringConstant.Add_College.localized, for: .normal)
    }

}
