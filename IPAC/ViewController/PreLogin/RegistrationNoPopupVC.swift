//
//  RegistrationNoPopupVC.swift
//  IPAC
//
//  Created by macOS on 24/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

protocol OpenForgotPassVC: class {
    func openVC()
}
class RegistrationNoPopupVC: UIViewController {

    //MARK:- Properties
    //==================
    var registrationNo = ""
    weak var delegate: OpenForgotPassVC?
    
    //MARK:- IBoutlets
    //==================
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.openVC()
        }
    }
}

//MARK:- Private Methods
//=====================
private extension RegistrationNoPopupVC {
   
    func initialSetup() {
        
        upperLabel.text = StringConstant.Registration_Successful.localized
        containerView.rounded(cornerRadius: 5.0, clip: true)
        lowerLabel.text = "\(StringConstant.Please_note_your_registration_no.localized) \(self.registrationNo)"
        okButton.setTitle(StringConstant.OK.localized, for: .normal)
    }
    
    
    
}

