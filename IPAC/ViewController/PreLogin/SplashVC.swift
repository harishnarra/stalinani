//
//  SplashVC.swift
//  IPAC
//
//  Created by Appinventiv on 25/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import SwiftyJSON

class SplashVC: BaseViewController {
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("LocalizationSystem.sharedInstance.getLanguage()", LocalizationSystem.sharedInstance.getLanguage())
        if LocalizationSystem.sharedInstance.getLanguage() == "" {
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            print("LocalizationSystem.sharedInstance.getLanguage()", LocalizationSystem.sharedInstance.getLanguage())
            AppUserDefaults.save(value: LocalizationSystem.sharedInstance.getLanguage() , forKey: .languagess)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.appVersionAPI()
    }
}



//MARK::- Versioning / FUNCTIO
extension SplashVC{
    func onViewDidLoad(){
        
        if CommonFunction.isRemembered() == true{
            CommonFunction.gotoHome()
        }else{
            let vc = LoginVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func normalUpdate(version : String) {
        let refreshAlert = UIAlertController(title: AlertMessage.alert.localized, message: "An update with version \(version) is available now. Do you want to update it?", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: AlertMessage.update.localized, style: .default, handler: { (action: UIAlertAction!) in
            self.gotoAppStore()
            self.appVersionAPI()
        }))
        refreshAlert.addAction(UIAlertAction(title: AlertMessage.skip.localized, style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
            if CommonFunction.getUser() == nil{
                self.onViewDidLoad()
            }else{
                self.getProfileData()
            }
        }))
        guard let tvc = ez.topMostVC else {return}
        tvc.present(refreshAlert, animated: true, completion: nil)
    }
    
    func forceUpdate(version : String) {
        let refreshAlert = UIAlertController(title: AlertMessage.alert.localized, message: "An update with version \(version) is available now. Proceed to update", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: AlertMessage.update.localized, style: .default, handler: { (action: UIAlertAction!) in
            self.gotoAppStore()
            self.appVersionAPI()
        }))
        guard let tvc = ez.topMostVC else {return}
        tvc.present(refreshAlert, animated: true, completion: nil)
    }
    func gotoAppStore(){
        guard let urlStr = URL(string: appStoreUrl) else {return}
        UIApplication.shared.open(urlStr, options: [:], completionHandler: nil)
    }
}

extension SplashVC{
    
    //MARK::- API
    func getProfileData() {
        WebServices.getProfile(success: { (json) in
            print_debug(json)
            let user = UserModel(json: json)
            CommonFunction.updateUser(user)
            self.onViewDidLoad()
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.onViewDidLoad()
            
        }
    }
    func appVersionAPI(){
        WebServices.appVersion(success: { [weak self] (json) in
            let result = json["RESULT"].dictionaryValue
            let resultData = AppVersion.init(dict : JSON(result))
            guard let version = ez.appVersion else {return}
            print_debug(((resultData.version_name).toDouble() ?? 0.0))
            print_debug((version.toDouble() ?? 0.0))
            if ((resultData.version_name).toDouble() ?? 0.0) > (version.toDouble() ?? 0.0){
                resultData.update_type == "3" ? self?.forceUpdate(version: resultData.version_name) : self?.normalUpdate(version : resultData.version_name)
            }else{
                if CommonFunction.getUser() == nil{
                    self?.onViewDidLoad()
                }else{
                    self?.getProfileData()
                }            }
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.onViewDidLoad()
        }
    }
}
