//
//  ForgotResetPasswordVC.swift
//  IPAC
//
//  Created by Appinventiv on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ForgotResetPasswordVC: BaseVC {
    
    // MARK:- VARIABLES
    //====================
    var screenNum : Int = 0  //screennum = 1 for reset password
    //screen number = 2 for forgot password
    //screen number = 3 for Sign up  send otp
    var userId = ""
    var registrationNo = ""
    var phoneNum    = ""
    var isRemember = false
    var isFromSignup = false
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var whiteView: UIView!
    
    @IBOutlet weak var ORLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak private var resetForgotPsswdTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var showButton: GradientButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dontHaveAcntLbl: UILabel!
    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet weak var resetPassButton: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        ORLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "OR", comment: "")
        
        self.initialSetup()
        if screenNum == 1{
            self.setNavigationTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reset_Password", comment: ""))
            self.bottomConstraint.constant = 30
            self.bottomConstraint.constant = 24

        }else if screenNum == 2{
            self.setNavigationTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Forgot_Password", comment: ""))
        }
        else{
            resetForgotPsswdTextField.text = self.registrationNo
            resetForgotPsswdTextField.isUserInteractionEnabled = false
            self.bottomConstraint.constant = 27
            self.setNavigationTitle(StringConstant.Verify_Mobile_No.localized)
            self.dontHaveAcntLbl.isHidden = true
            self.registerBtn.isHidden = true
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.sendButton.dropShadow(opacity: 0.6, offSet: CGSize.zero, radius: 7.0, scale: true)
        self.whiteView.rounded(cornerRadius: 5.0, clip: true)
        self.sendButton.rounded(cornerRadius: 5.0, clip: true)
        self.resetPassButton.rounded(cornerRadius: 5.0, clip: true)
    }
    
    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func showButtonTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if resetForgotPsswdTextField.isSecureTextEntry{
            showButton.setTitle(AppStrings.Hide.rawValue.localized, for: .normal)
            resetForgotPsswdTextField.isSecureTextEntry = false
        }
        else{
            showButton.setTitle(AppStrings.Show.rawValue.localized, for: .normal)
            resetForgotPsswdTextField.isSecureTextEntry = true
            
        }
    }
    
    
    @IBAction func registerBtnTap(_ sender: UIButton) {
        
    }
    
    @IBAction func sendButtonTapped(_ sender: GradientButton) {
        let enteredText = resetForgotPsswdTextField.text!
        print(screenNum)
        if screenNum == 1{
            self.view.endEditing(true)
            let result  = ValidationController.validatePassword(password: enteredText)
            if result.0{
                let param = [ApiKeys.userId: userId, ApiKeys.password: resetForgotPsswdTextField.text!]
                print_debug(param)
                
                WebServices.resetPassword(params: param, success: { (json) in
                    print(json)
                    self.popToViewController(atIndex: 0)
                    
                }) { (error) -> (Void) in
                    CommonFunction.showToast(msg: error.localizedDescription)
                }
            }
            else{
                CommonFunction.showToast(msg: result.1)
            }
        }
        else if screenNum == 2{
            self.view.endEditing(true)
            let result = ValidationController.validatePhone(phone: resetForgotPsswdTextField.text!)
            if result.0{
                WebServices.loginAPI(resetForgotPsswdTextField.text!, password: "", loginType: ApiKeys.otp, success: { (result) in
                    print(result)
                    let otpScene = OTPVC.instantiate(fromAppStoryboard: .Main)
                    otpScene.userId = result["user_id"].stringValue
                    otpScene.otpResponse = RegistrationResponse.init(json: result)
                    otpScene.userMobile = self.resetForgotPsswdTextField.text!
                    self.navigationController?.pushViewController(otpScene, animated: true)
                }, failure: { (error) -> (Void) in
                    CommonFunction.showToast(msg: error.localizedDescription)
                })
            }  
            else{
                print(result.1)
            }
        }
        else{

          
                WebServices.resendOTPAPI(userId: self.userId, success: { (json) in
                    print_debug(json)
                   // guard let otp = json.string else { return }
                  //  self.otpResponse?.otp = otp
                    let otpScene = OTPVC.instantiate(fromAppStoryboard: .Main)
                    otpScene.userMobile = self.phoneNum
                    otpScene.userId = self.userId
                    otpScene.isSignup = true
                    //otpScene.otpResponse = self.otpResponse
                    self.navigationController?.pushViewController(otpScene, animated: true)

                    
                }) { (error) -> (Void) in
                    CommonFunction.showToast(msg: error.localizedDescription)
                }

        }
    }
    
    
    @IBAction func resetPasswordButtonTapped(_ sender: UIButton) {
        
        let result = ValidationController.validatePhone(phone: resetForgotPsswdTextField.text!)
        
        if result.0 {
            self.view.endEditing(true)
            WebServices.forgotPass(resetForgotPsswdTextField.text!, success: { (json) in
            self.navigationController?.popViewController(animated: true)
            }) { (error) -> (Void) in
                CommonFunction.showToast(msg: error.localizedDescription)
            }
            
        } else {
            print(result.1)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        if isFromSignup{
            for vc in self.navigationController?.viewControllers ?? [] {
                if vc is LoginVC{
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    
}

extension ForgotResetPasswordVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength : Int
        if screenNum == 2{
            maxLength = 10
        }
        else{
            maxLength = 25
        }
            let currentString: NSString = resetForgotPsswdTextField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
    }
}


// MARK:- FUNCTIONS
//====================
extension ForgotResetPasswordVC {
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        sendButton.setTitle(AppStrings.DONE.rawValue.localized, for: .normal)
        if screenNum == 1{
            
            showButton.isUserInteractionEnabled = true
            showButton.isHidden = false
            showButton.setTitle(AppStrings.Show.rawValue.localized, for: .normal)
            sendButton.setTitle(AppStrings.DONE.rawValue.localized, for: .normal)
            
        }
        else if screenNum == 2{
            sendButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "LOGIN WITH OTP", comment: ""), for: .normal)
            //resetPassButton.setTitle(AppStrings.resetPass.rawValue.localized, for: .normal)
            resetPassButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reset_Password", comment: ""), for: .normal)
            showButton.isHidden = true
            showButton.isUserInteractionEnabled = false
            resetForgotPsswdTextField.isSecureTextEntry = false
        }
        else{
            sendButton.setTitle(StringConstant.SEND_OTP.localized, for: .normal)
            showButton.isHidden = true
            showButton.isEnabled = false
            resetForgotPsswdTextField.isSecureTextEntry = false
        }
    }
    
    private func initialSetup() {
        
        super.addImageInBackground()
        resetForgotPsswdTextField.delegate = self
        if screenNum == 1{
            resetForgotPsswdTextField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New_Password", comment: "")
            //resetForgotPsswdTextField.keyboardType = .phonePad
            
        }
        else if screenNum == 2{
            resetForgotPsswdTextField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mobile_Number", comment: "")
            resetForgotPsswdTextField.keyboardType = .numberPad
        }
        else{
            resetForgotPsswdTextField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "USERID", comment: "")
            resetForgotPsswdTextField.selectedTitle = LocalizationSystem.sharedInstance.localizedStringForKey(key: "USERID", comment: "")
            
        }
        self.sendButton.rounded(cornerRadius: 3.0, clip: true)
        self.sendButton.dropShadow(color: .shadowColor, opacity: 0.6, offSet: CGSize.zero, radius: 7.0, scale: true)
        CommonFunction.setAttributesto(currentTextField: resetForgotPsswdTextField)
        
        self.dontHaveAcntLbl.text = StringConstant.Dont_Have_An_Account.localized
        registerBtn.setTitle(StringConstant.Register.localized, for: .normal)
        self.view.layoutIfNeeded()
    }
    
    func setNavigationTitle(_ string: String) {
        titleLabel.text = string
    }
    
}
