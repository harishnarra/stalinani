//
//  DashboardPopUpViewController.swift
//  Stalinani
//
//  Created by HariTeju on 21/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class DashboardPopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
 
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var count = 0
    var referData : ReferralModel?
    var arrData : [ReferralResult]?  = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getDataFromAPI(loader : true)

        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()

        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        self.tableView.switchRefreshFooter(to: .normal)
       // if arrData?.count == 0 { self.tableView.isHidden = true } else { self.tableView.isHidden = false }
        self.arrData?.append(contentsOf : self.referData?.result ?? [])
        print(self.arrData ?? [])
        self.count = self.referData?.count ?? -1
        self.tableView.reloadData()
    }
    
    
    func getDataFromAPI(loader : Bool){
           WebServices.referralList(count: self.count.description,loader
               , success: { [weak self] (json) in
                   self?.referData = ReferralModel.init(json: json)
                
                   self?.setupUI()
                   
           }) {(error) -> (Void) in
               CommonFunction.showToast(msg: error.localizedDescription)
           }
       }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardPopUpTableViewCell") as! DashboardPopUpTableViewCell
        cell.selectionStyle = .none
        tableView.separatorStyle = .none
        cell.profileIcon.kf.setImage(with: URL(string : /arrData?[indexPath.row].user_image), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.profileName.text = /arrData?[indexPath.row].full_name
        cell.profileNum.text = /arrData?[indexPath.row].phone_number
        cell.lblDate.text = calculateFormats(date: /arrData?[indexPath.row].created_date, formatterReq: "d MMM, yyyy")
        return cell
    }
    
    func calculateFormats(date : String? , formatterReq : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.date(from: /date) ?? Date()
        formatter.dateFormat = formatterReq
        return /formatter.string(from: newDate)
    }
    
    
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
