//
//  DashboardFirstCell.swift
//  IPAC
//
//  Created by Appinventiv on 26/10/18.
//  Copyright © 2018 Admin. All rights reserved.

import UIKit

class DashboardFirstCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var progressFb: UIProgressView!
    @IBOutlet weak var progressTwitter: UIProgressView!
    @IBOutlet weak var progressWhatsapp: UIProgressView!
    @IBOutlet weak var progressYoutube: UIProgressView!
    @IBOutlet weak var progressMedia: UIProgressView!
    @IBOutlet weak var progressForm: UIProgressView!
    
    @IBOutlet var viewOutlets: [UIView]!
    @IBOutlet  var iconsOutlet: [UIImageView]!
    @IBOutlet var lblOutlets: [UILabel]!
    
    //MARK::- PROEPERTIES
    var tagSelected = 0
    var arrIcons = [#imageLiteral(resourceName: "icFb") , #imageLiteral(resourceName: "icProfileTwitter"),#imageLiteral(resourceName: "icWhatsapp"),#imageLiteral(resourceName: "icYoutube"),#imageLiteral(resourceName: "icMedia"),#imageLiteral(resourceName: "icForm")]

    //MARK::- FUNCTIONS
    
    func populate(task : Task?){
        var progressOutlets = [progressFb,progressTwitter,progressWhatsapp,progressYoutube,progressMedia,progressForm]
        var completedArray = [task?.completedFbTask,task?.completedTwitterTask,task?.completed_whatsapp_task,task?.completed_youtube_task,task?.completed_offline_task,task?.completed_form_task]
        var totalArray = [task?.totalFbTask,task?.totalTwitterTask,task?.total_whatsapp_task,task?.totalYoutubeTask,task?.total_offline_task,task?.total_form_task]
        
        for (index,_) in self.arrIcons.enumerated(){
            self.iconsOutlet[index].image = arrIcons[index]
            self.viewOutlets[index].isHidden = true
            if tagSelected == 0{
                self.viewOutlets[index].isHidden = false
            }else{
                self.viewOutlets[index].isHidden = !(index == tagSelected - 1)
            }
            self.lblOutlets[index].text = /completedArray[index]?.description + "/" + /totalArray[index]?.description
            progressOutlets[index]?.setProgress(Float((completedArray[index] ?? 0) / (totalArray[index] == 0 ? 1 : (totalArray[index] ?? 1))), animated: true)
        }
    }
}
