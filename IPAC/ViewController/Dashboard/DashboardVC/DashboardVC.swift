//
//  DashboardVC.swift
//  IPAC
//
//  Created by macOS on 16/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import EZSwiftExtensions
import PullToRefreshKit
import IBAnimatable

class DashboardVC: BaseViewController {
    
    //MARK:- Property Cell
    var dashboard: DashboardModel?
    var currentCollectionViewVisibleCells = 5
    var formUrl = ""
    var eventDateStr = (startDate: "", endDate: "")
    var eventDate = (startDate: Date(), endDate: Date())
    
    //MARK:- IBOutlets
    //=================
    @IBOutlet weak var dailyProgressLabel: UILabel!
    @IBOutlet weak var leaderboardNorecord: UILabel!
    @IBOutlet weak var stateRankTop: UILabel!
    @IBOutlet weak var districtRankTop: UILabel!
    @IBOutlet weak var leaderBoardLabel: UILabel!
    @IBOutlet weak var outOf8Label: UILabel!
    @IBOutlet weak var stateRankLabel: UILabel!
    @IBOutlet weak var outOf2label: UILabel!
    @IBOutlet weak var districtRankLabel: UILabel!
    @IBOutlet weak var rewardsLabel: UILabel!
 //   @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var viewLeaderBackground: AnimatableView!
    @IBOutlet weak var viewLeaderHeight: NSLayoutConstraint!
    @IBOutlet weak var friendsBtn: AnimatableButton!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var dateFilterView: UIView!
    @IBOutlet weak var startDateBtn: UIButton!
    @IBOutlet weak var startDateTxtFld: UITextField!
    @IBOutlet weak var endDateBtn: UIButton!
    @IBOutlet weak var endDateTxtFld: UITextField!
    @IBOutlet weak var platformBtn: UIButton!
    @IBOutlet weak var openPlatformPopupBtn: UIButton!
    @IBOutlet weak var platformPopupView: UIView!
    @IBOutlet weak var platforrmAllBtn: UIButton!
    @IBOutlet weak var platfomFbBtn: UIButton!
    @IBOutlet weak var platformTwiterBtn: UIButton!
    @IBOutlet weak var platformWhatsappButton: UIButton!
    @IBOutlet weak var platformYoutubeButton: UIButton!
    @IBOutlet weak var platformOfflineBtn: UIButton!
    @IBOutlet weak var bronzeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var platformOnlineBtn: UIButton!
    
    //TABLE HEADER OUTLETS
    @IBOutlet weak var imgUser: AnimatableImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblRewardCount: UILabel!
    @IBOutlet weak var collectionViewLeaderboard: UICollectionView!
    @IBOutlet weak var lblNoLeaderboard: UILabel!
    @IBOutlet weak var heightNoDataFooter: NSLayoutConstraint!
    
    
    //MARK::- PROPERTIES
    var tagSelected = 0
    var arrPlatform = ["","facebook","twitter","whatsapp","youtube","offline","online"]
    var count = 0
    

    //MARK:- Life-Cycle
    //===================
    override func viewDidLoad() {
        super.viewDidLoad()
        bronzeViewHeight.constant = 0
        
        
      
               
        
//
//        self.hideimage.isHidden = true
//        self.hideLabel.isHidden = true
//        self.hidelabel2.isHidden = true

     //   rankLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Rank", comment: "")
        rewardsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Rewards :", comment: "")
        districtRankLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "District_Rank", comment: "")
        stateRankLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "State_Rank", comment: "")
        outOf2label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Outof1", comment: "")
        outOf8Label.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Outof2", comment: "")
        btnViewAll.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_all", comment: ""), for: .normal)
        leaderBoardLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "LEADERBOARD", comment: "")
        dailyProgressLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "DAILY_PROGRESS", comment: "")

       
        
        initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        onDidLayoutSubviews()
    }
    
    @IBAction func friendsClicked(_ sender: Any) {
        
        let vc = DashboardPopUpViewController.instantiate(fromAppStoryboard: .Dashboard)
                     self.navigationController?.present(vc, animated: true, completion: nil)
                     
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view)
//        if !blurView.isHidden, !self.platformPopupView.frame.contains(point) {
//            self.hideShowPlatformPopup(status: true)
//            self.openPlatformPopupBtn.isSelected = false
//        }
    }
    
    //MARK:- IBACTION
    @IBAction func btnActionViewAll(_ sender: UIButton) {
        guard let dashboard = self.dashboard else { return }
        let leaderboardSceen = LeaderboadVC.instantiate(fromAppStoryboard: .Dashboard)
        leaderboardSceen.leaderboardData = dashboard.topLeaderboards
        leaderboardSceen.modalTransitionStyle = .crossDissolve
        leaderboardSceen.modalPresentationStyle = .overCurrentContext
        self.present(leaderboardSceen, animated: true, completion: nil)
    }
    
    @IBAction func openPlatformBtnTap(_ sender: UIButton) {
        self.hideShowPlatformPopup(status: sender.isSelected)
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func platformAllBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
    }
    
    @IBAction func platformFbBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Facebook", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
        
    }
    
    @IBAction func platformTwitterBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Twitter", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
    }
    @IBAction func platformWhatsappBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "WhatsApp", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
    }
    @IBAction func platformYoutubeBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Youtube", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
        
    }
    @IBAction func platformOfflineBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Offline", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
        
    }
    @IBAction func platformOnlineBtnTap(_ sender: UIButton) {
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Online", comment: ""), for: .normal)
        getFilteredData(tag: sender.tag)
        
    }
    
    @IBAction func notificationBtnTapped(_ sender: UIButton) {
        let notificationSceen = NotificationVC.instantiate(fromAppStoryboard: .Dashboard)
        self.navigationController?.pushViewController(notificationSceen, animated: true)
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        count = 0
        getDashboardData(loader: false , sender)
    }
    
    func getFilteredData(tag : Int){
        self.hideShowPlatformPopup(status: true)
        self.openPlatformPopupBtn.isSelected = false
        tagSelected = tag
        count = 0
        getDashboardData(loader: true)
    }
}

//MARK: - Collection view Delegate and DataSource
//============================================
extension DashboardVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let dashboard = self.dashboard else { return 0 }
        if dashboard.topLeaderboards.count > currentCollectionViewVisibleCells {
            return self.currentCollectionViewVisibleCells + 1
        }  else {
            return dashboard.topLeaderboards.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let dashboard = self.dashboard else { fatalError("failed to unwrap dashboard model") }
        if indexPath.row == currentCollectionViewVisibleCells , dashboard.topLeaderboards.count >= indexPath.row  {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreCollectionCell", for: indexPath) as? MoreCollectionCell else { fatalError("invalid collection view cell \(self)") }
            cell.populateCell(with: dashboard.topLeaderboards.count - currentCollectionViewVisibleCells)
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaderBoardCell", for: indexPath) as? LeaderBoardCell else { fatalError("invalid collection view cell \(self)") }
        cell.populateCell(with: dashboard.topLeaderboards[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/4) - 12, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(12, 12, 12, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let dashboard = self.dashboard else { return }
        if indexPath.item == currentCollectionViewVisibleCells  {
            currentCollectionViewVisibleCells = dashboard.topLeaderboards.count
            collectionView.reloadData()
        }
    }
}


// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension DashboardVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        default:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TaskHeaderView") as! TaskHeaderView
            if let dashboard = dashboard {
                header.setData(with: dashboard.tasks[section]  , section: section , tagSelected : self.tagSelected)
            }
            return header
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.dashboard?.tasks.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SocialFoterView") as! SocialFoterView
        return footer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && self.eventDate.endDate.daysFrom( Date()) == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardFirstCell", for: indexPath) as? DashboardFirstCell else{
                fatalError("Cell not found")
            }
            if let dashboard = dashboard {
                cell.tagSelected = self.tagSelected
                cell.populate(task: dashboard.tasks[indexPath.section])
                
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardSecondCell", for: indexPath) as? DashboardSecondCell else{
                fatalError("Cell not found")
            }
            if let dashboard = dashboard {
                cell.tagSelected = self.tagSelected
                
                cell.populate(task: dashboard.tasks[indexPath.section])
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 128
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    
    
}

//MARK: Methods
//========================
extension DashboardVC {
    func updateUI(){
        
        
        lblName.text = /self.dashboard?.userInfo.fullName.uppercased()
        lblRank.text = /self.dashboard?.userInfo.rank.description
        districtRankTop.text = /self.dashboard?.districtRank
        stateRankTop.text = self.dashboard?.stateRank
        outOf2label.text = "Out of " + (self.dashboard?.districttotal ?? "0")
        outOf8Label.text = "Out of " + (self.dashboard?.stateTotal ?? "0")
        print("self.dashboard?.districttotal", /self.dashboard?.districttotal)
        lblRewardCount.isHidden = true
        
        lblRewardCount.text = /self.dashboard?.userInfo.rewards.description
        imgUser.kf.setImage(with: URL(string : /self.dashboard?.userInfo.userImage), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
        lblNoLeaderboard.isHidden = !( self.dashboard?.topLeaderboards.count == 0 )
        viewLeaderBackground.backgroundColor = !( self.dashboard?.topLeaderboards.count == 0 ) ? UIColor.white : UIColor.clear
        viewLeaderHeight.constant = !( self.dashboard?.topLeaderboards.count == 0 ) ? 110 : 44
        
    }
    
    func initialSetup() {
        setTexts()
        self.tableView.isHidden = true
        handlePaging()
        registerXib()
        hideShowPlatformPopup(status: true)
        startDateTxtFld.delegate = self
        endDateTxtFld.delegate = self
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
        navigationController?.navigationBar.isHidden = true
        let date = Date()
        //guard let dateFrom = Calendar.current.date(byAdding: .day, value: -7, to: date) else{ return }
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
        guard let dateFrom = Calendar.current.date(from: comp) else {return}
        self.eventDate.startDate = dateFrom
        self.eventDate.endDate = date
        self.eventDateStr.startDate = dateFrom.toString(dateFormat: "yyyy-MM-dd")
        self.eventDateStr.endDate = date.toString(dateFormat: "yyyy-MM-dd")
        self.startDateTxtFld.text = dateFrom.toString(dateFormat: DashboardEventDateFormat)
        self.endDateTxtFld.text = date.toString(dateFormat: DashboardEventDateFormat)
        getDashboardData(loader: true)
    }
    
    func setTexts() {
      
        lblNoLeaderboard.text = AlertMessage.noRecordFound.localized
        titlelabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Dashboard", comment: "")
        startDateBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "startDate", comment: ""), for: .normal)
        endDateBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "endDate", comment: ""), for: .normal)
        platformBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "PLATFORM", comment: ""), for: .normal)
        openPlatformPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        platformPopupView.rounded(cornerRadius: 3.0, clip: true)
        dateFilterView.rounded(cornerRadius: 5.0, clip: true)
        platformYoutubeButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Youtube", comment: ""), for: .normal)
        platformWhatsappButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "WhatsApp", comment: ""), for: .normal)
        platforrmAllBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        platfomFbBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Facebook", comment: ""), for: .normal)
        platformTwiterBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Twitter", comment: ""), for: .normal)
        platformOfflineBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Offline", comment: ""), for: .normal)
        platformOnlineBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Online", comment: ""), for: .normal)
    }
    
    func registerXib(){
        tableView.register(UINib.init(nibName: "TaskHeaderView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "TaskHeaderView")
        tableView.register(UINib.init(nibName: "SocialFoterView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "SocialFoterView")
        tableView.register(UINib.init(nibName: "SocialCell", bundle: nil), forCellReuseIdentifier: "SocialCell")
        collectionViewLeaderboard.register(UINib(nibName: "LeaderBoardCell", bundle: nil), forCellWithReuseIdentifier: "LeaderBoardCell")
        collectionViewLeaderboard.register(UINib(nibName: "MoreCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MoreCollectionCell")
    }
    
    
    func hideShowPlatformPopup(status: Bool) {
        self.platformPopupView.isHidden = status
        //self.blurView.isHidden    = status
    }
    func getDashboardData(loader : Bool , _ refreshControl : UIRefreshControl? = nil) {
        let params = [ApiKeys.flag: self.count == 0 ? 1 : 2 , ApiKeys.start_date: self.eventDateStr.startDate, ApiKeys.end_date: self.eventDateStr.endDate , "count" : self.count , "task_platform" : self.arrPlatform[tagSelected] , "NEXT" : self.count] as [String: Any]
        WebServices.getDashboardData(params: params , loader ,success: { [weak self] (json)  in
            self?.tableView.switchRefreshFooter(to: .normal)
            refreshControl?.endRefreshing()
            self?.tableView.isHidden = false
            if self?.count == 0{
                self?.dashboard = DashboardModel(dict: json[ApiKeys.result])
            }else{
                let temp = DashboardModel(dict: json[ApiKeys.result])
                self?.dashboard?.tasks.append(contentsOf: temp.tasks )
            }
            self?.tableView.reloadData()
            self?.count = json[ApiKeys.next].stringValue.toInt() ?? -1
            self?.updateUI()
          //  self?.btnViewAll.isHidden = (self?.dashboard?.topLeaderboards.isEmpty ?? true)
            self?.lblFooter.isHidden = !(self?.dashboard?.tasks.isEmpty ?? true)
            self?.heightNoDataFooter.constant = (self?.lblFooter.isHidden)! ? 0 : 64
            
            self?.collectionViewLeaderboard.reloadData()
            let data = json["TERM_CONDITION"].dictionaryValue
            let termsCondition =  TermsCondition(dict :  JSON(data))
            if termsCondition.term_accept_status == "1"{
                let vc = AcceptTermsVC.instantiate(fromAppStoryboard: .ContactUs)
                vc.version = termsCondition.version
                vc.delegate = self
                self?.navigationController?.present(vc, animated: false, completion: nil)
            }else{
                let user = CommonFunction.getUser()
                if user?.is_bonus_received == "1"{return}
                
                //                if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || json["TOTAL_COMPLETE_TASK"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""{
                if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || SingletonTask.shared.completedTask == 0 || user?.facebookId == "" || user?.twitterId == ""{
                    
//                    let vc = BonusPopupVC.instantiate(fromAppStoryboard: .Form)
//                    self?.formUrl = json["CAMPAIGN_FORM_URL"].stringValue
//                    vc.isFormFill = json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1"
//                    vc.isFirstTask = SingletonTask.shared.completedTask == 0
//                    vc.isProof = json["IS_ID_PROOF_ADDED"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""
//                    vc.isPaytmDetail = json["PAYMENT_DETAIL_ADDED"].stringValue == "0"
//                    vc.delegate = self
//                    vc.isDismiss = !(SingletonTask.shared.completedTask >= 3)
//                    
//                    self?.navigationController?.present(vc, animated: false, completion: nil)
                }
            }
        }) { (error) -> (Void) in
            self.tableView.isHidden = false
            refreshControl?.endRefreshing()
            self.tableView.switchRefreshFooter(to: .normal)
            print_debug(error.localizedDescription)
            CommonFunction.showToast(msg: error.localizedDescription)

        }
    }
    
    func setStartDatePicker(txtField: UITextField) {
        _ = PKDatePicker.openDateMaxPickerIn(txtField,
                                             outPutFormate: EventDateFormat,
                                             mode: .date, minimumDate: dateManager.getMinimumStartDateAndTime(), maximumDate:
            Date(), minuteInterval: 1, selectedDate: Date(), doneBlock: { (selectStr) in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = EventDateFormat
                guard let date = dateFormatter.date(from: selectStr) else { return }
                let dateString = date.toString(dateFormat: "yyyy-MM-dd")
                if txtField === self.startDateTxtFld {
                    if date > self.eventDate.endDate{
                        CommonFunction.showToast(msg: AlertMessage.addValidStartEndDate)
                        return
                    }
                    self.eventDateStr.startDate = dateString
                    self.eventDate.startDate = date
                    txtField.text = date.toString(dateFormat: DashboardEventDateFormat)
                } else {
                    if date < self.eventDate.startDate{
                        CommonFunction.showToast(msg: AlertMessage.addValidStartEndDate)
                        return
                    }
                    self.eventDateStr.endDate = dateString
                    self.eventDate.endDate = date
                    txtField.text = date.toString(dateFormat: DashboardEventDateFormat)
                }
                self.count = 0
                self.getDashboardData(loader: true)
        })
    }
}

// MARK:- TEXT FIELD DELEGATE
//==============================================

extension DashboardVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField === self.startDateTxtFld {
            self.setStartDatePicker(txtField: startDateTxtFld)
            return true
        } else if textField === self.endDateTxtFld {
            self.setStartDatePicker(txtField: endDateTxtFld)
            return true
        }
        else {
            return true
        }
    }
    
}

//MARK::- TERMS CONDITION DELEGATE
extension DashboardVC : TermsAcceptDelegate , BonusPopupDelegate{
    func accepted(version: String) {
        let termsSceen = TemsAndCondition.instantiate(fromAppStoryboard: .Main)
        termsSceen.isNewTerms = true
        termsSceen.term_condition_version = version
        self.presentVC(termsSceen)
        
    }
    func done(status : Int){
        switch status{
        case 0:
            let vc = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
            vc.isSidePanel = false
            self.navigationController?.pushViewController(vc, animated: false)
            break
        case 1:
            break
        default:
            let vc = WebViewVC.instantiate(fromAppStoryboard: .Form)
            vc.url = self.formUrl
            self.navigationController?.pushViewController(vc, animated: false)
            break
            
        }
        //0 profile , 1 task , 2 form
    }
}
//MARK::- PAGINATION
extension DashboardVC{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableView.switchRefreshFooter(to: .normal) :  self.getDashboardData(loader: false)
        }
    }
}
