//
//  SocialCell.swift
//  IPAC
//
//  Created by macOS on 16/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SocialCell: UITableViewCell {

    let socialIconsList = [#imageLiteral(resourceName: "vectorSmartObject"),#imageLiteral(resourceName: "forma1Copy5")]
    
    @IBOutlet weak var socialIcons: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        progressView.rounded(cornerRadius: progressView.frame.height/2, clip: true)
    }
    
    func populateCell(with data: Task, indexPath: IndexPath) {
       self.socialIcons.image = socialIconsList[indexPath.row]
        switch indexPath.row {
        case 0:
            let score: NSMutableAttributedString = NSMutableAttributedString(string: "\(data.completedFbTask)/\(data.totalFbTask) ")
            score.setColorForText(textForAttribute: "\(data.totalFbTask) ", withColor: AppColors.Gray.gray166, font: AppFonts.ProximaNova_Semibold.withSize(12))
            self.scoreLabel.attributedText = score
            let completeFbTask = Float(data.completedFbTask)
            let totalFbTask = Float(data.totalFbTask)
            self.progressView.setProgress(completeFbTask/totalFbTask, animated: false)
            progressView.progressTintColor = AppColors.Blue.fbBulue
            progressView.trackTintColor = AppColors.Blue.fbIncompleteBlue
        case 1:
           
            let score: NSMutableAttributedString = NSMutableAttributedString(string: "\(data.completedTwitterTask)/\(data.totalTwitterTask) ")
            score.setColorForText(textForAttribute: "\(data.totalTwitterTask) ", withColor: AppColors.Gray.gray166, font: AppFonts.ProximaNova_Semibold.withSize(12))
            self.scoreLabel.attributedText = score
            
            let completeTwitterTask = Float(data.completedTwitterTask)
            let totalTwitterTask = Float(data.totalTwitterTask)
            self.progressView.setProgress(completeTwitterTask/totalTwitterTask, animated: false)
            progressView.progressTintColor = AppColors.Blue.twitterBlue
            progressView.trackTintColor = AppColors.Blue.twitterIncolpleteBlue

            //        case 2:
////            self.scoreLabel.text = "13/10"
////            self.progressView.setProgress(0.9, animated: false)
////            progressView.progressTintColor = AppColors.Green.whatsupGreen
//            let score: NSMutableAttributedString = NSMutableAttributedString(string: "\(data.completedTwitterTask)/\(data.totalTwitterTask) ")
//            score.setColorForText(textForAttribute: "\(data.totalTwitterTask) ", withColor: AppColors.Gray.gray166, font: AppFonts.ProximaNova_Semibold.withSize(12))
//            self.scoreLabel.attributedText = score
//            self.progressView.setProgress(0.0, animated: false)
//            progressView.progressTintColor = AppColors.Blue.fbBulue
        default:
            return
        }
    }
}
