//
//  SocialFoterView.swift
//  IPAC
//
//  Created by macOS on 17/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SocialFoterView: UITableViewHeaderFooterView {

   
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        whiteView.roundCorners([.bottomRight,.bottomLeft], radius: 5.0)
    }
    
}
