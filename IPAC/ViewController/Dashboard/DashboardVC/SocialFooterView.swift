//
//  SocialFooterView.swift
//  IPAC
//
//  Created by macOS on 17/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SocialFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var whiteView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        whiteView.roundCorners([.bottomRight,.bottomLeft], radius: 5.0)
    }
}
