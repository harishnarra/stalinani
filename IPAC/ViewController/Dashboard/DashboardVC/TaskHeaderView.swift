//
//  TaskHeaderView.swift
//  IPAC
//
//  Created by macOS on 16/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class TaskHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var roundView2: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var taskCompleteLabel: UILabel!
    @IBOutlet weak var rewardsCountLabel: UILabel!
    @IBOutlet weak var rewardBottomView: UIView!
    @IBOutlet weak var rewardsLabel: UILabel!
    @IBOutlet weak var bottomSeperatorView: UIView!
    @IBOutlet weak var topSeparatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bottomSeperatorView.backgroundColor = AppColors.Gray.gray234
        topSeparatorView.backgroundColor = AppColors.Gray.gray234

        bottomView.backgroundColor = AppColors.Pink.pink
        rewardBottomView.backgroundColor = AppColors.Pink.pink
        taskCompleteLabel.text = StringConstant.Task_Completed.localized
        rewardsLabel.text = StringConstant.Rewards.localized
        scoreLabel.textColor = .white
        rewardsCountLabel.textColor = .white
//        progressLabel.text = StringConstant.DAILY_PROGRESS.localized
        progressLabel.textColor = .white
        progressLabel.text = "8 July 2018"
        taskCompleteLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Task_Completed", comment: "")
        rewardsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Rewards", comment: "")

        
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        roundView2.roundCorners([.topRight,.topLeft], radius: 5.0)
        roundView2.backgroundColor = .black
    }
    
    func setData(with data: Task, section: Int , tagSelected : Int) {
        var completedArray = [data.completedFbTask,data.completedTwitterTask,data.completed_whatsapp_task,data.completed_youtube_task,data.completed_offline_task,data.completed_form_task]
        var totalArray = [data.totalFbTask,data.totalTwitterTask,data.total_whatsapp_task,data.totalYoutubeTask,data.total_offline_task,data.total_form_task]
        var totalTask = data.totalTask
        var completedTask = data.completedTask
        if tagSelected != 0{
            totalTask = totalArray[tagSelected - 1]
            completedTask = completedArray[tagSelected - 1]
        }
        
        let attributedAddImageString: NSMutableAttributedString = NSMutableAttributedString(string: "\(completedTask)/\(totalTask) " )
        attributedAddImageString.setColorForText(textForAttribute: "\(completedTask) ", withColor: AppColors.Gray.gray166, font: AppFonts.ProximaNova_Semibold.withSize(21))
        self.scoreLabel.attributedText = attributedAddImageString
        rewardsCountLabel.text = "\(data.rewards)"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let date =  formatter.date(from: data.addedOn) else { return }
        if date.daysFrom(Date()) != 0{
            self.scoreLabel.text = completedTask.description
        }
        let dateString = date.toString(dateFormat: "dd MMMM yyyy")
        progressLabel.text = dateString
    }
    
}
