//
//  DashboardPopUpTableViewCell.swift
//  Stalinani
//
//  Created by HariTeju on 21/07/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class DashboardPopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var profileIcon: AnimatableImageView!
    @IBOutlet weak var profileNum: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        // Initialization code
    }
    
    
    
    func configure(item : ReferralResult?){
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension DashboardPopUpTableViewCell{
    func calculateFormat(date : String? , formatterReq : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = formatter.date(from: /date) ?? Date()
        formatter.dateFormat = formatterReq
        return /formatter.string(from: newDate)
    }
}
