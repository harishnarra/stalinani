//
//  DashboardSecondCell.swift
//  IPAC
//
//  Created by Appinventiv on 26/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class DashboardSecondCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblFbTask: UILabel!
    @IBOutlet weak var lblTwitterTask: UILabel!
    @IBOutlet weak var lblWhatsappTask: UILabel!
    @IBOutlet weak var lblYoutubeProgress: UILabel!
    @IBOutlet weak var lblMediaTask: UILabel!
    @IBOutlet weak var lblFormTask: UILabel!
    @IBOutlet var iconOutlets: [UIImageView]!
    
    @IBOutlet weak var stackViewBottom: UIStackView!
    @IBOutlet weak var viewTwitter: UIView!
    @IBOutlet weak var viewWhatsapp: UIView!
    
    //MARK::- PROPERTIES
    
    var arrIcons = [#imageLiteral(resourceName: "icFb") , #imageLiteral(resourceName: "icProfileTwitter"),#imageLiteral(resourceName: "icWhatsapp"),#imageLiteral(resourceName: "icYoutube"),#imageLiteral(resourceName: "icMedia"),#imageLiteral(resourceName: "icForm")]
    var tagSelected = 0
    
    //MARK::- FUNCTIONS
    
    func populate(task : Task?){
        var completedArray = [task?.completedFbTask,task?.completedTwitterTask,task?.completed_whatsapp_task,task?.completed_youtube_task,task?.completed_offline_task,task?.completed_form_task]

        lblFbTask.text = task?.completedFbTask.description
        lblTwitterTask.text = task?.completedTwitterTask.description
        lblWhatsappTask.text = task?.completed_whatsapp_task.description
        lblYoutubeProgress.text = task?.completed_youtube_task.description
        lblMediaTask.text = task?.completed_offline_task.description
        lblFormTask.text = task?.completed_form_task.description
        hideView(val: tagSelected != 0)
        if tagSelected != 0{
                iconOutlets[0].image = self.arrIcons[tagSelected - 1]
                lblFbTask.text = completedArray[tagSelected - 1]?.description
        }
    }
    
    
    func hideView(val : Bool){
        stackViewBottom.isHidden = val
        viewTwitter.isHidden = val
        viewWhatsapp.isHidden = val
     
    }
}
