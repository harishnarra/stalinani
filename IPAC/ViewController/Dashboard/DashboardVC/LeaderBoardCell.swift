//
//  LeaderBoardCell.swift
//  IPAC
//
//  Created by macOS on 16/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class LeaderBoardCell: UICollectionViewCell {

    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var awardImage: UIImageView!
    @IBOutlet weak var awardCountContainerView: UIView!
    @IBOutlet weak var awardCountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingLabel.textColor = UIColor.black
        ratingLabel.isHidden = true
        self.setupCell()
    }
    
    func setupCell() {
        pictureImageView.rounded(cornerRadius: pictureImageView.frame.height/2, clip: true)
        nameLabel.font = AppFonts.ProximaNova_Semibold.withSize(10)
        ratingLabel.font = AppFonts.ProximaNova_Semibold.withSize(12)
        nameLabel.textColor = UIColor.white
        ratingLabel.textColor = UIColor.black
        awardCountContainerView.backgroundColor = UIColor.black
        awardCountContainerView.setBorder(color: UIColor.red, width: 1.0, cornerRadius: awardCountContainerView.frame.height/2)
    }
    
    func populateCell(with data: TopLeaderboard) {
        self.pictureImageView.kf.setImage(with: URL(string: data.userImage ), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
        if data.ranking <= 3 {
            rankLabel.isHidden = false
            awardImage.isHidden = false
            awardCountContainerView.isHidden = true
            awardCountLbl.isHidden = true
            rankLabel.text = String(describing: data.ranking)
        } else {
            rankLabel.isHidden = true
            awardImage.isHidden = true
            awardCountContainerView.isHidden = false
            awardCountLbl.isHidden = false
        
            awardCountLbl.text = String(describing: data.ranking)
        }
        
        self.nameLabel.text = data.fullName
    }

}
