//
//  MoreCollectionCell.swift
//  IPAC
//
//  Created by macOS on 21/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class MoreCollectionCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var moreLblContainerView: UIView!
    @IBOutlet weak var moreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        moreLblContainerView.rounded(cornerRadius: moreLblContainerView.frame.height/2, clip: true)
        moreLblContainerView.dropShadow(offSet: CGSize(width: 1, height: 1))
    }
    
    func populateCell(with count: Int) {
        let moreText: NSMutableAttributedString = NSMutableAttributedString(string: "+\(count) \n\(StringConstant.More.localized)")
        moreText.setColorForText(textForAttribute: "\(StringConstant.More.localized)", withColor: .black, font: AppFonts.Proxima_Nova_Rg_Regular.withSize(10))
        self.moreLabel.attributedText = moreText
    }

}
