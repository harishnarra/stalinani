//
//  NotificationVC.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable
import PullToRefreshKit

class NotificationVC: BaseViewController {
    
    //MARK: Properties
    //=================
    var notificationList = [NotificationModel]()
    var count = 0
    
    //MARK: IBOutlets
    //=================
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    
    
    //MARK: Life- Cycle
    //=================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()

    }
    
    
    //MARK: IBTargets
    //==============
    @IBAction func backBtnTapp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell else { fatalError("invalid cell \(self)")
        }
        
        cell.populateCell(with: self.notificationList[indexPath.row])
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = formatter.date(from: self.notificationList[indexPath.row].notificationDate) else { return UITableViewCell() }
        let formatterStr = DateFormatter()
        formatterStr.dateFormat = "d MMMM yyyy"
        let string = formatterStr.string(from: date)
        cell.lblDate.text = string
        cell.timeLabel.text = date.elapsedTime
        if indexPath.row == 0 {
            cell.lblDate.isHidden = false
        }else{
            guard let prevDate = formatter.date(from: self.notificationList[indexPath.row - 1].notificationDate) else { return UITableViewCell() }
            cell.lblDate.isHidden = Calendar.current.isDate(date, inSameDayAs: prevDate)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
            
        didSelect(index : indexPath.row)


    }
}

//MARK::- FUNCTION
extension NotificationVC{
    func didSelect(index : Int){
        switch /self.notificationList[index].notificationType{
        case "add_gallery" ,"add_article" ,"add_news","form":
            for item in self.navigationController?.viewControllers ?? []{
                if item is HomeTabBarVC{
                    (item as? HomeTabBarVC)?.selectedIndex = 0
                    break
                }
            }
            self.navigationController?.popViewController(animated: true)
        case "add_task":
            for item in self.navigationController?.viewControllers ?? []{
                if item is HomeTabBarVC{
                    (item as? HomeTabBarVC)?.selectedIndex = 1
                    break
                }
            }
            self.navigationController?.popViewController(animated: true)
        case "profile_request":
            let vc = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
            vc.isSidePanel = false
            self.navigationController?.pushVC(vc)
        default:
            break
        }
    }
}



//MARK: Private Methods
//========================
private extension NotificationVC {
    
    func initialSetup() {
        handlePaging()
        self.getNotifiactionList(loader: true)
        setTexts()
        tableView.delegate   = self
        tableView.dataSource = self
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))

    }
    
    func setTexts () {
        titleLabel.text = StringConstant.Notification.localized
        titleLabel.font = AppFonts.ProximaNova_Semibold.withSize(17)
        lblNoData.text = AlertMessage.noRecordFound.localized
    }
    
    
    func getNotifiactionList(loader: Bool) {
        WebServices.notification(params: ["count":self.count.description], loader: loader, success: { (json) in
            print_debug(json)
            let notificationData = json[ApiKeys.result].arrayValue
            if self.count == 0{
                self.notificationList = []
            }
            notificationData.forEach({ (notification) in
                let notification = NotificationModel(dict: notification)
                self.notificationList.append(notification)
            })
            self.lblNoData.isHidden = !(self.notificationList.isEmpty )
            self.tableView.switchRefreshFooter(to: .normal)
            self.count = Int(json["NEXT"].stringValue) ?? -1
            self.tableView.reloadData()
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
}

//MARK::- PAGINATION
extension NotificationVC{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableView.switchRefreshFooter(to: .normal) :  self.getNotifiactionList(loader: false)
        }
    }
}

//MARK::- PULL TO REFRESH
extension NotificationVC{
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.count = 0
        self.getNotifiactionList(loader: false)
        
    }
}

//MARK:- Prototype Cell
//=========================
class NotificationCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    var previousDate : Date?

    //MARK:- IBoutlets
    //================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imgIcon: AnimatableImageView!
    @IBOutlet weak var lblDate: UILabel!
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func populateCell( with data: NotificationModel) {
        
        switch /data.notificationType{
        case "add_gallery":
            self.titleLabel.text = "Gallery"
            self.imgIcon.image = #imageLiteral(resourceName: "icTasksNews")
            break
        case "add_article":
            self.titleLabel.text = "Article"
            self.imgIcon.image = #imageLiteral(resourceName: "icTasksNews")
            break
        case "add_news":
            self.titleLabel.text = "News"
            self.imgIcon.image = #imageLiteral(resourceName: "icTasksNews")
            break
        case "add_notification"://///
            self.titleLabel.text = "Notification"
            self.imgIcon.image = #imageLiteral(resourceName: "icTasksNews")
            break
        case "add_task":
            self.titleLabel.text = "Task"
            self.imgIcon.image = #imageLiteral(resourceName: "icTasksTasks")
            break
        case "bonus_received":////
            self.titleLabel.text = "Bonus Received"
            break
        case "profile_request":
            self.titleLabel.text = "Profile Request"
            break
        case "form":
            self.titleLabel.text = "Form"
            break
        default:
            self.titleLabel.text = /data.notificationType
            self.imgIcon.image = #imageLiteral(resourceName: "icSidemenuAbout")
            break
        }
        self.descLabel.text = data.notificationText
        
    }
}




