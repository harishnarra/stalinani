//
//  LeaderboadVC.swift
//  IPAC
//
//  Created by macOS on 30/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class LeaderboadVC: UIViewController {

    @IBOutlet weak private var popupView: UIView!
    @IBOutlet weak private var leaderboardLabel: UILabel!
    @IBOutlet weak private var leaderboardCollectionView: UICollectionView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var leaderboardData = [TopLeaderboard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view)
        if !self.popupView.frame.contains(point) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}


//MARK: - Collection view Delegate and DataSource
//============================================
extension LeaderboadVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return leaderboardData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaderBoardCell", for: indexPath) as? LeaderBoardCell else { fatalError("invalid collection view cell \(self)") }
        cell.populateCell(with: leaderboardData[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/4) - 12, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(12, 5, 12, 10) //2 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}

//MARK::- FUNCTIONS
 extension LeaderboadVC {
    func initialSetup() {
        setTexts()
        registerXib()
        setupView()
        leaderboardCollectionView.backgroundColor = UIColor.black
        leaderboardCollectionView.delegate   = self
        leaderboardCollectionView.dataSource = self
        self.lblNoData.text = AlertMessage.noRecordFound.localized
        self.lblNoData.isHidden = !(leaderboardData.count == 0)
        
    }
    
    func setTexts () {
        leaderboardLabel.text = StringConstant.LEADERBOARD.localized
        leaderboardLabel.font = AppFonts.ProximaNova_Semibold.withSize(12)
    }
    
    func registerXib(){
        leaderboardCollectionView.register(UINib(nibName: "LeaderBoardCell", bundle: nil), forCellWithReuseIdentifier: "LeaderBoardCell")
    }
    
    func setupView() {
        popupView.rounded(cornerRadius: 5.0, clip: true)
    }
}
