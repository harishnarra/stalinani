//
//  WhatsappPopupVC.swift
//  IPAC
//
//  Created by Appinventiv on 02/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit


//MARK::- PROTOCOL
protocol WhatsappPopupDismiss : class{
    func dismiss()
}

class WhatsappPopupVC: BaseViewController {
    
    
    //MARK::- PROPERTIES
    weak var delegate : WhatsappPopupDismiss?
    var task : TaskTabModel?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MATK::- BUTTON ACTION
    @IBAction func btnActionOk(_ sender: UIButton) {
        self.dismissVC {
            self.delegate?.dismiss()
        }
        
    }
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
}

////MARK::- API
//extension WhatsappPopupVC{
//    private func updateTask(taskId: String , _ success : SuccessResponse? = nil){
//        let params = [ApiKeys.taskId: taskId]
//        WebServices.updateTask(params: params, success: { [weak self](result) in
//            print_debug(result)
//            success?(result)
//
//        }) { (error) -> (Void) in
//            CommonFunction.showToast(msg: error.localizedDescription)
//        }
//    }
//}

