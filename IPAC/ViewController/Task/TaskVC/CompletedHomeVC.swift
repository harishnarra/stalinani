//
//  CompletedHomeVC.swift
//  Stalinani
//
//  Created by Haritej on 18/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import PullToRefreshKit


class CompletedHomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var completedBtLabel: UILabel!
    var count = 0
    var referData : CompletedHomeModel?
    var arrData : [COmpletedHomeTaskResultData]?  = []
    var recieveTaskID : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isHidden = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.handlePaging()
        self.getDataFromAPICompletedHome(loader: true)
    }
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func getDataFromAPICompletedHome(loader : Bool){
        WebServices.completedHomeList(count: self.count.description, taskID: self.recieveTaskID?.description ?? "", loader
            , success: { [weak self] (json) in
                self?.referData = CompletedHomeModel.init(json: json)
                self?.tableView.isHidden = false
                self?.tableView.switchRefreshFooter(to: .normal)
                for item in self?.referData?.getcompletedHomeResult ?? []{
                    self?.arrData?.append(item)
                }
                if self?.count == 0{
                    self?.arrData = self?.referData?.getcompletedHomeResult ?? []
                }
                self?.count = self?.referData?.next?.toInt() ?? -1
                self?.tableView.reloadData()
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableView.switchRefreshFooter(to: .normal) :
                self.getDataFromAPICompletedHome(loader : false)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompletedHomeCell") as! CompletedHomeCell
        cell.selectionStyle = .none
        tableView.separatorStyle = .none
        cell.completedHomePhoto.kf.setImage(with: URL(string : /arrData?[indexPath.row].userImage), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.completedHomeName.text = /arrData?[indexPath.row].fullName
        
        if /arrData?[indexPath.row].like_type == "clap" {
            cell.completedHomeLikeType.image = UIImage(named: "Clap")
        } else if /arrData?[indexPath.row].like_type == "heart" {
            cell.completedHomeLikeType.image = UIImage(named: "FilledHeart")
        } else if /arrData?[indexPath.row].like_type == "fist" {
            cell.completedHomeLikeType.image = UIImage(named: "Fist")
        } else {
            cell.completedHomeLikeType.image = UIImage(named: "EmptyHeart")
        }
        return cell
    }
}

  
