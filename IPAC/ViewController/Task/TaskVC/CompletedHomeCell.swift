//
//  CompletedHomeCell.swift
//  Stalinani
//
//  Created by Haritej on 18/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class CompletedHomeCell: UITableViewCell {

    @IBOutlet weak var completedHomePhoto: AnimatableImageView!
    @IBOutlet weak var completedHomeName: UILabel!
    @IBOutlet weak var completedHomeLikeType: AnimatableImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
