//
//  FacebookTaskVC.swift
//  IPAC
//
//  Created by Appinventiv on 19/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//
import UIKit
import WebKit

//MARK::- PROTOCOL
protocol FacebookTaskDelegates : class {
    func refresh()
}

class FacebookTaskVC: BaseViewController, WKNavigationDelegate {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var webViewW: WKWebView!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK::- PROEPRTIES
    private var jsHandler = ""
    private var mpAjaxHandler = "mpAjaxHandler"
    
    var isNeedToCheckContent = false
    var task : TaskTabModel?
    weak var delegate : FacebookTaskDelegates?
    var currentUrl = ""
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonFunction.showLoader()
        self.webViewW.navigationDelegate = self

        
        
       // webViewW.navigationDelegate = self
        
//         if let url =  URL(string: "https://www.google.com/"){
//                   let request = URLRequest(url:url)
//                   self.webViewW.load(request)
//               }else{
//                   self.popVC()
//                   CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
//               }
        onViewDidLoad()
    }
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        if !isPost(str : /self.task?.post_url){
            self.popVC()
            CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
            return
        }
        isNeedToCheckContent = true
        if let url =  URL(string: /self.task?.post_url){
            let request = URLRequest(url:url)
            self.webViewW.load(request)
        }else{
            self.popVC()
            CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
        }
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
           CommonFunction.showLoader()
           print("one")
          let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
                 
                 webView.evaluateJavaScript(jscript)
           
       }
       func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
           CommonFunction.hideLoader()
        
       
            currentUrl = /webView.url?.absoluteString
              if isNeedToCheckContent{
                
                evaluate(script: "document.documentElement.outerHTML") { (resul, error) in
                    print(resul ?? "")
                }


              }
           print("two")
       }
    func evaluateJavaScript(_ javaScriptString: String,
                            completionHandler: ((_ res: String?, Error?) -> Void)? = nil) {
        
    }
    func evaluate(script: String, completion: @escaping (Any?, Error?) -> Void) {
        var finished = false

        evaluateJavaScript(script, completionHandler: { (result, error) in
            if error == nil {
                if result != nil {
                    completion(result, nil)
                    if self.isPost(str: /self.task?.post_url) && (result?.contains("/ufi/reaction"))! ||  !self.isPost(str: /self.task?.post_url) && (result?.contains("add=true"))!{
                        self.updateTask()
                                       }else{
                                           CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                                           self.popVC()
                                       }
                }
            } else {
                completion(nil, error)
            }
            finished = true
        })

        while !finished {
            RunLoop.current.run(mode: RunLoop.Mode(rawValue: "NSDefaultRunLoopMode"), before: NSDate.distantFuture)
        }
    }
    
       
       func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("three")
           
       }
       func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
           CommonFunction.hideLoader()
           CommonFunction.showToast(msg: error.localizedDescription)
           print("four")
           
       }
       func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
           if let str = navigationAction.request.url?.absoluteString{
             //urlStr is what you want, I guess.
              // print(urlStr)
            
//            print_debug(/request.url?.absoluteString)
//            let str = /request.url?.absoluteString
            let taskComplete = str.contains("add=true")
            if ((navigationAction.request.url?.scheme?.lowercased().isEqual(mpAjaxHandler.lowercased()) ?? false) && taskComplete)  {
                let arrOne = self.task?.post_url.split("facebook.com/")
                let arrTwo = self.currentUrl.split("facebook.com/")
                let userNameFromBackend = arrOne?.last?.replacingOccurrences(of: "/", with: "")
                let userNameInWeb = arrTwo.last?.replacingOccurrences(of: "/", with: "")
                if userNameFromBackend == userNameInWeb{
                    self.updateTask()
                }
                           decisionHandler(.allow)
            }
            decisionHandler(.cancel)
            
            
               
           
          }
          
           //        return true
          // decisionHandler(.allow)
       }


//MARK::- FUNCTIONS
    func onViewDidLoad(){
        self.webViewW.navigationDelegate = self
//        webVieww.delegate = self
        if self.task?.post_url != "" {
        if let url =  URL(string: /self.task?.post_url){
            let request = URLRequest(url:url)
            self.webViewW.load(request)
        }
        if !self.isPost(str: /self.task?.post_url){
            if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
                do {
                    jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
                }
                catch let error {
                    print("Error: \(error)")
                }
            }
        }
        if !CommonFunction.isFbPopupHidden(){
            let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
            vc.isFacebook = true
            self.presentVC(vc)
        }
        } else {
            let pageURL = "https://www.google.co.in/"
            if let url =  URL(string: pageURL){
                let request = URLRequest(url:url)
                self.webViewW.load(request)
            }
            if !self.isPostFollow(urls: pageURL, id:/CommonFunction.getUser()?.facebookId){
                print(pageURL,/CommonFunction.getUser()?.facebookId)
                if let anExtension = Bundle.main.url(forResource: "EventHandler", withExtension: "js") {
                    do {
                        jsHandler = try String(contentsOf: anExtension, encoding: String.Encoding.utf8)
                    }
                    catch let error {
                        print("Error: \(error)")
                    }
                }
            }
            if !CommonFunction.isFbPopupHidden(){
                let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
                vc.isFacebook = true
                self.presentVC(vc)
            }
        }
    }
    func isPost(str : String) -> Bool{
        return (str.contains("/posts") || str.contains("/videos") || str.contains("/photos"))
        
    }
    func isPostFollow(urls : String, id: String) -> Bool{

      return (urls.contains("subscriptions") || urls.contains("subscription") && urls.contains("add") && urls.contains(id)
        || (urls.contains("follow_mutator") && urls.contains("status=true") && urls.contains(id)))
    }
}

//extension FacebookTaskVC : WKNavigationDelegate {
//    
//}

//MARK::- API
extension FacebookTaskVC{
    private func updateTask(){
        let params = [ApiKeys.taskId: /self.task?.task_id]
        WebServices.updateTask(params: params, success: { [weak self](result) in
            print_debug(result)
            self?.delegate?.refresh()
            self?.navigationController?.popViewController(animated: false)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.popVC()
        }
    }
}

