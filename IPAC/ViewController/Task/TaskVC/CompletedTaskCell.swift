//
//  CompletedTaskCell.swift
//  Stalinani
//
//  Created by Haritej on 17/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class CompletedTaskCell: UITableViewCell {

    @IBOutlet weak var completedPhoto: AnimatableImageView!
    @IBOutlet weak var completedName: UILabel!
    @IBOutlet weak var completedPhone: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
