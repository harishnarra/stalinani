//
//  YoutubeStepPopupVC.swift
//  IPAC
//
//  Created by Appinventiv on 15/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

class TwitterFollowStepPopupVC: BaseViewController {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var btnShowAgain: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    
    //MARK::- PROPERTIES
    var isFacebook = false
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    
    @IBAction func btnActionOK(_ sender: UIButton) {
        if btnShowAgain.isSelected{
            isFacebook ? CommonFunction.hideFbPopup() : CommonFunction.hideTwitterFollowPopup()
        }
        self.dismissVC(completion: nil)
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    @IBAction func btnActionShowAgain(_ sender: UIButton) {
       // sender.isSelected.toggled = true
        
    }
    
}

//MARK::- FUNCTIONS
extension TwitterFollowStepPopupVC {
    func onViewDidLoad(){
        lblDesc.text = isFacebook ? "If you have already liked the page/post, please dislike and like it again to complete the task.".localized : "Please complete the task from the web page and tap the back to app button, your task status will be updated basis on task performed.".localized
    }
}
