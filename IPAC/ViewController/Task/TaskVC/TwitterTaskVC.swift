//
//  TwitterTaskVC.swift
//  IPAC
//
//  Created by Appinventiv on 17/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit


//MARK::- PROTOCOL
protocol TwitterTaskDelegate : class {
    func refresh()
}

class TwitterTaskVC: BaseViewController, WKNavigationDelegate {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var webView:WKWebView!
    //MARK::- PROEPRTIES
    var task : TaskTabModel?
    weak var delegate : TwitterTaskDelegate?
    var urlToLoad = ""
    
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunction.showLoader()

        onViewDidLoad()
    }
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        CommonFunction.showLoader()

        checkIfFolloOrLikeDone()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        CommonFunction.showLoader()
        print("one")
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
                 
                 webView.evaluateJavaScript(jscript)
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonFunction.hideLoader()
        print("two")
//        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='none'"//dual size
//        webView.evaluateJavaScript(js, completionHandler: nil)
//        
        //CommonFunction.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
        CommonFunction.showToast(msg: error.localizedDescription)
        print("three")
        
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        CommonFunction.hideLoader()
        CommonFunction.showToast(msg: error.localizedDescription)
        print("four")
        
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString{
          //urlStr is what you want, I guess.
            print(urlStr)
            
            print_debug(urlStr)
            if ((/urlStr).contains("complete?")) {
                               checkIfFolloOrLikeDone()
                           }
       }
       
        //        return true
        decisionHandler(.allow)
    }
}

//MARK::- FUNCTIONS
extension TwitterTaskVC{
    func onViewDidLoad(){
        self.webView.navigationDelegate = self
        switch (self.task?.action ?? .follow){
        case .follow:
            urlToLoad = "https://twitter.com/intent/follow?screen_name=" + /self.task?.twitter_follow_name
            print(urlToLoad)
            
        case .like:
            urlToLoad = /self.task?.post_url
        default:
            break
        }
        if let url =  URL(string: urlToLoad){
            let request = URLRequest(url:url)
            self.webView.load(request)
        }
        if !CommonFunction.isTwitterFollowPopupHidden(){
            let vc = TwitterFollowStepPopupVC.instantiate(fromAppStoryboard: .Form)
            self.presentVC(vc)
        }
        
    }
}

//MARK::- API

extension TwitterTaskVC{
    private func updateTask(){
        let params = [ApiKeys.taskId: /self.task?.task_id]
        WebServices.updateTask(params: params, success: { [weak self](result) in
            print_debug(result)
            self?.delegate?.refresh()
            self?.navigationController?.popViewController(animated: false)
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    private func checkIfFolloOrLikeDone(){
        if (self.task?.action ?? .follow) == .follow{
            TwitterController.shared.getUserFollowings(task: self.task, isSuccess: { (val) in
                if val==true {
                    self.updateTask()
                }else{
                    CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                    CommonFunction.hideLoader()

                    self.popVC()
                }
            })
        } else if (self.task?.action ?? .follow) == .like{
            TwitterController.shared.getUserLikes(task: self.task, isSuccess: { (val) in
                if val==true {
                    self.updateTask()
                }else{
                    CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
                    CommonFunction.hideLoader()

                    self.popVC()
                }
            })
        }
        else if (self.task?.action ?? .follow) == .comment{
            TwitterController.shared.getUserLikes(task: self.task, isSuccess: { (val) in
                                    self.updateTask()

            })
        }
        CommonFunction.hideLoader()

    }
    
    
    
    
    
}



