//
//  TaskVCExtension.swift
//  IPAC
//
//  Created by Appinventiv on 22/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import TwitterKit
import EZSwiftExtensions
import FBSDKShareKit
import PullToRefreshKit
import CoreLocation
import TwitterCore
import FacebookShare
import FBSDKLoginKit
import FacebookLogin

//MARK::- DELEGATE TASK
extension TaskVC : RefreshData,TaskComplete,TaskCompleteDelegate , ImageUploaded , YoutubeTaskDelegate,TwitterTaskDelegate , FacebookTaskDelegate{
    func refresh(){
        self.count = 0
        self.getHomeTask(loader: false )
    }
    func success(arr: [Image]) {
        refresh()
    }
}

extension TaskVC : QRCodeScannerControllerDelegate , WhatsappPopupDismiss {
    
    func finishScanning(withResult result: String) {
        self.refresh()
    }
    
    func dismiss() {
        guard let url  = URL(string: "whatsapp://app") else {return}
        if UIApplication.shared.canOpenURL(url) {
            UIImageView().kf.setImage(with: URL(string : /self.taskLists[(shareCellIndexPath?.row) ?? 0].task_media_set.first?.mediaUrl), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (img, _, _, _) in
                self.isShare = false
                UIImageWriteToSavedPhotosAlbum(img ?? UIImage(), self,#selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            })
        }else{
            CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
        }
    }
    
    //MARK::- SELECTOR TARGET
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            CommonFunction.showToast(msg: error.localizedDescription)
        } else {
            AlertsClass.shared.showAlertController(withTitle: "", message: self.isShare ? AlertMessage.shareImageMsg.localized :  AlertMessage.whatsappDPMsg.localized, buttonTitles: [TitleType.ok.localized ], responseBlock: { (tag) in
                switch tag{
                case .yes:
                    guard let url  = URL(string: "whatsapp://app") else {return}
                    if UIApplication.shared.canOpenURL(url){
                        if self.isShare{
                            self.updateTask(taskId: self.task?.task_id ?? "", { (_) in
                                UIApplication.shared.open(url, options: [:], completionHandler: { (val) in
                                    val ? self.refresh() : ()
                                    CommonFunction.showToast(msg: AlertMessage.reviewTheTask.localized)
                                })
                            })
                        }else{
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                    else{
                        CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
                    }
                default:
                    break
                }
              
            }
        )
    }
}
}



//MARK:- Webservices
extension TaskVC{
    
    func share(indexPath: IndexPath){
        let task = self.taskLists[indexPath.row]
        self.task = task
        switch task.task_type {
        case .online:
            switch task.action{
            case .form:
                let vc = WebViewVC.instantiate(fromAppStoryboard: .Form)
                vc.task = task
                vc.flag = 2
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
            
        case .offline:
            switch task.action{
            case .downLoadMedia:
                let vc = TaskDetailVC.instantiate(fromAppStoryboard: .Form)
                vc.task = task
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            case .event:
                if CLLocationManager.authorizationStatus() == .denied{
                    LocationManager.shared.updateLocation()
                    return
                }
                LocationManager.shared.updateLocation()
                let vc = EventTaskVC.instantiate(fromAppStoryboard: .Form)
                vc.task = task
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            case .qrcode:
                let sceen = QRCodeScannerController()
                sceen.delegate = self
                sceen.taskId = task.task_id
                sceen.taskCode = task.task_code
                self.navigationController?.pushViewController(sceen, animated: true)
            default:
                break
            }
            
        case .whatsapp:
            switch task.action{
            case .setdp:
                let vc = WhatsappPopupVC.instantiate(fromAppStoryboard: .Form)
                vc.delegate = self
                vc.task = task
                self.navigationController?.presentVC(vc)
            case .aboutUs,.createGroup :
                guard let url  = URL(string: "whatsapp://app") else {return}
                if UIApplication.shared.canOpenURL(url) {
                    self.updateTask(taskId: task.task_id, { (_) in
                        UIApplication.shared.open(url, options: [:], completionHandler: { (val) in
                            CommonFunction.showToast(msg: AlertMessage.reviewTheTask.localized)
                            val ? self.refresh() : ()
                        })
                        
                    })
                } else {
                    CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
                }
            
            case .share, .image, .link, .video:
                if /task.task_media_set.first?.mediaUrl != "" &&  /task.task_media_set.first?.mediaType != "2"{
                    CommonFunction.showLoader()
                    UIImageView().kf.setImage(with: URL(string : /task.task_media_set.first?.mediaUrl), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (img, _, _, _) in
                        CommonFunction.hideLoader()
                        self.isShare = true
                        UIImageWriteToSavedPhotosAlbum(img ?? UIImage(), self,#selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                        //self.shareImageToWhatsapp(img: img ?? UIImage() , task : task)
                    })
                    
                    
                    
                }else{
                    let encryptMsg = task.task_description.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
                    guard let url  = URL(string: "whatsapp://send?text=\(encryptMsg)") else {return}
                    if UIApplication.shared.canOpenURL(url) {
                        self.updateTask(taskId: task.task_id, { (_) in
                            UIApplication.shared.open(url, options: [:], completionHandler: { (val) in
                                val ? self.refresh() : ()
                                CommonFunction.showToast(msg: AlertMessage.reviewTheTask.localized)
                                
                            })
                        })
                    } else {
                        CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
                    }
                }
                
            default:
                break
                
            }
            
        case .youtube:
            switch task.action{
            case .like,.subscribe,.share,.comment:
                let vc = YoutubeTaskVC.instantiate(fromAppStoryboard: .Form)
                vc.delegate = self
                vc.task = task
                self.pushVC(vc)
                
            default:
                break
            }
            
        case .install:
            switch task.action{
            case .install:
//                let vc = YoutubeTaskVC.instantiate(fromAppStoryboard: .Form)
//                vc.delegate = self
//                vc.task = task
//                self.pushVC(vc)
                
                print("we have to add here")
                
            default:
                break
            }
            
        case .facebook:
            switch task.action{
            case .like:
                print_debug(task.post_url)
             //   let vc = FacebookTaskVC.instantiate(fromAppStoryboard: .Form)
                let vc = FacebookTaskNewVC.instantiate(fromAppStoryboard: .Form)

               vc.delegate = self
                vc.task = task
                self.pushVC(vc)
                
            case .follow:
                print_debug(task.post_url)
             //   let vc = FacebookTaskVC.instantiate(fromAppStoryboard: .Form)
                let vc = FacebookTaskNewVC.instantiate(fromAppStoryboard: .Form)

                vc.delegate = self
                vc.task = task
                self.pushVC(vc)
                
            case .post:
                          print_debug(task.post_url)
                       //   let vc = FacebookTaskVC.instantiate(fromAppStoryboard: .Form)
                          let vc = FacebookTaskNewVC.instantiate(fromAppStoryboard: .Form)

                          vc.delegate = self
                          vc.task = task
                          self.pushVC(vc)
                
            case .share:
                print(/CommonFunction.getUser()?.facebookId)
                print_debug(task.post_url)
                //   let vc = FacebookTaskVC.instantiate(fromAppStoryboard: .Form)
                   let vc = FacebookTaskNewVC.instantiate(fromAppStoryboard: .Form)

                   vc.delegate = self
                   vc.task = task
                   self.pushVC(vc)
                
//                if /CommonFunction.getUser()?.facebookId == ""{
//                    FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result, tocken) in
//                        let param = [ApiKeys.facebookId: /result.id, ApiKeys.fbUsername: /result.name]
//                        self.updateProfileFb(params: param, loader: false, task: nil)
//                        let content = ShareLinkContent()
//                        content.contentURL = URL(string: task.post_url)!
//                        ShareDialog.init(fromViewController: self, content: content, delegate: self)
//                       // ShareDialog.show(from: self, with: content, delegate: self)
//                    }) { (error) in
//                        if let err = error {
//                            CommonFunction.showToast(msg: err.localizedDescription)
//                        } else {
//                            CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
//                        }
//                    }
//                }else{
//                    let content = ShareLinkContent()
//                    content.contentURL = URL(string: task.post_url)!
//                    ShareDialog.init(fromViewController: self, content: content, delegate: self)
//
//                }
                
            default:
                break
            }
            
        case .twitter:
            
            switch task.action{
            case .like , .follow:
                if TWTRTwitter.sharedInstance().sessionStore.session() != nil && TWTRTwitter.sharedInstance().sessionStore.session()?.userID == /CommonFunction.getUser()?.twitterId{
                    let vc = TwitterTaskVC.instantiate(fromAppStoryboard: .Form)
                    vc.delegate = self
                    vc.task = task
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                TwitterController.shared.loginWithTwitter(completion: { (success, twitterData) in
                    if success {
                        if twitterData?.userID != /CommonFunction.getUser()?.twitterId && /CommonFunction.getUser()?.twitterId != ""{
                            CommonFunction.showToast(msg: AlertMessage.twitterAccountNotMatched.localized)
                            TwitterController.shared.logout()
                            return
                        }
                        let param = [ApiKeys.twitterId: /twitterData?.userID, ApiKeys.twitterUsername: /twitterData?.name]
                        self.updateProfileTwitter(params: param , loader: false, task: nil)
                        let vc = TwitterTaskVC.instantiate(fromAppStoryboard: .Form)
                        vc.delegate = self
                        vc.task = task
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
                    }
                })
                
            case .tweet:
                if TWTRTwitter.sharedInstance().sessionStore.session() != nil && TWTRTwitter.sharedInstance().sessionStore.session()?.userID == /CommonFunction.getUser()?.twitterId{
                    let param = ["user_id": /TWTRTwitter.sharedInstance().sessionStore.session()?.userID , "count": "1"] as [String : Any]
                    self.goToTwitterWebView(param: param, task: task)
                    return
                }
                TwitterController.shared.loginWithTwitter(completion: { (success, twitterData) in
                    if success {
                        if twitterData?.userID != /CommonFunction.getUser()?.twitterId && /CommonFunction.getUser()?.twitterId != ""{
                            CommonFunction.showToast(msg: AlertMessage.twitterAccountNotMatched.localized)
                            TwitterController.shared.logout()
                            return
                        }
                        let params = [ApiKeys.twitterId: /twitterData?.userID, ApiKeys.twitterUsername: /twitterData?.name]
                        self.updateProfileTwitter(params: params, loader: false, task: nil)
                        let param = ["user_id": /twitterData?.userID , "count": "1"] as [String : Any]
                        self.goToTwitterWebView(param: param, task: task)
                    }
                    else {
                        CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
                    }
                })
                case .comment:
                if TWTRTwitter.sharedInstance().sessionStore.session() != nil && TWTRTwitter.sharedInstance().sessionStore.session()?.userID == /CommonFunction.getUser()?.twitterId{
                    let param = ["user_id": /TWTRTwitter.sharedInstance().sessionStore.session()?.userID , "count": "1"] as [String : Any]
                    self.goToTwitterWebView2(param: param, task: task)
                    let twetterSceen = TwitterWebViewVC.instantiate(fromAppStoryboard: .Home)
                                      twetterSceen.taskModel = task
                                      twetterSceen.isRetweet = true
                                      twetterSceen.delegate = self
                                      self.navigationController?.pushViewController(twetterSceen, animated: true)
                                      
                    return
                }
                TwitterController.shared.loginWithTwitter(completion: { (success, twitterData) in
                    if success {
                        if twitterData?.userID != /CommonFunction.getUser()?.twitterId && /CommonFunction.getUser()?.twitterId != ""{
                            CommonFunction.showToast(msg: AlertMessage.twitterAccountNotMatched.localized)
                            TwitterController.shared.logout()
                            return
                        }
                        let params = [ApiKeys.twitterId: /twitterData?.userID, ApiKeys.twitterUsername: /twitterData?.name]
                        self.updateProfileTwitter(params: params, loader: false, task: nil)
                        let param = ["user_id": /twitterData?.userID , "count": "1"] as [String : Any]
                        self.goToTwitterWebView2(param: param, task: task)
                        let twetterSceen = TwitterWebViewVC.instantiate(fromAppStoryboard: .Home)
                                                          twetterSceen.taskModel = task
                                                          twetterSceen.isRetweet = true
                                                          twetterSceen.delegate = self
                                                          self.navigationController?.pushViewController(twetterSceen, animated: true)
                    }
                    else {
                        CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
                    }
                })
                
            case .retweet:
                if TWTRTwitter.sharedInstance().sessionStore.session() != nil && TWTRTwitter.sharedInstance().sessionStore.session()?.userID == /CommonFunction.getUser()?.twitterId{
                    let twetterSceen = TwitterWebViewVC.instantiate(fromAppStoryboard: .Home)
                    twetterSceen.taskModel = task
                    twetterSceen.isRetweet = true
                    twetterSceen.delegate = self
                    self.navigationController?.pushViewController(twetterSceen, animated: true)
                    return
                }
                TwitterController.shared.loginWithTwitter(completion: { (success, twitterData) in
                    if success {
                        if twitterData?.userID != /CommonFunction.getUser()?.twitterId && /CommonFunction.getUser()?.twitterId != ""{
                            CommonFunction.showToast(msg: AlertMessage.twitterAccountNotMatched.localized)
                            TwitterController.shared.logout()
                            return
                        }
                        let param = [ApiKeys.twitterId: /twitterData?.userID, ApiKeys.twitterUsername: /twitterData?.name]
                        self.updateProfileTwitter(params: param , loader: false, task: nil)
                        let twetterSceen = TwitterWebViewVC.instantiate(fromAppStoryboard: .Home)
                        twetterSceen.taskModel = task
                        twetterSceen.isRetweet = true
                        twetterSceen.delegate = self
                        self.navigationController?.pushViewController(twetterSceen, animated: true)
                    }
                    else {
                        CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
                    }
                })
            default:
                break
            }
        default:
            print_debug(task.task_type)
        }
    }
    
    
}
