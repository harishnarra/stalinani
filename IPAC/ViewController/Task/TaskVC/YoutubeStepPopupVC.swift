//
//  YoutubeStepPopupVC.swift
//  IPAC
//
//  Created by Appinventiv on 15/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class YoutubeStepPopupVC: BaseViewController {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var btnShowAgain: UIButton!
    
    //MARK::- PROPERTIES
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    
    @IBAction func btnActionOK(_ sender: UIButton) {
        if btnShowAgain.isSelected{
            CommonFunction.hideYoutubePopup()
        }
        self.dismissVC(completion: nil)
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    @IBAction func btnActionShowAgain(_ sender: UIButton) {
       // sender.isSelected.toggle()
    }
    
    
}
