//
//  TaskCell.swift
//  IPAC
//
//  Created by macOS on 29/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVKit
import iCarousel
import Kingfisher
import AVFoundation
import EZSwiftExtensions
import IBAnimatable

class TaskCell: UITableViewCell {

    @IBOutlet weak var completedHeight: NSLayoutConstraint!
    @IBOutlet weak var completedLabel: UILabel!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var awardLabel: UILabel!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var taskDescLabel: UILabel!
    @IBOutlet weak var viewForCarousel: iCarousel!
    @IBOutlet weak var carouselViewHeight: NSLayoutConstraint!
    @IBOutlet weak var completedOthersLabel: UILabel!
    @IBOutlet weak var completedUser1: AnimatableImageView!
    @IBOutlet weak var completedUser2: AnimatableImageView!
    @IBOutlet weak var socialContainerView: UIView!
    @IBOutlet weak var socialImageView: UIImageView!
    @IBOutlet weak var timerBtn: UIButton!
    @IBOutlet weak var tweetBtn: GradientButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var isTaskCompleteBtn: UIButton!
    @IBOutlet weak var tweetBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var horizontalSeperatorView: UIView!
    @IBOutlet weak var btnInstruction: UIButton!
    @IBOutlet weak var btnUploadFile: UIButton!
    @IBOutlet weak var viewAction: UIView!
    @IBOutlet weak var heightInstructionBtn: NSLayoutConstraint!
    @IBOutlet weak var heightUploadFileBtn: NSLayoutConstraint!
    
    
    var mediaURls : [String] = []
    var mediaTypes: [String] = []
    var mediaThumbnail = [String]()
    var task : TaskTabModel?
    var parentVC: UIViewController?
    var countdown : Timer? =  Timer()
    var totalTimeInSec = 59
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.timerBtn.isHidden =  true
        self.timerBtn.setTitle("", for: .normal)
        countdown?.invalidate()
        countdown = nil
        backView.backgroundColor =  UIColor.black
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timerBtn.isHidden = true
        viewForCarousel.dataSource = self
        viewForCarousel.delegate = self
        viewForCarousel.reloadData()
        viewForCarousel.type = .invertedTimeMachine
        backView.backgroundColor =  UIColor.black
        self.backView.rounded(cornerRadius: 5.0, clip: false)
        self.backgroundColor = .clear
        
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
         tweetBtn.rounded(cornerRadius: tweetBtn.frame.height/2, clip: true)
         viewAction.rounded(cornerRadius: viewAction.frame.height/2, clip: true)
        socialContainerView.rounded(cornerRadius: socialContainerView.frame.height/2, clip: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnActionInstruction(_ sender: UIButton) {
        if self.task?.instruction_video_url != ""{
            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            vc.url = self.task?.instruction_video_url
            vc.desc = self.task?.instruction_description
            ez.topMostVC?.presentVC(vc)
            
        }else{
            let vc = TaskInstructionVC.instantiate(fromAppStoryboard: .Form)
            vc.desc = self.task?.instruction_description
            ez.topMostVC?.presentVC(vc)
        }
    }
    
    
    func configureCellWithTask(_ task: TaskTabModel, parent: UIViewController? = nil) {
        self.task = task
        self.viewAction.isHidden = true
        self.timerBtn.setImage(#imageLiteral(resourceName: "icVerificationWatch"), for: .normal)
        self.btnUploadFile.isHidden = !(task.action == .setdp) || !(task.action == .video) || !(task.action == .image)
        self.btnInstruction.isHidden = (task.instruction_video_url == "") && (task.instruction_description == "")
        self.taskNameLabel.text = task.task_title
        self.taskDescLabel.text = task.task_description
        tweetBtn.setTitle(task.action.rawValue.uppercased(), for: .normal)
        self.awardLabel.text = task.points
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = formatter.date(from: task.end_date) else { return }
        guard let dateCreated = formatter.date(from: task.start_date) else { return }
        let displayDate = dateCreated.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
        lblDate.text = displayDate
        var now = Date()
        self.mediaTypes.removeAll()
        self.mediaURls.removeAll()
        self.mediaThumbnail.removeAll()
        if now >= date  {
            self.stopTimer()
            timerBtn.isHidden = true
            backView.backgroundColor =  UIColor.init(hexString: "#8c8c8c")
        //    backView.backgroundColor =  UIColor.black


        } else if task.taskComplete != "1" && task.taskComplete != "2"{
            totalTimeInSec = date.secondsFrom(now)
            self.startTimer()
        }
        timerBtn.setTitleColor(AppColors.Orange.orange, for: .normal)
        _ = task.task_media_set.map({ (media) in
            self.mediaTypes.append(media.mediaType ?? "")
            self.mediaURls.append(media.mediaUrl ?? "")
            self.mediaThumbnail.append(media.mediaThumb ?? "")
        })
        self.carouselViewHeight.constant = 180
        if task.task_type == .facebook {
            socialImageView.image = #imageLiteral(resourceName: "icFacebook1")
        } else if task.task_type == .twitter {
            socialImageView.image = #imageLiteral(resourceName: "icTwitter")
        }else if task.task_type == .whatsapp {
            socialImageView.image = #imageLiteral(resourceName: "icWatsapp")

        }else if task.task_type == .youtube {
            socialImageView.image = #imageLiteral(resourceName: "icYoutube-1")
            
        }else if task.task_type == .install {
            socialImageView.image = #imageLiteral(resourceName: "icInfo-1")
            
        }else if task.task_type == .offline {
            socialImageView.image = #imageLiteral(resourceName: "icMedia-1")

        }else if task.task_type == .online {
            socialImageView.image = #imageLiteral(resourceName: "icForm1")
            tweetBtn.setTitle(task.action.rawValue.uppercased() + "(" + task.total_survay_form_complete  + "/" + task.total_survay_form + ")" , for: .normal)
            
        }else{
            socialImageView.image = nil
        }
        
        if task.taskComplete == "1" {//completed
            self.timerBtn.isHidden = true
            self.btnUploadFile.isHidden = true
            self.timerBtn.setImage(nil, for: .normal)
            self.viewAction.isHidden = false
            self.btnInstruction.isHidden = true
            isTaskCompleteBtn.setImage(#imageLiteral(resourceName: "icCheck") , for: .normal)
        } else if task.taskComplete == "2" {//review
            self.timerBtn.isHidden = true
            self.timerBtn.setImage(nil, for: .normal)
            isTaskCompleteBtn.setImage(#imageLiteral(resourceName: "icCheckYallow") , for: .normal)

        }else{
            isTaskCompleteBtn.setImage(#imageLiteral(resourceName: "icDisablecheck") , for: .normal)
            //incompleted
        }
        if task.task_media_set.isEmpty  {
            self.carouselViewHeight.constant = 0
        } else {
            self.carouselViewHeight.constant = 180
        }
        self.layoutIfNeeded()
        viewForCarousel.reloadData()
        self.parentVC = parent
        if btnInstruction.isHidden && btnUploadFile.isHidden{
            self.heightUploadFileBtn.constant = 0
            self.heightInstructionBtn.constant = 0
        }else{
            self.heightUploadFileBtn.constant = 24
            self.heightInstructionBtn.constant = 24
        }
        
    }
    
}

extension TaskCell : iCarouselDelegate,iCarouselDataSource{
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return mediaURls.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var tempView: UIView
        tempView = UIView.init(frame: CGRect(x:0 , y:0, width:self.frame.width - 10,height:210))
        let carouselImageview = UIImageView()
        carouselImageview.frame = CGRect(x: 25, y: 0, width: tempView.bounds.width - 60, height: tempView.bounds.height - 20)
        carouselImageview.contentMode = .scaleAspectFill
        carouselImageview.rounded(cornerRadius: 3.0, clip: true)
        tempView.addSubview(carouselImageview)
        if let url = URL(string: mediaURls[index]), mediaTypes[index] == "1" {
            // print(url)
            ImageDownloader.default.authenticationChallengeResponder = self
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "icHomeImage"))
        } else if let url = URL(string: mediaThumbnail[index]), mediaTypes[index] == "2" {
            let image = UIImageView()
            image.frame = CGRect(origin: carouselImageview.center, size: CGSize(width: 35, height: 35))
            image.center = carouselImageview.center
            image.image = #imageLiteral(resourceName: "playIcon")
            image.contentMode = .center
            tempView.addSubview(image)
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "icHomeImage"))
            
        }
        else {
            
            carouselImageview.image = UIImage(named: "videoPlayer")
        }
        // layoutIfNeeded()
        return tempView
        
    }
    
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .fadeMin)
        {
            return 0.1
        } else if (option == .fadeMinAlpha)
        {
            return 0.0
        } else if (option == .fadeMax)
        {
            return 0.3;
        }
        else if option == .showBackfaces {
            return 2
        }
        else if option == .wrap {
            return 1
        }
        return value;
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if mediaTypes[index] == "2" {
            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            vc.url = /mediaURls[index]
            vc.desc = /self.task?.task_title
            ez.topMostVC?.presentVC(vc)
//            let videoURL = URL(string: mediaURls[index])
//            let player = AVPlayer(url: videoURL!)
//            let playerViewController = AVPlayerViewController()
//            playerViewController.player = player
//            parentVC?.present(playerViewController, animated: true) {
//                playerViewController.player!.play()
//            }
        } else {
            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
            previewVC.previewImage = mediaURls[index]
            previewVC.tittle = task?.task_title ?? ""
            
            parentVC?.navigationController?.pushViewController(previewVC, animated: true)
        }
    }
    
}

// setTime
extension TaskCell {
    
    func startTimer() {
        if !(countdown?.isValid ?? false){
            countdown = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
       self.timerBtn.isHidden = false
        self.timerBtn.setTitle("\(timeFormatted(totalTimeInSec))", for: .normal)
        if totalTimeInSec != 0 {
            totalTimeInSec -= 1
        } else {
            stopTimer()
        }
    }
    
    func stopTimer() {
        countdown?.invalidate()
        self.timerBtn.isHidden = true
        backView.backgroundColor =  UIColor.init(hexString: "#8c8c8c")


    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let dayss = (totalSeconds / (3600 * 24))
        let hourss = (totalSeconds % (3600*24)) / (3600)
        let minss = ((totalSeconds % (3600*24)) % (3600)) / 60
        let secss = ((((totalSeconds % (3600*24)) % (3600)) % 60) % 60)

        return String(format: "%2d\(StringConstant.d.localized) %02d\(StringConstant.h.localized) %02d\(StringConstant.m.localized) %02d\(StringConstant.s.localized)", dayss, hourss, minss, secss)
    }
    
}

