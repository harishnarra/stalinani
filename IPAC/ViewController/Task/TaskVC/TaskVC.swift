//
//  HomeVC.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import TwitterKit
import EZSwiftExtensions
import FBSDKShareKit
import PullToRefreshKit
import CoreLocation
import TwitterCore

class TaskVC: BaseVC {
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var statusStackView: UIStackView!
    @IBOutlet weak var platformStackView: UIStackView!
    @IBOutlet weak var startDateTxtField: UITextField!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var platFormPopup: UIView!
    @IBOutlet weak var platformAllBtn: UIButton!
    @IBOutlet weak var platformWhatsappBtn: UIButton!
    @IBOutlet weak var platformYoutubeBtn: UIButton!
    @IBOutlet weak var platformOfflineBtn: UIButton!
    @IBOutlet weak var platformOnlineBtn: UIButton!
    @IBOutlet weak var dateLabel: UIButton!
    @IBOutlet weak var statusLabel: UIButton!
    @IBOutlet weak var platformLabel: UIButton!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var platFbBtn: UIButton!
    @IBOutlet weak var paltformTwiterBtn: UIButton!
    @IBOutlet weak var openplatformBtn: UIButton!
    @IBOutlet weak var statusPopup: UIView!
    @IBOutlet weak var statusAllBtn: UIButton!
    @IBOutlet weak var statusIncompleteBtn: UIButton!
    @IBOutlet weak var statusInReviewBtn: UIButton!
    @IBOutlet weak var statusCompleteBtn: UIButton!
    @IBOutlet weak var openStatusPopupBtn: UIButton!
    @IBOutlet weak var lblCurrentDate: UILabel!
    @IBOutlet weak var lblDoneTask: UILabel!
    @IBOutlet weak var progressTask: UIProgressView!
    var sendTaskID: String?
    var tapGesture = UITapGestureRecognizer()
    
    @IBOutlet weak var lblNoData: UILabel!
    
    var totalTask = 0
    var totalCompletedTask = 0
    var taskLists: [TaskTabModel] = []
    var platformList = [StringConstant.All.localized, StringConstant.Facebook.localized, StringConstant.Twitter.localized]
    var startDate = ""
    var platform = ""
    var status = ""
    var shareCellIndexPath: IndexPath?
    var user: UserModel?
    var count = 0
    var formUrl = ""
    var task : TaskTabModel?
    var documentInteractionController : UIDocumentInteractionController?
    var isShare = false
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let vc = TaskPopUpViewController.instantiate(fromAppStoryboard: .Home)
//              self.navigationController?.present(vc, animated: false, completion: nil)
              
        
      //  print("newnaddi: ", LocalizationSystem.sharedInstance.localizedStringForKey(key: "Unique_ID_Number", comment: ""))

        
        self.dateLabel.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "DATE", comment: ""), for: .normal)
        self.platformLabel.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "PLATFORM", comment: ""), for: .normal)
        self.statusLabel.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "STATUS", comment: ""), for: .normal)

        let dateString = Date().toString(dateFormat: "yyyy-MM-dd")
        self.startDate = dateString
        self.initialSetup()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.topView.rounded(cornerRadius: 5.0, clip: true)
        self.homeTableView.rounded(cornerRadius: 5.0, clip: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view)
        if !blurView.isHidden, self.openplatformBtn.isSelected, !self.platFormPopup.frame.contains(point) {
            self.hideShowPlatformPopup(status: true)
            self.openplatformBtn.isSelected = false
        }
        if !blurView.isHidden, self.openStatusPopupBtn.isSelected, !self.platFormPopup.frame.contains(point) {
            self.hideShowStatusPopup(status: true)
            self.openStatusPopupBtn.isSelected = false
        }
    }
    
    // MARK:- IBACTIONS
    
    @IBAction func openPlatformBtnTap(_ sender: UIButton) {
        self.hideShowPlatformPopup(status: sender.isSelected)
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func platformAllBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.All.localized , value : "")
    }
    
    @IBAction func platformFbBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.Facebook.localized , value : "facebook")
    }
    
    @IBAction func platformTwitterBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.Twitter.localized , value : "twitter")
    }
    
    @IBAction func platformWhatsappBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.WhatsApp.localized , value : "whatsapp")
    }
    @IBAction func platformYoutubeBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.youtube.localized , value : "youtube")
    }
    @IBAction func platformOfflineTaskBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.offline.localized , value : "offline")
    }
    @IBAction func platformOnlineTaskBtnTap(_ sender: UIButton) {
        selectPlatForm(title : StringConstant.online.localized , value : "online")
        
    }
    
    @IBAction func openStatusPopupBtnTap(_ sender: UIButton) {
        self.hideShowStatusPopup(status: sender.isSelected)
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func statusAllBtnTap(_ sender: UIButton) {
        selectStatus(title : StringConstant.All.localized , value : "")
    }
    
    @IBAction func statusIncompleteBtn(_ sender: UIButton) {
        selectStatus(title : StringConstant.Incomplete.localized , value : "2")
        
    }
    
    @IBAction func statusCompleteBtn(_ sender: UIButton) {
        selectStatus(title : StringConstant.Complete.localized , value : "1")
        
    }
    @IBAction func statusInReviewBtn(_ sender: UIButton) {
        selectStatus(title : StringConstant.inReview.localized , value : "3")
        
    }
    
    @IBAction func notificationBtnTap(_ sender: UIButton) {
//        let notificationSceen = NotificationVC.instantiate(fromAppStoryboard: .Dashboard)
//        self.navigationController?.pushViewController(notificationSceen, animated: true)
        
        
        let vc = HelpTaskViewController.instantiate(fromAppStoryboard: .Home)
        self.navigationController?.present(vc, animated: false, completion: nil)
        
    }
    
    // MARK:- Targets
    @objc func shareLikeBtnTap(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.homeTableView) else { return }
        self.shareCellIndexPath = index
        self.share(indexPath: index)
    }
    
    @objc func uploadFile(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.homeTableView) else { return }
        let indexCell = index.row
        let vc = UploadImageForMediaTaskVC.instantiate(fromAppStoryboard: .Form)
        vc.task = self.taskLists[indexCell]
        vc.delegate = self
        self.navigationController?.presentVC(vc)
        
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        self.count = 0
        self.getHomeTask(loader: false , sender)
    }
    
    //MARK::- FUNCTIONS
    func selectPlatForm(title : String , value : String){
        openplatformBtn.setTitle(title, for: .normal)
        self.platform = value
        self.hideShowPlatformPopup(status: true)
        self.openplatformBtn.isSelected = false
        count = 0
        self.getHomeTask(loader: true)
    }
    
    func selectStatus(title : String , value : String){
        self.status = value
        openStatusPopupBtn.setTitle(title, for: .normal)
        self.hideShowStatusPopup(status: true)
        self.openStatusPopupBtn.isSelected = false
        count = 0
        self.getHomeTask(loader: true)
    }
    @objc func rightViewTapped(_ sender: UIGestureRecognizer) {
        print("Right button is tapped")
        let tag = sender.view!.tag
        print(tag)
        
        self.sendTaskID = self.taskLists[tag].task_id
        
        
        let vc = CompletedTaskVC.instantiate(fromAppStoryboard: .Home)
        vc.recieveTaskID = self.sendTaskID
          self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}

// MARK:- TEXT FIELD DELEGATE
//==============================================

extension TaskVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField === self.startDateTxtField {
            self.setStartDatePicker()
            return true
        }
        return true
    }
    
}

// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension TaskVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.taskLists.count
        return count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as? TaskCell else{
            fatalError()
        }
        cell.configureCellWithTask(taskLists[indexPath.row], parent: self)
        cell.completeView.tag = indexPath.row
        let rightTap = UITapGestureRecognizer(target: self, action: #selector(rightViewTapped(_:)))
        cell.completeView.addGestureRecognizer(rightTap)
        cell.completeView.isHidden = true
        cell.completedUser1.isHidden = true
        cell.completedUser2.isHidden = true
        cell.completedLabel.isHidden = true
        cell.completedOthersLabel.isHidden = true
        cell.completedHeight.constant = 0
        
      //  let countCheck:Int? = self.taskLists[indexPath.row].task_completed_user_count?.toInt()
        
//        if self.newspost[indexPath.row].is_like == "dislike" {
//            if self.newspost[indexPath.row].clicks_user_list?.count == 0 {
//            } else if self.newspost[indexPath.row].clicks_user_list?.count == 2 &&  self.newspost[indexPath.row].clicks_user_count?.count ?? 0 >= 2 {
//                print("and ", self.newspost[indexPath.row].clicks_user_count ?? "", " others")
//                cell.likeView.isHidden = false
//                cell.likeImageOne.isHidden = false
//                cell.likeImageTwo.isHidden = false
//                cell.likeOthersLabel.isHidden = false
//                let labelCount:Int? = self.newspost[indexPath.row].clicks_user_count?.toInt()
//                let count2 = (labelCount ?? 2) - 2
//                cell.likeOthersLabel.text = "and \(count2.description) others"
//                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            }
//
//            else  if self.newspost[indexPath.row].clicks_user_list?.count == 1 {
//                cell.likeView.isHidden = false
//                cell.likeImageOne.isHidden = false
//                cell.likeImageTwo.isHidden = true
//                cell.likeOthersLabel.isHidden = true
//                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            } else  if self.newspost[indexPath.row].clicks_user_list?.count == 2 {
//                cell.likeView.isHidden = false
//                cell.likeImageOne.isHidden = false
//                cell.likeImageTwo.isHidden = false
//                cell.likeOthersLabel.isHidden = true
//                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            }
//
//        } else {
//            let liketype = self.newspost[indexPath.row].like_type
//            print(liketype ?? "")
//            if liketype == "clap" {
//                cell.reactionButton.setImage(UIImage(named: "Clap"), for: .normal)
//            } else if liketype == "heart" {
//                cell.reactionButton.setImage(UIImage(named: "FilledHeart"), for: .normal)
//            } else if liketype == "fist" {
//                cell.reactionButton.setImage(UIImage(named: "Fist"), for: .normal)
//            } else {
//                cell.reactionButton.setImage(UIImage(named: "EmptyHeart"), for: .normal)
//            }
//            if self.newspost[indexPath.row].clicks_user_list?.count == 0 {
//            } else if self.newspost[indexPath.row].clicks_user_list?.count == 2 &&  self.newspost[indexPath.row].clicks_user_count?.count ?? 0 >= 2 {
//                print("and ", self.newspost[indexPath.row].clicks_user_count ?? "", " others")
//                cell.likeView.isHidden = false
//                cell.likeImageOne.isHidden = false
//                cell.likeImageTwo.isHidden = false
//                cell.likeOthersLabel.isHidden = false
//                let labelCount:Int? = self.newspost[indexPath.row].clicks_user_count?.toInt()
//                let count2 = (labelCount ?? 2) - 2
//                cell.likeOthersLabel.text = "and \(count2.description) others"
//                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            } else  if self.newspost[indexPath.row].clicks_user_list?.count == 1 {
//                cell.likeView.isHidden = false
//                cell.likeImageOne.isHidden = false
//                cell.likeImageTwo.isHidden = true
//                cell.likeOthersLabel.isHidden = true
//                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            } else  if self.newspost[indexPath.row].clicks_user_list?.count == 2 {
//                cell.likeView.isHidden = false
//                cell.likeImageOne.isHidden = false
//                cell.likeImageTwo.isHidden = false
//                cell.likeOthersLabel.isHidden = true
//                cell.likeImageOne.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//                cell.likeImageTwo.kf.setImage(with: URL(string: self.newspost[indexPath.row].clicks_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            }
//        }
        
//        if countCheck ?? 0 >= 2 {
//            cell.completeView.isHidden = false
//            cell.completedUser1.isHidden = false
//            cell.completedUser2.isHidden = false
//            cell.completedLabel.isHidden = false
//            cell.completedOthersLabel.isHidden = false
//            cell.completedHeight.constant = 45
//            print("CCCCC", self.taskLists[indexPath.row].task_completed_user_list?.count ?? 0)
//
//
//            cell.completedOthersLabel.text = self.taskLists[indexPath.row].task_completed_user_count
//        } else {
//
//            if self.taskLists[indexPath.row].task_completed_user_list?.count == 0 {
//                cell.completeView.isHidden = true
//                cell.completedUser1.isHidden = true
//                cell.completedUser2.isHidden = true
//                cell.completedLabel.isHidden = true
//                cell.completedOthersLabel.isHidden = true
//                cell.completedHeight.constant = 0
//
//            } else  if self.taskLists[indexPath.row].task_completed_user_list?.count == 1 {
//
//                cell.completeView.isHidden = false
//                cell.completedUser1.isHidden = false
//                cell.completedUser2.isHidden = true
//                cell.completedLabel.isHidden = false
//                cell.completedOthersLabel.isHidden = true
//                cell.completedHeight.constant = 45
//                cell.completedUser1.kf.setImage(with: URL(string: self.taskLists[indexPath.row].task_completed_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            } else  if self.taskLists[indexPath.row].task_completed_user_list?.count == 2 {
//
//                cell.completeView.isHidden = false
//                cell.completedUser1.isHidden = false
//                cell.completedUser2.isHidden = false
//                cell.completedLabel.isHidden = false
//                cell.completedOthersLabel.isHidden = true
//                cell.completedHeight.constant = 45
//                cell.completedUser1.kf.setImage(with: URL(string: self.taskLists[indexPath.row].task_completed_user_list?[0].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//
//                cell.completedUser2.kf.setImage(with: URL(string: self.taskLists[indexPath.row].task_completed_user_list?[1].user_image ?? ""), placeholder:  #imageLiteral(resourceName: "icDashboardOtheruser"))
//            }
//        }
        
        cell.tweetBtn.addTarget(self, action: #selector(self.shareLikeBtnTap(_:)), for: .touchUpInside)
        cell.btnUploadFile.addTarget(self, action: #selector(self.uploadFile(_:)), for: .touchUpInside)
        if self.taskLists[indexPath.row].taskComplete == "1"{
            cell.backView.backgroundColor =  UIColor.init(hexString: "#8c8c8c")
        }
        return cell
    }
    
}


extension TaskVC : SharingDelegate {
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {

        if let index = shareCellIndexPath {
            self.taskLists[index.row].taskComplete = "1"
            self.updateTask(taskId: self.taskLists[index.row].task_id , { (_) in
                //self.homeTableView.reloadData()
                self.reloadWithoutAnimation()
            })
        }
        print_debug(results)
    }

    func sharer(_ sharer: Sharing, didFailWithError error: Error) {

        print_debug(error)
              CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
    }

    func sharerDidCancel(_ sharer: Sharing) {
        print_debug("cancel")
        CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
    }
    
//    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
//        
//    }
//    
//    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
//      
//    }
//    
//    func sharerDidCancel(_ sharer: FBSDKSharing!) {
//        
//    }
}


//MARK:- SOCIAL LOGIN
//============================
extension TaskVC {
    
    
    func goToTwitterWebView(param: [String: Any], task: TaskTabModel) {
        print_debug(param)
        let twetterSceen = TwitterWebViewVC.instantiate(fromAppStoryboard: .Home)
        twetterSceen.taskModel = task
        let pasteboard = UIPasteboard.general
        pasteboard.string = task.task_description
        CommonFunction.showToast(msg: StringConstant.Task_description_copied.localized)
        twetterSceen.delegate = self
        self.navigationController?.pushViewController(twetterSceen, animated: true)
    }
    func goToTwitterWebView2(param: [String: Any], task: TaskTabModel) {
        print_debug(param)
        let twetterSceen = TwitterWebViewVC.instantiate(fromAppStoryboard: .Home)
        twetterSceen.taskModel = task
        let pasteboard = UIPasteboard.general
        pasteboard.string = task.task_description
        CommonFunction.showToast(msg: StringConstant.Comment_copied.localized)
        twetterSceen.delegate = self
       // self.navigationController?.pushViewController(twetterSceen, animated: true)
    }
}


//MARK::- TERMS CONDITION DELEGATE
extension TaskVC : TermsAcceptDelegate , BonusPopupDelegate{
    func accepted(version: String) {
        let termsSceen = TemsAndCondition.instantiate(fromAppStoryboard: .Main)
        termsSceen.isNewTerms = true
        termsSceen.term_condition_version = version
        self.presentVC(termsSceen)
        
    }
    func done(status : Int){
        //0 profile , 1 task , 2 form
        switch status{
        case 0:
            let vc = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
            vc.isSidePanel = false
            self.navigationController?.pushViewController(vc, animated: false)
        case 1:
            break
        default:
            let vc = WebViewVC.instantiate(fromAppStoryboard: .Form)
            vc.url = self.formUrl
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}


//MARK::- PAGINATION
extension TaskVC{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        homeTableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.homeTableView.switchRefreshFooter(to: .normal) :  self.getHomeTask(loader: false)
        }
    }
}


//MARK::- FUNCTION DUCUMENTATION INTERACTOR
extension TaskVC : UIDocumentInteractionControllerDelegate{
    func shareImageToWhatsapp(img : UIImage , task : TaskTabModel?){
        let urlWhats = "whatsapp://app"
        if let whatsappURL = URL(string: urlWhats) {
            if UIApplication.shared.canOpenURL(whatsappURL) {
                let image = img
                self.updateTask(taskId: /self.task?.task_id) { (json) in
                    self.refresh()
                    CommonFunction.showToast(msg: AlertMessage.reviewTheTask.localized)
                }
                if let imageData = UIImageJPEGRepresentation(image, 1.0) {
                    guard let tempFile = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wai") else {return}
                    do {
                        try imageData.write(to: tempFile, options: .atomic)
                        self.task  = task
                        self.documentInteractionController = UIDocumentInteractionController.init(url: tempFile)
                        self.documentInteractionController?.uti = "net.whatsapp.image"
                        self.documentInteractionController?.delegate = self
                        self.documentInteractionController?.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                    } catch {
                        print(error)
                    }
                }
                
            } else {
                CommonFunction.showToast(msg: AlertMessage.installWhatsapp.localized)
            }
        }
    }
    func documentInteractionController(_ controller: UIDocumentInteractionController, didEndSendingToApplication application: String?) {
        //        self.updateTask(taskId: /self.task?.task_id) { (json) in
        //            self.refresh()
        //        }
    }
    
    func documentInteractionController(_ controller: UIDocumentInteractionController, willBeginSendingToApplication application: String?) {
        
    }
    
}



// MARK:- FUNCTIONS
//====================
extension TaskVC {
    
    /// Invite Page Content Initial Setup
    func initialSetup() {
        super.addImageInBackground()
        self.homeTableView.dataSource = self
        self.homeTableView.delegate = self
        handlePaging()
        homeTableView.register(UINib.init(nibName: "TaskCell", bundle: nil), forCellReuseIdentifier: "TaskCell")
        self.homeTableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
        navigationController?.navigationBar.isHidden = true
        //        self.setSideMenuButton(image: #imageLiteral(resourceName: "icHomeMenu"))
        self.startDateTxtField.text = Date().toString(dateFormat: EventDateFormat)
        self.startDateTxtField.delegate = self
        titleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Tasks", comment: "")
        setupPopupView()
        hideShowPlatformPopup(status: true)
        hideShowStatusPopup(status: true)
        self.user = CommonFunction.getUser()
        self.getHomeTask(loader: true)
    }
    
    func setupPopupView() {
        platFormPopup.rounded(cornerRadius: 3.0, clip: true)
        platformAllBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        platFbBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Facebook", comment: ""), for: .normal)
        paltformTwiterBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Twitter", comment: ""), for: .normal)
        platformWhatsappBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "WhatsApp", comment: ""), for: .normal)
        platformYoutubeBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Youtube", comment: ""), for: .normal)
        platformOfflineBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Offline", comment: ""), for: .normal)
        platformOnlineBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Online", comment: ""), for: .normal)
        self.openplatformBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        statusPopup.rounded(cornerRadius: 3.0, clip: true)
        statusAllBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        statusIncompleteBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Incomplete", comment: ""), for: .normal)
        self.openStatusPopupBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: ""), for: .normal)
        statusCompleteBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Complete", comment: ""), for: .normal)
        statusInReviewBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "In Review", comment: ""), for: .normal)
    }
    
    
    func hideShowPlatformPopup(status: Bool) {
        self.platFormPopup.isHidden = status
        self.blurView.isHidden    = status
    }
    
    func hideShowStatusPopup(status: Bool) {
        self.statusPopup.isHidden = status
        self.blurView.isHidden    = status
    }
    
    func setupCurrentDateProgress(){
        self.lblCurrentDate.text = self.startDateTxtField.text
        self.lblDoneTask.text = "(" + self.totalCompletedTask.description + "/" + self.totalTask.description  + ")"
        if self.status == "" || status == "1"{
            self.progressTask.progress = (self.totalCompletedTask.toFloat / (self.totalTask == 0 ? 1.0 : self.totalTask.toFloat ))
        }else{
            self.progressTask.progress = 0.0
        }
    }
    
    
    func updateProfileTwitter(params: [String: String], loader: Bool, task: TaskTabModel?) {
        WebServices.updateProfile(param: params, loader: loader, success: { [weak self] (json) in
            print_debug(json)
            guard let currentUser = CommonFunction.getUser() else { return }
            currentUser.twitterId = params[ApiKeys.twitterId]
            currentUser.twitterUsername = params[ApiKeys.twitterUsername]
            CommonFunction.updateUser(currentUser)
        }) { (error) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    func updateProfileFb(params: [String: String], loader: Bool, task: TaskTabModel?) {
        WebServices.updateProfile(param: params, loader: loader, success: { [weak self] (json) in
            print_debug(json)
            guard let currentUser = CommonFunction.getUser() else { return }
            currentUser.facebookId = /params[ApiKeys.fbUsername]
            currentUser.fbUsername = /params[ApiKeys.facebookId]
            CommonFunction.updateUser(currentUser)
        }) { (error) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    func getHomeTask(loader: Bool, _ refreshControl : UIRefreshControl? = nil){
        
        var params: [String:Any] = ["count": self.count]
        if  !self.startDate.isEmpty {
            params[ApiKeys.start_date] = startDate
        }
        if !status.isEmpty {
            params[ApiKeys.status] = status
        }
        if !platform.isEmpty {
            params[ApiKeys.platform] = platform
        }
        WebServices.getHomeTask(params: params,loader : loader ,success: { [weak self] (json) in
            print_debug(json)
            self?.homeTableView.switchRefreshFooter(to: .normal)
            refreshControl?.endRefreshing()
            self?.totalTask = json["TOTAL"].intValue
            self?.totalCompletedTask = json["TOTAL_COMPLETE_TASK"].intValue
            self?.setupCurrentDateProgress()
            var tasks = [TaskTabModel]()
            for obj in json[ApiKeys.result].arrayValue {
                let obj = TaskTabModel(from: obj)
                tasks.append(obj)
            }
            self?.count ==  0 ? (self?.taskLists = tasks) : (self?.taskLists += tasks)
            self?.reloadWithoutAnimation()
            self?.lblNoData?.isHidden = !(self?.taskLists.isEmpty ?? true)
            self?.count = json[ApiKeys.next].intValue
            //self?.homeTableView.reloadData()
            let data = json["TERM_CONDITION"].dictionaryValue
            let termsCondition =  TermsCondition(dict :  JSON(data))
            SingletonTask.shared.completedTask = (termsCondition.completed_task.toInt() ?? 0)
            if termsCondition.term_accept_status == "1"{
                let vc = AcceptTermsVC.instantiate(fromAppStoryboard: .ContactUs)
                vc.version = termsCondition.version
                vc.delegate = self
                self?.navigationController?.present(vc, animated: false, completion: nil)
            }else{
                let user = CommonFunction.getUser()
                if user?.is_bonus_received == "1"{return}
                //                if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || json["TOTAL_COMPLETE_TASK"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""{
                if json["IS_ID_PROOF_ADDED"].stringValue == "0" || json["PAYMENT_DETAIL_ADDED"].stringValue == "0" || json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1" || SingletonTask.shared.completedTask == 0  || user?.facebookId == "" || user?.twitterId == ""{
//                    let vc = BonusPopupVC.instantiate(fromAppStoryboard: .Form)
//                    self?.formUrl = json["CAMPAIGN_FORM_URL"].stringValue
//                    vc.isFormFill = json["IS_CAMPAIGN_FORM_FILLED"].stringValue == "1"
                    
                    
//                    vc.isFirstTask = SingletonTask.shared.completedTask == 0
//                    vc.isProof = json["IS_ID_PROOF_ADDED"].stringValue == "0" || user?.facebookId == "" || user?.twitterId == ""
//                    vc.isPaytmDetail = json["PAYMENT_DETAIL_ADDED"].stringValue == "0"
//                    vc.isDismiss = !(SingletonTask.shared.completedTask >= 3)
//                    vc.delegate = self
//                    self?.navigationController?.present(vc, animated: false, completion: nil)
                }
            }
            
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
            self.lblNoData?.isHidden = !(self.taskLists.isEmpty )
            self.homeTableView.switchRefreshFooter(to: .normal)
            refreshControl?.endRefreshing()
            
        }
    }
    
    func setStartDatePicker() {
        _ = PKDatePicker.openDateMaxPickerIn(self.startDateTxtField,
                                             outPutFormate: EventDateFormat,
                                             mode: .date, minimumDate: dateManager.getMinimumStartDateAndTime(), maximumDate:
            Date(), minuteInterval: 1, selectedDate: Date(), doneBlock: { (selectStr) in
                self.startDateTxtField.text = selectStr
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = EventDateFormat
                guard let date = dateFormatter.date(from: selectStr) else { return }
                let dateString = date.toString(dateFormat: "yyyy-MM-dd")
                self.startDate = dateString
                self.count = 0
                self.getHomeTask(loader: true)
        })
    }
    
    
    func updateTask(taskId: String , _ success : SuccessResponse? = nil){
        let params = [ApiKeys.taskId: taskId]
        WebServices.updateTask(params: params, success: { (result) in
            print_debug(result)
            // self?.homeTableView.reloadData()
            success?(result)
            
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}


//MARK::- RELOAD TABLE

extension TaskVC {
    func reloadWithoutAnimation() {
        self.homeTableView.switchRefreshFooter(to: .normal)
        let lastScrollOffset = homeTableView.contentOffset
        homeTableView.reloadData()
        if self.count == 0{
            homeTableView.setContentOffset(CGPoint.zero, animated: false)
        }else{
            homeTableView.setContentOffset(lastScrollOffset, animated: false)
            
        }
    }
}
