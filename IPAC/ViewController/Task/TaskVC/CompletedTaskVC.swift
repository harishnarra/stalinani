//
//  CompletedTaskVC.swift
//  Stalinani
//
//  Created by Haritej on 17/08/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CompletedTaskVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var completedBtLabel: UILabel!
    
    var count = 0
    var referData : CompletedTaskModel?
    var arrData : [COmpletedTaskResultData]?  = []
    var recieveTaskID : String?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDataFromAPICompletedTask(loader: true)

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    @IBAction func closeBtn(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    func getDataFromAPICompletedTask(loader : Bool){
        WebServices.completedTaskList(count: self.count.description, taskID: self.recieveTaskID?.description ?? "", loader
                  , success: { [weak self] (json) in
                      self?.referData = CompletedTaskModel.init(json: json)
                   
                      self?.setupUI()
                      
              }) {(error) -> (Void) in
                  CommonFunction.showToast(msg: error.localizedDescription)
              }
          }
       
    func setupUI(){
        self.tableView.switchRefreshFooter(to: .normal)
       // if arrData?.count == 0 { self.tableView.isHidden = true } else { self.tableView.isHidden = false }
        self.arrData?.append(contentsOf : self.referData?.getcompletedTaskResult ?? [])
        print(self.arrData ?? [])
        self.count = self.referData?.count ?? -1
        self.tableView.reloadData()
    }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return arrData?.count ?? 0
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "CompletedTaskCell") as! CompletedTaskCell
           cell.selectionStyle = .none
           tableView.separatorStyle = .none
           cell.completedPhoto.kf.setImage(with: URL(string : /arrData?[indexPath.row].userImage), placeholder: #imageLiteral(resourceName: "icPlaceholder"), options: nil, progressBlock: nil, completionHandler: nil)
           cell.completedName.text = /arrData?[indexPath.row].fullName
           cell.completedPhone.text = /arrData?[indexPath.row].phone_number
           return cell
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
