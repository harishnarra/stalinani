//
//  InnerCell3.swift
//  tableViewDemo
//
//  Created by naresh banavath on 14/07/20.
//  Copyright © 2020 naresh banavath. All rights reserved.
//

import UIKit
import IBAnimatable

class InnerCell3: UITableViewCell {

    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userText: AnimatableTextView!
    @IBOutlet weak var submitPollBtn: UIButton!
    
      var isSubmitClicked:() -> (Void) = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        submitPollBtn.addRightToLefttGradient()
        submitPollBtn.setCornerRadius(radius: 5.0)
        
        // Initialization code
    }

    @IBAction func submitBtnAction(_ sender: UIButton) {
           isSubmitClicked()
           //submitBtn.setBackgroundColor(color: UIColor.gray, state: .normal)
       }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
