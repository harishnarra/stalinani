//
//  RewardDetailVC.swift
//  IPAC
//
//  Created by Appinventiv on 22/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RewardDetailVC: BaseViewController {

    //MARK::- OUTLETS
    
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var campaignExpesnsesLabel: UILabel!
    @IBOutlet weak var taskRewardsLabel: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var totalEarningsLabel: UILabel!

    
    @IBOutlet weak var lblTotalEarning: UILabel!
    @IBOutlet weak var lblBonus: UILabel!
    @IBOutlet weak var lblTaskRewards: UILabel!
    @IBOutlet weak var lblCampaignExpenses: UILabel!
    
    //MARK::- PROPERTIES
    var walletData : ReferralModel?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionCross(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    
}

//MARK::- FUNCTIONS
extension RewardDetailVC{
    func onViewDidLoad(){
        lblTotalEarning.text = walletData?.userInfo?.total_point.toInt()?.description ?? "0"
        lblBonus.text = walletData?.walletDetail?.bonus.toInt()?.description ?? "0"
        lblTaskRewards.text = walletData?.walletDetail?.reward.toInt()?.description ?? "0"
        lblCampaignExpenses.text = walletData?.walletDetail?.expense.toInt()?.description ?? "0"
        
                navigationLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reward_Details", comment: "")
        totalEarningsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Total_Earnings", comment: "")
        bonusLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Bonus", comment: "")
        taskRewardsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Task_Rewards", comment: "")
        campaignExpesnsesLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Campaign_Expenses", comment: "")
        
        
        
        
    }
}
