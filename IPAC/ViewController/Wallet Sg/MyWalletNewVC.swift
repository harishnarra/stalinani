//
//  MyWalletNewVC.swift
//  IPAC
//
//  Created by HariTeju on 12/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class MyWalletNewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    var selectedCellIndexPath: IndexPath?
    let selectedCellHeight: CGFloat = 140.0
    let unselectedCellHeight: CGFloat = 90.0

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var redView: UIView!
    

    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    @IBOutlet weak var rewardtransactionLael: UIButton!
    @IBOutlet weak var pointsearnedLabel: UILabel!
    @IBOutlet weak var myRewardsLabel: UILabel!
    
    @IBOutlet weak var lblTotalEarning: UILabel!
    @IBOutlet weak var lblLastUpdateEarning: UILabel!
    @IBOutlet weak var levelsLabela: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var walletData : ReferralModel?
    var arrData : [ReferralResult]?  = []
    var count = 0
    var status = 0 // 0 all , 1 earned  , 3 redeemed
    var startDateStr : String?
    var endDateStr : String?
    var startDate : Date?
    var endDate : Date?
    
    var walletDataNew : MainSubclass?
    var arrDataNew : [MainAchievement]?  = []
    var levelArrDataNew : [MainLevel]?  = []
    
    var constraintValues : [String : Int] = [:]
      var animatedRowIndexPath : Int?
    
    var backgroundImages: [String] = ["ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg","ic_purplebg","ic_bluebg","ic_greenbg","ic_orangebg","ic_yellowbg"]

    
    
    @IBOutlet weak var heightss: NSLayoutConstraint!
    override func viewDidLoad() {
            super.viewDidLoad()
            
           // print("sasssaas", LocalizationSystem.sharedInstance.getLanguage())
            
            //  print("NaadiRemember_me:", StringConstant.Remember_me.localized)
           // print("sasssaas:", LocalizationSystem.sharedInstance.localizedStringForKey(key: "Remember_me", comment: ""))
            myRewardsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "My_Rewards", comment: "")
            pointsearnedLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Points_Earned", comment: "")
            lastUpdateLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Last_Update", comment: "")
            levelsLabela.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Levels", comment: "")
         rewardtransactionLael.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reward_Transactions", comment: ""), for: .normal)
           //  navigationLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reward_Transactions", comment: "")

            
            tableView.backgroundColor = UIColor.clear
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            
            tableView.separatorStyle = .none
            tableView.delegate = self
            tableView.dataSource = self
            onViewDidLoadw()
            
    //        headerView.addTapGesture { [weak self] (_) in
    //            let detailVC = RewardDetailVC.instantiate(fromAppStoryboard: .ContactUs)
    //          //  detailVC.walletData = self?.walletData
    //            self?.presentVC(detailVC)
    //        }

            // Do any additional setup after loading the view.
        }

    @IBAction func didTaponRewardTransaction(_ sender: Any) {
        
        let vc = RewardTransactionsVC.instantiate(fromAppStoryboard: .ContactUs)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    


      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         // #warning Incomplete implementation, return the number of rows
          return walletDataNew?.levels.count ?? 0
     }

     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let constraintValue = constraintValues["\(indexPath.row)"]
        switch indexPath.row % 2 {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnimationTBLeftCell", for: indexPath) as! AnimationTBLeftCell
            cell.circleCenterXConstraint.constant = CGFloat(constraintValue ?? 0)
            cell.homeImg.image  = UIImage(named: backgroundImages[indexPath.row])
            cell.homeLabel.text = walletDataNew?.levels[indexPath.row].descriptionField
            
            cell.bottomLabel.text = walletDataNew?.levels[indexPath.row].levelTagName
            if LocalizationSystem.sharedInstance.getLanguage() == "en" {
                cell.topLabel.text = walletDataNew?.levels[indexPath.row].name
            } else {
                cell.topLabel.text = walletDataNew?.levels[indexPath.row].nameTn
            }
            
            cell.showHideView.setCornerRadius(radius: 35)

            
            if walletDataNew?.levels[indexPath.row].isUnlocked == true {
                
                cell.showHideView.isHidden = true
                
            } else {
                cell.showHideView.isHidden = false
                
            }
            cell.rewardImg.kf.setImage(with: URL(string: walletDataNew?.levels[indexPath.row].image ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))

            

            return cell
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnimationTBRightCell", for: indexPath) as! AnimationTBRightCell
            cell.circleCenterXConstraint.constant = CGFloat(constraintValue ?? 0)
            cell.rightImg.image  = UIImage(named: backgroundImages[indexPath.row])
            cell.rightLabel.text = walletDataNew?.levels[indexPath.row].descriptionField
            
            cell.bottomLabel.text = walletDataNew?.levels[indexPath.row].levelTagName
            if LocalizationSystem.sharedInstance.getLanguage() == "en" {
                cell.topLabel.text = walletDataNew?.levels[indexPath.row].name
            } else {
                cell.topLabel.text = walletDataNew?.levels[indexPath.row].nameTn
            }
            cell.showHideView.setCornerRadius(radius: 35)

            
            if walletDataNew?.levels[indexPath.row].isUnlocked == true {
                
                cell.showHideView.isHidden = true
                
            } else {
                cell.showHideView.isHidden = false
                
            }
            cell.rewardImg.kf.setImage(with: URL(string: walletDataNew?.levels[indexPath.row].image ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))

            return cell
            
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnimationTBRightCell", for: indexPath) as! AnimationTBRightCell
            return cell
        }
        
    }
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 140
     }
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
         
         animateCell(cell: tableView.cellForRow(at: indexPath)!, indexpath: indexPath , rowNO: indexPath.row)

     }
    

     private func animateCell(cell : UITableViewCell , indexpath : IndexPath , rowNO : Int)
     {
         if indexpath.row != animatedRowIndexPath
         {
               closeAnimationBeforeOpenAnother()
         }
       
         
             switch cell {
             case is AnimationTBRightCell:
                
                 let rightcell = tableView.cellForRow(at: indexpath) as! AnimationTBRightCell
                // var opacityValue = rightcell.upperLowerViews[0].alpha
                 if rightcell.circleCenterXConstraint.constant != 0
                 {
                      rightcell.circleCenterXConstraint.constant = 0
                      rightcell.rightLabel.text = walletDataNew?.levels[indexpath.row].descriptionField
                     // opacityValue = 1.0
                 }
                 else{
                      
                      animatedRowIndexPath = rowNO
                      rightcell.circleCenterXConstraint.constant = -(cell.contentView.frame.size.width/2 - 45)
                      rightcell.rightLabel.text = (walletDataNew?.levels[indexpath.row].descriptionField ?? "") + " \((walletDataNew?.levels[indexpath.row].name ?? ""))"
                    // opacityValue = 0.0
                 }

                 constraintValues["\(rowNO)"] = Int(rightcell.circleCenterXConstraint.constant)
                 UIView.animate(withDuration: 0.5) {
                     rightcell.contentView.layoutIfNeeded()
                   // rightcell.rightLabel.text = self.walletDataNew?.levels[indexpath.row].descriptionField

                    
                    // rightcell.upperLowerViews.forEach({$0.alpha = opacityValue})
                     
                 }
                 
             case is AnimationTBLeftCell:
                 let leftcell = tableView.cellForRow(at: indexpath) as! AnimationTBLeftCell
                // var opacityValue = leftcell.upperLowerViews[0].alpha
                 if leftcell.circleCenterXConstraint.constant != 0
                 {
                     leftcell.circleCenterXConstraint.constant = 0
                    leftcell.homeLabel.text = walletDataNew?.levels[indexpath.row].descriptionField
                   //  opacityValue = 1.0
                 }
                 else{
                      
                     animatedRowIndexPath = rowNO
                  //   opacityValue = 0.0
                     leftcell.circleCenterXConstraint.constant = cell.contentView.frame.size.width/2 - 45
                  leftcell.homeLabel.text = (walletDataNew?.levels[indexpath.row].descriptionField ?? "") + " \((walletDataNew?.levels[indexpath.row].name ?? ""))"

                 }
                constraintValues["\(rowNO)"] = Int(leftcell.circleCenterXConstraint.constant)
                 UIView.animate(withDuration: 0.5) {
                     leftcell.contentView.layoutIfNeeded()
                 //   leftcell.homeLabel.text = self.walletDataNew?.levels[indexpath.row].descriptionField

                     //leftcell.upperLowerViews.forEach({$0.alpha = opacityValue})
                 }
             default:
                 print("default")
                 
                 
             }
         
     }
    private func closeAnimationBeforeOpenAnother() {
        if let rowno = animatedRowIndexPath
        {
            print(rowno)
            let indexpath = IndexPath(row: rowno, section: 0)
            switch rowno % 2{
            case 0:
                guard let leftcell = tableView.cellForRow(at: indexpath) as? AnimationTBLeftCell else {
                    constraintValues["\(rowno)"] = 0;return}
                leftcell.circleCenterXConstraint.constant = 0
                constraintValues["\(rowno)"] = 0
                leftcell.homeLabel.text = walletDataNew?.levels[indexpath.row].descriptionField

                UIView.animate(withDuration: 0.5) {
                    leftcell.contentView.layoutIfNeeded()
                }
            case 1:
                guard let rightcell = tableView.cellForRow(at: indexpath) as? AnimationTBRightCell else {
                    constraintValues["\(rowno)"] = 0;return
                }
                rightcell.circleCenterXConstraint.constant = 0
                constraintValues["\(rowno)"] = 0
                rightcell.rightLabel.text = self.walletDataNew?.levels[indexpath.row].descriptionField

                UIView.animate(withDuration: 0.5) {
                    rightcell.contentView.layoutIfNeeded()
                }
            default:
                print("default")
                
                
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return walletDataNew?.levels.count ?? 0
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if selectedCellIndexPath == indexPath {
//            return selectedCellHeight
//        }
//        return unselectedCellHeight
//    }
//    private func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
//        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
//            selectedCellIndexPath = nil
//        } else {
//            selectedCellIndexPath = indexPath as IndexPath
//        }
//
//        tableView.beginUpdates()
//        tableView.endUpdates()
//
//        if selectedCellIndexPath != nil {
//            // This ensures, that the cell is fully visible once expanded
//            tableView.scrollToRow(at: indexPath as IndexPath, at: .none, animated: true)
//        }
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWalletNewVCCell", for: indexPath) as! MyWalletNewVCCell
//        cell.firstView.rounded(cornerRadius: 5.0, clip: true)
//        cell.secondView.rounded(cornerRadius: 5.0, clip: true)
//        cell.rewardBackground.roundCorners(corners: [.topLeft, .topRight], radius: 5.0)
//        cell.rewardBackground2.roundCorners(corners: [.topLeft, .topRight], radius: 5.0)
//        cell.volunteername.textColor = .white
//        cell.volunteername1.textColor = .white
//        cell.level.textColor = .white
//        cell.level1.textColor = .white
//
//
//        cell.firstView.dropShadow(color: UIColor.init(red: 79/255, green: 114/255, blue: 161/255, alpha:0.3), opacity: 1, offSet: CGSize.zero, radius: 10, scale: true)
//        cell.secondView.dropShadow(color: UIColor.init(red: 79/255, green: 114/255, blue: 161/255, alpha:0.3), opacity: 1, offSet: CGSize.zero, radius: 10, scale: true)
//
//        cell.selectionStyle = .none
//
//
//
//
//        if (indexPath.row) % 2 == 0 {
//
//            cell.firstView.isHidden = false
//            cell.secondView.isHidden = true
//
//
//        } else {
//
//            cell.firstView.isHidden = true
//            cell.secondView.isHidden = false
//
//
//        }
//        cell.checkView.setCornerRadius(radius: 27.5)
//
//        if walletDataNew?.levels[indexPath.row].isUnlocked == true {
//
//            cell.checkView.isHidden = true
//
//        } else {
//            cell.checkView.isHidden = false
//
//        }
//
//        cell.rewardText?.text = walletDataNew?.levels[indexPath.row].descriptionField
//        cell.rewardsText2.text = walletDataNew?.levels[indexPath.row].descriptionField
//        cell.level.text = walletDataNew?.levels[indexPath.row].levelTagName
//        cell.level1.text = walletDataNew?.levels[indexPath.row].levelTagName
//        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
//            cell.volunteername.text = walletDataNew?.levels[indexPath.row].name
//              cell.volunteername1.text = walletDataNew?.levels[indexPath.row].name
//        } else {
//        cell.volunteername.text = walletDataNew?.levels[indexPath.row].nameTn
//        cell.volunteername1.text = walletDataNew?.levels[indexPath.row].nameTn
//        }
//
//
//        cell.rewardsPic.kf.setImage(with: URL(string: walletDataNew?.levels[indexPath.row].image ?? ""), placeholder:  #imageLiteral(resourceName: "placeHolder"))
//
//
//        cell.rewardBackground.image  = UIImage(named: backgroundImages[indexPath.row])
//        cell.rewardBackground2.image  = UIImage(named: backgroundImages[indexPath.row])
//
//
//
//       // cell.rewardsPic.
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(walletDataNew?.levels[indexPath.row].descriptionField ?? "")
////        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWalletNewVCCell", for: indexPath) as! MyWalletNewVCCell
////        if (indexPath.row) % 2 == 0 {
////            cell.firstViewWidth.constant = 20
////        } else {
////            cell.secondViewWidth.constant = 20
////        }
////
//       // cell.firstViewWidth.con
//
//    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
////        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWalletNewVCCell", for: indexPath) as! MyWalletNewVCCell
////        if (indexPath.row) % 2 == 0 {
////            cell.firstViewWidth.constant = 139
////        } else {
////            cell.secondViewWidth.constant = 139
////        }
//    }
//
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        
        if(offset > 230){
            self.heightss.constant = 43
            //self.redView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 0)
        }else{
            self.heightss.constant = 230 - offset
            
            // self.redView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 200 - offset)
            
        }
        
        //        let rect = CGRect(x: 0, y: self.redView.frame.maxY, width: self.view.bounds.size.width, height:(self.view.bounds.size.height - (self.redView.frame.maxY)))
        //        tableView.frame = rect
    }
    
}

extension MyWalletNewVC{
    func onViewDidLoadw(){
       // self.tableView.isHidden = true
        headerView.addTapGesture { [weak self] (_) in
            let detailVC = RewardDetailVC.instantiate(fromAppStoryboard: .ContactUs)
            detailVC.walletData = self?.walletData
            self?.presentVC(detailVC)
        }
       // self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
       // configureTableView()
        //handlePaging()
        getDataFromAPI(loader : true)
        getDataFromAPIs(loader: true)
    }
    
    func setupUIw(){
        //self.tableView.switchRefreshFooter(to: .normal)
       // self.lblTotalEarning.text = self.walletData?.userInfo?.total_point.toInt()?.description ?? "0"
        self.lblTotalEarning.text = self.walletDataNew?.totalWalletAmount ?? "0"
        for item in self.walletData?.result ?? []{
            self.arrData?.append(item)
        }
        if count == 0{
            self.arrData = self.walletData?.result ?? []
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.count = self.walletData?.count ?? -1
//        self.tableViewDataSource?.items = self.arrData
//        self.tableView.reloadData()
//        self.tableView.isHidden = false
//        self.lblFooterNoData.isHidden = !(self.arrData?.isEmpty ?? true)
        if let dateReward = formatter.date(from: /self.walletDataNew?.uSERINFO?.updatedDate) {
            formatter.dateFormat = "d MMMM yyyy"
            self.lblLastUpdateEarning.text = formatter.string(from: dateReward)
        }
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        if let dateExpense = formatter.date(from: /self.walletData?.userInfo?.updated_expense_date) {
//            formatter.dateFormat = "d MMMM yyyy"
//
//        }
        
    }
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.count = 0
        self.getDataFromAPI(loader : false)
    }
}
extension MyWalletNewVC{
    func getDataFromAPI(loader : Bool){
        //0 all , 1 reward redeemed , 2 reward earned
        var tempStat = 0
        if status == 2{
            tempStat = 1
        }else if status == 1{
            tempStat = 3
            
        }
        
        WebServices.getWallet(dict : ["count" : count , "status" : tempStat , "start_date" : /startDateStr , "end_date" : /endDateStr],loader
            , success: { [weak self] (json) in
                self?.walletData = ReferralModel.init(json: json)
                self?.tableView.reloadData()
                self?.setupUIw()
                
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}
extension MyWalletNewVC{
    func getDataFromAPIs(loader : Bool){
        //0 all , 1 reward redeemed , 2 reward earned
        var tempStat = 0
        if status == 2{
            tempStat = 1
        }else if status == 1{
            tempStat = 3
            
        }
        
        WebServices.getWalletReward(dict : ["count" : "0" , "status" : tempStat , "start_date" : /startDateStr , "end_date" : /endDateStr],loader
            , success: { [weak self] (json) in
                
                self?.walletDataNew = MainSubclass.init(fromJson: json)
                print("Data Checking: ", self?.walletDataNew?.totalWalletAmount ?? "Tested")
                self?.tableView.reloadData()
                //self?.walletData = ReferralModel.init(json: json)
                self?.setupUIw()
                
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}
extension UIImageView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
