//
//  MyWalletVC.swift
//  IPAC
//
//  Created by Appinventiv on 23/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import PullToRefreshKit
import IBAnimatable

class MyWalletVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnSideMenu: MenuButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblTotalEarning: UILabel!
    @IBOutlet weak var lblTotalRedeemable: UILabel!
    @IBOutlet weak var lblLastUpdateEarning: UILabel!
    @IBOutlet weak var lblLastUpdateRedeemable: UILabel!
    @IBOutlet weak var lblFooterNoData: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    //MARK::- PROPERTIES
    var walletData : ReferralModel?
    var arrData : [ReferralResult]?  = []
    var count = 0
    var status = 0 // 0 all , 1 earned  , 3 redeemed
    var startDateStr : String?
    var endDateStr : String?
    var startDate : Date?
    var endDate : Date?
    
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        onDidLayoutSubviews()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionAdd(_ sender: UIButton) {
       // let vc = RewardTransactionsVC.instantiate(fromAppStoryboard: .ContactUs)
       // vc.delegate = self
          let vc = MyWalletNewVC.instantiate(fromAppStoryboard: .ContactUs)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionFilter(_ sender: UIButton) {
        let filterVC = FilterVC.instantiate(fromAppStoryboard: .ContactUs)
        filterVC.toDateStr = endDateStr
        filterVC.fromDateStr =  startDateStr
        filterVC.toDate = endDate
        filterVC.fromDate =  startDate
        filterVC.selectedType = self.status
        filterVC.delegate = self
        self.presentVC(filterVC)
    }
}

//MARK::- FUNCTIONS
extension MyWalletVC{
    func onViewDidLoad(){
        self.tableView.isHidden = true
        headerView.addTapGesture { [weak self] (_) in
            let detailVC = RewardDetailVC.instantiate(fromAppStoryboard: .ContactUs)
            detailVC.walletData = self?.walletData
            self?.presentVC(detailVC)
        }
        self.tableView.enablePullToRefresh(tintColor: AppColors.Gray.gray208 ,target: self, selector: #selector(refreshWhenPull(_:)))
        configureTableView()
        handlePaging()
        getDataFromAPI(loader : true)
    }
    
    func setupUI(){
        self.tableView.switchRefreshFooter(to: .normal)
        self.lblTotalEarning.text = self.walletData?.userInfo?.total_point.toInt()?.description ?? "0"
        self.lblTotalRedeemable.text = self.walletData?.walletDetail?.expense.toInt()?.description ?? "0"
        for item in self.walletData?.result ?? []{
            self.arrData?.append(item)
        }
        if count == 0{
            self.arrData = self.walletData?.result ?? []
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.count = self.walletData?.count ?? -1
        self.tableViewDataSource?.items = self.arrData
        self.tableView.reloadData()
        self.tableView.isHidden = false
        self.lblFooterNoData.isHidden = !(self.arrData?.isEmpty ?? true)
        if let dateReward = formatter.date(from: /self.walletData?.userInfo?.updated_date) {
            formatter.dateFormat = "d MMMM yyyy"
            self.lblLastUpdateEarning.text = formatter.string(from: dateReward)
        }
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let dateExpense = formatter.date(from: /self.walletData?.userInfo?.updated_expense_date) {
            formatter.dateFormat = "d MMMM yyyy"
            self.lblLastUpdateRedeemable.text = formatter.string(from: dateExpense)
        }
    }
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.count = 0
        self.getDataFromAPI(loader : false)
    }
}

//MARK::- TABLEVIEW CONFIGURE
extension MyWalletVC{
    func configureTableView(){
        tableViewDataSource = TableViewCustomDatasource.init(items: self.arrData , height: UITableViewAutomaticDimension, estimatedHeight: 40, tableView: tableView, cellIdentifier: Cells.WalletTableViewCell.rawValue, configureCellBlock: { (cell, item, indexpath) in
            guard let cell = cell as? WalletTableViewCell , let item = item as? ReferralResult else {return}
            cell.configure(item : item)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            guard let date = formatter.date(from: /self.arrData?[indexpath.row].created_date) else { return }
            if indexpath.row == 0 {
                cell.lblDate.isHidden = false
            }else{
                guard let prevDate = formatter.date(from: /self.arrData?[indexpath.row - 1].created_date) else { return }
                cell.lblDate.isHidden = Calendar.current.isDate(date, inSameDayAs: prevDate)
            }
        }, aRowSelectedListener: nil, willDisplayCell: nil)
    }
}
//MARK::- PAGINATION
extension MyWalletVC{
    func handlePaging(){
        let footer = DefaultRefreshFooter.footer()
        footer.refreshMode = .scroll
        footer.tintColor = UIColor.gray
        footer.setText("", mode: .pullToRefresh)
        footer.setText("", mode: .noMoreData)
        footer.setText("loading", mode: .refreshing)
        footer.setText("", mode: .scrollAndTapToRefresh)
        footer.setText("", mode: .tapToRefresh)
        tableView.configRefreshFooter(with: footer, container: self) {
            self.count == -1 ? self.tableView.switchRefreshFooter(to: .normal) :  self.getDataFromAPI(loader : false)
        }
    }
}
//MARK::- API
extension MyWalletVC{
    func getDataFromAPI(loader : Bool){
     //0 all , 1 reward redeemed , 2 reward earned
        var tempStat = 0
        if status == 2{
            tempStat = 1
        }else if status == 1{
            tempStat = 3

        }

        WebServices.getWallet(dict : ["count" : count , "status" : tempStat , "start_date" : /startDateStr , "end_date" : /endDateStr],loader
            , success: { [weak self] (json) in
                self?.walletData = ReferralModel.init(json: json)
                self?.setupUI()
                
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

//MARK::- FILTER DELEGATE
extension MyWalletVC : FilterDelegate{
    func apply(selectedType : Int , start : String? , end : String? ,startDate : Date? , endDate : Date? ){
        self.status = selectedType
        self.startDate = startDate
        self.endDate = endDate
        self.startDateStr = start
        self.endDateStr = end
        self.count = 0
        getDataFromAPI(loader : false)
        
    }
}

//MARK::- ADD EXPENSE DELEGATE
extension MyWalletVC : AddExpenseProtocol{
    func updateData() {
        self.status = 0
        self.startDate = nil
        self.endDate = nil
        self.startDateStr = nil
        self.endDateStr = nil
        self.count = 0
        getDataFromAPI(loader : false)
    }
}
