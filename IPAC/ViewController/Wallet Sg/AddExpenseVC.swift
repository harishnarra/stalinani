//
//  AddExpenseVC.swift
//  IPAC
//
//  Created by Appinventiv on 23/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

protocol AddExpenseProtocol : class{
    func updateData()
}

class AddExpenseVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtCampaignTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCampaignDate: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSubmit: AnimatableButton!
    @IBOutlet weak var txtParticular: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!
    
    //MARK::- PROPERTIES
    var imageLimit = 2
    var taskArrayId : [String]? = []
    var taskArrayTitle : [String]? = []
    var taskArray : [TaskModel]? = []
    var selectedTask = 0
    var imageArray = [Image]()
    var expensesArray = [Expense]()
    weak var delegate : AddExpenseProtocol?
    var imgSize = 0
    var dateString = ""
    var isUploadOption = true
    
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnSubmit.addLeftToRightGradient()
        onDidLayoutSubviews()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionSubmit(_ sender: UIButton) {
        if validateData(){
            hitAddExpenseAPI()
        }
    }
    
}

//MARK::- FUNCTIONS
extension AddExpenseVC{
    func onViewDidLoad(){
        getOfflineTasks()
        txtAmount.delegate = self
        btnSubmit.addLeftToRightGradient()
        CommonFunction.setAttributesto(currentTextField: txtCampaignTitle)
        CommonFunction.setAttributesto(currentTextField: txtCampaignDate)
        CommonFunction.setAttributesto(currentTextField: txtParticular)
        CommonFunction.setAttributesto(currentTextField: txtAmount)
        registerXIB()
    }
    
    func registerXIB(){
        tableView.register(UINib(nibName: "UploadImageFooter", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "UploadImageFooter")
        tableView.register(UINib(nibName: "CardFooter", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "CardFooter")
        tableView.register(UINib(nibName: "CardHeader", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "CardHeader")
        
    }
    
    func validateData() -> Bool{
        if (self.txtCampaignTitle?.text?.isEmpty) ?? true{
            CommonFunction.showToast(msg: AlertMessage.addCampaignTitle.localized)
            return false
        }
        if (self.txtCampaignDate?.text?.isEmpty) ?? true{
            CommonFunction.showToast(msg: AlertMessage.addCampaignDate.localized)
            return false
        }
        if self.expensesArray.count == 0{
            CommonFunction.showToast(msg: AlertMessage.addOneEntry.localized)
            return false
        }
        return true
    }
}

//MARK::- API
extension AddExpenseVC{
    func getOfflineTasks(){
        WebServices.getOfflineTask(success: { [weak self] (json) in
            let data = OfflineTaskModel.init(json: json)
            self?.taskArray = data.tasks ?? []
            for item in self?.taskArray ?? []{
                self?.taskArrayId?.append(/item.taskId)
                self?.taskArrayTitle?.append(/item.taskTitle)
            }
            self?.selectedTask = 0
            self?.txtCampaignTitle.text = /self?.taskArrayTitle?.first
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    func hitAddExpenseAPI(){
        var params : [String : Any] = [:]
        params["campaign_date"] = dateString
        params["campaign_title"] = /txtCampaignTitle.text
        params["task_id"] = /self.taskArrayId?[selectedTask]
        var dictArr = Array<[String : String]>()
        for item in self.expensesArray{
            var dict = [String : String]()
            dict["amount"] = item.amount
            dict["particular"] = item.particular
            var images = [String]()
            let _ = item.arrImages.map({ (img)  in
                images.append(img.url)
            })
            let image = images.joined(separator: ",")
            dict["media_url"]  = image
            dictArr.append(dict)
        }
        params["expense_file"] =  ArrayOfDictToString.toJson(dictArr)
        WebServices.addExpense(params: params, success: { [weak self] (json) in
            self?.navigationController?.popViewController(animated: false)
            self?.delegate?.updateData()
        }) {(error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

//MARK::- TEXTFIELD DELEGATE
extension AddExpenseVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCampaignTitle{
            openMultipicker(textField : textField)
        }else if textField == txtCampaignDate{
            setStartDatePicker(textField : textField)
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtAmount{
            return (textField.text ?? "").count - range.length < 8
        }
        return true
    }
}
//MARK::- EXTENSION DATEPICKER
extension AddExpenseVC{
    func setStartDatePicker(textField: UITextField) {
        _ = PKDatePicker.openDateMaxPickerIn(textField,
                                             outPutFormate: EventDateFormat,
                                             mode: .date, minimumDate: dateManager.getMinimumStartDateAndTime(), maximumDate:
            Date(), minuteInterval: 1, selectedDate: Date(), doneBlock: { [weak self] (selectStr) in
                print_debug(selectStr)
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                guard let date = formatter.date(from: selectStr) else { return }
                let dateInString = date.toString(dateFormat: "MM-dd-yyyy")
                self?.dateString = dateInString
                self?.txtCampaignDate.text = date.toString(dateFormat: AddExpenseFormat)
        })
    }
}

//MARK::- EXTENSION MULTIPICKER
extension AddExpenseVC{
    func openMultipicker(textField : UITextField){
        MultiPicker.noOfComponent = 1
        MultiPicker.openMultiPickerIn(textField, firstComponentArray: taskArrayTitle ?? [], secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { [weak self] (data, response, index) in
            self?.txtCampaignTitle.text = /self?.taskArrayTitle?[index]
            self?.selectedTask = index
        }
    }
}

//MARK::- IMAGE PICKER AND UPLOAD
extension AddExpenseVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.uploadImageToS3(image: imagePicked)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func uploadImageToS3(image: UIImage){
        CommonFunction.showLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            CommonFunction.hideLoader()
            if uploaded {
                self.imageArray.append(Image.init(url: imageUrl, size: self.imgSize.description))
                self.imgSize = 0
                self.isUploadOption = false
                //self.tableView.insertRows(at: [IndexPath(row: self.imageArray.count - 1, section: 0)], with: .automatic)
                self.tableView.reloadData()
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, size: {
            (val) in
            self.imgSize = val
        }, failure: { (err : Error) in
            CommonFunction.hideLoader()
            CommonFunction.showToast(msg: err.localizedDescription)

        })
    }
}


//MARK::- TABLE VIEW DELEGATE
extension AddExpenseVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            if indexPath.row == self.imageArray.count {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageCell") as? UploadImageCell else { fatalError("invalid cell \(self)")}
                return cell
                
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadedCardCell") as? UploadedCardCell else { fatalError("invalid cell \(self)")}
                cell.btnCross.isHidden = false
                cell.configureCell(image: imageArray[indexPath.row], index: indexPath)
                cell.btnCross.tag = indexPath.row
                cell.btnCross.addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
                return cell
            }
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadedCardCell") as? UploadedCardCell else { fatalError("invalid cell \(self)")}
            cell.btnCross.isHidden = true
            cell.configureCell(image: self.expensesArray[indexPath.section - 1].arrImages[indexPath.row], index: indexPath)
            return cell
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.expensesArray.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return isUploadOption ? self.imageArray.count + 1 : self.imageArray.count
        // return (self.imageArray.count   == imageLimit ? self.imageArray.count  : self.imageArray.count + 1)
        default:
            return self.expensesArray[section - 1].arrImages.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0 ? 0 : UITableViewAutomaticDimension)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section{
        case 0 :
            return UIView()
        default:
            let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CardHeader") as! CardHeader
            footer.btnCross.tag = (section - 1)
            footer.btnCross.addTarget(self, action: #selector(removeSection(_:)), for: .touchUpInside)
            footer.configure(paticular : self.expensesArray[section - 1].particular , amount : self.expensesArray[section - 1].amount)
            return footer
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let uploadfooter = tableView.dequeueReusableHeaderFooterView(withIdentifier: "UploadImageFooter") as! UploadImageFooter
            uploadfooter.btnDone.addTarget(self, action: #selector(addSection(_:)), for: .touchUpInside)
            uploadfooter.btnUploadFIle.addTarget(self, action: #selector(showUpload(_:)), for: .touchUpInside)
            
            return uploadfooter
        default :
            let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CardFooter") as! CardFooter
            return footer
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == self.imageArray.count{
            //upload image
            self.captureImage(on: self)
        }
    }
}

//MARK::- TARGETS
extension AddExpenseVC{
    
    @objc func removeSection(_ sender: UIButton) {
        self.view.endEditing(true)
        self.expensesArray.remove(at: sender.tag)
        self.tableView.reloadData()
    }
    
    @objc func deleteImage(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imageArray.remove(at: sender.tag)
        self.tableView.reloadData()
    }
    
    @objc func addSection(_ sender: UIButton) {
        if  self.txtParticular.text?.trimmed() == ""{
            CommonFunction.showToast(msg: AlertMessage.addCampaignParticular.localized)
            return
        }
        if  self.txtAmount.text?.trimmed() == ""{
            CommonFunction.showToast(msg: AlertMessage.addCampaignAmount.localized)
            return
        }
        self.expensesArray.insert(Expense.init(particular: /txtParticular.text, amount: /txtAmount.text, arrImages: self.imageArray ), at: 0)
        self.txtParticular.text = ""
        self.txtAmount.text = ""
        self.imageArray = []
        self.tableView.reloadData()
        self.view.endEditing(true)
    }
    
    @objc func showUpload(_ sender : UIButton){
        if imageLimit == self.imageArray.count {
            CommonFunction.showToast(msg: AlertMessage.maxLimitReached.localized)
            return
        }
        self.isUploadOption = true
        self.tableView.reloadData()
    }
}


