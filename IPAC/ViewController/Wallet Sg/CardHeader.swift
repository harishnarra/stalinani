//
//  CardHeader.swift
//  IPAC
//
//  Created by Appinventiv on 23/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class CardHeader: UITableViewHeaderFooterView {

    //MARK::- OUTLETS
    @IBOutlet weak var lblName: UILabel! //particular field
    @IBOutlet weak var lblSize: UILabel!  //amt field
    @IBOutlet weak var btnCross: UIButton!
    
    //MARK::- LIFECYCLE
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(paticular : String , amount : String){
        lblName.text = paticular
        lblSize.text = amount
    }
}
