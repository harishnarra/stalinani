//
//  UploadImageCell.swift
//  IPAC
//
//  Created by Appinventiv on 24/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

class UploadImageCell: UITableViewCell {
    
    @IBOutlet weak var viewBorder: AnimatableView!
    
    //MARK::- LIFECYELE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        viewBorder.addDashedBorder(color: UIColor.lightGray, lineWidth: 1)
    }

}
