//
//  UploadedCardCell.swift
//  IPAC
//
//  Created by Appinventiv on 24/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class UploadedCardCell: UITableViewCell {

    //MARK::- OUTLET
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var btnCross: UIButton!
    
    
    //MARK::- PROEPRTY
    
    //MARK::- LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK::- CONFIGURE
    func configureCell(image : Image , index : IndexPath){
        img.kf.setImage(with: URL(string : image.url))
        lblName.text = "File " + (index.row + 1).description
        lblSize.text = image.size + " KB"
    }

}
