//
//  FilterVC.swift
//  IPAC
//
//  Created by Appinventiv on 22/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import IBAnimatable

protocol FilterDelegate : class{
    func apply(selectedType : Int , start : String? , end : String? , startDate : Date? , endDate : Date?)
}

class FilterVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet var btnOptionsCollection: [UIButton]!
    @IBOutlet weak var txtFrom: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTo: SkyFloatingLabelTextField!
    @IBOutlet weak var btnApply: AnimatableButton!
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var filterTranscationsLabel: UILabel!

    
    //MARK::- PROPERTIES
    var selectedType = 0
    weak var delegate : FilterDelegate?
    var fromDateStr : String?
    var toDateStr : String?
    var fromDate : Date?
    var toDate : Date?
    var isStartDate = true
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionResetFilter(_ sender: UIButton) {
        self.selectedType = 0
        updateSelectedType()
        clearDate()
        
    }
    @IBAction func btnActionClearDate(_ sender: UIButton) {
        clearDate()
    }
    @IBAction func btnActionApply(_ sender: AnimatableButton) {
        if (toDate == nil && fromDate != nil) || (toDate != nil && fromDate == nil) || ((toDate != nil && fromDate != nil) && toDate ?? Date() < fromDate ?? Date()){
            CommonFunction.showToast(msg:  AlertMessage.addValidStartEndDate.localized )
            return
        }
        delegate?.apply(selectedType: self.selectedType, start: self.fromDateStr, end: self.toDateStr, startDate: self.fromDate, endDate: self.toDate)
        self.dismissVC(completion: nil)
        
    }
    @IBAction func btnActionSelectOption(_ sender: UIButton) {
        self.selectedType = sender.tag
        updateSelectedType()
        //0 all , 1 reward redeemed , 2 reward earned
    }
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
}

//MARK::- FUNCTIONS
extension FilterVC{
    func onViewDidLoad(){
        CommonFunction.setAttributesto(currentTextField: txtFrom)
        CommonFunction.setAttributesto(currentTextField: txtTo)
        btnApply.addLeftToRightGradient()
        updateSelectedType()
        txtTo.delegate = self
        txtFrom.delegate = self
        txtTo.text = self.toDateStr
        txtFrom.text = self.fromDateStr
        txtFrom.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "FROM", comment: "")
        txtTo.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "TO", comment: "")

        self.txtFrom.text = fromDate?.toString(dateFormat: "dd/MM/yyyy")
        self.txtTo.text = toDate?.toString(dateFormat: "dd/MM/yyyy")
        let str = LocalizationSystem.sharedInstance.localizedStringForKey(key: "All", comment: "") as String
        let str2 = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reward_Redeemed", comment: "") as String
        let str3 = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reward_Earned", comment: "") as String

        
        btnOptionsCollection[0].setTitle(str, for: .normal)
        btnOptionsCollection[1].setTitle(str2, for: .normal)
        btnOptionsCollection[2].setTitle(str3, for: .normal)

        
        
        filterTranscationsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Filter_Transactions", comment: "")
        resetBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "RESET", comment: ""), for: .normal)
        dateLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "DATE", comment: "")
        btnApply.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "APPLY", comment: ""), for: .normal)
        clearBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "CLEAR", comment: ""), for: .normal)


    }
    func updateSelectedType(){
        for item in btnOptionsCollection{
            item.isSelected = item.tag == selectedType
        }
    }
    func clearDate(){
        fromDate = nil
        toDate = nil
        fromDateStr = nil
        toDateStr = nil
        txtFrom.text = ""
        txtTo.text = ""
    }
}

//MARK::- TEXTFIELD DELEGATE
extension FilterVC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        setStartDatePicker(textField : textField)
        return true
    }
}

//MARK::- EXTENSION DATEPICKER
extension FilterVC{
    func setStartDatePicker(textField: UITextField) {
        self.isStartDate = textField == txtFrom
        _ = PKDatePicker.openDateMaxPickerIn(textField,
                                             outPutFormate: EventDateFormat,
                                             mode: .date, minimumDate: dateManager.getMinimumStartDateAndTime(), maximumDate:
            Date(), minuteInterval: 1, selectedDate: Date(), doneBlock: { (selectStr) in
                print_debug(selectStr)
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                guard let date = formatter.date(from: selectStr) else { return }
                let dateInString = date.toString(dateFormat: "yyyy-MM-dd")
                if self.isStartDate{
                    self.txtFrom.text = dateInString
                    self.fromDateStr = dateInString
                    self.fromDate = date
                    self.txtFrom.text = date.toString(dateFormat: "dd/MM/yyyy")

                }else{
                    self.txtTo.text = dateInString
                    self.toDateStr = dateInString
                    self.toDate = date
                    self.txtTo.text = date.toString(dateFormat: "dd/MM/yyyy")

                }
        })
    }
}
