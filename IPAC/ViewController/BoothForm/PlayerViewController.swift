//
//  PlayerViewController.swift
//  IPAC
//
//  Created by Appinventiv on 31/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class PlayerViewController: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var lblDescription: UILabel!//hidden
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var txtViewDesc: UITextView!
    
    //MARK::- PROPERTIES
    var url : String?
    var desc : String?
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }

    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK::- FUNCTION
    func onViewDidLoad(){
        lblDescription.text = /desc//hidden
        txtViewDesc.text = /desc
        let player = AVPlayer(url: URL(string : /url)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        playerController.view.frame = viewPlayer.bounds
        self.addChildViewController(playerController)
        viewPlayer.addSubview(playerController.view)
        playerController.showsPlaybackControls = true
        player.play()
    }
    
}
