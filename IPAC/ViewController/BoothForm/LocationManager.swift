//
//  LocationManager.swift
//  IPAC
//
//  Created by Appinventiv on 01/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationUpdated  : class{
    func fetchNewLocation()
}

class LocationManager: NSObject,CLLocationManagerDelegate {
    
    var locationManager : CLLocationManager?
    var currentLoc : CLLocation?
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var address : String?
    weak var delegateLocationUpdate : LocationUpdated?
    
    override init() {
        super.init()
        locationInitializer()
        updateLocation()
    }
    
    static let shared = LocationManager()
    
    func updateLocation(){
        if CLLocationManager.authorizationStatus() == .denied{
            if CommonFunction.getUser() != nil{
                settingsAlert()
                return
            }
     
        }
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
    }
    func stopUpdateLocation(){
        locationManager?.delegate = nil
        locationManager?.stopUpdatingLocation()
    }
    func locationInitializer(){
        locationManager?.delegate = nil
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager?.requestAlwaysAuthorization()
        locationManager?.requestWhenInUseAuthorization()
    }
    
    func settingsAlert(){
        AlertsClass.shared.showAlertController(withTitle: "Allow Permission", message: "Please allow location permission from settings", buttonTitles: ["Settings" , "Cancel"]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                guard let url = URL(string:UIApplicationOpenSettingsURLString) else {return}
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            default:
                break
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse,.authorizedAlways:
            locationManager?.startUpdatingLocation()
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
        case .restricted,.denied:
            break
            //self.settingsAlert()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLoc = locations.last
        if let lat = currentLoc?.coordinate.latitude ,let lng = currentLoc?.coordinate.longitude {
            if lat == 0.0 && lng == 0.0 {return}
                latitude = lat
                longitude = lng
                delegateLocationUpdate?.fetchNewLocation()
                Utility.functions.calculateAddress(lat: lat, long: lng, responseBlock: { [weak self] (coordinates, address,name,_,_,_) in
                    self?.address = address
                    self?.locationManager?.stopUpdatingLocation()
                    self?.locationManager?.delegate = nil
                })
        }
    }
}


