//
//  TaskDetailVC.swift
//  IPAC
//
//  Created by Appinventiv on 01/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import iCarousel
import Kingfisher
import AVFoundation
import EZSwiftExtensions
import AVKit
import Photos

protocol TaskCompleteDelegate : class{
    func refresh()
}

class TaskDetailVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnMedia: GradientButton!
    @IBOutlet weak var btnTimer: UIButton!
    @IBOutlet weak var viewCarousel: iCarousel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var isTaskCompleteBtn: UIButton!
    
    //MARK::- PROEPERTIES
    var arrImg : [Image] = []
    var isUpdated = false
    var task : TaskTabModel?
    var delegate : TaskCompleteDelegate?
    var img = UIImage()
    
    var countdown : Timer? =  Timer()
    var totalTimeInSec = 59
    var mediaURls : [String] = []
    var mediaTypes: [String] = []
    var mediaThumbnail = [String]()
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        onDidLayoutSubviews()
    }
    
    //MARK::- BUTTON ACTION
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        if isUpdated{
            delegate?.refresh()
            CommonFunction.showToast(msg: AlertMessage.reviewTheTask.localized)
            
        }else{
            CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionShare(_ sender: UIButton) {
        let someText:String =  "Download file: " + /self.task?.task_media_set.first?.mediaUrl
        let activityViewController = UIActivityViewController(activityItems : [someText], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func btnActionDownloadMedia(_ sender: UIButton) {
        if /self.task?.task_media_set.first?.mediaType != "2"{
            UIImageWriteToSavedPhotosAlbum(self.img, self,#selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }else{
            "".downloadVideoLinkAndCreateAsset( /self.task?.task_media_set.first?.mediaUrl)
            
        }
    }
    
    
    @IBAction func btnActionUploadMedia(_ sender: UIButton) {
        let vc = UploadImageForMediaTaskVC.instantiate(fromAppStoryboard: .Form)
        vc.delegate = self
        vc.imageArray = self.arrImg
        vc.task = self.task
        self.navigationController?.presentVC(vc)
    }
    
    //MARK::- SELECTOR TARGET
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            CommonFunction.showToast(msg: error.localizedDescription)
        } else {
            CommonFunction.showToast(msg: AlertMessage.imageSavedSuccessfully.localized)
        }
    }
    
}

//MARK::- DELEGATE
extension TaskDetailVC : ImageUploaded{
    func success(arr: [Image]) {
        self.arrImg = arr
        self.isUpdated = true
        self.tableViewDataSource?.items = self.arrImg as Array<AnyObject>
        self.tableView.reloadData()
    }
}


//MARK::- FUNCTIONS
extension TaskDetailVC{
    func onViewDidLoad(){
        configureTableView()
        self.btnTimer.setImage(#imageLiteral(resourceName: "icVerificationWatch"), for: .normal)
        self.btnTimer.isHidden = true
        btnMedia.rounded(cornerRadius: btnMedia.frame.height/2, clip: true)
        viewCarousel.dataSource = self
        viewCarousel.delegate = self
        viewCarousel.reloadData()
        viewCarousel.type = .invertedTimeMachine
        self.lblTitle.text = task?.task_title
        self.lblDesc.text = task?.task_description
        self.lblPoint.text = task?.points
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = formatter.date(from: /task?.end_date) else { return }
        guard let dateCreated = formatter.date(from: /task?.start_date) else { return }
        let displayDate = dateCreated.toString(dateFormat: "MMMM dd 'at,' h:mm a", timeZone: .current)
        lblTime.text = displayDate
        let now = Date()
        self.mediaTypes.removeAll()
        self.mediaURls.removeAll()
        self.mediaThumbnail.removeAll()
        if now >= date  {
            self.stopTimer()
            btnTimer.isHidden = true
        } else if task?.taskComplete != "1" && task?.taskComplete != "2"{
            totalTimeInSec = date.secondsFrom(now)
            self.startTimer()
        }
        btnTimer.setTitleColor(AppColors.Orange.orange, for: .normal)
        _ = task?.task_media_set.map({ (media) in
            self.mediaTypes.append(media.mediaType ?? "")
            self.mediaURls.append(media.mediaUrl ?? "")
            self.mediaThumbnail.append(media.mediaThumb ?? "")
        })
        if task?.taskComplete == "1" {//completed
            self.btnTimer.isHidden = true
            isTaskCompleteBtn.setImage(#imageLiteral(resourceName: "icCheck") , for: .normal)
        } else if task?.taskComplete == "2" {//review
            self.btnTimer.isHidden = true
            isTaskCompleteBtn.setImage(#imageLiteral(resourceName: "icCheckYallow") , for: .normal)
        }else{
            isTaskCompleteBtn.setImage(#imageLiteral(resourceName: "icDisablecheck") , for: .normal)
        }
        viewCarousel.reloadData()
        if /self.task?.task_media_set.first?.mediaType != "2"{
            ez.requestImage(/self.task?.task_media_set.first?.mediaUrl) { [weak self] (img) in
                self?.img = img ?? UIImage()
            }
        }
    }
    
    //MARK::- TABLEVIEW CONFIGURE
    func configureTableView(){
        self.tableViewDataSource = TableViewCustomDatasource.init(items: self.arrImg as Array<AnyObject>, height: UITableViewAutomaticDimension, estimatedHeight: 2.0, tableView: tableView, cellIdentifier: "TaskDetailTableViewCell", configureCellBlock: {  (cell, item, indexpath) in
            guard let cell = cell as? TaskDetailTableViewCell , let item = item as? Image else {return}
            cell.configure(data: item, index: indexpath.row)
        }, aRowSelectedListener: nil, willDisplayCell: nil)
    }
}



extension TaskDetailVC : iCarouselDelegate,iCarouselDataSource{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return mediaURls.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var tempView: UIView
        tempView = UIView.init(frame: CGRect(x:0 , y:0, width: self.viewCarousel.frame.width - 10,height:self.viewCarousel.frame.height))
        let carouselImageview = UIImageView()
        carouselImageview.frame = CGRect(x: 0, y: 0, width: tempView.bounds.width - 0, height: tempView.bounds.height - 0)
        carouselImageview.contentMode = .scaleAspectFill
        carouselImageview.rounded(cornerRadius: 3.0, clip: true)
        tempView.addSubview(carouselImageview)
        if let url = URL(string: mediaURls[index]), mediaTypes[index] == "1" {
            ImageDownloader.default.authenticationChallengeResponder = self
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "icHomeImage"))
        } else if let url = URL(string: mediaThumbnail[index]), mediaTypes[index] == "2" {
            let image = UIImageView()
            image.frame = CGRect(origin: carouselImageview.center, size: CGSize(width: 35, height: 35))
            image.center = carouselImageview.center
            image.image = #imageLiteral(resourceName: "playIcon")
            image.contentMode = .center
            tempView.addSubview(image)
            carouselImageview.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "icHomeImage"))
        }
        else {
            carouselImageview.image = UIImage(named: "videoPlayer")
        }
        return tempView
    }
    
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .fadeMin)
        {
            return 0.1
        } else if (option == .fadeMinAlpha)
        {
            return 0.0
        } else if (option == .fadeMax)
        {
            return 0.3;
        }
        else if option == .showBackfaces {
            return 2
        }
        else if option == .wrap {
            return 1
        }
        return value;
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if mediaTypes[index] == "2" {
            let vc =  PlayerViewController.instantiate(fromAppStoryboard: .Form)
            vc.url = /mediaURls[index]
            vc.desc = /self.task?.task_title
            ez.topMostVC?.presentVC(vc)
            //            let videoURL = URL(string: mediaURls[index])
            //            let player = AVPlayer(url: videoURL!)
            //            let playerViewController = AVPlayerViewController()
            //            playerViewController.player = player
            //            self.present(playerViewController, animated: true) {
            //                playerViewController.player!.play()
            //            }
            
        } else {
            let previewVC = ImagePreviewVC.instantiate(fromAppStoryboard: .Home)
            previewVC.previewImage = mediaURls[index]
            previewVC.tittle = task?.task_title ?? ""
            self.navigationController?.pushViewController(previewVC, animated: true)
        }
    }
    
}

// setTime
extension TaskDetailVC {
    
    func startTimer() {
        if !(countdown?.isValid ?? false){
            countdown = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        self.btnTimer.isHidden = false
        self.btnTimer.setTitle("\(timeFormatted(totalTimeInSec))", for: .normal)
        if totalTimeInSec != 0 {
            totalTimeInSec -= 1
        } else {
            stopTimer()
        }
    }
    
    func stopTimer() {
        countdown?.invalidate()
        self.btnTimer.isHidden = true
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let dayss = (totalSeconds / (3600 * 24))
        let hourss = (totalSeconds % (3600*24)) / (3600)
        let minss = ((totalSeconds % (3600*24)) % (3600)) / 60
        let secss = ((((totalSeconds % (3600*24)) % (3600)) % 60) % 60)
        
        return String(format: "%2d\(StringConstant.d.localized) %02d\(StringConstant.h.localized) %02d\(StringConstant.m.localized) %02d\(StringConstant.s.localized)", dayss, hourss, minss, secss)
    }
    
}


