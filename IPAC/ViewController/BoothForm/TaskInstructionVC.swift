//
//  TaskInstructionVC.swift
//  IPAC
//
//  Created by Appinventiv on 31/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class TaskInstructionVC: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    //MARK::- PROPERTIES
    var desc : String?
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    //MARK::- BUTTON ACTION
    @IBAction func btnActionOk(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    
    //MARK::- FUNCTIONS
    func onViewDidLoad(){
        btnOK.setTitle(AlertMessage.ok.localized, for: .normal)
        lblDesc.text = /desc
        lblTitle.text = AlertMessage.taskInstruction.localized
    }

}
