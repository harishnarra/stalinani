//
//  EventTaskVC.swift
//  IPAC
//
//  Created by Appinventiv on 01/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import EZSwiftExtensions
import IBAnimatable

protocol TaskComplete : class{
    func refresh()
}

class EventTaskVC: BaseViewController {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnCheckin: AnimatableButton!
    
    //MARK::- PROPERTIES
    var task : TaskTabModel?
    weak var delegate : TaskComplete?
    var userMarker = GMSMarker()
    var eventMarker = GMSMarker()
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        CommonFunction.showToast(msg: AlertMessage.taskNotComplete.localized)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionCurrentLocation(_ sender: UIButton) {
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: LocationManager.shared.latitude , longitude: LocationManager.shared.longitude  , zoom: 16.0)
        mapView?.animate(to: camera)
    }
    @IBAction func btnActionEventLocation(_ sender: UIButton) {
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: Double.init(/self.task?.latitude) ?? 0.0 , longitude: Double.init(/self.task?.longitude) ?? 0.0  , zoom: 16.0)
        mapView?.animate(to: camera)
    }
    @IBAction func btnActionCheckin(_ sender: UIButton) {
        if (getDistanceBetweenLatLongs(lat: LocationManager.shared.latitude, long: LocationManager.shared.longitude, latitude: self.task?.latitude.toDouble() ?? 0.0, longitude: self.task?.longitude.toDouble() ?? 0.0).toDouble() ?? 0.0) <= (self.task?.radius.toDouble() ?? 0.0){
            updateTask()
        }else{
            CommonFunction.showToast(msg: AlertMessage.invalidAction.localized)
        }
        
    }
    
    
    //MARK::- FUNCTIONS
    func onViewDidLoad(){
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: LocationManager.shared.latitude , longitude: LocationManager.shared.longitude  , zoom: 14.0)
        mapView?.animate(to: camera)
    
        if (getDistanceBetweenLatLongs(lat: LocationManager.shared.latitude, long: LocationManager.shared.longitude, latitude: self.task?.latitude.toDouble() ?? 0.0, longitude: self.task?.longitude.toDouble() ?? 0.0).toDouble() ?? 0.0) <= (self.task?.radius.toDouble() ?? 0.0){
            
            btnCheckin.setBackgroundColor(UIColor.clear, forState: .normal)
            btnCheckin.addLeftToRightGradient()
            btnCheckin.setTitleColor(UIColor.white, for: .normal)
        }
        self.setupMapView()
        
    }
    
    func setupMapView(){
        let currentLoc = CLLocationCoordinate2D(latitude: LocationManager.shared.latitude , longitude: LocationManager.shared.longitude )
        let eventLoc = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double.init(/self.task?.latitude) ?? 0.0), longitude: CLLocationDegrees(Double.init(/self.task?.longitude) ?? 0.0))
        self.mapView.clear()
        userMarker.icon  = #imageLiteral(resourceName: "icLocationYallow")
        userMarker.position = currentLoc
        eventMarker.icon = #imageLiteral(resourceName: "icLocationGreen")
        Utility.functions.calculateAddress(lat: self.task?.latitude.toDouble() ?? 0.0, long: self.task?.longitude.toDouble() ?? 0.0) { (_, address, _, _, _, _) in
            self.eventMarker.snippet = address
        }
        eventMarker.position = eventLoc
        userMarker.snippet = LocationManager.shared.address
        userMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        eventMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        userMarker.map = mapView
        eventMarker.map = mapView
    }

}

//MARK::- API
extension EventTaskVC{
    func updateTask(){
        let params = [ApiKeys.taskId : /self.task?.task_id]
        WebServices.updateTask(params: params , success: { [weak self](result) in
            print_debug(result)
            self?.navigationController?.popViewController(animated: false)
            self?.delegate?.refresh()
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
}

