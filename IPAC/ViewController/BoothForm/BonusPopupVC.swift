//
//  BonusPopupVC.swift
//  IPAC
//
//  Created by Appinventiv on 25/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

protocol BonusPopupDelegate : class{
    func done(status : Int)//        //0 profile , 1 task , 2 form
    
}

class BonusPopupVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK::- PROPERTIES
    weak var delegate : BonusPopupDelegate?
    var isFirstTask = false //append to array if true
    var isFormFill = false
    var isPaytmDetail = false
    var isProof = false
    var isDismiss = true
    var arrField : [String] = []
    var arrData = [AlertMessage.completeFirstTask.localized,AlertMessage.completeBoothForm.localized,AlertMessage.completePaytm.localized,AlertMessage.completeProof.localized]
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    //MARK::- BUTTON ACTION
    
    @IBAction func btnActionOk(_ sender: UIButton) {
        self.dismissVC {
            self.delegate?.done(status: self.isFirstTask ? 1 : ((self.isFormFill) ? 2 : 0) ) //0 profile , 1 task , 2 form
        }
    }
    @IBAction func btnActionDismiss(_ sender: UIButton) {
        if isDismiss{
            self.dismiss(animated: false)
        }
    }
    
}

//MARK::- FUNCTIONS
extension BonusPopupVC{
    func onViewDidLoad(){
        if isFirstTask{
            arrField.append(arrData[0])
        }
        if isFormFill{
            arrField.append(arrData[1])
        }
        if isPaytmDetail{
            arrField.append(arrData[2])
        }
        if isProof{
            arrField.append(arrData[3])
        }
        configureTableView()
        lblTitle.text = AlertMessage.bonusReward.localized
        lblDescription.text = AlertMessage.bonusRewardMessage.localized
        btnOk.setTitle(AlertMessage.ok.localized, for : .normal)
    }
}

//MARK::- TABLEVIEW CONFIGURE
extension BonusPopupVC{
    func configureTableView(){
        tableViewDataSource = TableViewCustomDatasource.init(items: self.arrField as Array<AnyObject>, height: UITableViewAutomaticDimension, estimatedHeight: 20, tableView: tableView, cellIdentifier: "BonusPopupTableViewCell", configureCellBlock: { [weak self] (cell, item, indexPath) in
            guard let cell = cell as? BonusPopupTableViewCell else {return}
            cell.lblTitle.text = "\(indexPath.row + 1). \(/self?.arrField[indexPath.row])"
            }, aRowSelectedListener: nil, willDisplayCell: nil)
    }
}
