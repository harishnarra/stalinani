//
//  BonusReceivedVC.swift
//  IPAC
//
//  Created by Appinventiv on 30/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class BonusReceivedVC: BaseViewController {

    //MARK::- OUTLETS
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var viewS: FBShimmeringView!
    
    //MARK::- PROPERTIES
    var descriptionStr : String?
    var titleStr  : String?
    
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }

    //MARK::- BUTTON ACTION
    @IBAction func btnActionOk(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    
}

//MARK::- FUNCTIONS
extension BonusReceivedVC{
    func onViewDidLoad(){
        btnOk.setTitle(AlertMessage.ok
            .localized , for: .normal)
        lblTitle.text = /self.titleStr
        lblDescription.text = /self.descriptionStr
        viewS.contentView = self.imgIcon
        viewS.shimmeringSpeed = 100
        viewS.isShimmering = true        
    }
}
