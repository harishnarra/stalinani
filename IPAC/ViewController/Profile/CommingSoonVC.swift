//
//  CommingSoonVC.swift
//  IPAC
//
//  Created by macOS on 16/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class CommingSoonVC: BaseVC {

    @IBOutlet weak var commingSoonImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        commingSoonImage.rounded(cornerRadius: 5.0, clip: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
