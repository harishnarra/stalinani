//
//  IdProofCell.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class IdProofCell: UITableViewCell {

    @IBOutlet weak var idProofTxtFld: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommonFunction.setAttributesto(currentTextField: idProofTxtFld)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
