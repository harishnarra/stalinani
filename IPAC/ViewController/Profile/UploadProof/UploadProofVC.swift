//
//  UploadProofVC.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit


protocol Update : class{
    func update()
}

class UploadProofVC: UIViewController {
    
    //MARK:- Properties
    //==================
    private var uploadDocuments = UploadDocuments()
    private var isLoadMoreCellTapp = false
    weak var delegate : Update?
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view)
        
        if  !self.containerView.frame.contains(point) {
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
    }
    
    //MARK: - IB Action and Target
    //===============================
    @objc func submitBtnTap(_ sender: UIButton) {
        self.view.endEditing(true)
        if validation {
            self.uploadDetails(loader: true)
        }
    }
    
    @objc func uploadBtnTap(_ sender: UIButton) {
        self.captureImage(on: self)
    }
    
    @objc func addMoreCellBtnTap(_ sender: UIButton) {
        self.isLoadMoreCellTapp = !self.isLoadMoreCellTapp
        self.tableView.reloadData()
    }
    
    @objc func deleteUploadedDocsBtnTap(_ sender: UIButton) {
       self.uploadDocuments.proofRawImage.remove(at: 0)
        self.uploadDocuments.proofImage.remove(at: 0)
        self.tableView.reloadData()
    }
    
    @objc func deleteUploadedDocsOneBtnTap(_ sender: UIButton) {
        self.uploadDocuments.proofRawImage.remove(at: 1)
        self.uploadDocuments.proofImage.remove(at: 1)
        self.tableView.reloadData()
    }
    
    @objc func stopUploadingDocs(_ sender: UIButton) {
        self.uploadDocuments.proofRawImage.remove(at: 0)
        
        if !self.uploadDocuments.proofImage.isEmpty {
            self.uploadDocuments.proofImage.remove(at: 0)
        }
        AWSController.cancelAllRequest()
        self.tableView.reloadData()
    }
    
    @objc func stopUploadingDocOne(_ sender: UIButton) {
        self.uploadDocuments.proofRawImage.remove(at: 1)
        if self.uploadDocuments.proofImage.count > 1 {
            self.uploadDocuments.proofImage.remove(at: 1)
        }
        AWSController.cancelAllRequest()
        self.tableView.reloadData()
    }
    
    //MARK:- VALIDATION
    //====================
    private var validation: Bool {
        
        switch self.uploadDocuments.proofType{
        case .panCard: //2
            if self.uploadDocuments.idNumber.count != 10 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
        case .voterCard: //1
            if self.uploadDocuments.idNumber.count < 8 || self.uploadDocuments.idNumber.count > 15 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
        case .drivingLience://3
            if self.uploadDocuments.idNumber.count < 10 || self.uploadDocuments.idNumber.count > 18 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
        default:
            if self.uploadDocuments.idNumber.count < 8 || self.uploadDocuments.idNumber.count > 15 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
            
        }
        
        if self.uploadDocuments.idNumber.count < 10 {
            CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
            return false
        }else if self.uploadDocuments.proofImage.count == 0{
            CommonFunction.showToast(msg: StringConstant.You_Need_To_Upload.localized)
            return false
        }
        else {
             return true
        }
    }
}


//MARK:-
//MARK:- Table view Delegate and DataSource
extension UploadProofVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        } else if section == 1{
            let count = self.uploadDocuments.proofRawImage.count
            if count == 2 {
                return count
            } else { //if count == 1{
                if self.uploadDocuments.proofImage.count == 1 {
                    return 2
                } else {
                    return 1
                }
            }
            // return count == 2 ? (count) :(1)
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "IdProofCell") as? IdProofCell else { fatalError("invalid cell \(self)")
            }
            cell.idProofTxtFld.delegate = self
            
            if indexPath.row == 0 {
                cell.idProofTxtFld.placeholder = StringConstant.Select_ID.localized
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: CGFloat(cell.idProofTxtFld.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                
                button.setImage(#imageLiteral(resourceName: "icHomeDropdown"), for: .normal)
                cell.idProofTxtFld.rightView = button
                cell.idProofTxtFld.rightViewMode = .always
                cell.idProofTxtFld.text = self.uploadDocuments.proofName
                return cell
            } else {
                cell.idProofTxtFld.placeholder = StringConstant.UI_Number.localized
                cell.idProofTxtFld.rightViewMode = .never
                cell.idProofTxtFld.text = self.uploadDocuments.idNumber
                return cell
            }
            
            
        case 1:
            
            if self.uploadDocuments.proofRawImage.isEmpty {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadFileCell") as? UploadFileCell else { fatalError("invalid cell \(self)")
                }
                cell.uploadBtn.addTarget(self, action: #selector(self.uploadBtnTap(_:)), for: .touchUpInside)
                return cell
                
            } else if self.uploadDocuments.proofRawImage.count == 1 {
                
                
                if self.uploadDocuments.proofImage.count == 1 {
                    
                    switch indexPath.row {
                    case 0:
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageFileCell") as? UploadImageFileCell else { fatalError("invalid cell \(self)")
                        }
                        cell.documentImage.image = self.uploadDocuments.proofRawImage[0]
                        cell.deleteUploadDocumentBtn.isHidden = false   // dont change this
                         cell.stopUploadingDocsBtn.isHidden = true // dont change this
                        cell.progressView.isHidden = true // dont change this
                        var kb = 0.0
                        if let imageData = UIImagePNGRepresentation(self.uploadDocuments.proofRawImage[0]) {
                            let bytes = imageData.count
                            kb = Double(bytes) / 1000.0 // Note the difference
                            //let KB = Double(bytes) / 1024.0 // Note the difference
                        }
                        
                     
                        cell.dataCountLabel.text = "\(String(kb)) KB"
                        cell.deleteUploadDocumentBtn.addTarget(self, action: #selector(self.deleteUploadedDocsBtnTap(_:)), for: .touchUpInside)
                        return cell
                    case 1:
                        
                        if self.isLoadMoreCellTapp {
                            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadFileCell") as? UploadFileCell else { fatalError("invalid cell \(self)")
                            }
                            cell.uploadBtn.addTarget(self, action: #selector(self.uploadBtnTap(_:)), for: .touchUpInside)
                            return cell
                            
                        } else {
                            
                            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreCell") as? AddMoreCell else { fatalError("invalid cell \(self)")     }
                            cell.addMoreCellBtn.addTarget(self, action: #selector(self.addMoreCellBtnTap(_:)), for: .touchUpInside)
                            return cell
                            
                        }
                        
                        
                    default:
                        fatalError()
                    }
                    
                } else  {
                    
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageFileCell") as? UploadImageFileCell else { fatalError("invalid cell \(self)")
                        }
                    cell.label.text = StringConstant.ImageFile1.localized
                    cell.documentImage.image = self.uploadDocuments.proofRawImage[0]
                    cell.stopUploadingDocsBtn.isHidden = false
                    cell.progressView.isHidden = false // dont change this
                    cell.progressView.progress = 0.0 // dont change this
                    cell.deleteUploadDocumentBtn.isHidden = true // dont change this
                    cell.dataCountLabel.text = ""
                    cell.stopUploadingDocsBtn.addTarget(self, action: #selector(stopUploadingDocs(_:)), for: .touchUpInside)
                        return cell
                    
                }
            } else {
                
                switch indexPath.row {
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageFileCell") as? UploadImageFileCell else { fatalError("invalid cell \(self)")
                    }
                    cell.deleteUploadDocumentBtn.isHidden = false
                    cell.stopUploadingDocsBtn.isHidden = true
                    cell.documentImage.image = self.uploadDocuments.proofRawImage[0]
                    cell.label.text = StringConstant.ImageFile1.localized
                    return cell
                    
                case 1:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadImageFileCell") as? UploadImageFileCell else { fatalError("invalid cell \(self)")
                    }
                    var kb = 0.0
                    if let imageData = UIImagePNGRepresentation(self.uploadDocuments.proofRawImage[1]) {
                        let bytes = imageData.count
                         kb = Double(bytes) / 1000.0 // Note the difference
                       
                        //let KB = Double(bytes) / 1024.0 // Note the difference
                    }
                    
                    if self.uploadDocuments.proofImage.count == 2 {
                        cell.deleteUploadDocumentBtn.isHidden = false
                        cell.stopUploadingDocsBtn.isHidden = true
                        cell.progressView.isHidden = true
                        cell.dataCountLabel.text = "\(String(kb)) KB"
                    } else {
                        cell.deleteUploadDocumentBtn.isHidden = true
                        cell.stopUploadingDocsBtn.isHidden = false
                        cell.progressView.isHidden = false
                        cell.dataCountLabel.text = ""
                    }
                  cell.progressView.progress = 0.0
                       cell.stopUploadingDocsBtn.addTarget(self, action: #selector(stopUploadingDocOne(_:)), for: .touchUpInside)
                    cell.documentImage.image = self.uploadDocuments.proofRawImage[1]
                    cell.deleteUploadDocumentBtn.addTarget(self, action: #selector(self.deleteUploadedDocsOneBtnTap(_:)), for: .touchUpInside)
                    cell.label.text = StringConstant.ImageFile2.localized
                    return cell
                default :
                    fatalError()
            }
                
            }
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as? ButtonCell else { fatalError("invalid cell \(self)") }
            cell.submitBtn.setTitle(StringConstant.SUBMIT.localized, for: .normal)
            cell.submitBtn.addTarget(self, action: #selector(submitBtnTap(_:)), for: .touchUpInside)
            return cell
        default:
            fatalError("cell not found")
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK:-
//MARK:- Private Method

extension UploadProofVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let index = textField.tableViewIndexPath(self.tableView) else { return false}
        switch index.row {
        case 0:
            MultiPicker.noOfComponent = 1
            MultiPicker.openMultiPickerIn(textField, firstComponentArray: CommonFunction.proofArray, secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { (data, response, index) in
                
                let proofType = ProofType(rawValue: index+1)
                self.uploadDocuments.proofType = proofType ?? .voterCard
                self.uploadDocuments.proofName = data
                textField.text =  data
                self.uploadDocuments.idNumber =  ""
                self.tableView.reloadData()
                print_debug(index)
                print_debug(self.uploadDocuments.proofType.rawValue)
                print_debug(self.uploadDocuments.proofType)
            }
            return true
        case 1:
            return true
        default:
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.tableView) else { return false}
        switch indexPath.row {
        case 1 : // first name text field
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else {
                //return (textField.text ?? "").count - range.length < 20
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                return (string == filtered) && (textField.text ?? "").count - range.length < (self.uploadDocuments.proofType == .panCard ? 10 : 20)
//
            }
            
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView) else { return }
        if indexPath.row == 1 {
            self.uploadDocuments.idNumber = textField.text ?? ""
        }
    }
    
}

//MARK:-
//MARK:- Private Method
private extension UploadProofVC {
    
    func initialSetup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.containerView.rounded(cornerRadius: 5.0, clip: true)
        navigationController?.navigationBar.isHidden = true
        registerCell()
    }
    func registerCell() {
        self.tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.tableView.register(UINib(nibName: "UploadFileCell", bundle: nil), forCellReuseIdentifier: "UploadFileCell")
        self.tableView.register(UINib(nibName: "UploadImageFileCell", bundle: nil), forCellReuseIdentifier: "UploadImageFileCell")
        self.tableView.register(UINib(nibName: "AddMoreCell", bundle: nil), forCellReuseIdentifier: "AddMoreCell")
    }
    
    func uploadDetails(loader: Bool) {
        let currentUser = CommonFunction.getUser()
        let params = self.uploadDocuments.getDictionary()
       // params[ApiKeys.user_id] = currentUser?.userId ?? ""
        print_debug(params)
        WebServices.uploadProofFromMyProfile(params: params, loader: loader, success: { (results) in
            print_debug(results)
            let user = UserModel(json: results)
            CommonFunction.updateUser(user)
            self.delegate?.update()
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

// MARK:-  Image Picker Delegate
//==================================
extension UploadProofVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            print_debug(imagePicked)
            self.uploadDocuments.proofRawImage.append(imagePicked)
            self.tableView.reloadData()
            uploadImageToS3(image: imagePicked)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func uploadImageToS3(image: UIImage){
       
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            
            if uploaded {
                
                if self.uploadDocuments.proofRawImage.count == 1 {
                    self.uploadDocuments.proofImage.append(imageUrl)
                  
                    self.tableView.reloadData()
                } else {
                    self.uploadDocuments.proofImage.append(imageUrl)
                    self.tableView.reloadData()
                }
                
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
             DispatchQueue.main.async {
                if self.uploadDocuments.proofRawImage.count == 1 {
                    guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? UploadImageFileCell else { return }
                        cell.progressView.setProgress(Float(uploadPercentage), animated: true)
                    
                } else if self.uploadDocuments.proofRawImage.count == 2 {
                    
                    guard let cell = self.tableView.cellForRow(at: IndexPath(row: 1, section: 1)) as? UploadImageFileCell else { return }
                        cell.progressView.setProgress(Float(uploadPercentage), animated: true)
                }
            }

            
        }, size: {
            (_) in
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == "The Internet connection appears to be offline.") {
                CommonFunction.showToast(msg: "Please check your internet connection.")
            }
            // self.activityIndicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
        })
    }
}


////MARK: Extension AppInventivCropperDelegate
////MARK:
//extension UploadProofVC: CropperDelegate {
//
//
//    func imageCropperDidCancelCrop() {
//
//        print_debug("Crop cancelled")
//
//    }
//    func imageCropper(didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
//
//
//        print_debug(cropRect)
//
//        if let nav = self.navigationController{
//            nav.navigationBar.isHidden = true
//        }
//        self.uploadDocuments.proofRawImage.append(croppedImage)
//        self.tableView.reloadData()
//        uploadImageToS3(image: croppedImage)
//
//    }
//}
//
//
