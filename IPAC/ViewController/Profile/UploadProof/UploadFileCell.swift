//
//  UploadFileCell.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class UploadFileCell: UITableViewCell {

    @IBOutlet weak var youNeedToUploadLabel: UILabel!
    @IBOutlet weak var uploadFileLabel: UILabel!
    @IBOutlet weak var dashedBorderView: UIView!
    
    @IBOutlet weak var uploadBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setText()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        dashedBorderView.addDashedBorder(color: .lightGray, lineWidth: 0.8)

    }
    
    func setText() {
        youNeedToUploadLabel.text = StringConstant.You_Need_To_Upload.localized
        uploadFileLabel.text = StringConstant.UPLOAD_FILE.localized
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
