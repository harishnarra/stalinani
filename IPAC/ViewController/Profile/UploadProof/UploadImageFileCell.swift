//
//  UploadImageFileCell.swift
//  IPAC
//
//  Created by macOS on 05/09/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class UploadImageFileCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var documentImage: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var stopUploadingDocsBtn: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var deleteUploadDocumentBtn: UIButton!
    @IBOutlet weak var dataCountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        documentImage.clipsToBounds = true
        deleteUploadDocumentBtn.isHidden = true
       // stopUploadingDocsBtn.isHidden = true
     //   progressView.isHidden = false
        self.progressView.progress = 0.0
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.progressView.rounded(cornerRadius: progressView.frame.height/2, clip: true)
        bgView.setBorder(color: AppColors.appThemeColor, width: 1, cornerRadius: 5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
