////
//  EditProfileVC.swift
//  IPAC
//
//  Created by Appinventiv on 13/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
class EditProfileVC: UIViewController {
    
    enum ImagePickerOpenFor{
        case UserPhoto, ID
    }
    var imagePickerOpenFor = ImagePickerOpenFor.UserPhoto
    
    // MARK:- VARIABLES
    //====================
    @IBOutlet weak var reimbursementView: UIView!
    var currentUser : UserModel?
    var headingArray = ["",LocalizationSystem.sharedInstance.localizedStringForKey(key: "Social_Profiles", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "ID_Proof", comment: "")]
    var socialProfilesLabelsArray = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "Facebook", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Twitter", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mobile_Number", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "WhatsApp_Number", comment: "")]

   
    var firstCardLabelsArray = ["",LocalizationSystem.sharedInstance.localizedStringForKey(key: "USERID", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Email_Id", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "State", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "District", comment: "")]
    var idProofType = ""
    var selectedIDType : String?
    var proofType :String?
    var uplodedFirstImage = false
    var isSecondCellDeleted = false
    var cardsCount = 0
    var isLocationField = true
    var pickedImage : UIImage?
    
    var IDImage : UIImage?
    var IDImage2 : UIImage?
    var IDimages1 : String?
    var IDimages2 : String?
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var editProfileTableView: UITableView!
    @IBOutlet weak var btnEditPaytm: UIButton!
    @IBOutlet weak var txtPaytm: SkyFloatingLabelTextField!
    
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Profile_Edit", comment: "")
        
        reimbursementView.isHidden = true
        txtPaytm.isUserInteractionEnabled = false
        if self.currentUser?.idProofs == nil {
            let idProof = IdProof(json: [])
            self.currentUser?.idProofs = [idProof]
            CommonFunction.updateUser(currentUser!)
        }
        self.initialSetup()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    

    
    // MARK:- IBACTIONS
    //====================
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func saveInfoTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if self.validateData() {
            self.hitEditProfileAPI()
        }
        
    }
    
    @IBAction func btnActionEditPaytm(_ sender: UIButton) {
        if !sender.isSelected{
            txtPaytm.isUserInteractionEnabled = true
            if txtPaytm.text == "-" {
                txtPaytm.text = ""
            }
          //  sender.isSelected.toggle()
        }else{
            updatePaytmNo()
        }
    }
    
}
//MARK::- API
extension EditProfileVC{
    func updatePaytmNo(){
        let validation  = ValidationController.validatePhone(phone: /txtPaytm.text)
        if !validation.0{
            CommonFunction.showToast(msg: validation.1)
            return
        }
        let param = ["paytm_number" : /txtPaytm.text]
        WebServices.updateProfile(dict: param, success: { [weak self] (json) in
            let user = UserModel(json: json)
            CommonFunction.updateUser(user)
            self?.txtPaytm.isUserInteractionEnabled = false
            //self?.btnEditPaytm.isSelected.toggle()
            self?.currentUser?.paytmNumber = /self?.txtPaytm.text
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension EditProfileVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterViewid") as! FooterView
        footer.backgroundColor = .clear
        return footer
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewid") as! HeaderView
        header.headingLabel.text = headingArray[section]
        header.imgAlert.isHidden = true
        header.lblAlert.isHidden = true
        return header
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0 :
            return 5
        case 1:
            return 4
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch  indexPath.section {
            
        case 0:
            switch indexPath.row{
                
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "profileImageCellid") as? ProfileImageCell else {
                    fatalError()
                }
                cell.nameTextField.delegate = self
                cell.editProfileImageButton.addTarget(self, action: #selector(editImage), for: .touchUpInside)
                cell.addName(user : currentUser ?? nil)
                if let image = pickedImage {
                    cell.profileImageView.image = image
                }
                return cell
                
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCellid") as? LocationCell else {
                    fatalError()
                }
                cell.editTextField.placeholder = "State"
                cell.editTextField.delegate = self
                cell.editTextField.isUserInteractionEnabled = false
                cell.editTextField.text = currentUser?.stateName
                cell.dropDownBtn.isHidden = false
                
                //cell.setUserInfo(user : currentUser ?? nil,isLocation: true,idProofType : self.idProofType)
                return cell
                break
            case 4:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCellid") as? LocationCell else {
                    fatalError()
                }
                cell.dropDownBtn.isHidden = false
                cell.editTextField.placeholder = "District"
                cell.editTextField.delegate = self
                cell.editTextField.isUserInteractionEnabled = false
                cell.editTextField?.text = currentUser?.districtName
                return cell
                break
                
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "nameTextFieldCellid") as? NameTextFieldCell else {  fatalError()    }
                if indexPath.row == 1{
                    cell.editInfoTextField.isEnabled = false
                    cell.editInfoTextField.textColor = UIColor.init(red: 134/255, green: 145/255, blue: 167/255, alpha: 1.0)
                    cell.editInfoTextField.lineColor = .clear
                    cell.editInfoTextField.title = "USER ID"
                    cell.editInfoTextField.titleColor = UIColor.init(red: 136/255, green: 147/255, blue: 168/255, alpha: 1.0)
                    cell.editInfoTextField.text = currentUser?.registerationNo
                }
                
                cell.editInfoTextField.placeholder = firstCardLabelsArray[indexPath.row]
                cell.setUserInfo(user: currentUser ?? nil, index: indexPath)
                cell.editInfoTextField.delegate = self
                return cell
            }
            break
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "nameTextFieldCellid") as? NameTextFieldCell else {  fatalError() }
            cell.editInfoTextField.placeholder = socialProfilesLabelsArray[indexPath.row]
            if indexPath.row == 3 || indexPath.row == 4 {
                cell.editInfoTextField.keyboardType = .phonePad
            }
            cell.setUserInfo(user: currentUser ?? nil, index: indexPath)
            cell.editInfoTextField.delegate = self
            
            return cell
            break
            
        default:
            if indexPath.row == 0{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCellid") as? LocationCell else {
                    fatalError()
                }
                cell.dropDownBtn.isHidden = false
                if let id = Int(currentUser?.idProofs?[0].proofType ?? "0") { //proofType
                    cell.editTextField.placeholder = "Proof Type"

                    if id == 0 {
                        cell.setUserInfo(user : currentUser ?? nil,isLocation: false,idProofType : iDDataSource[id])
                    } else {
                        cell.setUserInfo(user : currentUser ?? nil,isLocation: false,idProofType : iDDataSource[id - 1])
                    }
                }
                cell.editTextField.delegate = self
                
                return cell
            } else if indexPath.row == 1 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCellid") as? LocationCell else {
                    fatalError()
                }
                
                cell.editTextField.placeholder = StringConstant.UI_Number.localized
                
                if let uiNumber = currentUser?.idProofs?[0].idNumber  {//, !uiNumber.isEmpty {
                    cell.editTextField.text = uiNumber
                }
                cell.editTextField.keyboardType = .emailAddress
                cell.editTextField.delegate = self
                cell.dropDownBtn.isHidden = true
                
                return cell
            }
            else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadCellid") as? UploadCell else {
                    fatalError()
                }
                cell.imagePickerButton.addTarget(self, action: #selector(openPicker(_:)), for: UIControlEvents.touchUpInside)
                //cell.addMoreButton.addTarget(self, action: #selector(openPicker(_:)), for: UIControlEvents.touchUpInside)
                cell.delegate = self
                //    if let idNumber = self.currentUser?.idProofs?[0].idNumber, !idNumber.isEmpty {
                
                if self.cardsCount == 1 {
                    cell.dashedBorderView.isHidden = false
                    cell.heightUploadView.constant = 88

                    cell.imagePickerButton.isHidden = false
                    cell.upeerview.isHidden = false
                    cell.stackViewHeight.constant = 91
                    cell.lowerView.isHidden = true
                    if let idProof = self.currentUser?.idProofs?[0], let proofType = idProof.proofType {
                        if let id = Int(proofType ) {
                            cell.firstIDnameLabel.text = iDDataSource[(id - 1 )]
                        } else {
                            cell.firstIDnameLabel.text = iDDataSource[0]
                        }
                    }
                    else{
                        cell.firstIDnameLabel.text = iDDataSource[0]
                        cell.secondIDname.text = iDDataSource[0]
                    }
                    cell.firstImage.kf.setImage(with: URL(string: IDimages1 ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))
                    cell.firstProgressView.isHidden = true//

                } else if self.cardsCount == 2 {
                    cell.dashedBorderView.isHidden = true
                    cell.heightUploadView.constant = 0
                    cell.imagePickerButton.isHidden = true
                    cell.lowerView.isHidden = false
                    cell.upeerview.isHidden = false
                    cell.stackViewHeight.constant = 182

                    if let idProof = self.currentUser?.idProofs?[0], let proofType = idProof.proofType{
                        if let id = Int(proofType )  {
                            cell.firstIDnameLabel.text = iDDataSource[(id - 1 )]
                            cell.secondIDname.text = iDDataSource[(id - 1 )]
                        } else {
                            cell.firstIDnameLabel.text = iDDataSource[0]
                            cell.secondIDname.text = iDDataSource[0]
                        }
                    }else{
                        cell.firstIDnameLabel.text = iDDataSource[0]
                        cell.secondIDname.text = iDDataSource[0]
                    }
                    cell.firstImage.kf.setImage(with: URL(string: IDimages1 ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))
                    cell.secondImage.kf.setImage(with: URL(string: IDimages2 ?? ""), placeholder: #imageLiteral(resourceName: "icHomeImageerror"))
                    cell.firstProgressView.isHidden = true
                    cell.secondProgress.isHidden = true
                    

                    } else {
                    cell.dashedBorderView.isHidden = false
                    cell.heightUploadView.constant = 88

                    cell.imagePickerButton.isHidden = false
                    cell.stackViewHeight.constant = 0
                    cell.lowerView.isHidden = true
                    cell.upeerview.isHidden = true
                    }
                return cell
                
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        if indexPath == [1,0]{
            self.fbLogin()
            print("fb")
        }
        else if indexPath == [1,1]{
            self.twitterLogin()
            print("twitter")
        }else{
            guard let cell = tableView.cellForRow(at: indexPath) as? LocationCell else {return}
            if indexPath == [0,3]{
                self.setPickerInputForTextField((cell.editTextField)!,true)
            }
            else if indexPath == [2,0]{
                
                self.setPickerInputForTextField((cell.editTextField),false)
            }
        }
        
        
        
        
    }
    
    @objc func openPicker(_ sender: UIButton){
        self.view.endEditing(true)
        
        //        guard let index = sender.tableViewIndexPath(self.editProfileTableView) else { return }
        //        guard let cell = self.editProfileTableView.cellForRow(at: index) as? UploadCell else { return }
       // self.cardsCount += 1
        imagePickerOpenFor = ImagePickerOpenFor.ID
        self.captureImage(on: self)

    }
    
}



extension EditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagePicked = info[UIImagePickerControllerEditedImage] as? UIImage{
            if  imagePickerOpenFor == ImagePickerOpenFor.UserPhoto{
                self.uploadImageToS3(image: imagePicked)
                self.pickedImage = imagePicked
                
            }else {
                
                let index =  IndexPath(row: 2, section: 2)
                let cell = self.editProfileTableView.cellForRow(at: index) as! UploadCell
                if self.cardsCount == 0{
                    cell.upeerview.isHidden = false
                    cell.lowerView.isHidden = true
                    cell.stackViewHeight.constant = 91
                    cell.firstImage.image = imagePicked
                    self.cardsCount = self.cardsCount + 1
                    self.editProfileTableView.reloadData()

                    imagePicked.uploadImageToS3(success: { (status, URL) in
                        if status{
                            cell.firstProgressView.isHidden = true
                            if (self.currentUser?.idProofs?.count ?? 0) == 0 {
                            let idProof = IdProof(json: [])
                            self.currentUser?.idProofs?.append(idProof)
                            self.currentUser?.idProofs?.first?.image = URL
                            }else{
                                self.currentUser?.idProofs?.first?.image = URL
                            }
                            self.IDimages1 = URL
                            self.editProfileTableView.reloadData()

                        }
                        
                    }, progress: { (pro) in
                        
                        DispatchQueue.main.async {
                            
                            cell.firstProgressView.setProgress(Float(pro), animated: true)
                            cell.firstProgressView.isHidden = false
                            //cell.contentView.layoutIfNeeded()

                        }
                        
                    }, size: {
                        (_) in
                    }, failure: { (error) in
                        print(error.localizedDescription)
                        self.editProfileTableView.reloadData()
                    })
                    
                }else if self.cardsCount == 1 {
                    cell.upeerview.isHidden = false
                    cell.lowerView.isHidden = false
                    cell.stackViewHeight.constant = 182
                    cell.secondImage.image = imagePicked
                    cell.dashedBorderView.isHidden = true
                    cell.heightUploadView.constant = 0

                    cell.imagePickerButton.isHidden = true
                    self.cardsCount = self.cardsCount + 1
                    self.editProfileTableView.reloadData()
                    
                    imagePicked.uploadImageToS3(success: { (status, URL) in
                        if status{
                            cell.secondProgress.isHidden = true
                            if (self.currentUser?.idProofs?.count ?? 0) == 0 {
                                let idProof = IdProof(json: [])
                                self.currentUser?.idProofs?.append(idProof)
                                self.currentUser?.idProofs?.append(idProof)
                                self.currentUser?.idProofs?.first?.image = self.IDimages1
                                self.currentUser?.idProofs?.last?.image = URL

                            }else if (self.currentUser?.idProofs?.count ?? 0) == 1{
                                let idProof = IdProof(json: [])
                                self.currentUser?.idProofs?.append(idProof)
                                self.currentUser?.idProofs?.last?.image = URL
                            }else{
                                self.currentUser?.idProofs?.last?.image = URL
                            }
                            self.IDimages2 = URL
                            self.editProfileTableView.reloadData()
                        }
                        
                    }, progress: { (pro) in
                        
                        DispatchQueue.main.async {
                            cell.secondProgress.setProgress(Float(pro), animated: true)
                            cell.secondProgress.isHidden = false
                            cell.firstProgressView.isHidden = true
                            //cell.contentView.layoutIfNeeded()

                        }
                        
                    }, size: {
                        (_) in
                    }, failure: { (error) in
                        print(error.localizedDescription)
                        self.editProfileTableView.reloadData()
                    })
                    
                }else{
                    
                }

                }
            }
            picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {

        picker.dismiss(animated: true, completion: nil)
    }
}


extension EditProfileVC : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let cell: UITableViewCell = editProfileTableView.cell(forItem: textField) else {return}
        
        let textFieldIndexPath = self.editProfileTableView.indexPath(for: cell)
        
        if let index = textFieldIndexPath{
            
            self.setNewData(index: index)
            //  CommonFunction.updateUser(currentUser!)
            
        }
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let cell: UITableViewCell = editProfileTableView.cell(forItem: textField)!
        //   guard let cell: UITableViewCell = editProfileTableView.cellForRow(at: textField) as
        let textFieldIndexPath = self.editProfileTableView.indexPath(for: cell)
        let index = self.editProfileTableView.indexPath(for: cell)
        print_debug(textFieldIndexPath)
        if textFieldIndexPath == [0,3]{
            self.setPickerInputForTextField(textField as! SkyFloatingLabelTextField, true)
        }
        else if textFieldIndexPath == [2,0]{
            self.setPickerInputForTextField(textField as! SkyFloatingLabelTextField, false)
        } else if textFieldIndexPath == [1, 2]    {  //IndexPath(row: 2, section: 1)
            return false
            
        } else if  index == IndexPath(row: 3, section: 1) || index == IndexPath(row: 4, section: 1) {
            return true
        }
        else{
            print_debug(" ")
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let index = textField.tableViewIndexPath(self.editProfileTableView)
        print_debug(index)
        if index == IndexPath(row: 2, section: 0) {
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else if (textField.text ?? "").count - range.length < 50 {
                
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERSS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return true
                
            } else {
                return false
            }
        } else if index == IndexPath(row: 0, section: 1) || index == IndexPath(row: 1, section: 1) {
            
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else  {
                
                return (textField.text ?? "").count - range.length < 30
            }
        } else if index == IndexPath(row: 3, section: 1) || index == IndexPath(row: 4, section: 1) {
            
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else  {
                
                return (textField.text ?? "").count - range.length < 10
            }
        } else if index == IndexPath(row: 1, section: 2){
            print(self.proofType)
            print(self.idProofType)
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else {
                //return (textField.text ?? "").count - range.length < 20
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                return (string == filtered) && (textField.text ?? "").count - range.length < ((currentUser?.idProofs?[0].proofType == "2") ? 10 : 20)
                //
            }
        }
        
        else {
            return true
        }
        
    }
}

// MARK:- FUNCTIONS
//====================
extension EditProfileVC {
    
    /// Invite Page Content Initial Setup
    private func initialSetup() {
        
        navigationTitleLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Profile_Edit", comment: "")
        super.addImageInBackground()
        editProfileTableView.delegate = self
        editProfileTableView.dataSource = self
        txtPaytm.text = currentUser?.paytmNumber == "" ? "-" : currentUser?.paytmNumber
        editProfileTableView.backgroundColor = .clear
        
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        editProfileTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderViewid")
        
        let footerNib = UINib.init(nibName: "FooterView", bundle: Bundle.main)
        editProfileTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "FooterViewid")
        
        editProfileTableView.register(UINib.init(nibName: "ProfileImageCell", bundle: nil), forCellReuseIdentifier: "profileImageCellid")
        editProfileTableView.register(UINib.init(nibName: "NameTextFieldCell", bundle: nil), forCellReuseIdentifier: "nameTextFieldCellid")
        editProfileTableView.register(UINib.init(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCellid")
        editProfileTableView.register(UINib.init(nibName: "UploadCell", bundle: nil), forCellReuseIdentifier: "UploadCellid")
        
    }
    
    func validateData() -> Bool  {
        guard let user = self.currentUser else { return false}
        let name = ValidationController.validateName(name: user.fullName ?? "")
        if name.0 == false {
            CommonFunction.showToast(msg: name.1)
            return false
        }
        let email = ValidationController.validateEmail(email: user.emailId ?? "")
        if email.0 == false {
            CommonFunction.showToast(msg: email.1)
            return false
        }

                let whatsNo = ValidationController.validatePhone(phone: user.whatsupNumber ?? "")
                if whatsNo.0 == false {
                    CommonFunction.showToast(msg: whatsNo.1)
                    return false
                }
        
        switch /user.idProofs?.first?.proofType{
        case "1": //voter
           
            if (user.idProofs?.first?.idNumber?.count ?? 0) < 8 || (user.idProofs?.first?.idNumber?.count ?? 0) > 15 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
            
        case "2": //pan
            if (user.idProofs?.first?.idNumber?.count ?? 0) != 10 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
        case "3": //license
            if (user.idProofs?.first?.idNumber?.count ?? 0) < 10 || (user.idProofs?.first?.idNumber?.count ?? 0) > 18 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
        default:
            if (user.idProofs?.first?.idNumber?.count ?? 0) < 8 || (user.idProofs?.first?.idNumber?.count ?? 0) > 15 {
                CommonFunction.showToast(msg: StringConstant.Please_Enter_Valid_Id_No.localized)
                return false
            }
        }
  
        if /user.idProofs?.first?.image == ""  {
            CommonFunction.showToast(msg: AlertMessage.addAtleastOneImage.localized)
            return false
        }


        
        return true
    }
    
    private func uploadImageToS3(image: UIImage){
        
         CommonFunction.showLoader()
        
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            
            if uploaded {
                 CommonFunction.hideLoader()
                self.currentUser?.userImage = imageUrl
                self.editProfileTableView.reloadData()
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, size: {
            (_) in
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == "The Internet connection appears to be offline.") {
                CommonFunction.showToast(msg: "Please check your internet connection.")
            }
            CommonFunction.hideLoader()
        })
    }
    
    @objc func editImage()
    {
        self.captureImage(on: self)
    }
    
    
    private func hitEditProfileAPI() {
        //let user = CommonFunction.getUser()
        if self.IDimages1 == nil && self.IDimages2 == nil{
            currentUser?.isProofUploaded = 0
        }else{
            currentUser?.isProofUploaded = 1
        }
        WebServices.editProfileAPI(user: self.currentUser! , success: { (json) in
            let edittedUser = UserModel(json: json)
            print_debug(edittedUser)
            CommonFunction.updateUser(edittedUser)
            self.navigationController?.popViewController(animated: true)
            //self.editProfileTableView.reloadData()
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    private func setNewData(index : IndexPath)
    {
        switch index.section{
        case 0:
            switch index.row{
            case 0:
                guard let cell = editProfileTableView.cellForRow(at: index) as? ProfileImageCell else {return}
                currentUser?.fullName =  cell.nameTextField.text!
            case 2:
                guard let emailCell = editProfileTableView.cellForRow(at: index) as? NameTextFieldCell else {fatalError()}
                // emailEntered = emailCell.editInfoTextField.text!
                currentUser?.emailId = emailCell.editInfoTextField.text!
                //                case 3:
                //                    guard let locationCell = editProfileTableView.cellForRow(at: index) as? LocationCell else {fatalError()}
            //                    currentUser?.state = locationCell.editTextField.text!
            case 4:
                guard let districtCell = editProfileTableView.cellForRow(at: index) as? NameTextFieldCell else {fatalError()}
                currentUser?.district = districtCell.editInfoTextField.text!
            default:
                print_debug("default")
            }
        case 1:
            switch index.row{
            case 0:
                guard let fbCell = editProfileTableView.cellForRow(at: index) as? NameTextFieldCell else {fatalError()}
                self.fbLogin()
            //currentUser?.facebookId = fbCell.editInfoTextField.text!
            case 1:
                guard let twitterCell = editProfileTableView.cellForRow(at: index) as? NameTextFieldCell else {fatalError()}
                self.twitterLogin()
                //currentUser?.twitterId = twitterCell.editInfoTextField.text!
                
            case 3:
                guard let whatsappNumCell = editProfileTableView.cellForRow(at: index) as? NameTextFieldCell else {fatalError()}
                currentUser?.whatsupNumber = whatsappNumCell.editInfoTextField.text!
                
            case 4:
                guard let paytmNumCell = editProfileTableView.cellForRow(at: index) as? NameTextFieldCell else {fatalError()}
            // currentUser?.paytmNumber = paytmNumCell.editInfoTextField.text!
            default:
                print_debug("default")
            }
            
        case 2:
            switch index.row {
            case 0:
                
                currentUser?.idProofs?[0].proofType = self.selectedIDType
            case 1:
                //currentUser?.idProofs?.isProofUploaded
                
                guard let selectIdCell = editProfileTableView.cellForRow(at: index) as? LocationCell else {
                    fatalError()
                }
                if let user = self.currentUser , let proofs = user.idProofs, let firstIndex = proofs.first {
                    firstIndex.idNumber = selectIdCell.editTextField.text
                    print_debug(firstIndex.idNumber)
                }
                //   currentUser?.idProofs?[0].idNumber = selectIdCell.editTextField.text
                print_debug(selectIdCell.editTextField.text)
                print_debug(currentUser?.idProofs?[0].idNumber)
            default:
                break
            }
            
        default:
            print_debug("default")
            
        }
    }
    
    
    
    func setPickerInputForTextField(_ field: SkyFloatingLabelTextField,_ isLocation : Bool) {
        if isLocation{
            isLocationField = true
        }
        else{
            isLocationField = false
        }
        let picker = UIPickerView()
        field.inputView = picker
        picker.delegate = self
        picker.dataSource = self
        print("")
    }
    
}


extension EditProfileVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isLocationField{
            return statesArray.count
        }
        else{
            return iDDataSource.count
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isLocationField{
            return statesArray[row]
        }
        else{
            return iDDataSource[row]
        }
        
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isLocationField{
            //            guard let cell = editProfileTableView.cellForRow(at: [0,3]) as? LocationCell else {return}
            //            cell.editTextField.text = statesArray[row]
            
            currentUser?.state = statesArray[row]
            self.editProfileTableView.reloadData()
            
        }
        else{
            let proofTypee = ProofType(rawValue: row+1) ?? .voterCard
            self.selectedIDType = "\(proofTypee.rawValue)"
            self.proofType = iDDataSource[row]
            guard let cell = editProfileTableView.cellForRow(at: [2,0]) as? LocationCell else {return}
            cell.editTextField.text = iDDataSource[row]
            currentUser?.idProofs?[0].proofType = "\(proofTypee.rawValue)"
            currentUser?.idProofs?.first?.idNumber = ""
            self.editProfileTableView.reloadData()
        }
    }
}

extension EditProfileVC : UpdatecellProtocal{
    
    
    func deletedImage(image: Int) {
        if image == 1{
            if (currentUser?.idProofs?.count ?? 0) == 0{
                return
            }
            currentUser?.idProofs?[0].image = nil
            self.IDimages1 = nil
            self.cardsCount = self.cardsCount - 1
            if self.cardsCount == 1{
                self.IDimages1 = IDimages2
                currentUser?.idProofs?[0].image = IDimages2
                self.IDImage2 = nil
                currentUser?.idProofs?.last?.image = nil
            }
        } else {
            if (currentUser?.idProofs?.count ?? 0) == 1{
                return
            }
            IDImage2 = nil
            currentUser?.idProofs?[1].image = nil
            cardsCount = cardsCount - 1
        }
        self.editProfileTableView.reloadData()
    }
    
    
}

//MARK::- FUNCTION FB TWITTER LOGIN
extension EditProfileVC{
    private func twitterLogin() {
        TwitterController.shared.loginWithTwitter(completion: { (success, twitterData) in
            if success {
                guard let twitterData = twitterData else { return }
       
                let param = [ApiKeys.twitterId: twitterData.userID, ApiKeys.twitterUsername: twitterData.name]
                self.updateImage(params: param, loader: false, completion: { (_) in
                    self.currentUser?.twitterUsername = twitterData.screen_name
                    self.currentUser?.twitterId = twitterData.userID
                    self.editProfileTableView.reloadData()

                })
            } else {
                CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
            }
        })
    }
    
    
    private func fbLogin() {
        
        FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result, tocken) in
            print_debug(result)
       
            let param = [ApiKeys.facebookId: result.id, ApiKeys.fbUsername: result.name]
            self.updateImage(params: param, loader: false, completion: {(_) in
                self.currentUser?.facebookId = result.id
                self.currentUser?.fbUsername = result.name
                self.editProfileTableView.reloadData()
            })
        }) { (error) in
            if let err = error {
                CommonFunction.showToast(msg: err.localizedDescription)
            } else {
                CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
            }
        }
    }
    
    func updateImage(params: [String: String], loader: Bool , completion : @escaping SuccessResponse) {
        WebServices.updateProfile(param: params, loader: true, success: { (json) in
            print_debug(json)
            completion(json)
            
        }) { (error) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
}

