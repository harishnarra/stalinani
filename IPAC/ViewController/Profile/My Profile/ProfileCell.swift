//
//  ProfileCell.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var editProfileButton: GradientButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        btnChangePassword.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Change_Password", comment: ""), for: .normal)
        
        btnChangePassword.setTitleColor(.red, for: .normal)
     //   editProfileButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Edit_Profile", comment: ""), for: .normal)
        
        editProfileButton.roundCorners([.bottomRight,.bottomLeft], radius: 5.0)
        backView.rounded(cornerRadius: 5.0, clip: true)
        profileImage.rounded(cornerRadius: 5.0, clip: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setUserId(userId : String) -> NSAttributedString
    {
        let attributedString = NSMutableAttributedString(string: userId, attributes: [
            .font: UIFont(name: "ProximaNova-Semibold", size: 12.5)!,
            .foregroundColor: UIColor.white
            ])
        //attributedString.addAttribute(.font, value: UIFont(name: "ProximaNova-Regular", size: 12.5)!, range: NSRange(location: 0, length: 8))
        
        return attributedString
    }
    
    
    func setLabelText(name :String,userID : String,emailId : String,district : String,state : String,mobile : String, userImage: String)
    {
        editProfileButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Edit_Profile", comment: ""), for: .normal)
        self.userIdLabel.attributedText =  self.setUserId(userId: "\(userID)")
        self.userIdLabel.text = userID
        self.nameLabel.text = name
        self.emailLabel.text = emailId
        let distric = district.trimmingCharacters(in: .whitespaces)
        self.locationLabel.text = distric+", "+state
        self.mobileLabel.text = mobile
        self.profileImage.kf.setImage(with: URL(string: userImage) , placeholder: #imageLiteral(resourceName: "icPlaceholder"))
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //editProfileButton.layer.sublayers![0].frame = editProfileButton.bounds
    }
    
    
}
