//
//  SocialProfilesCell.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SocialProfilesCell: UITableViewCell {
    
    @IBOutlet weak var socialIconImageView: UIImageView!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var socialProfileLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLabelText(user : UserModel? ,index : Int)
    {
       
        switch index{
        case 0:
            setTextOnLabel(socialProfileLabel, text: user?.fbUsername)
        case 1:
            setTextOnLabel(socialProfileLabel, text: user?.twitterUsername)
        case 2:
            setTextOnLabel(socialProfileLabel, text: user?.whatsupNumber)
        default:
            setTextOnLabel(socialProfileLabel, text: user?.paytmNumber)

        }
    }
    
    func setTextOnLabel(_ label: UILabel, text: String?) {
        guard let string = text, !string.isEmpty else {
            label.text = "-"
            return
        }
        label.text = text
    }
    
}
