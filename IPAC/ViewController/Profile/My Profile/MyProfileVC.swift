//
//  MyProfileVC.swift
//  IPAC
//
//  Created by Appinventiv on 31/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class MyProfileVC: BaseVC {
    
    // MARK:- VARIABLES
    //====================
    
    var user : UserModel?
    var  index : Int = 0
    var UIDNumber : String = ""
    var socialIcons = [#imageLiteral(resourceName: "vectorSmartObject"),#imageLiteral(resourceName: "forma1Copy5"),#imageLiteral(resourceName: "icProfileWhatsapp")]
    var isSidePanel = true
    
    // MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var reimbursementView: UIView!
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var txtPaytm: SkyFloatingLabelTextField!
    @IBOutlet weak var btnEditPaytm: UIButton!
    @IBOutlet weak var imgPaytmAlert: UIImageView!
    @IBOutlet weak var lblPaytmAlert: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSideMenu: MenuButton!
    
    // MARK:- LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reimbursementView.isHidden = true
        navigationLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Profile", comment: "")
        user = CommonFunction.getUser()
        self.populateUserData()
        txtPaytm.isUserInteractionEnabled = false
        btnSideMenu.isHidden = !isSidePanel
        btnBack.isHidden = isSidePanel

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProfileData()
        //self.updateView()
    }
    func populateUserData() {
        //guard let user = self.user else { return }
        self.initialSetup()
        //self.getProfileData()
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        profileTableView.rounded(cornerRadius: 5.0, clip: true)
    }
    // MARK:- IBACTIONS
    //====================
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionEditPaytm(_ sender: UIButton) {
        if !sender.isSelected{
            txtPaytm.isUserInteractionEnabled = true
          //  sender.isSelected.toggle()
        }else{
            updatePaytmNo()
        }
    }
    @IBAction func btnActionNotification(_ sender: UIButton) {
        let notificationSceen = NotificationVC.instantiate(fromAppStoryboard: .Dashboard)
        self.navigationController?.pushViewController(notificationSceen, animated: true)
    }
    
}

// MARK:- TEXT FIELD DELEGATE
//==============================================

extension MyProfileVC {
    
    func getProfileData() {
        WebServices.getProfile(success: { (json) in
            print_debug(json)
            let user = UserModel(json: json)
            CommonFunction.updateUser(user)
            self.user = user
            self.updateView()
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    
    
    func updateImage(params: [String: String], loader: Bool , completion : @escaping SuccessResponse) {
        WebServices.updateProfile(param: params, loader: loader, success: { (json) in
            print_debug(json)
            completion(json)
            
        }) { (error) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
    func updateView() {
        guard let user = CommonFunction.getUser() else { return }
        self.user = user
        txtPaytm.text = /user.paytmNumber
        lblPaytmAlert.isHidden = /user.paytmNumber != ""
        imgPaytmAlert.isHidden = /user.paytmNumber != ""
        btnEditPaytm.isHidden = /user.paytmNumber != ""
        profileTableView.reloadData()
    }
    
}

// MARK:- TABLE VIEW DELEGATE AND DATASOURCE
//==============================================
extension MyProfileVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let view = UIView()
            view.backgroundColor = .clear
            return view
        default:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewid") as! HeaderView
           // header.backgroundColor = .black
            if section == 1{
                header.headingLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Social_Profiles", comment: "")
                header.headingLabel.textColor = .white
                header.lblAlert.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Add_Social_Account", comment: "")
                header.lblAlert.isHidden = (/user?.fbUsername != "" && /user?.twitterUsername != "")
                header.imgAlert.isHidden = (/user?.fbUsername != "" && /user?.twitterUsername != "")
            }
            else{
                header.headingLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "ID_Proof", comment: "")
                header.headingLabel.textColor = .white
                header.lblAlert.text = "No_id_proof_added"
                header.lblAlert.isHidden = user?.idProofs?[0].proofType != nil
                header.imgAlert.isHidden = user?.idProofs?[0].proofType != nil
                
            }
            return header
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //  guard let count = user?.idProofs?.count, count > 0 else { return 2 }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 1:
            return 3
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0{
            let view = UIView()
            //view.backgroundColor = .black
            return view
        }
        else{
            let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterViewid") as! FooterView
           // footer.backgroundColor = .black
            return footer
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCellid", for: indexPath) as? ProfileCell else{
                fatalError("ButtonWithImageCell not found")
            }
            cell.editProfileButton.addTarget(self, action: #selector(openEditProfile), for: .touchUpInside)
            cell.btnChangePassword.addTarget(self, action: #selector(openChangePassword), for: .touchUpInside)
            
            cell.setLabelText(name: user?.fullName ?? "", userID: user?.registerationNo ?? "", emailId: user?.emailId ?? "" , district :user?.districtName ?? "",state : user?.stateName ?? "", mobile: user?.phoneNumber ?? "", userImage: user?.userImage ?? "")
            cell.rounded(cornerRadius: 5.0, clip: true)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SocialProfilesCellid", for: indexPath) as? SocialProfilesCell else{
                fatalError("ButtonWithImageCell not found")
            }
            cell.setLabelText(user: user , index: indexPath.row)
            cell.socialIconImageView.image = socialIcons[indexPath.row]
            if indexPath.row == 2 {
                cell.lineView.isHidden = true
            } else {
                cell.lineView.isHidden = false
            }
            
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "IDProofCellid", for: indexPath) as? IDProofCell else{
                fatalError("ButtonWithImageCell not found")
            }
            
            cell.setLabel(index: Int((user?.idProofs?[0].proofType ?? "0")), IdNum: user?.idProofs?[0].idNumber)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == [1,0]  {
            fbLogin()
        }       
        else if indexPath == [1,1] {
            self.twitterLogin()
        }
            
        else if indexPath == [2,0] {
            
            if /user?.idProofs?[0].proofType == ""{
                let uploadProofSceen = UploadProofVC.instantiate(fromAppStoryboard: .Profile)
                uploadProofSceen.delegate = self
                self.view.addSubview(uploadProofSceen.view)
                addChildViewController(uploadProofSceen)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    
}

// MARK:- FUNCTIONS
//====================
extension MyProfileVC {
    
    private func initialSetup() {
        super.addImageInBackground()
        self.navigationLabel.text = "Profile"
        self.registerXibs()
        profileTableView.dataSource = self
        profileTableView.delegate = self
        navigationController?.navigationBar.isHidden = true
    }
    
    func registerXibs() {
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        profileTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderViewid")
        let footerNib = UINib.init(nibName: "FooterView", bundle: Bundle.main)
        profileTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "FooterViewid")
        profileTableView.register(UINib.init(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCellid")
        profileTableView.register(UINib.init(nibName: "SocialProfilesCell", bundle: nil), forCellReuseIdentifier: "SocialProfilesCellid")
        profileTableView.register(UINib.init(nibName: "IDProofCell", bundle: nil), forCellReuseIdentifier: "IDProofCellid")
    }
    
    func setProofType(index : Int) -> String{
        switch index{
        case 1:
            return "Voter Card"
        case 2:
            return "Adhaar Card"
        case 3:
            return "Pan Card"
        case 4:
            return "Driving licence"
        default:
            return  ""
        }
    }
    
    
    
    @objc private func openEditProfile(){       
        let editProfileScene = EditProfileVC.instantiate(fromAppStoryboard: .Profile)
        editProfileScene.currentUser = self.user ?? nil
        editProfileScene.idProofType = setProofType(index : self.index)
        let count = self.user?.idProofs?.count
        
        if count == 1 {
            editProfileScene.cardsCount = 1
            editProfileScene.IDimages1 = self.user?.idProofs?.first?.image
            //editProfileScene.isSecondCellDeleted = true
        } else if count == 2 {
            editProfileScene.cardsCount = 2
            editProfileScene.IDimages1 = /self.user?.idProofs?.first?.image
            editProfileScene.IDimages2 = /self.user?.idProofs?.last?.image

        } else if count == 0 {
            editProfileScene.cardsCount = 0
            
        }
                else {
                    editProfileScene.cardsCount = 0
                }
        
        self.navigationController?.pushViewController(editProfileScene, animated: true)
    }
    
    @objc func openChangePassword(){
        let vc = ChangePasswordVC.instantiate(fromAppStoryboard: .Profile)
        self.navigationController?.present(vc, animated: false, completion: nil)
    }
    
    private func twitterLogin() {
        if let twitterId = self.user?.twitterId, let twitterUserName = self.user?.twitterUsername, !twitterId.isEmpty, !twitterUserName.isEmpty {
            return
        } else {
            TwitterController.shared.loginWithTwitter(completion: { (success, twitterData) in
                if success {
                    guard let twitterData = twitterData else { return }
                    guard let currentUser = self.user else { return }
                    let param = [ApiKeys.twitterId: twitterData.userID, ApiKeys.twitterUsername: twitterData.name]
                    self.updateImage(params: param, loader: false, completion: {(_) in
                        self.user?.twitterUsername = twitterData.screen_name
                        self.user?.twitterId = twitterData.userID
                        currentUser.twitterId = twitterData.userID
                        currentUser.twitterUsername = twitterData.screen_name
                        CommonFunction.updateUser(currentUser)
                        self.updateView()
                    })
                } else {
                    CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
                }
            })
        }
    }
    
    private func fbLogin() {
        if let fbUserName = self.user?.fbUsername, let fbId = self.user?.facebookId, !fbUserName.isEmpty, !fbId.isEmpty {
            return 
        }
        
        FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result, tocken) in
            print_debug(result)
            guard let currentUser = self.user else { return }
            let param = [ApiKeys.facebookId: result.id, ApiKeys.fbUsername: result.name]
            self.updateImage(params: param, loader: false, completion: { (_) in
                self.user?.facebookId = result.id
                self.user?.fbUsername = result.name
                currentUser.facebookId = result.id
                currentUser.fbUsername = result.name
                CommonFunction.updateUser(currentUser)
                self.updateView()
            })
  
        }) { (error) in
            if let err = error {
                CommonFunction.showToast(msg: err.localizedDescription)
            } else {
                CommonFunction.showToast(msg: StringConstant.CouldNotLoginTryAgain.localized)
            }
        }
    }
}


//MARK::- API
extension MyProfileVC{
    func updatePaytmNo(){
        let validation  = ValidationController.validatePhone(phone: /txtPaytm.text)
        if !validation.0{
            CommonFunction.showToast(msg: validation.1)
            return
        }
        let param = ["paytm_number" : /txtPaytm.text]
        WebServices.updateProfile(dict: param, success: { [weak self] (json) in
            let user = UserModel(json: json)
            CommonFunction.updateUser(user)
            self?.txtPaytm.isUserInteractionEnabled = false
          //  self?.btnEditPaytm.isSelected.toggle()
            self?.user = user
            self?.updateView()
        }) { (error) -> (Void) in
            CommonFunction.showToast(msg: error.localizedDescription)
        }
    }
}

//MARK::- DELEGATE
extension MyProfileVC : Update{
    func update(){
        updateView()
    }
}
