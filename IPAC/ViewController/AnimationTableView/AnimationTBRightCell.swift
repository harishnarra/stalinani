//
//  AnimationTBCell1.swift
//  tableViewDemo
//
//  Created by naresh banavath on 17/07/20.
//  Copyright © 2020 naresh banavath. All rights reserved.
//

import UIKit

class AnimationTBRightCell: UITableViewCell {

    
    
    @IBOutlet weak var rewardImg: UIImageView!
       @IBOutlet weak var showHideView: UIView!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var rightImg: UIImageView!
    @IBOutlet var upperLowerViews: [UIView]!
    @IBOutlet weak var circleCenterXConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
