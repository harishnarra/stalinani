//
//  AcceptTermsVC.swift
//  IPAC
//
//  Created by Appinventiv on 24/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import EZSwiftExtensions

protocol TermsAcceptDelegate : class{
    func accepted(version : String)
}

class AcceptTermsVC: BaseViewController {

    //MARK::- PROERPTIES
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var termsconditions: UILabel!
    @IBOutlet weak var termsandConditiondata: UILabel!
    
     var version : String?
     weak var delegate : TermsAcceptDelegate?
    
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsconditions.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "TERMS_&_CONDITIONS", comment: "")
          termsandConditiondata.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "erms and conditions have been updated. Please accept to the new terms and conditions", comment: "")
          okBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "OK", comment: ""), for: .normal)
        
    }
    
    //MARK::- BUTTON ACTION

    @IBAction func btnActionDone(_ sender: UIButton) {
        self.dismiss(animated: false)
        delegate?.accepted(version: /self.version)
    }
}
