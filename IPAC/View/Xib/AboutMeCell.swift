//
//  AboutMeCell.swift
//  IPAC
//
//  Created by Appinventiv on 13/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class AboutMeCell: UITableViewCell {
    
    var aboutMe = "Social activist with a focus on rural development and child welfare. I would love to contribute towards the growth of my nation in as many ways as possible."

    @IBOutlet weak var aboutMeTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.text = AppStrings.aboutMe.rawValue.localized
        aboutMeTextView.text = aboutMe
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
