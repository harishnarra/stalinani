//
//  ProfileImageCell.swift
//  IPAC
//
//  Created by Appinventiv on 13/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ProfileImageCell: UITableViewCell {
    
    // MARK:- VARIABLES
    //====================
    
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var editProfileImageButton: UIButton!
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization cod
        nameTextField.delegate = self
        CommonFunction.setAttributesto(currentTextField: nameTextField)

  
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        profileImageView.rounded(cornerRadius: 5.0, clip: true)
        editProfileImageButton.rounded(cornerRadius: 11.0, clip: true)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func addName(user : UserModel?){
    
        guard let user = user else { return }
        self.nameTextField.text = user.fullName
        print_debug(user.userImage)
        self.profileImageView.kf.setImage(with: URL(string: user.userImage ?? ""), placeholder: #imageLiteral(resourceName: "icPlaceholder"))
        
    }
    
    @IBAction func editNameTapped(_ sender: UIButton) {
        self.nameTextField.text = ""
        self.nameTextField.becomeFirstResponder()
    }
    
    
}

extension ProfileImageCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text!.isBlank{
           self.nameTextField.text = CommonFunction.getUser()?.fullName
        }
        else
        {
            if textField.text!.isName{
                CommonFunction.getUser()?.fullName = textField.text!
            }
            else{
                CommonFunction.showToast(msg: "Invalid Name")
            }
        }
       
    }
    
}
