    //
//  NewsCell.swift
//  IPAC
//
//  Created by Appinventiv on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    // MARK:- VARIABLES
    //====================
    
    
    // MARK:- IBOUTLETS
    //====================
    
    @IBOutlet weak var newsHeadingLabel: UILabel!
    @IBOutlet weak var newsBodyLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var readMoreBtn: UIButton!
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        readMoreBtn.setTitle(AppStrings.readMore.rawValue.localized, for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
 
}
