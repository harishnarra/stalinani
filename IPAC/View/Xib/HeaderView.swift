//
//  DemoHeaderView.swift
//  Onboarding
//
//  Created by Appinventiv on 24/05/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation
import UIKit

class HeaderView : UITableViewHeaderFooterView{
    

    @IBOutlet weak var whiteBgView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var lblAlert: UILabel!
    @IBOutlet weak var imgAlert: UIImageView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        whiteBgView.roundCorners([.topRight,.topLeft], radius: 5.0)
        whiteBgView.backgroundColor = UIColor.black
    }
    
    
}
