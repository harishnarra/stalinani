//
//  ButtonCell.swift
//  IPAC
//
//  Created by macOS on 22/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var submitBtn: GradientButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        submitBtn.rounded(cornerRadius: 5.0, clip: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
