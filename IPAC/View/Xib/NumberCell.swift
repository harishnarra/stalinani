//
//  NumberCell.swift
//  IPAC
//
//  Created by Appinventiv on 13/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class NumberCell: UITableViewCell {
    
    var mobileNumber = "9898989898"
    var countryCode = "+91"
    
    @IBOutlet weak var countryCodeButton: UIButton!
 
    @IBOutlet weak var numberTextField: UITextField!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        countryCodeButton.setTitle(countryCode, for: .normal)
        numberTextField.text = mobileNumber
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
