//
//  UploadCell.swift
//  IPAC
//
//  Created by Appinventiv on 27/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit


protocol UpdatecellProtocal : class {
    
    func deletedImage(image: Int)
}

class UploadCell: UITableViewCell {

    //@IBOutlet weak var addMoreButton: UIButton!
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var upeerview: UIView!
    @IBOutlet weak var dashedBorderView: UIView!
    @IBOutlet weak var imagePickerButton: UIButton!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    //MARK:======== FirstView ==============
    @IBOutlet weak var firstDatalength: UILabel!
    @IBOutlet weak var firstProgressView: UIProgressView!
    @IBOutlet weak var firstIDnameLabel: UILabel!
    @IBOutlet weak var firstImage: UIImageView!
    weak var delegate : UpdatecellProtocal?
    //MARK:======== SecondView ==============
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var secondIDname: UILabel!
    @IBOutlet weak var secondProgress: UIProgressView!
    @IBOutlet weak var secondDatalenght: UILabel!
    @IBOutlet weak var youNeedToUploadLabel: UILabel!
    @IBOutlet weak var heightUploadView: NSLayoutConstraint!
    
    var imageArray = [UIImage]()
    var imageURLs = [String]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.upeerview.isHidden = true
        self.lowerView.isHidden = true
        self.upeerview.setBorder(color: #colorLiteral(red: 0.7294117647, green: 0.7725490196, blue: 0.8392156863, alpha: 0.5), width: 1.5, cornerRadius: 5.0)
        self.lowerView.setBorder(color: #colorLiteral(red: 0.7294117647, green: 0.7725490196, blue: 0.8392156863, alpha: 0.5), width: 1.5, cornerRadius: 5.0)
        //self.stackViewHeight.constant = 0.0
        //self.addMoreButton.isHidden = true
        self.firstImage.rounded(cornerRadius: 5.0, clip: true)
        self.secondImage.rounded(cornerRadius: 5.0, clip: true)
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        dashedBorderView.addDashedBorder(color: .lightGray, lineWidth: 0.8)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func firstCrossbtn(_ sender: UIButton) {
//        if let _ = secondImage.image{
//            self.stackViewHeight.constant = 94.0
//            self.dashedBorderView.isHidden = false
//            self.imagePickerButton.isHidden = false
//            //self.addMoreButton.isHidden = false
//        }else{
//            self.stackViewHeight.constant = 0.0
//            self.dashedBorderView.isHidden = false
//            self.imagePickerButton.isHidden = false
//           // self.addMoreButton.isHidden = true
//
//    }
//        self.upeerview.isHidden = true
//        self.firstImage.image = nil
//        self.firstProgressView.isHidden = false
        self.delegate?.deletedImage(image: 1)
 //       self.firstProgressView.progress = 0.0
      //  layoutIfNeeded()
        
    }
    @IBAction func secondCrossButton(_ sender: UIButton) {
//        if let _ = firstImage.image{
//              self.stackViewHeight.constant = 94.0
//              self.dashedBorderView.isHidden = false
//            self.imagePickerButton.isHidden = false
//
//            //self.addMoreButton.isHidden = false
//
//        }else{
//            self.stackViewHeight.constant = 0.0
//            self.dashedBorderView.isHidden = false
//            self.imagePickerButton.isHidden = false
//
//            //self.addMoreButton.isHidden = true
//        }
//
//        self.lowerView.isHidden = true
//        self.secondImage.image = nil
//        self.secondProgress.isHidden = false
        self.delegate?.deletedImage(image: 2)
//        self.secondProgress.progress = 0.0
//        self.secondProgress.isHidden = false
    }
    
}

