//
//  LabelAndButtonCell.swift
//  Blurbb
//
//  Created by Appinventiv on 24/05/18.
//  Copyright © 2018 Rupali Gupta. All rights reserved.
//

import UIKit

class FooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var whiteView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
   
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        whiteView.roundCorners([.bottomRight,.bottomLeft], radius: 5.0)
    }
    
    
    
}
