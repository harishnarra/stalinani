//
//  StringConstant.swift
//  IPAC
//
//  Created by macOS on 13/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

struct StringConstant {
    
    //MARK: LoginVC
    //========================
    static let Remember_me = "Remember_me"
    static let Unique_ID_Number = "Unique ID Number"

    
    //MARK:- RegisterVC
    //===================
    static let YES                      = "YES"
    static let NO                       = "NO"
    static let Full_Name                = "Full Name"
    static let Gender                   = "Gender"
    static let Male                     = "Male"
    static let Female                   = "Female"
    static let Other                    = "Other"
    static let Sign_Up                  = "SignUp"
    static let Date_Of_Birth            = "Date Of Birth"
    static let Email_Id                 = "Email_Id"
    static let Password                 = "Password"
    static let Mobile_Number            = "Mobile_Number"
    static let WhatsApp                 = "WhatsApp"
    static let CollegeState             = "CollegeState"
    static let HomeState                = "HomeState"
    static let College_District         = "College District"
    static let Current_District         = "Current District"

    static let College_Name             = "College Name"
    static let Referal_Code_Optional    = "Referal Code Optional"
    static let WhatsApp_same_as_Mobile_Number = "WhatsApp same as Mobile Number"
    static let Are_you_a_College              = "Are you a College"
   
    static let I_Accept_Terms_Condition             = "I Accept Terms Condition"
    static let TREMS_CONDITIONS                      = "TREMS_CONDITIONS"
    static let Enter_Your_Correct_Full_Name         = "Enter Your Correct Full Name"
    static let Please_Enter_Dob                     = "Please Enter Dob"
    static let Enter_valid_phone_number             = "Enter valid phone number"
    static let Plese_Select_are_you_college_student = "Please Select are you college student"
    static let Please_select_state                  = "Please_select_state"
    static let Please_select_district               = "Please_select_district"
    static let Please_enter_your_college_name       = "Please_enter_your_college_name"
    static let Please_accept_Terms                  = "Please_accept_Terms"
    static let Hide = "Hide"
    static let Show = "Show"
    
    //MARK: ChooseCollegeVC
    //========================
    static let Search_College = "Search_College"
    static let Add_College    = "Add_College"
    
    //MARK: TemsAndCondition
    //======================
 
    
    //MARK: RegistrationNoPopupVC
    //========================
    static let OK = "OK"
    static let Registration_Successful = "Registration Successful"
    static let Please_note_your_registration_no = "Please note your registration no"
    
    
    //MARK: Edit Profile
    //========================
    static let UI_Number = "UI_Number"
    
    //MARK: DashboardVC
    //========================
    static let Dashboard = "Dashboard"
    static let date     = "8 July 2018 - 10 July 2018"
    static let Ratings  = "Ratings"
    static let Rank     = "Rank"
    static let LeaderBoard = "LeaderBoard"
    static let LEADERBOARD = "LEADERBOARD"
    static let view_all   = "view_all"
    static let More     = "More"
    static let startDate = "startDate"
    static let endDate = "endDate"
    static let platform = "platform"
    
    //MARK: HometabBarVC
    //========================
    static let Home = "Home"
    static let Tasks = "Tasks"
    
    //MARK: HomeVC
    //========================
    static let All          = "All"
    static let Facebook     = "Facebook"
    static let Twitter      = "Twitter"
    static let Incomplete   = "Incomplete"
    static let Complete     = "Complete"
    static let Whatsapp  = "WhatsApp"
    static let youtube = "YouTube"
    static let offline = "Offline-Tasks"
    static let online = "Online-Form"
    static let inReview = "In Review"

    //MARK: HomeCell
    //========================
    static let LIKE = "LIKE"
    
    //MARK: TaskCell
    //========================
    static let Expired = "Expired"
    static let h        = "h"
    static let m        = "m"
    static let s        = "s"
    static let d        = "d"
    
    //MARK: TaskHeaderView
    //========================
    static let Task_Completed = "Task_Completed"
    static let Rewards = "Rewards"
    static let DAILY_PROGRESS = "DAILY_PROGRESS"
    
    //MARK: StoryVC
    //===================
    static let Story = "Story"
    
    //MARK:- ContactUsVC
    //==================
    static let Contact_Us = "Contact_Us"
    static let Support_Helpline = "Support_Helpline"
    
    //MARK:- TaskFeedbackVC
    //======================
    static let Write_Something = "Write_Something"
    static let Write_Description = "Write_Description"
    static let SEND = "SEND"
   
    
    
    //MARK:- ForgotResetPasswordVC
    //======================
    static let Dont_Have_An_Account = "Dont_Have_An_Account"
    static let Register             = "Register"
    static let Verify_Mobile_No     = "Verify_Mobile_No"
     static let SEND_OTP            = "SEND_OTP"
    
    //MARK:- TwitterWebViewVC
    //===========================
    static let Back_To_App = "Back_To_App"
    static let Task_description_copied = "Task_description_copied"
    static let Comment_copied = "Comment_copied"

    //MARK: NotificationVC
    //======================
    static let Notification = "Notification"
    
    //MARK: UploadProofVC
    //========================
    static let SUBMIT = "SUBMIT"
    static let You_Need_To_Upload = "You_Need_To_Upload"
    static let Select_ID = "Select_ID"
    static let UPLOAD_FILE = "UPLOAD_FILE"
    static let Please_Enter_Valid_Id_No = "Please_Enter_Valid_Id_No"
    static let ImageFile1       = "ImageFile1"
    static let ImageFile2       = "ImageFile2"
    static let CouldNotLoginTryAgain = "CouldNotLoginTryAgain"
    
    //MARK: ProofArray
    //========================
    static let Voter_Card = "Voter ID"
    static let Aadhar_Card = "Aadhar_Card"
    static let Pan_Card = "Pan_Card"
    static let Driving_Licence = "Driving_Licence"
}


enum Constants : String{
    case ok = "Ok"
    case textCopied = "Text Copied"
    case referralMsg = "Use the referral code "
}

enum Cells : String{
    case ReferralTableViewCell = "ReferralTableViewCell"
    case WalletTableViewCell = "WalletTableViewCell"
    case RewardtransactionsCell = "RewardtransactionsCell"
    case DashboardPopUpTableViewCell = "DashboardPopUpTableViewCell"

}


//var sideMenuLabelsArray = ["",LocalizationSystem.sharedInstance.localizedStringForKey(key: "Tasks", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Profile", comment: ""), LocalizationSystem.sharedInstance.localizedStringForKey(key: "My_Rewards", comment: "") ,LocalizationSystem.sharedInstance.localizedStringForKey(key: "About_DMK", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Contact Us", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "FAQs", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Privacy_Policy", comment: ""), LocalizationSystem.sharedInstance.localizedStringForKey(key: "Referral", comment: ""), "" , LocalizationSystem.sharedInstance.localizedStringForKey(key: "LogOut", comment: "")]



struct AlertMessage{
    
    static let addCampaignTitle = "Please add Campaign Title"
    static let addCampaignDate = "Please add Campaign Date"
    static let addCampaignParticular = "Please add Campaign Particular"
    static let addCampaignAmount = "Please add Campaign Expense amount"
    static let addOneEntry = "Please add atleast one entry"
    static let alert = "Alert"
    static let skip = "Skip"
    static let update = "Update"
    static let addValidStartEndDate = "Please enter valid start and end date"
    static let noInternet = "Please check your internet connection."
    static let completeFirstTask = "Complete your first task."
    static let completeBoothForm = "Fill Mark Your Booth form."
    static let completeProof = "Add your ID proof and social profiles."
    static let completePaytm = "Add your Reimbursement details."
    static let form = "Form"
    static let markYourBooth = "Mark Your Booth"
    static let shareReferralCodeFirst = "Share your referral code"
    static let shareReferralCodeLast = "with your friends to help them gain unprecedented campaign experience, contribute meaningfully to spread their leader\'s vision and avail scores of added benefits and opportunities!!!"
    static let backToApp = "Back to App"
    static let bonusReward = "Bonus Reward"
    static let bonusRewardMessage = "Please complete following things to earn 50 bonus reward points."
    static let noRecordFound = "No Record Found!"
    static let writeDescription = "Please enter description"
    static let ok = "OK"
    //BONUS RECEIVED POPUP
    static let congratulations = "Congratulations!"
    static let taskInstruction = "Instructions"
    
    static let addAtleastOneImage = "Please add atleast one image"
    static let imageSavedSuccessfully  = "Image saved to photos successfully"
    static let youNeedToUpdateTwoScreenshots = "You need to upload two screenshots"
    static let youHaveToAddTwoScreenshot = "You have to add two Screenshots"
    static let installWhatsapp = "Please install Whatsapp application"
    
    static let maxLimitReached = "Maximum limit reached"
    static let enterId = "Please enter ID number"
    static let accountBlockedByAdmin = "Your account is blocked by admin"
    static let enterColleeName = "Please enter college name"
    static let selectProofType = "Select Proof Type"
    static let acceptTerms = "Please accept updated terms and conditions"
    static let taskNotComplete  = "Task not complete"
    static let dataChecking  = "Please Select Option"
    static let dataChecking2  = "Please Enter Something!"
    static let commentCheck = "Please Enter Comment!"

    static let twitterAccountNotMatched = "Twitter account not matched with account linked to profile.Please login with same account or update twitter account from Profile"
    static let wrongQR = "Wrong QR code"
    static let fileSavedSuccessfully = "File saved successfully"
    static let reviewTheTask = "We will review the task and update you shortly"
    static let allowPermission = "Please allow photo permission from settings"
    static let whatsappDPMsg = "Image is saved to album successfully.Please follow the previously shown steps."
    static let invalidAction = "Action not allowed";
    static let shareImageMsg = "Image is saved to album successfully, share the saved image on whatsapp to complete the task"
}


struct TitleType{
    static let approved = "APPROVED"
    static let pending = "PENDING APPROVAL"
    static let ok = "Ok"
    static let cancel = "Cancel"
}
