//
//  AppColor.swift
//  IPAC
//
//  Created by Sajal on 11/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

import UIKit

struct AppColors {
    
    static let appThemeColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    static let whiteColor       = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let borderColor = #colorLiteral(red: 0.3098039216, green: 0.4459255338, blue: 0.629286468, alpha: 0.5)
    static let pickerBorder = #colorLiteral(red: 0.3098039216, green: 0.4459255338, blue: 0.629286468, alpha: 0.35)
    
    struct Gray {
        static let gray234 = #colorLiteral(red: 0.9176470588, green: 0.9098039216, blue: 0.9176470588, alpha: 1)
        static let gray166 = #colorLiteral(red: 0.6509803922, green: 0.6666666667, blue: 0.7098039216, alpha: 1)
        static let gray208 = #colorLiteral(red: 0.8156862745, green: 0.831372549, blue: 0.862745098, alpha: 1) //208 212 220
        static let gray    = UIColor.gray
        static let lightGray    = UIColor.lightGray
    }
    
    struct BlackColor {
        static let black    = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        static let black3   = #colorLiteral(red: 0.01176470588, green: 0.1215686275, blue: 0.2235294118, alpha: 1)
        static let black12  = #colorLiteral(red: 0.04705882353, green: 0.1137254902, blue: 0.2039215686, alpha: 1)
        static let black7   = #colorLiteral(red: 0.02745098039, green: 0.1019607843, blue: 0.168627451, alpha: 1) // 7 26 43
    }
    
    struct Pink {
        static let pink = #colorLiteral(red: 0.9882352941, green: 0.1333333333, blue: 0.5450980392, alpha: 1)
    }
    
    struct Orange {
        static let orange = #colorLiteral(red: 0.9843137255, green: 0.5294117647, blue: 0.4549019608, alpha: 1) //251 135 116
    }
    struct Blue {
        static let fbBulue                  = #colorLiteral(red: 0.2784313725, green: 0.3490196078, blue: 0.5764705882, alpha: 1)
        static let twitterBlue              = #colorLiteral(red: 0, green: 0.6, blue: 0.9137254902, alpha: 1)
        static let seperatorBlue            = #colorLiteral(red: 0.5490196078, green: 0.7529411765, blue: 0.9803921569, alpha: 1) //140 192 250
        static let darkBlue                 = #colorLiteral(red: 0.08235294118, green: 0.2745098039, blue: 0.4745098039, alpha: 1) // 21 70 121
        static let blueBorder               = #colorLiteral(red: 0.3647058824, green: 0.6862745098, blue: 0.9568627451, alpha: 1) // 93 175 244
        static let fbIncompleteBlue         = #colorLiteral(red: 0.7725490196, green: 0.8235294118, blue: 0.9843137255, alpha: 1)
        static let twitterIncolpleteBlue    = #colorLiteral(red: 0.7176470588, green: 0.9137254902, blue: 1, alpha: 1)
        static let activityIndicator           = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
    }
    
    struct Green {
         static let whatsupGreen = #colorLiteral(red: 0.3333333333, green: 0.8039215686, blue: 0.4235294118, alpha: 1)
    }
    struct label{
        static let dark = #colorLiteral(red: 0.07058823529, green: 0.1529411765, blue: 0.1647058824, alpha: 1)

    }
}
