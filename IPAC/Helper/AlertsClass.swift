
import UIKit
import EZSwiftExtensions

typealias AlertBlock = (_ success: AlertTag) -> ()

enum AlertTag {
    case done
    case yes
    case no
}

class AlertsClass: NSObject{
    
    static let shared = AlertsClass()
    var responseBack : AlertBlock?
    
    override init() {
        super.init()
    }
    
    //MARK: Alert Controller
    func showAlertController(withTitle title : String?, message : String, buttonTitles : [String], responseBlock : @escaping AlertBlock){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitles.count > 0 ? buttonTitles.first : nil, style: .default, handler: { action in
            ez.topMostVC?.dismiss(animated: true, completion: nil)
            self.responseBack?(.yes)
        }))
        if buttonTitles.count > 1{
        alert.addAction(UIAlertAction(title: buttonTitles.count > 1 ? buttonTitles.last : nil, style: .cancel, handler: { action in
            ez.topMostVC?.dismiss(animated: true, completion: nil)
            self.responseBack?(.no)
        }))
        }
        responseBack = responseBlock
        ez.topMostVC?.presentVC(alert)
    }
}

