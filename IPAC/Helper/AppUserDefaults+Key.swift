//
//  AppUserDefaults+Key.swift
//  StarterProj
//
//  Created by MAC on 07/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation

extension AppUserDefaults {

    enum Key : String {
        
        case Accesstoken
        case tutorialDisplayed
        case userData
        case pushStatus
        case deviceToken
        case cat
        case subCat
        case otherCat
        case subCatName
        case subCatId
        case date
        case languagess
    }
}
