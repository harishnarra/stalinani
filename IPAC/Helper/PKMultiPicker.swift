//
//  PKMultiPicker.swift
//
//

import UIKit

class PKMultiPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    internal typealias PickerDone = (_ firstValue: String, _ secondValue: String, _ firstSelectedIndex:Int) -> Void
    private var doneBlock : PickerDone!
    
    private var firstValueArray : [String]?
    private var secondValueArray = [String]()
    static var noOfComponent = 2
    
    
    class func openMultiPickerIn(_ textField: UITextField? , firstComponentArray: [String], secondComponentArray: [String], firstComponent: String?, secondComponent: String?, titles: [String]?, doneBlock: @escaping PickerDone) {
        
        let picker = PKMultiPicker()
        picker.doneBlock = doneBlock
        

        picker.backgroundColor = AppColors.appThemeColor
        picker.tintColor       = AppColors.whiteColor
        
        picker.openPickerInTextField(textField, firstComponentArray: firstComponentArray, secondComponentArray: secondComponentArray, firstComponent: firstComponent, secondComponent: secondComponent)
        
        if titles != nil {
            let label  = UILabel(frame: CGRect(x: UIScreen.main.bounds.size.width/4 - 10, y: 0, width: 100, height: 30))
            label.text = titles![0].uppercased()
            label.font = AppFonts.ProximaNova_Semibold.withSize(14.0)
            label.textColor = AppColors.BlackColor.black
            picker.addSubview(label)
            
            if PKMultiPicker.noOfComponent > 1 {
                let label = UILabel(frame: CGRect(x: UIScreen.main.bounds.size.width * 3/4 - 50, y: 0, width: 100, height: 30))
                label.text = titles![1].uppercased()
                label.font = AppFonts.ProximaNova_Semibold.withSize(14.0)
                label.textColor = AppColors.BlackColor.black
                picker.addSubview(label)
            } else {
                label.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30)
                label.textAlignment = NSTextAlignment.center
            }
        }
    }
    
    
    private func openPickerInTextField(_ textField: UITextField?, firstComponentArray: [String], secondComponentArray: [String], firstComponent: String?, secondComponent: String?) {
        
        firstValueArray  = firstComponentArray
        secondValueArray = secondComponentArray
        
        self.delegate = self
        self.dataSource = self
        
     
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancelButtonTapped))
//        cancelButton.tintColor = UIColor.black
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(pickerDoneButtonTapped))
//        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action:nil)
        
        cancelButton.tintColor = AppColors.borderColor
        doneButton.tintColor   = AppColors.borderColor
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        toolbar.tintColor     = AppColors.borderColor
        toolbar.barTintColor  = AppColors.borderColor
        toolbar.barStyle      = .default
        toolbar.isTranslucent = true
        
        let array = [cancelButton, spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.backgroundColor = UIColor.lightText
        
        textField?.inputView = self
        textField?.inputAccessoryView = toolbar
        
        let index = self.firstValueArray?.index(where: {$0 == firstComponent })
        self.selectRow(index ?? 0, inComponent: 0, animated: true)
        
        if PKMultiPicker.noOfComponent > 1 {
            let index1 = self.secondValueArray.index(where: {$0 == secondComponent })
            self.selectRow(index1 ?? 0, inComponent: 1, animated: true)
        }
    }
   
    @IBAction private func pickerCancelButtonTapped(){
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    @IBAction private func pickerDoneButtonTapped(){
        
        UIApplication.shared.keyWindow?.endEditing(true)
        
        let index1 : Int?
        let firstValue : String?
        index1 = self.selectedRow(inComponent: 0)
        
        if firstValueArray?.count == 0{return}
        else{firstValue = firstValueArray?[index1!]}
        
        var index2 :Int!
        var secondValue: String!
        if PKMultiPicker.noOfComponent > 1 {
            index2 = self.selectedRow(inComponent: 1)
            secondValue = secondValueArray[index2]
        }
        self.doneBlock(firstValue!, secondValue ?? "", index1!)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if component == 0 {
            return firstValueArray!.count
        }
        return secondValueArray.count
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return PKMultiPicker.noOfComponent
    }

    

    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as? UILabel
        if label == nil {
            label = UILabel()
            label?.text = self.getTitle(pickerView: pickerView, row: row, component: component)
            label?.textAlignment = .center
            label?.font = AppFonts.ProximaNova_Semibold.withSize(14.0)
            label?.textColor = AppColors.BlackColor.black
        }
        
        return label!
    }
    
    func getTitle(pickerView:UIPickerView, row:Int, component: Int)->String {
        switch component {
            
        case 0:
            return firstValueArray?[row] ?? ""
        case 1:
            return secondValueArray[row]
        default:
            return ""
        }
    }
}

class PKDatePicker: UIDatePicker {
    
    var isSetInTextField = false
    internal typealias PickerDone = (_ selection: String) -> Void
    private var doneBlock: PickerDone!
    private var datePickerFormat: String = ""
    private var textField: UITextField?
    private var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.datePickerFormat
        let enUSPOSIXLocale: Locale = Locale(identifier: LocalizationSystem.sharedInstance.getLanguage())
        
        dateFormatter.locale = enUSPOSIXLocale
        
        return dateFormatter
    }
    //LocalizationSystem.sharedInstance.getLanguage()
    
    class func openDatePickerIn(_ textField: UITextField?, outPutFormate: String, mode: UIDatePickerMode, minimumDate: Date = Date(), isSetInTextField: Bool = false, minuteInterval: Int = 1, selectedDate: Date?, doneBlock: @escaping PickerDone) -> PKDatePicker {
        
        
        let picker = PKDatePicker()
        
        picker.isSetInTextField = isSetInTextField
        picker.backgroundColor  = AppColors.appThemeColor
        picker.tintColor        = AppColors.borderColor
        
        picker.doneBlock = doneBlock
        picker.datePickerFormat = outPutFormate
        picker.datePickerMode = mode
        picker.dateFormatter.dateFormat = outPutFormate
        picker.textField = textField
        if let sDate = selectedDate {
            picker.setDate(sDate, animated: false)
        }
        picker.minuteInterval = minuteInterval
        
        if mode == .time {
            let dateFormatte = dateManager.utcDateFormatter()
            dateFormatte.dateFormat = "dd MMM yy"
            let today = dateFormatte.string(from: Date())
            let minDay = dateFormatte.string(from: minimumDate)
            
            picker.minimumDate = today.lowercased() == minDay.lowercased() ? Date() : minimumDate
        }
        else {
            picker.minimumDate = minimumDate
           
        }
        
        picker.setValue(AppColors.BlackColor.black, forKeyPath: "textColor")
        picker.openDatePickerInTextField(textField)
        
        return picker
    }
    
    
    class func openDateMaxPickerIn(_ textField: UITextField?, outPutFormate: String, mode: UIDatePickerMode, minimumDate: Date = dateManager.getMinimumStartDateAndTime(), maximumDate: Date = Date(), isSetInTextField: Bool = true, minuteInterval: Int = 1, selectedDate: Date?, doneBlock: @escaping PickerDone) -> PKDatePicker {
        
        
        let picker = PKDatePicker()
        
        picker.isSetInTextField = isSetInTextField
        picker.backgroundColor  = AppColors.whiteColor
        picker.tintColor        = AppColors.whiteColor
        
        picker.doneBlock = doneBlock
        picker.datePickerFormat = outPutFormate
        picker.datePickerMode = mode
        picker.dateFormatter.dateFormat = outPutFormate
        picker.textField = textField
        if let sDate = selectedDate {
            picker.setDate(sDate, animated: false)
        }
        picker.minuteInterval = minuteInterval
        
        if mode == .time {
            let dateFormatte = dateManager.utcDateFormatter()
            dateFormatte.dateFormat = "dd MMM yy"
            let today = dateFormatte.string(from: Date())
            let minDay = dateFormatte.string(from: minimumDate)
            
            picker.minimumDate = today.lowercased() == minDay.lowercased() ? Date() : minimumDate
        }
        else {
            picker.minimumDate = minimumDate
            picker.maximumDate = maximumDate
        }
        
        picker.setValue(AppColors.BlackColor.black, forKeyPath: "textColor")
        picker.openDatePickerInTextField(textField)
        
        return picker
    }
    private func openDatePickerInTextField(_ textField: UITextField?) {

        if let text = textField?.text, !text.isEmpty, let selDate = self.dateFormatter.date(from: text) {
            self.setDate(selDate, animated: false)
        }
        
        self.addTarget(self, action: #selector(PKDatePicker.datePickerChanged(_:)), for: UIControlEvents.valueChanged)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancelButtonTapped))
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(pickerDoneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action:nil)
        
        cancelButton.tintColor = UIColor.black
        doneButton.tintColor = UIColor.black
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        toolbar.tintColor     = AppColors.appThemeColor
       // toolbar.barTintColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2919788099)
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        
        let array = [cancelButton, spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.backgroundColor = UIColor.lightText
        
        textField?.inputView = self
        textField?.inputAccessoryView = toolbar
    }
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {

    }
    
    @IBAction private func pickerCancelButtonTapped(){
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    @IBAction private func pickerDoneButtonTapped(){
        UIApplication.shared.keyWindow?.endEditing(true)
        
        let selected = self.dateFormatter.string(from: self.date)
        if !self.isSetInTextField {
            self.textField?.text = selected
        }
        
        self.doneBlock(selected)
    }
}
