//
//  String+Validations.swift
//  IPAC
//
//  Created by Appinventiv on 11/07/18.
//  Copyright © 2018 Appinventiv. All rights reserved.
//

import Foundation
let MIN_PASS_LENGTH = 6
let MAX_PASS_LENGTH = 30

extension String {
    
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}$", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }
    
    var isName: Bool {
        return !isEmpty && range(of: "[^a-zA-Z ]", options: .regularExpression) == nil
    }
    
    //validate Password
//    var isValidPassword: Bool {
//        do {
//            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
//            if(regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil) {
//                if(self.count >= MIN_PASS_LENGTH && self.count <= MAX_PASS_LENGTH){
//                    return true
//                }else{
//                    return false
//                }
//            }else{
//                return false
//            }
//        } catch {
//            return false
//        }
//    }
    
    var isValidPassword: Bool {
            if(self.count >= MIN_PASS_LENGTH && self.count <= MAX_PASS_LENGTH){
                    return true
            }else{
                    return false
            }
       
        
    }
    
    var isPhoneNumber: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[0-9]{10}$", options: .caseInsensitive)
            if(regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil) {
                if(self.count == 10){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }

    }

}
