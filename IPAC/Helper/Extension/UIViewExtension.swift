//
//  UIViewExtension.swift
//  IPAC
//
//  Created by Admin on 10/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder?.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}


extension UIView{
    
    func rounded(cornerRadius: CGFloat, clip: Bool ){
        
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = clip
    }
    
    func setBorder(color: UIColor, width: CGFloat , cornerRadius: CGFloat){
        
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
    
    
    func setBorderWithOutCorner(color: UIColor, width: CGFloat){
        
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        
        self.clipsToBounds = true
    }
    
    func dropShadow(color: UIColor = .shadowColor , opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 0.8, scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    //Dashed line border around UIView
    
    func addDashedBorder(color: UIColor, lineWidth: CGFloat) {
        let color = color.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        //  let frameSize = self.frame.size
        // let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        //shapeLayer.bounds = shapeRect
        shapeLayer.frame = self.bounds
        // shapeLayer.position = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [2,2] //[3,3]
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 8.0).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer();
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.frame.width - thickness, y: 0, width: thickness, height:self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        self.layer.addSublayer(border)
    }
    
    
    //round corners
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    
    //add background
    
    func addBackgroundImage(){
        
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let backImageView = UIImageView.init(frame: CGRect(x:0,y:0, width:width,height: height))
        backImageView.image = #imageLiteral(resourceName: "icSplashBg")
        backImageView.clipsToBounds = true
        backImageView.contentMode = .scaleAspectFill
        
        self.sendSubview(toBack: backImageView)
    }
    
    //for gradient
    
    func addLeftToRightGradient(colors : [CGColor] = [#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)], addAtBack: Bool = false){
        self.layoutIfNeeded()
        let gradient = CAGradientLayer()
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = self.bounds
        gradient.cornerRadius = 3.0
        if addAtBack {
            self.layer.insertSublayer(gradient, at: 1)
            return
        }
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    
    func addRightToLefttGradient(colors: [CGColor] =  [#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)], addAtBack: Bool = false){
        self.layoutIfNeeded()
        let gradient = CAGradientLayer()
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = self.bounds
        gradient.cornerRadius = 3.0
        if addAtBack {
            self.layer.insertSublayer(gradient, at: 1)
            return  
        }
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    var tableViewCell : UITableViewCell? {
        
        var subviewClass = self
        
        while !(subviewClass is UITableViewCell){
            
            guard let view = subviewClass.superview else { return nil }
            
            subviewClass = view
        }
        return subviewClass as? UITableViewCell
    }
    
    func tableViewIndexPath(_ tableView: UITableView) -> IndexPath? {
        
        if let cell = self.tableViewCell {
            
            return tableView.indexPath(for: cell)
            
        }
        return nil
    }
    
    var collectionViewCell : UICollectionViewCell? {
        
        var subviewClass = self
        
        while !(subviewClass is UICollectionViewCell){
            
            guard let view = subviewClass.superview else { return nil }
            
            subviewClass = view
        }
        
        return subviewClass as? UICollectionViewCell
    }
    
    func collectionViewIndexPath(_ collectionView: UICollectionView) -> IndexPath? {
        
        if let cell = self.collectionViewCell {
            
            return collectionView.indexPath(for: cell)
            
        }
        return nil
    }
}



