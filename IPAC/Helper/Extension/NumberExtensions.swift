//
//  NumberExtensions.swift
//  IPAC
//
//  Created by Appinventiv on 30/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension Int {
    func getHMS(completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(self / 3600, (self % 3600) / 60, (self % 3600) % 60)
    }
    
    func getTimeString() -> String {
        return self < 10 ? "0\(self)" : "\(self)"
    }
}
