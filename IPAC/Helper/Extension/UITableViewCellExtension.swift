//
//  UITableViewCellExtension.swift
//  IPAC
//
//  Created by Appinventiv on 07/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UITableViewCell: AuthenticationChallengeResponsable {
    public func downloader(_ downloader: ImageDownloader, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: ((challenge.protectionSpace.serverTrust))!))
    }
    
    public func downloader(_ downloader: ImageDownloader, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: ((challenge.protectionSpace.serverTrust))!))
    }
}

extension UIViewController: AuthenticationChallengeResponsable {
    public func downloader(_ downloader: ImageDownloader, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: ((challenge.protectionSpace.serverTrust))!))
    }
    
    public func downloader(_ downloader: ImageDownloader, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: ((challenge.protectionSpace.serverTrust))!))
    }
}
