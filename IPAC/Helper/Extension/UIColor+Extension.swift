//
//  UIColor+Extension.swift
//  IPAC
//
//  Created by Appinventiv on 27/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
    static let textFieldTextColor = UIColor.init(red:18/255, green:39/255 ,blue:82/255, alpha:1.0)
    static let floatingTitleColor = UIColor.init(red: 18/255, green: 39/255, blue: 82/255, alpha: 0.2)
    static let shadowColor = UIColor.init(red: 94/255, green: 186/255, blue: 243/255, alpha: 0.3)
    static let gradientStartColor = UIColor.init(red: 86/255, green: 132/255, blue: 245/255, alpha: 1.0)
    static let gradientStopColor = UIColor.init(red: 96/255, green: 198/255, blue: 243/255, alpha: 1.0)
    
}
