//
//  DateManager.swift
//  Shopoholic
//
//  Created by appinventiv on 26/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation

let dateManager = DateManager.shared

class DateManager{
    
    let dateFormatter: DateFormatter
    static let shared = DateManager()
    
    init() {
        self.dateFormatter = DateFormatter()
        self.dateFormatter.timeZone = TimeZone.current
    }
    
    func string(from timestamp: Double, dateFormat: String = "hh:mm a", inMilliSeconds: Bool = true)->String{
        
        self.dateFormatter.dateFormat = dateFormat
        
        if inMilliSeconds{
            return self.dateFormatter.string(from: Date(timeIntervalSince1970: timestamp/1000))
        }else{
            return self.dateFormatter.string(from: Date(timeIntervalSince1970: timestamp))
        }
    }
    
  
    
    func utcDateFormatter()-> DateFormatter {
        let enUSPOSIXLocale: Locale = Locale(identifier: LocalizationSystem.sharedInstance.getLanguage())
        dateFormatter.locale = enUSPOSIXLocale
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return dateFormatter
    }
    
    func stringFrom(date: Date, dateFormat: String = "dd/MM/yyyy")->String{
        self.dateFormatter.dateFormat = dateFormat
        
        return self.dateFormatter.string(from: date)
    }
    
    func elapsedTime(from timestamp: Double, inMilliSeconds: Bool = true)->String{
   
        if inMilliSeconds{
            return Date(timeIntervalSince1970: timestamp/1000).elapsedTime
        }else{
            return Date(timeIntervalSince1970: timestamp).elapsedTime
        }
    }
    

    /**
     returns today, yesterday and the date in string
     */
    func getNotationFor(timestamp: Double, inMilliSeconds: Bool = true)->String{
        
        var timeInterval: Double!
        
        if inMilliSeconds{
            timeInterval = timestamp/1000
        }else{
            timeInterval = timestamp
        }
        
        let date = Date(timeIntervalSince1970: timeInterval)
        
        let numberOfdays = Date().daysFrom(date)
        
        if numberOfdays == 0{
            return "Today"
        }else if numberOfdays == 1{
            return "Yesterday"
        }else{
            return self.stringFrom(date: date)
        }
    }
    
    func getMinimumStartDateAndTime() -> Date {
      
      
        let year = Date().year - MaximumAllowedYear
        let month = Date().month
        let day   = Date().day
        let dateFormtter = DateFormatter()
        dateFormtter.dateFormat = "dd-MM-yyyy"
        let fromDate = "\(day)-\(month)-\(year)"
        guard let date = dateFormtter.date(from: fromDate) else { return Date(timeIntervalSince1970: 1) }
        print_debug(date)
        return date
    }
    
    func getMaximumStartDateAndTime() -> Date {
        
        let year = Date().year - MinimumAllowYear
        let month = Date().month
        let day   = Date().day
        let dateFormtter = DateFormatter()
        dateFormtter.dateFormat = "dd-MM-yyyy"
        let fromDate = "\(day)-\(month)-\(year)"
        guard let date = dateFormtter.date(from: fromDate) else { return Date(timeIntervalSince1970: 1) }
        print_debug(date)
        return date
    }
}
