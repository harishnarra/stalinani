//
//  GradientButton.swift
//  IPAC
//
//  Created by Appinventiv on 27/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class GradientButton: UIButton {

    override func draw(_ rect: CGRect) {
        // Drawing code
        self.addLeftToRightGradient()
    }

}
