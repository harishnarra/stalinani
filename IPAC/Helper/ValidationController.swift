    //
    //  ValidationController.swift
    //  IPAC
    //
    //  Created by Appinventiv on 11/07/18.
    //  Copyright © 2018 Admin. All rights reserved.
    //

import Foundation
import Foundation
    
final class ValidationController {
        
    static func validatePassword(password: String)-> (Bool, String) {
        if password.isBlank {
             CommonFunction.showToast(msg: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password can't be empty", comment: ""))
            return (false, LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password can't be empty", comment: ""))
        }
        else if !password.isValidPassword{
               CommonFunction.showToast(msg: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password must be of minimum 8 characters", comment: ""))
            return (false, LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password must be of minimum 8 characters", comment: ""))
        }
        else{
            return (true, "")
        }
    }

    static func validateName(name : String)-> (Bool,String)
    {
        if name.isBlank{
             CommonFunction.showToast(msg: AppStrings.enterName.rawValue.localized)
            
            return (false,AppStrings.enterName.rawValue.localized)
        }
        else if !name.isName{
             CommonFunction.showToast(msg: AppStrings.invalidName.rawValue.localized)
            
            return(false,AppStrings.invalidName.rawValue.localized)
        }
        else{
            return(true,"")
        }
        
    }

    static func validatePhone(phone: String)-> (Bool,String){
        if phone.isBlank{
             CommonFunction.showToast(msg: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Phone number can't be empty", comment: ""))
            return (false,LocalizationSystem.sharedInstance.localizedStringForKey(key: "Phone number can't be empty", comment: ""))
            
          //
        }
        else if !phone.isPhoneNumber{
            //
              CommonFunction.showToast(msg: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter_valid_phone_number", comment: ""))
            return(false, LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter_valid_phone_number", comment: ""))
        }
        else{
            return(true,"")
        }
    }


    static func validateEmail(email:String) -> (Bool,String){
        if email.isBlank {
            
            CommonFunction.showToast(msg: AppStrings.enterEmail.rawValue.localized)
            return(false,AppStrings.enterEmail.rawValue.localized)
            
        } else if !email.isEmail {
             CommonFunction.showToast(msg: AppStrings.invalidEmail.rawValue.localized)
            return (false,AppStrings.invalidEmail.rawValue.localized)
        }
        return (true,"")
    }
    
    
    static func validateUpload(selectedID : String,IdNumber : String) -> (Bool,String)
    {
        if selectedID.isBlank{
            CommonFunction.showToast(msg: "Please Select ID Type")
            return(false,"Please Select ID Type")
        }
        else if IdNumber.isBlank{
            CommonFunction.showToast(msg: "Please enter UI number")
            return(false,"Please enter UI number")
        }
        else{
            return(true,"")
        }
    }


    static func validateSignUpFields(name:String,email:String,password:String,mobileNum : String) -> (Bool,String) {
        
        let nameValidation = ValidationController.validateName(name: name)
        let emailValidation = ValidationController.validateEmail(email: email)
        let passwordValidation = ValidationController.validatePassword(password: password)
        let mobileValidation = ValidationController.validatePhone(phone: mobileNum)
        if !nameValidation.0{
            return(false,nameValidation.1)
        }else if !emailValidation.0{
            return(false,emailValidation.1)
        }
        else if !passwordValidation.0{
            return(false,passwordValidation.1)
        }
        else if !mobileValidation.0{
            return(false,mobileValidation.1)
        }
        else{
            return (true,"")
        }
    }

    
    static func validateLogin(mobile : String,password : String)-> (Bool,String){
        let mobileValidation = ValidationController.validatePhone(phone: mobile)
        let passwordValidation = ValidationController.validatePassword(password: password)
        if !mobileValidation.0{
            return(false,mobileValidation.1)
        }
        else if !passwordValidation.0{
            return(false,passwordValidation.1)
        }
        else{
            return (true,"")
        }
    
    }
    
    
    static func validateEditInfo(user : UserModel) -> (Bool,String){
//        if let name = user.fullName{
//            if name.isName{
//                return(true,"")
//            }
//            else{
//                return(false,"Invalid Name")
//            }
//        }
//        else{
//            return(false ,"Name Can't be empty")
//        }
        
        if let email = user.emailId {
            if email.isEmail{
                return(true ,"")
            }
            else{
                return(false,"Invalid Email")
            }
        }
        else{
            return(false,"Email can't be empty")
        }
    }
    
    
        
}

