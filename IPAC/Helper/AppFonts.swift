//
//  AppFonts.swift
//  IPAC
//
//  Created by Admin on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit


enum AppFonts : String {
    
    case ProximaNova_Semibold
    case ProximaNova_Bold
    case proximanovalight
    case ProximaNova_Extrabold
    case Proxima_Nova_Rg_Regular
}

extension AppFonts {
    
    func withSize(_ fontSize: CGFloat) -> UIFont {
        
        return UIFont(name: self.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    func withDefaultSize() -> UIFont {
        
        return UIFont(name: self.rawValue, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
    }
    
}
