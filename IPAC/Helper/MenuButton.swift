//
//  MenuButton.swift
//  IPAC
//
//  Created by Appinventiv on 03/08/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class MenuButton: UIButton {

    override func awakeFromNib() {
        self.addTarget(self, action: #selector(self.menuTapped), for: .touchUpInside)
    }
    
    @objc func menuTapped() {
        viewController().sideMenuViewController?._presentLeftMenuViewController()
    }
    
    func viewController() -> UIViewController {
        var responder: UIResponder? = self
        while !(responder is UIViewController) {
            responder = responder?.next
            if nil == responder {
                break
            }
        }
        return (responder as? UIViewController)!
    }

}
