//
//  CommonFunction.swift
//  IPAC
//
//  Created by Admin on 12/07/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Toaster
//import KVNProgress
import EZSwiftExtensions
import UserNotifications
import TwitterKit


class SingletonTask{
    
    var completedTask = 0
    
    static let shared = SingletonTask()
}


class CommonFunction {
    
//    static let proofArray  = [StringConstant.Voter_Card.localized, StringConstant.Aadhar_Card.localized, StringConstant.Pan_Card.localized, StringConstant.Driving_Licence.localized]
    static let proofArray  = [StringConstant.Voter_Card.localized, StringConstant.Pan_Card.localized, StringConstant.Driving_Licence.localized]

    class func showToast(msg: String, _ completion : (()->())? = nil) {
        
        if let currentToast = ToastCenter.default.currentToast {
            currentToast.cancel()
        }
        let toast = Toast(text: msg)
        ez.topMostVC?.view?.endEditing(true)
        toast.show()
    }
    
    class func setAttributesto(currentTextField : SkyFloatingLabelTextField)
    {
        currentTextField.placeholderColor = UIColor.lightGray
        currentTextField.textColor = UIColor.white
        currentTextField.titleColor = UIColor.red
        currentTextField.lineColor = UIColor.lightGray
        currentTextField.selectedTitleColor = UIColor.red
        currentTextField.selectedLineColor = UIColor.red
        currentTextField.lineHeight = 0.8
        currentTextField.selectedLineHeight = 1.2
        currentTextField.font = UIFont.init(name: "ProximaNova-Regular", size: 15)
    }
    
    
    static func showLoader() {
        ez.topMostVC?.view?.endEditing(true)
        Utility.functions.loader()
//        let conf = KVNProgressConfiguration.default()
//        conf?.isFullScreen = false
//        KVNProgress.setConfiguration(conf)
//        KVNProgress.show()
    }
    
    class func hideLoader() {
        Utility.functions.removeLoader()

       // KVNProgress.dismiss()

//         self.activityIndicator!.stopAnimating()
//        self.activityIndicator?.isHidden = true
//        self.activityIndicator!.removeFromSuperview()
    }
//
//    static var activityIndicator: UIActivityIndicatorView?
//   class func showLoader() {
//
//    activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
//
//        activityIndicator!.hidesWhenStopped = true
//        activityIndicator!.color = AppColors.Blue.activityIndicator
//
//        if let window = sharedAppDelegate.window {
//        activityIndicator!.center = window.convert(window.center, from: activityIndicator)
//        window.addSubview(activityIndicator!)
//       // window.is
//        } else {
//            let window = UIWindow(frame: UIScreen.main.bounds)
//            activityIndicator!.center = window.convert(window.center, from: activityIndicator)
//            window.addSubview(activityIndicator!)
//        }
//
//        activityIndicator!.startAnimating()
//    }
//
    static func getUser() -> UserModel? {
        guard let decoded = UserDefaults.standard.object(forKey: "User") as? Data else { return nil }
        guard let user = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserModel else { return nil }
        return user
    }
    
    static func saveUser(_ user: UserModel) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(encodedData, forKey: "User")
    }
    
    static func isRemembered() -> Bool{
        guard let val = UserDefaults.standard.object(forKey: "isRemembered") as? Bool else { return false }
        return val
    }
    
    static func rememberLoginCred(){
          UserDefaults.standard.set(true, forKey: "isRemembered")
    }
    
    static func isYoutubePopupHidden() -> Bool{
        guard let val = UserDefaults.standard.object(forKey: "isYoutubePopupHidden") as? Bool else { return false }
        return val
    }
    
    static func isTwitterFollowPopupHidden() -> Bool{
        guard let val = UserDefaults.standard.object(forKey: "isTwitterFollowPopupHidden") as? Bool else { return false }
        return val
    }
    static func isFbPopupHidden() -> Bool{
        guard let val = UserDefaults.standard.object(forKey: "isFbPopupHidden") as? Bool else { return false }
        return val
    }
    
    static func hideYoutubePopup(){
        UserDefaults.standard.set(true, forKey: "isYoutubePopupHidden")
    }
    static func hideTwitterFollowPopup(){
        UserDefaults.standard.set(true, forKey: "isTwitterFollowPopupHidden")
    }
    static func hideFbPopup(){
        UserDefaults.standard.set(true, forKey: "isFbPopupHidden")
    }
    
    static func removeUser(){
        UserDefaults.standard.set(nil, forKey: "User")
    }
    
    static func updateUser(_ user: UserModel) {
        guard let oldUser = getUser() else { return }
        oldUser.state = user.state
        oldUser.district = user.district
        oldUser.fullName = user.fullName
        oldUser.registerationNo = user.registerationNo
        oldUser.idProofs = user.idProofs
        oldUser.isActive = user.isActive
        oldUser.paytmNumber = user.paytmNumber
        oldUser.whatsupNumber = user.whatsupNumber
        oldUser.phoneNumber = user.phoneNumber
        oldUser.emailId = user.emailId
        oldUser.userId = user.userId
        oldUser.userImage = user.userImage
        oldUser.twitterId = user.twitterId
        oldUser.emailApproved = user.emailApproved
        oldUser.facebookId = user.facebookId
        oldUser.isProofUploaded = user.isProofUploaded
        oldUser.language = user.language
        oldUser.districtName = user.districtName
        oldUser.campaignParticipated = user.campaignParticipated
        oldUser.fbUsername = user.fbUsername
        oldUser.twitterUsername = user.twitterUsername
        oldUser.stateName = user.stateName
        oldUser.is_bonus_received = user.is_bonus_received
        saveUser(oldUser)
    }
    
  
    static func gotoHome(){
        let homeScene = HomeTabBarVC.instantiate(fromAppStoryboard: .Home)
        let sideVc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let sideMenu = SSASideMenu(contentViewController: UINavigationController.init(rootViewController: homeScene), leftMenuViewController: sideVc )
        sideMenu.backgroundImage = #imageLiteral(resourceName: "icSidemenuBg")
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = UIApplication.shared.delegate as? SSASideMenuDelegate
        sharedAppDelegate.window?.rootViewController = sideMenu
        sharedAppDelegate.window?.makeKeyAndVisible()

    }
    
    static func gotoHomeWithProfile(){
        let homeScene = MyProfileVC.instantiate(fromAppStoryboard: .Profile)
        let sideVc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        sideVc.selecteMenu  = 2
        let sideMenu = SSASideMenu(contentViewController: UINavigationController.init(rootViewController: homeScene), leftMenuViewController: sideVc )
        sideMenu.backgroundImage = #imageLiteral(resourceName: "icSidemenuBg")
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = UIApplication.shared.delegate as? SSASideMenuDelegate
        sharedAppDelegate.window?.rootViewController = sideMenu
        sharedAppDelegate.window?.makeKeyAndVisible()

    }
    
    
    static func logout(){
        AppUserDefaults.removeAllValues()
        CommonFunction.removeUser()
        FacebookController.shared.facebookLogout()
        GoogleLoginController.shared.logout()
        TwitterController.shared.logout()
        let loginScene = LoginVC.instantiate(fromAppStoryboard: .Main)
        let navigationScene = UINavigationController.init()
        navigationScene.viewControllers = [loginScene]
        navigationScene.navigationBar.isHidden = true
        UIApplication.shared.applicationIconBadgeNumber = 0
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()
        UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
           sharedAppDelegate.window?.rootViewController = navigationScene
            sharedAppDelegate.window?.makeKeyAndVisible()

        }) { (completed) -> Void in
        }
    }

    static func gotoForgotController(useId: String, phoneNo: String) {
        let nvc = UINavigationController()

         let loginScene = LoginVC.instantiate(fromAppStoryboard: .Main)
        let forgotResetPasswordVC = ForgotResetPasswordVC.instantiate(fromAppStoryboard: .Main)
        forgotResetPasswordVC.screenNum = 2
        let resetScene = ForgotResetPasswordVC.instantiate(fromAppStoryboard: .Main)
        resetScene.screenNum = 1
        resetScene.userId = useId
       // nvc.setViewControllers([loginScene,forgotResetPasswordVC, resetScene], animated: true)
        nvc.viewControllers = [loginScene,forgotResetPasswordVC, resetScene]
        nvc.isNavigationBarHidden = true
        sharedAppDelegate.window?.rootViewController = nvc
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    
   
}
