//
//  Webservices.swift
//  StarterProj
//
//  Created by Gurdeep on 16/12/16.
//  Copyright © 2016 Gurdeep. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import EZSwiftExtensions

enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}

extension WebServices {
    
    //sbm
    
    static func postAddSubscription(token : String , params : JSONDictionary =  [:] ,success : @escaping SuccessResponse , failure : @escaping FailureResponse){
        let request = Alamofire.request("https://www.googleapis.com/youtube/v3/subscriptions?access_token=\(token)&part=snippet",
            method: .post,
            parameters: params,
            encoding: JSONEncoding.default,
            headers: [:]) //["Authorization":"Bearer \(token)", "content-type" : "application/json"])
        print_debug(params.description)
        CommonFunction.showLoader()
        request.responseString { (data) in
            print(data.value ?? "")
        }
        request.responseJSON { (response:DataResponse<Any>) in
            CommonFunction.hideLoader()
            switch(response.result) {
            case .success(let value):
                print_debug(JSON(value))
                let json = JSON(value)
                if (response.response?.statusCode ?? 0) == 200{
                    success(json)
                }else{
                    CommonFunction.showToast(msg: "Something Went wrong")
                }
            case .failure(let e):
                failure(e as NSError)
                print_debug(e.localizedDescription)
            }
        }
    }
    
    static func postYoutubeComment(token : String , params : JSONDictionary =  [:] ,success : @escaping SuccessResponse , failure : @escaping FailureResponse){
        let request = Alamofire.request("https://www.googleapis.com/youtube/v3/commentThreads?access_token=\(token)&part=snippet",
            method: .post,
            parameters: params,
            encoding: JSONEncoding.default,
            headers: [:]) //["Authorization":"Bearer \(token)", "content-type" : "application/json"])
        print_debug(params.description)
        CommonFunction.showLoader()
        request.responseString { (data) in
            print(data.value ?? "")
        }
        request.responseJSON { (response:DataResponse<Any>) in
            CommonFunction.hideLoader()
            switch(response.result) {
            case .success(let value):
                print_debug(JSON(value))
                let json = JSON(value)
                if (response.response?.statusCode ?? 0) == 200{
                    success(json)
                }else{
                    CommonFunction.showToast(msg: "Something Went wrong")
                }
            case .failure(let e):
                failure(e as NSError)
                print_debug(e.localizedDescription)
            }
        }
    }
    
    static func postYoutubeLike(id :  String   , token : String , params : JSONDictionary =  [:] ,success : @escaping SuccessResponse , failure : @escaping FailureResponse){
        let request = Alamofire.request("https://www.googleapis.com/youtube/v3/videos/rate?access_token=\(token)",
            method: .post,
            parameters: params,
            encoding: URLEncoding.default,
            headers: [:]) //["Authorization":"Bearer \(token)", "content-type" : "application/json"])
        print_debug(params.description)
        CommonFunction.showLoader()
        request.responseString { (data) in
            print(data.value ?? "")
        }
        request.responseJSON { (response:DataResponse<Any>) in
            CommonFunction.hideLoader()
            switch(response.result) {
            case .success(let value):
                print_debug(JSON(value))
                let json = JSON(value)
                if (response.response?.statusCode ?? 0) == 204{
                    success(json)
                }else{
                    CommonFunction.showToast(msg: "Something Went wrong")
                }
            case .failure(let e):
                failure(e as NSError)
                print_debug(e.localizedDescription)
            }
        }
    }
    
    
    static func logout(success : @escaping SuccessResponse , failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.PUT(endPoint: .Logout, parameters: [:], headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) in
            failure(e)
        })
    }
    
    static func twitter(success : @escaping SuccessResponse , failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .twitter, parameters: [:], headers: headers, loader: false, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) in
            failure(e)
        })
    }
    
    static func logs(device_id : String , deactive_date : String , success : @escaping SuccessResponse , failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let params = [ ApiKeys.device_id : device_id , ApiKeys.deactive_date : deactive_date]
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.POST(endPoint: .logs, parameters: params , headers: headers, loader: false, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
        }
    }
    
    static func appVersion(success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        let params = [ "platform" : "2" ]
        AppNetworking.GET(endPoint: .appVersion, parameters: params , headers: [:], loader: false, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
        }
        
    }
    static func getDailyReward(_ loader : Bool = true ,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {

        guard let token = CommonFunction.getUser()?.accessToken else { return }
            let headers = [ApiKeys.accessToken: token]
        let params = ["":""]
        AppNetworking.GET(endPoint: .dailyreward, parameters: params , headers: headers, loader: false, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.succeswithempty) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
        }
        
    }
    
    
    static func aceeptTerms(version : String , success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        guard let versionApp = ez.appVersion else {return}
        let params = [ "term_condition_version" : version , "app_version" : versionApp]
        AppNetworking.POST(endPoint: .termconditionaccept, parameters: params , headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
        }
    }
    
    
    static func addExpense(params : JSONDictionary? ,success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.POST(endPoint: .addExpense, parameters: params ?? [:], headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
            
        }
    }
    
    static func getOfflineTask( success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        let params : JSONDictionary = [:]
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .offlineTask, parameters: params , headers: headers  , loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func changePassword(old : String? , new : String?, success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        let params = ["oldpassword" : /old , "password" : /new ]
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.POST(endPoint: .changePassword, parameters: params, headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
            
        }
    }
    
    static func updateProfile(dict : JSONDictionary?, success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.POST(endPoint: .profile, parameters: dict ?? [ : ], headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (e : Error) in
            failure(e)
            
        }
    }
    
    static func getWallet(dict : JSONDictionary?,_ loader : Bool = true, success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .getWallet, parameters: dict ?? [:] , headers: headers  , loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func getWalletReward(dict : JSONDictionary?,_ loader : Bool = true, success : @escaping SuccessResponse, failure : @escaping FailureResponse){
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .level, parameters: dict ?? [:] , headers: headers  , loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func referralList(count : String,_ loader : Bool = true ,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["count" : count]
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .referralList, parameters: params , headers: headers  , loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func completedTaskList(count : String, taskID : String, _ loader : Bool = true ,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["count" : count, "task_id" : taskID]
          guard let token = CommonFunction.getUser()?.accessToken else { return }
          let headers = [ApiKeys.accessToken: token]
          AppNetworking.POST(endPoint: .completedTask, parameters: params , headers: headers  , loader: loader, success: { (json) in
              if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                  success(json)
              } else {
                  
                  let errorCode = json["CODE"]
                  let  errorMsg = json["MESSAGE"]
                  
                  let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                  failure(error)
              }
          }, failure: { (e : Error) -> Void in
              failure(e)
          })
      }
      
    static func completedHomeList(count : String, taskID : String, _ loader : Bool = true ,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
           let params = ["count" : count, "news_id" : taskID]
             guard let token = CommonFunction.getUser()?.accessToken else { return }
             let headers = [ApiKeys.accessToken: token]
             AppNetworking.POST(endPoint: .completedHome, parameters: params , headers: headers  , loader: loader, success: { (json) in
                 if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                     success(json)
                 } else {
                     
                     let errorCode = json["CODE"]
                     let  errorMsg = json["MESSAGE"]
                     
                     let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                     failure(error)
                 }
             }, failure: { (e : Error) -> Void in
                 failure(e)
             })
         }
         
    
    
    static func HomeKnowledgeList(_ loader : Bool = true ,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["":""]
           guard let token = CommonFunction.getUser()?.accessToken else { return }
           let headers = [ApiKeys.accessToken: token]
           AppNetworking.GET(endPoint: .homeknowledgelist, parameters: params , headers: headers  , loader: loader, success: { (json) in
               if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                   success(json)
               } else {
                   
                   let errorCode = json["CODE"]
                   let  errorMsg = json["MESSAGE"]
                   
                   let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                   failure(error)
               }
           }, failure: { (e : Error) -> Void in
               failure(e)
           })
       }
    
    
    
    
    
    static func HomePollData(_ loader : Bool = true ,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
           let params = ["":""]
              guard let token = CommonFunction.getUser()?.accessToken else { return }
              let headers = [ApiKeys.accessToken: token]
              AppNetworking.GET(endPoint: .poll, parameters: params , headers: headers  , loader: loader, success: { (json) in
                  if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                      success(json)
                  } else {
                      
                      let errorCode = json["CODE"]
                      let  errorMsg = json["MESSAGE"]
                      
                      let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                      failure(error)
                  }
              }, failure: { (e : Error) -> Void in
                  failure(e)
              })
          }
    
    static func requestToSubmitPoll(pollId: String, optionsId : [String], optionsName : [String], success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        
        
        let optionsIdArray = JSON(optionsId)
        let optionsNameArray = JSON(optionsName)

        let optionsIdString = optionsIdArray.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!

        let optionsNameString = optionsNameArray.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
//
        let params = ["poll_id":pollId, "option_id" : optionsIdString ,"option_name" : optionsNameString]
        AppNetworking.POST(endPoint: .votePoll, parameters: params, headers: headers, loader: true, success: { (json) in
            
            print("MainnnJSON: ", json)
            
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
       
    
    static func registerUserAPI(registerationNo: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["registeration_no" : registerationNo]
        AppNetworking.POST(endPoint: .registerUser, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func resendOTPAPI(userId: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["user_id": userId]
        AppNetworking.POST(endPoint: .resendOTP, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
 //Comment
    static func getallComments(count: String, newsID: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        let params = ["count": count , "news_id" : newsID]
        AppNetworking.POST(endPoint: .getallcomments, parameters: params, headers : headers, loader: true, success: { (json) in
             if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                   success(json)
               } else {
                   
                   let errorCode = json["CODE"]
                   let  errorMsg = json["MESSAGE"]
                   
                   let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                   failure(error)
               }
           }, failure: { (e : Error) -> Void in
               failure(e)
           })
       }
    
    static func addComments(usercomment: String, newsID: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
              let params = ["user_comment": usercomment, "news_id" : newsID]
        guard let token = CommonFunction.getUser()?.accessToken else { return }
             let headers = [ApiKeys.accessToken: token]
              AppNetworking.POST(endPoint: .addcomments, parameters: params, headers : headers, loader: true, success: { (json) in
                  if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                      success(json["RESULT"])
                  } else {
                      
                      let errorCode = json["CODE"]
                      let  errorMsg = json["MESSAGE"]
                      
                      let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                      failure(error)
                  }
              }, failure: { (e : Error) -> Void in
                  failure(e)
              })
          }
    static func editComments(commentid: String, usercomment: String, newsID: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
                 let params = ["comment_id": commentid, "user_comment": usercomment, "news_id" : newsID]
        guard let token = CommonFunction.getUser()?.accessToken else { return }
                    let headers = [ApiKeys.accessToken: token]
                 AppNetworking.POST(endPoint: .editcomments, parameters: params, headers : headers, loader: true, success: { (json) in
                     if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                         success(json["RESULT"])
                     } else {
                         
                         let errorCode = json["CODE"]
                         let  errorMsg = json["MESSAGE"]
                         
                         let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                         failure(error)
                     }
                 }, failure: { (e : Error) -> Void in
                     failure(e)
                 })
             }
       
    
    
   //Lika nd Dislike Media
    static func likenews(liketype: String, newsID: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["like_type": liketype, "news_id" : newsID]
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.POST(endPoint: .likenews, parameters: params, headers : headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func dislikenews(newsID: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
           let params = ["news_id" : newsID]
           guard let token = CommonFunction.getUser()?.accessToken else { return }
           let headers = [ApiKeys.accessToken: token]
           AppNetworking.POST(endPoint: .dislikenews, parameters: params, headers : headers, loader: true, success: { (json) in
               if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                   success(json["RESULT"])
               } else {
                   
                   let errorCode = json["CODE"]
                   let  errorMsg = json["MESSAGE"]
                   
                   let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                   failure(error)
               }
           }, failure: { (e : Error) -> Void in
               failure(e)
           })
       }
    
    
    static func loginWithOTPAPI(_ otp: String,_ phoneNum : String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["phone_no": phoneNum, "login_type": "otp"]
        AppNetworking.POST(endPoint: .login, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
                print(json["RESULT"])
                print("")
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func verifyOTP(_ userID :String, _ otp: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["user_id": userID, "otp": otp, ApiKeys.device_id: DeviceDetail.device_id, ApiKeys.deviceToken: DeviceDetail.deviceToken, ApiKeys.platform: "ios"]
        AppNetworking.POST(endPoint: .verifyOTP, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
                print(json["RESULT"])
                print("")
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func loginAPI(_ phoneNum: String, password: String, loginType: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = [ApiKeys.phoneNo: phoneNum, ApiKeys.loginType: loginType, ApiKeys.password: password, ApiKeys.deviceToken: DeviceDetail.deviceToken, ApiKeys.device_id: DeviceDetail.device_id,ApiKeys.platform : "ios"]
        AppNetworking.POST(endPoint: .login, parameters: params, loader: true, success: { (json) in
            
            print_debug(json)
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func signUpAPI(_ userID: String, proofType: String, proofImage: String, idNumber: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["user_id": userID, "proof_type": proofType, "proof_image": proofImage, "id_number": idNumber]
        AppNetworking.POST(endPoint: .signup, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    
    static func uploadDocuments(params: JSONDictionary,loader: Bool, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: .signup, parameters: params, loader: loader, success: { (json) in
            print_debug(json)
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    
    static func forgotPass(_ userID: String, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        let params = ["email": userID]
        AppNetworking.POST(endPoint: .forgotPass, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                
                CommonFunction.showToast(msg: json[ApiKeys.message].stringValue)
                success(json[ApiKeys.result])
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func resetPassword(params: JSONDictionary, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        // let params = ["email": userID]
        AppNetworking.POST(endPoint: .resetPassword, parameters: params, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
                CommonFunction.showToast(msg: json[ApiKeys.message].stringValue)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getStateList(params: JSONDictionary,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        //guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: ""]
        AppNetworking.GET(endPoint: .address, parameters: params, headers: headers, loader: false, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                print_debug(json)
                success(json[ApiKeys.result])
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func registerUser(params: JSONDictionary,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        //guard let token = CommonFunction.getUser()?.accessToken else { return }
        var params = params
        params[ApiKeys.device_id] = DeviceDetail.device_id
        params[ApiKeys.deviceToken] = DeviceDetail.deviceToken
        params[ApiKeys.platform] = "ios"
        let headers = [ApiKeys.accessToken: ""]
        AppNetworking.POST(endPoint: .registration, parameters: params, headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                print_debug(json)
                success(json[ApiKeys.result])
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func getDistrictList(params: JSONDictionary,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        //// guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: ""]
        AppNetworking.GET(endPoint: .address, parameters: params, headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                print_debug(json)
                success(json[ApiKeys.result])
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func getHomeData(params: JSONDictionary,loader: Bool,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .home, parameters: params, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                
                print_debug(json)
                success(json)
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    static func getHomeTask(params: JSONDictionary, loader : Bool, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = ["Accesstoken": token]
        AppNetworking.GET(endPoint: .task, parameters: params, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                print_debug(json)
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    static func notification(params: JSONDictionary, loader : Bool, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = ["Accesstoken": token]
        AppNetworking.GET(endPoint: .notification, parameters: params, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                print_debug(json)
                success(json)
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func updateTask(params: JSONDictionary,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = ["Accesstoken": token]
        AppNetworking.POST(endPoint: .task, parameters: params, headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                print_debug(json)
                success(json["RESULT"])
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    static func getTweetList(userName: String,params: JSONDictionary,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        //        guard let token = CommonFunction.getUser()?.accessToken else { return }
        //        let headers = ["Accesstoken": token]
        let url = URL(string: "https://twitter.com/")
        
        do {
            let contents = try String(contentsOf: url!)
            print_debug(contents)
        } catch {
            print_debug(error.localizedDescription)
        }
        //        AppNetworking.GET_TWEET(endPoint: url, success: { (json) in
        //            print_debug(json)
        //        }) { (error) in
        //            print_debug(error)
        //            print_debug(error.localizedDescription)
        //        }
    }
    
    static func getContactusDetail(params: JSONDictionary,loader : Bool,success : @escaping Success, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .contactus, parameters: params, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                
                success(json[ApiKeys.result], json[ApiKeys.helpLineNo].stringValue)
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func saveContact(params: JSONDictionary,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.POST(endPoint: .contactus, parameters: params, headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                
                success(json)
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    static func getDashboardData(params: JSONDictionary,_ loader : Bool = true, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.GET(endPoint: .leaderboard, parameters: params, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json)
            } else {
                let errorCode = json[ApiKeys.code]
                let  errorMsg = json[ApiKeys.message]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    static func getProfile(success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = ["Accesstoken": token]
        AppNetworking.GET(endPoint: .profile, parameters: [:], headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    
    static func editProfileAPI(user : UserModel,success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = ["Accesstoken": token]
        
        print_debug(user.facebookId!)
        
        
        
        //        if let proofIds = user.idProofs {
        //            proofIds.map({ (proof) -> T in
        //
        //            })
        //            params["id_proof"] = proofId
        //        }
        var images = [String]()
        if let proofs = user.idProofs {
            let _ = proofs.map({ (proof)  in
                if let image = proof.image {
                    images.append(image)
                }
                
            })
        }
        let image = images.joined(separator: ",")
        
        var params = [String: Any]()
        params = ["email": user.emailId!, "state": user.state!, "district": user.district!, "facebookId": user.facebookId!,"twitterId" : user.twitterId!,"name" : user.fullName!,"profileImage" : user.userImage!,"whatsupNo" : user.whatsupNumber!,"paytm_number" : user.paytmNumber!, "isProofUploaded": user.isProofUploaded ?? "0", "proof_image": image, "proof_type": user.idProofs?[0].proofType ?? "1", "id_number": user.idProofs?[0].idNumber ?? ""]
        print_debug(params)
        
        //  print_debug(user.dictionaryRepresentation())
        AppNetworking.POST(endPoint: .profile, parameters: params, headers: headers, loader: true, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func uploadProofFromMyProfile(params : JSONDictionary, loader: Bool, success : @escaping SuccessResponse, failure : @escaping FailureResponse) {
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = ["Accesstoken": token]
        AppNetworking.POST(endPoint: .profile, parameters: params, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func updateProfile(param:JSONDictionary, loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        guard let token = CommonFunction.getUser()?.accessToken else { return }
        let headers = [ApiKeys.accessToken: token]
        AppNetworking.PUT(endPoint: .updateProfile, parameters: param, headers: headers, loader: loader, success: { (json) in
            if let errorCode = json["CODE"].int, (errorCode == error_codes.success || errorCode == error_codes.successNoData) {
                success(json["RESULT"])
            } else {
                let errorCode = json["CODE"]
                let  errorMsg = json["MESSAGE"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    //    static func signUpAPI(image : UIImage ,parameters : JSONDictionary, success : @escaping UserControllerSuccess, failure : @escaping FailureResponse) {
    //
    //        // Configure Parameters and Headers
    //        AppNetworking.POSTWithImage(endPoint: .signup, parameters: parameters, image: ["profile_image" : image], loader: true, success: { (json) -> () in
    //
    //
    //            if let errorCode = json["CODE"].int, errorCode == error_codes.success {
    //
    //                if let result = json["RESULT"].dictionary {
    //
    //                    if let rspMsg = json["MESSAGE"].string {
    //
    //                        let userInfo = User(dictionary: result)
    //                        userInfo.responseString = rspMsg
    //                        success(userInfo)
    //
    //                    }
    //
    //                    if let access_token = result["accesstoken"]?.string {
    //                        AppUserDefaults.save(value: access_token, forKey: .Accesstoken)
    //                    }
    //                }
    //
    //            } else {
    //
    //                let errorCode = json["CODE"]
    //                let  errorMsg = json["MESSAGE"]
    //
    //                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
    //                failure(error)
    //            }
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
    //
    //    static func loginAPI(parameters : JSONDictionary, success : @escaping UserControllerSuccess, failure : @escaping FailureResponse) {
    //
    //        // Configure Parameters and Headers
    //        AppNetworking.POST(endPoint: .login, parameters: parameters, loader: true, success: { (json) -> () in
    //
    //
    //            if let errorCode = json["CODE"].int, errorCode == error_codes.success {
    //
    //                if let result = json["RESULT"].dictionary {
    //
    //                    if let resultDictionary = json["RESULT"].dictionaryObject {
    //
    //                        AppUserDefaults.save(value: resultDictionary, forKey: .userData)
    //
    //                    }
    //
    //                    if let rspMsg = json["MESSAGE"].string {
    //
    //                        let userInfo = User(dictionary: result)
    //                        userInfo.responseString = rspMsg
    //                        success(userInfo)
    //
    //                    }
    //
    //                    if let access_token = result["accesstoken"]?.string {
    //                        AppUserDefaults.save(value: access_token, forKey: .Accesstoken)
    //                    }
    //                }
    //
    //            } else {
    //
    //                let errorCode = json["CODE"]
    //                let  errorMsg = json["MESSAGE"]
    //
    //                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
    //                failure(error)
    //            }
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
    //
    //
    ////    static func changePasswordAPI(parameters : ChangePasswordData, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
    ////
    ////        // Configure Parameters and Headers
    ////
    ////        let params = ["password" : parameters.newPassword,
    ////                      "oldpassword" : parameters.oldPassword,
    ////                      "accesstoken" : AppUserDefaults.value(forKey: .Accesstoken).stringValue]
    ////
    ////        AppNetworking.POST(endPoint: .changepassword, parameters: params, loader: true, success: { (json : JSON) -> Void in
    ////            guard let _ = json.dictionary else {
    ////
    ////                let e = NSError(localizedDescription: "")
    ////
    ////                // check error code and present respective error message from here only
    ////
    ////                failure(e)
    ////
    ////                return
    ////            }
    ////
    ////            success(json)
    ////        }, failure: { (e : Error) -> Void in
    ////            failure(e)
    ////        })
    ////    }
    ////
    //
    //    static func forgotPasswordAPI(email : String, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
    //
    //        // Configure Parameters and Headers
    //
    //        let params = ["email" : email]
    //
    //        AppNetworking.POST(endPoint: .forgotpassword, parameters: params, loader: true, success: { (json : JSON) -> Void in
    //            guard let _ = json.dictionary else {
    //
    //                let e = NSError(localizedDescription: "")
    //
    //                // check error code and present respective error message from here only
    //
    //                failure(e)
    //
    //                return
    //            }
    //
    //            success(json)
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
    //
    //
    //    static func resetPasswordAPI(parameters : JSONDictionary, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
    //
    //        // Configure Parameters and Headers
    //
    //        AppNetworking.POST(endPoint: .resetpassword, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
    //            guard let _ = json.dictionary else {
    //
    //                let e = NSError(localizedDescription: "")
    //
    //                // check error code and present respective error message from here only
    //
    //                failure(e)
    //
    //                return
    //            }
    //
    //            success(json)
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
    //
    //
    //
    //    static func profileAPI(success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
    //
    //        // Configure Parameters and Headers
    //
    //        AppNetworking.GET(endPoint: .profiledata, parameters: [:], loader: true, success: { (json : JSON) -> Void in
    //            guard let _ = json.dictionary else {
    //
    //                let e = NSError(localizedDescription: "")
    //
    //                // check error code and present respective error message from here only
    //
    //                failure(e)
    //
    //                return
    //            }
    //
    //            success(json)
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
    //
    //    static func getUserList(parameters : JSONDictionary,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
    //
    //        // Configure Parameters and Headers
    //
    //        AppNetworking.GET(endPoint: .Managefriends, parameters: parameters, loader: false, success: { (json : JSON) -> Void in
    //            guard let _ = json.dictionary else {
    //
    //                let e = NSError(localizedDescription: "")
    //
    //                // check error code and present respective error message from here only
    //
    //                failure(e)
    //
    //                return
    //            }
    //
    //            success(json)
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
    //
    //    static func logoutAPI(success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
    //
    //        // Configure Parameters and Headers
    //
    //        var parameters : [String:Any] = [:]
    //
    //        if AppUserDefaults.value(forKey: .Accesstoken) != JSON.null {
    //
    //            parameters["accesstoken"] = AppUserDefaults.value(forKey: .Accesstoken).stringValue
    //
    //        }
    //
    //        AppNetworking.PUT(endPoint: .logout, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
    //            guard let _ = json.dictionary else {
    //
    //                let e = NSError(localizedDescription: "")
    //
    //                // check error code and present respective error message from here only
    //
    //                failure(e)
    //
    //                return
    //            }
    //
    //            success(json)
    //        }, failure: { (e : Error) -> Void in
    //            failure(e)
    //        })
    //    }
}

//MARK:- Error Codes
//==================
struct error_codes {
    
    static let success = 200
    static let successNoData = 202
    static let unAuthorize = 401
    static let blockedByAdmin = 101
    static let succeswithempty = 422
    
}

