//
//  AppNetworking.swift
//  StarterProj
//
//  Created by Gurdeep on 16/12/16.
//  Copyright © 2016 Gurdeep. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

typealias JSONDictionary = [String : Any]
typealias JSONDictionaryArray = [JSONDictionary]
typealias SuccessResponse = (_ json : JSON) -> ()
typealias TweetsResponse = (_ string : String) -> ()
typealias isSuccess = (_ bool : Bool) -> ()

typealias Success = (_ json : JSON, _ contact: String) -> ()
typealias FailureResponse = (Error) -> (Void)



extension Notification.Name {
    
    static let NotConnectedToInternet = Notification.Name("NotConnectedToInternet")
}

enum AppNetworking {
    
    static let username = "admin"
    static let password = "12345"
    
    static func POST(endPoint : WebServices.EndPoint,
                     parameters : JSONDictionary = [:],
                     headers : HTTPHeaders = [:],
                     loader : Bool = true,
                     success : @escaping (JSON) -> Void,
                     failure : @escaping (Error) -> Void) {
        
        
        request(URLString: endPoint.path, httpMethod: .post, parameters: parameters, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    static func POSTWithImage(endPoint : WebServices.EndPoint,
                              parameters : [String : Any] = [:],
                              image : [String:UIImage]? = [:],
                              headers : HTTPHeaders = [:],
                              loader : Bool = true,
                              success : @escaping (JSON) -> Void,
                              failure : @escaping (Error) -> Void) {
        
        upload(URLString: endPoint.path, httpMethod: .post, parameters: parameters,image: image ,headers: headers, loader: loader, success: success, failure: failure )
    }
    
    static func GET(endPoint : WebServices.EndPoint,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (Error) -> Void) {
        
//        request(URLString: endPoint.path, httpMethod: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers, loader: loader, success: success, failure: failure)
        request(URLString: endPoint.path, httpMethod: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers, loader: loader, success: success, failure: failure)

    }
    

    
    static func GET_TWEET(endPoint : String,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (Error) -> Void) {
        
        //request(URLString: endPoint, httpMethod: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers, loader: loader, success: success, failure: failure)
        
        requestTweet(URLString: endPoint, httpMethod: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    
    static func PUT(endPoint : WebServices.EndPoint,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    failure : @escaping (Error) -> Void) {
        
        request(URLString: endPoint.path, httpMethod: .put, parameters: parameters, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    static func DELETE(endPoint : WebServices.EndPoint,
                       parameters : JSONDictionary = [:],
                       headers : HTTPHeaders = [:],
                       loader : Bool = true,
                       success : @escaping (JSON) -> Void,
                       failure : @escaping (Error) -> Void) {
        
        request(URLString: endPoint.path, httpMethod: .delete, parameters: parameters, headers: headers, loader: loader, success: success, failure: failure)
    }
    
    private static func request(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: URLEncoding = .default,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                failure : @escaping (Error) -> Void) {
        
        if loader { CommonFunction.showLoader() }
        
        guard let data = "\(username):\(password)".data(using: String.Encoding.utf8) else { return  }
        
        let base64 = data.base64EncodedString()
        let selectedLanguage = AppUserDefaults.value(forKey: .languagess).stringValue

        var header : HTTPHeaders = ["Authorization" : "Basic \(base64)",
            "content-type": "application/x-www-form-urlencoded", "lang" : LocalizationSystem.sharedInstance.getLanguage()]
        if headers["Accesstoken"] != nil {
            header["Accesstoken"] = headers["Accesstoken"]
        }
        
        let request = Alamofire.request(URLString,
                                        method: httpMethod,
                                        parameters: parameters,
                                        encoding: encoding,
                                        headers: header)
        
        request.responseString { (data) in
        }
        
        request.responseJSON { (response) in
            
            if loader { CommonFunction.hideLoader() }
            
            print_debug("===================== METHOD =========================")
            print_debug(httpMethod)
            print_debug("===================== ENCODING =======================")
            print_debug(encoding)
            print_debug("===================== URL STRING =====================")
            print_debug(URLString)
            print_debug("===================== HEADERS ========================")
            print_debug(header)
            print_debug("===================== PARAMETERS =====================")
            print_debug(parameters.description)
            print_debug("===================== STATUS CODE =====================")
            print_debug(response.response?.statusCode)
            if (response.response?.statusCode ?? 0) == error_codes.blockedByAdmin{
                CommonFunction.showToast(msg: AlertMessage.accountBlockedByAdmin.localized)
                CommonFunction.logout()
                return
            }
 
            switch(response.result) {
            case .success(let value):
                print_debug("===================== RESPONSE =======================")
                print_debug(JSON(value))
                let json = JSON(value)
                if let errorCode = json["CODE"].int, (errorCode == error_codes.unAuthorize || errorCode == error_codes.blockedByAdmin  ){
                    CommonFunction.showToast(msg:json["MESSAGE"].stringValue )
                    CommonFunction.logout()
                    return
                }
                success(json)
                
            case .failure(let e):
                print_debug(response.result)
                print_debug("===================== FAILURE =======================")
                print_debug(e)
                print_debug(e.localizedDescription)
                
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
                
                failure(e as NSError)
                
            }
        }
    }
    
    
    
    private static func requestTweet(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: URLEncoding = .httpBody,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                failure : @escaping (Error) -> Void) {
        
        if loader { CommonFunction.showLoader() }
        
        guard let data = "\(username):\(password)".data(using: String.Encoding.utf8) else { return  }
        
      //  let base64 = data.base64EncodedString()
        
       // var header  :
//        var header : HTTPHeaders = ["Authorization" : "Basic \(base64)",
//            "content-type": "application/x-www-form-urlencoded"]
//        if headers["Accesstoken"] != nil {
//            header["Accesstoken"] = headers["Accesstoken"]
//        }
//
        
        
        let request = Alamofire.request(URLString,
                                        method: httpMethod,
                                        parameters: parameters,
                                        encoding: encoding,
                                        headers: [:])
        
        request.responseString { (data) in
            print(data.value ?? "")
        }
        
        request.responseJSON { (response:DataResponse<Any>) in
            
            if loader { CommonFunction.hideLoader() }
            
            print_debug("===================== METHOD =========================")
            print_debug(httpMethod)
            print_debug("===================== ENCODING =======================")
            print_debug(encoding)
            print_debug("===================== URL STRING =====================")
            print_debug(URLString)
            print_debug("===================== HEADERS ========================")
           // print_debug(header)
            print_debug("===================== PARAMETERS =====================")
            print_debug(parameters.description)
            
            switch(response.result) {
            case .success(let value):
                print_debug("===================== RESPONSE =======================")
                print_debug(JSON(value))
                let json = JSON(value)
                if let errorCode = json["CODE"].int, (errorCode == error_codes.unAuthorize || errorCode == error_codes.blockedByAdmin  ){
                    CommonFunction.showToast(msg:json["MESSAGE"].stringValue )
                    CommonFunction.logout()
                    return
                }
                success(json)
                
            case .failure(let e):
                print_debug(data)
                print_debug("===================== FAILURE =======================")
                print_debug(e)
                print_debug(e.localizedDescription)
                
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
                
                failure(e as NSError)
                
            }
        }
    }
    
    
    private static func upload(URLString : String,
                               httpMethod : HTTPMethod,
                               parameters : JSONDictionary = [:],
                               image : [String:UIImage]? = [:],
                               headers : HTTPHeaders = [:],
                               loader : Bool = true,
                               success : @escaping (JSON) -> Void,
                               failure : @escaping (Error) -> Void) {
        
        if loader { CommonFunction.showLoader() }
        
        guard let data = "\(username):\(password)".data(using: String.Encoding.utf8) else { return  }
        
        let base64 = data.base64EncodedString()
        
        let selectedLanguage = AppUserDefaults.value(forKey: .languagess).stringValue

        var header : HTTPHeaders = ["Authorization" : "Basic \(base64)",
            "content-type": "application/x-www-form-urlencoded", "lang" : LocalizationSystem.sharedInstance.getLanguage()]
        if AppUserDefaults.value(forKey: .Accesstoken) != JSON.null {
            
            header["Accesstoken"] = AppUserDefaults.value(forKey: .Accesstoken).stringValue
            
        }
        
        let url = try! URLRequest(url: URLString, method: httpMethod, headers: header)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let image = image {
                for (key , value) in image{
                    //                    print_debug("key = \(key) value = \(value)")
                    if let img = UIImageJPEGRepresentation(value, 0.6){
                        //                        print_debug(img)
                        multipartFormData.append(img, withName: key, fileName: "image.jpg", mimeType: "image/jpg")
                    }
                }
            }
            
            for (key , value) in parameters{
                
                multipartFormData.append((value as AnyObject).data(using : String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         with: url, encodingCompletion: { encodingResult in
                            
                            switch encodingResult{
                            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                                
                                upload.responseJSON(completionHandler: { (response:DataResponse<Any>) in
                                    switch response.result{
                                    case .success(let value):
                                        if loader { CommonFunction.hideLoader() }
                                        
                                        
                                        print_debug(value)
                                        success(JSON(value))
                                        let json = JSON(value)
                                        if let errorCode = json["CODE"].int, (errorCode == error_codes.unAuthorize || errorCode == error_codes.blockedByAdmin  ){
                                            CommonFunction.showToast(msg:json["MESSAGE"].stringValue )
                                            CommonFunction.logout()
                                            return
                                        }
                                        
                                    case .failure(let e):
                                        if loader { CommonFunction.hideLoader() }
                                        
                                        
                                        if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                                            NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                                        }
                                        failure(e)
                                    }
                                })
                                
                            case .failure(let e):
                                //                if loader { hideLoader() }
                                
                                
                                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                                }
                                
                                failure(e)
                            }
        })
        
    }
}




