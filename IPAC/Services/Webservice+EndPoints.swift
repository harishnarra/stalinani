//
//  Webservice+EndPoints.swift
//  StarterProj
//
//  Created by Gurdeep on 06/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation


//let BASE_URL = "https://ipac.appinventive.com/api/"//dev
//let BASE_URL = "http://campaign4india.com/api/"//live

//let BASE_URL = "http://13.233.212.168/api/"//local
//let BASE_URL = "http://13.233.199.182/api/"//local //issue//
//let BASE_URL = "http://13.233.212.168/api/"

//


//let STATIC_PAGE_BASE_URL = "https://ipac.appinventive.com/" //dev



//let STATIC_PAGE_BASE_URL = "http://campaign4india.com/" //live
//let STATIC_PAGE_BASE_URL = "http://13.233.199.182/" //live //issue
let STATIC_PAGE_BASE_URL = "https://stalinani.in/" //new



//let BASE_URL = "https://stalinani.in/staging/api/" //staging
let BASE_URL = "https://stalinani.in/api/" //Live



//https://stalinani.in/staging/api/news/getallcomments
let tutorialUrl = "https://s3-ap-southeast-1.amazonaws.com/ipacappbucket/ipac/app_walkthrough.mp4"
let facebookUrl = "https://tn-vms.s3.ap-south-1.amazonaws.com/mobile_facebook_help.mp4"
let twitterUrl = "https://tn-vms.s3.ap-south-1.amazonaws.com/mobile_twitter_help.mp4"

let appStoreUrl = "https://www.apple.com/itunes/"

extension WebServices {

    enum EndPoint : String {
        
        case registerUser = "Register_User"
        case resendOTP = "resendotp"
        case verifyOTP = "verifyotp"
        case signup = "Signup"
        case forgotPass = "Forgot"
        case home = "News"
        case profile = "Profile"
        case login = "Login"
        case resetPassword = "Resetpass"
        case contactus
        case leaderboard
        case task
        case address
        case registration
        case updateProfile = "update_profile"
        case notification
        case getallcomments = "news/getallcomments"
        case addcomments = "news/addcomment"
        case editcomments = "news/editcomment"
        case likenews = "news/likenews"
        case dislikenews = "news/dislikenews"
        
        //sbm
        case Logout
        case referralList  = "referal_user_list"
        case homeknowledgelist  = "home_faq"
        case changePassword = "Change_Password"
        case getWallet = "wallet_logs"
        case level = "level"
        case offlineTask = "offline_task"
        case addExpense = "campaign_expence"
        case termconditionaccept = "termconditionaccept"
        case appVersion = "app_Version"
        case poll = "poll"
        case logs
        case twitter
        case votePoll = "poll/votepoll"
        case dailyreward = "task/dailyreward"
        case completedTask = "task/getPeerGroup"
        case completedHome = "news/getPeerGroup"
        
        var path : String {
            let url = BASE_URL
            return url + self.rawValue
        }
    }
}



